package com.cans.lightning.form.bean.fieldCtrl;

/**
 * 控件值抽象接口
 */
public interface FormFieldValueInterface {

    /**
     * 将数据库展示值 转换成前端展示值
     */
    default String getShowValue(Object dbValue) {
        return dbValue == null ? null : dbValue.toString();
    }

    /**
     * 将value转换成 数据库存储值
     */
    default String getDbValue(Object frontValue) {
        return frontValue == null ? null : frontValue.toString();
    }
}
