package com.cans.lightning.form.bean.fieldCtrl;

import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

/**
 * 自定义控件缓存
 */
@Slf4j
public class CustomControlCache {

    private final static Map<String, FormFieldCustomControl> customControlMap = Maps.newConcurrentMap();

    public static void addCustomControl(FormFieldCustomControl customControl) {
        if (!customControlMap.containsKey(customControl.getKey())) {
            try {
                customControl.init();
            }
            catch (Exception e) {
                log.error("init custom control error, will skip this.", e);
            }
            customControlMap.put(customControl.getKey(), customControl);
            log.info("add custom control: {}", customControl.getKey());
        }
    }

    public static void removeCustomControl(String key) {
        customControlMap.remove(key);
        log.info("remove custom control: {}", key);
    }

    public static FormFieldCustomControl getCustomControl(String key) {
        return customControlMap.get(key);
    }

    public static Map<String, FormFieldCustomControl> getCustomControlMap() {
        return new HashMap<>(customControlMap);
    }
}
