package com.cans.lightning.form.bean.fieldCtrl;

/**
 * 表单控件类
 */
public abstract class FormFieldControl implements FormFieldValidateInterface, FormFieldValueInterface{

    /**
     * 控件的唯一key值
     */
    public abstract String getKey();

    /**
     * 控件的显示文本
     */
    public abstract String getText();


}
