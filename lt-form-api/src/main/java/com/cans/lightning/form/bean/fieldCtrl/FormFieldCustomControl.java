package com.cans.lightning.form.bean.fieldCtrl;

import com.cans.lightning.form.enums.FieldDbType;

/**
 * 表单自定义控件的基类抽象类
 */
public abstract class FormFieldCustomControl extends FormFieldControl {

    private boolean valid;

    /**
     * 自定义控件初始化逻辑, 由自定义控件自己实现
     */
    public void init() {}

    /**
     * 字段的数据库类型, 默认 VARCHAR
     */
    public FieldDbType getFieldDbType() {
        return FieldDbType.VARCHAR;
    }

    /**
     * 获取自定义控件长度, 默认255
     */
    public String getLength() {
        return "255";
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    @Override
    public boolean canUse() {
        return isValid();
    }
}
