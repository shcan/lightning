package com.cans.lightning.form.bean.fieldCtrl;

/**
 * 自定义控件校验接口  需要判断接口是否可用
 */
public interface FormFieldValidateInterface {

    /**
     * 控件是否可用
     */
    public abstract boolean canUse();
}
