package com.cans.lightning.form.enums;

/**
 * 字段数据库类型
 */
public enum FieldDbType {
    //
    VARCHAR("VARCHAR", "文本"),
    //
    DECIMAL("DECIMAL", "数字"),

    // 3字节 从1000-01-01到9999-12-31
    DATE("DATE", "日期"),

    // timestamp和datetime的区别
    // 1. 4字节 timestamp: '1970-01-01 00:00:01.000000' UTC到'2038-01-19 03:14:07.999999' UTC
    // 2. 8字节 datetime: '1000-01-01 00:00:00.000000'到'9999-12-31 23:59:59.999999'
    TIMESTAMP("TIMESTAMP", "日期时间"),
    //
    DATETIME("DATETIME", "日期时间"),
    //
    LONGTEXT("LONGTEXT", "大文本"),
    //
    LONG("LONG",""),
    //
    INTEGER("INTEGER","");

    private final String key;
    private final String text;

    FieldDbType(String key, String text) {
        this.key = key;
        this.text = text;
    }

    public String getKey() {
        return key;
    }

    public String getText() {
        return text;
    }

    public static FieldDbType getFieldDbType(String key) {
        for (FieldDbType fieldDbType : FieldDbType.values()) {
            if (fieldDbType.getKey().equalsIgnoreCase(key)) {
                return fieldDbType;
            }
        }
        return null;
    }

}
