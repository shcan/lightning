package com.cans.report.generator;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.nio.file.Paths;

/**
 * 测试 MybatisPlus代码生成器
 */
@Slf4j
public class MybatisPlusCodeGeneratorTests {

    @Test
    void testGenerateCode() {
        FastAutoGenerator.create("jdbc:mysql://localhost:3306/lightning", "root", "123456")
                .globalConfig(builder -> builder
                        .author("qpy")
                        .outputDir("..../generator/src/main/java")
                        .commentDate("yyyy-MM-dd")
                )
                .packageConfig(builder -> builder
                        .parent("com.cans.report.business")
                        .entity("entity")
                        .mapper("mapper")
                        .service("service")
                        .serviceImpl("service.impl")
                        .xml("mapper.xml")
                )
                .strategyConfig(builder -> builder
                        .entityBuilder()
                        .enableLombok()
                )
                .templateEngine(new FreemarkerTemplateEngine())
                .execute();
    }
}
