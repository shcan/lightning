package com.cans.report.engine.bean;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.cans.report.engine.bean.configbean.StatsField;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static com.cans.report.engine.bean.StatsDesignBeanTests.getTestStatsDesignBean;
import static com.cans.report.engine.bean.sqlbean.WhereSqlTests.*;
import static com.cans.report.engine.bean.sqlbean.WhereSqlTests.bTableUid;

/**
 * @author qpy
 * @date 2024-08-01 19:01
 * @description 多表统计bean测试
 */
@Slf4j
public class MulStatsDesignBeanTests {

    public final static String randomName = "多表统计随便取表名";

    @Test
    public void testBuildMulStatsDesignBean() {
        MulStatsDesignBean mulStatsDesignBean = getTestMulStatsDesignBean();
        
        log.info("mulStatsDesignBean: {}", JSON.toJSONString(mulStatsDesignBean));
    }
    
    public static MulStatsDesignBean getTestMulStatsDesignBean() {

        // 只要信息由当前bean信息来构建
        StatsDesignBean mainStatsBean = getTestStatsDesignBean(randomName, randomName, randomName, randomName);

        StatsDesignBean mulATable = getTestStatsDesignBean(aTableDbName, aTableUid, bTableDbName, bTableUid);
        StatsDesignBean mulBTable = getTestStatsDesignBean(aTableDbName, aTableUid + "_1", bTableDbName, bTableUid + "_1");
        mulATable.setId(11111L);
        mulATable.setId(22222L);

        // 统计项字段需要单独设置
        List<StatsField> statsFields = new ArrayList<>();
        statsFields.addAll(mulATable.getStatsFields());
        statsFields.addAll(mulBTable.getStatsFields());

        MulStatsDesignBean.Builder builder = MulStatsDesignBean.builder();
        // 1. 添加子表
        builder.addStats(mulATable)
                .addStats(mulBTable)
                .addGroupByField(mainStatsBean.getGroupByFields())
                .addStatsField(statsFields);
        MulStatsDesignBean build = builder.build();
        
        // 不需要tables属性
        build.setTables(null);
        
        return build;
                
    }
}
