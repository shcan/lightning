package com.cans.report.engine.bean;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.io.*;

/**
 * @author qpy
 * @date 2024-07-17 11:19
 * @description
 */
@Slf4j
public class SerializableTests {

    /**
     * 写对象到文件
     */
    @Test
    public void testWriteObject() throws IOException {

        QueryDesignBean queryDesignBean = new QueryDesignBean();

        new ObjectOutputStream(new FileOutputStream("C:\\Users\\qpy\\Desktop\\query.txt")).writeObject(queryDesignBean);

    }

    /**
     * serialVersionUID的作用:
     *      1. 当对象增加删除字段, JDK会默认根据字段来生成 serialVersionUID, 
     *             比如当前 serialVersionUID = 5733644836733243899, 删除一个字段后serialVersionUID = -7812857421587680278, 
     *             导致调用ObjectInputStream.readObject()方法会抛出InvalidClassException异常
     *         所以指定固定的serialVersionUID, 即使删除, 增加字段都可以反序列化    
     *      2. serialVersionUID不支持继承, 所以子类需要指定serialVersionUID的值       
     */
    @Test
    public void testReadObject() throws IOException, ClassNotFoundException {
        Object o = new ObjectInputStream(new FileInputStream("C:\\Users\\qpy\\Desktop\\query.txt")).readObject();
        log.info("" + o);
    }
}
