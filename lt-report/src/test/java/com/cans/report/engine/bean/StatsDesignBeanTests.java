package com.cans.report.engine.bean;

import com.alibaba.fastjson.JSON;
import com.cans.report.engine.bean.configbean.*;
import com.cans.report.engine.bean.sqlbean.WhereSqlTests;
import com.cans.report.engine.constants.ReportConstants;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;

import java.util.Collections;

import static com.cans.report.engine.bean.sqlbean.WhereSqlTests.*;
import static com.cans.report.engine.bean.sqlbean.WhereSqlTests.bTableUid;

/**
 * @author qpy
 * @date 2024-07-31 13:48
 * @description 统计报表设计bean测试
 */
@Slf4j
public class StatsDesignBeanTests {

    @Test
    public void test() {
        StatsDesignBean statsDesignBean = getTestStatsDesignBean(aTableDbName, aTableUid, bTableDbName, bTableUid);
        log.info("{}", JSON.toJSONString(statsDesignBean));
    }

    /**
     * 测试实现 java.lang.Object#clone 克隆方法
     */
    @Test
    public void testsClone() throws CloneNotSupportedException {
        StatsDesignBean statsDesignBean = getTestStatsDesignBean(aTableDbName, aTableUid, bTableDbName, bTableUid);
        StatsDesignBean clone = (StatsDesignBean) statsDesignBean.clone();
        log.info("{}", JSON.toJSONString(clone));
    }

    
    public static StatsDesignBean getTestStatsDesignBean(String aTableDbName, String aTableUid, String bTableDbName, String bTableUid) {
        StatsDesignBean.Builder builder = StatsDesignBean.builder();
        builder.id(1L);

        // 1. 添加表
        Table aTable = Table.builder()
                .dbName(aTableDbName)
                .uidName(aTableUid)
                .addField(new Field().setShowName("选部门").setDbName("field0001").setFieldType(ReportConstants.FieldType.TEXT.getKey()).setDbType(ReportConstants.FieldDbType.VARCHAR.getKey()))
                .addField(new Field().setShowName("文本1").setDbName("field0002").setFieldType(ReportConstants.FieldType.TEXT.getKey()).setDbType(ReportConstants.FieldDbType.VARCHAR.getKey()))
                .addField(new Field().setShowName("下拉1").setDbName("field0010").setFieldType(ReportConstants.FieldType.SELECT.getKey()).setDbType(ReportConstants.FieldDbType.VARCHAR.getKey()))
                .addField(new Field().setShowName("单选1").setDbName("field0011").setFieldType(ReportConstants.FieldType.SELECT.getKey()).setDbType(ReportConstants.FieldDbType.VARCHAR.getKey()))
                .addField(new Field().setShowName("数字1").setDbName("field0015").setFieldType(ReportConstants.FieldType.NUMBER.getKey()).setDbType(ReportConstants.FieldDbType.DECIMAL.getKey()))
                .addField(new Field().setShowName("日期1").setDbName("field0013").setFieldType(ReportConstants.FieldType.DATE.getKey()).setDbType(ReportConstants.FieldDbType.DATE.getKey()))
                .addField(new Field().setShowName("日期时间1").setDbName("field0014").setFieldType(ReportConstants.FieldType.DATETIME.getKey()).setDbType(ReportConstants.FieldDbType.DATETIME.getKey()))
                .build();
        Table bTable = Table.builder()
                .dbName(bTableDbName)
                .uidName(bTableUid)
                .addField(new Field().setShowName("字段6").setDbName("field0006").setFieldType(ReportConstants.FieldType.TEXT.getKey()).setDbType(ReportConstants.FieldDbType.VARCHAR.getKey()))
                .addField(new Field().setShowName("字段7").setDbName("field0007").setFieldType(ReportConstants.FieldType.TEXT.getKey()).setDbType(ReportConstants.FieldDbType.VARCHAR.getKey()))
                .build();
        builder.addTable(aTable);
        builder.addTable(bTable);

        // 2. 添加连接表
        JoinTable joinTable = JoinTable.builder().aTableDbName(aTable.getDbName()).aTableUidName(aTable.getUidName())
                .bTableDbName(bTable.getDbName()).bTableUidName(bTable.getUidName())
                .joinType(ReportConstants.JoinType.LEFT.name())
                .joinConditions(Collections.singletonList(JoinCondition.of("id", "formmain_id")))
                .build();
        builder.addJoinTable(joinTable);

        // 添加统计项字段
        StatsField idCountStatsField = (StatsField) new StatsField().setCalcType(ReportConstants.SqlCalcType.COUNT.getSql()).setTableUidName(aTable.getUidName()).setDbName("id");
        StatsField idMaxStatsField = (StatsField) new StatsField().setCalcType(ReportConstants.SqlCalcType.MAX.getSql()).setTableUidName(aTable.getUidName()).setDbName("id");
        StatsField idMinStatsField = (StatsField) new StatsField().setCalcType(ReportConstants.SqlCalcType.MIN.getSql()).setTableUidName(aTable.getUidName()).setDbName("id");
        StatsField idSumStatsField = (StatsField) new StatsField().setCalcType(ReportConstants.SqlCalcType.SUM.getSql()).setTableUidName(aTable.getUidName()).setDbName("id");
        StatsField idAvgStatsField = (StatsField) new StatsField().setCalcType(ReportConstants.SqlCalcType.AVG.getSql()).setTableUidName(aTable.getUidName()).setDbName("id");
        builder.addStatsField(idCountStatsField);
        builder.addStatsField(idMaxStatsField);
        builder.addStatsField(idMinStatsField);
        builder.addStatsField(idSumStatsField);
        builder.addStatsField(idAvgStatsField);

        
        // 添加分组字段
        GroupByField field0001Gy = (GroupByField) new GroupByField().setTableUidName(aTable.getUidName()).setDbName("field0001");
        GroupByField field0002Gy = (GroupByField) new GroupByField().setTableUidName(aTable.getUidName()).setDbName("field0002");
        GroupByField field0010Gy = (GroupByField) new GroupByField().setTableUidName(aTable.getUidName()).setDbName("field0010");
        GroupByField field0013Gy = (GroupByField) new GroupByField().setTableUidName(aTable.getUidName()).setDbName("field0013");
        GroupByField field0014Gy = (GroupByField) new GroupByField().setTableUidName(aTable.getUidName()).setDbName("field0014");
        builder.addGroupByField(field0001Gy);
        builder.addGroupByField(field0002Gy);
        builder.addGroupByField(field0010Gy);
        builder.addGroupByField(field0013Gy);
        builder.addGroupByField(field0014Gy);


        // 添加排序字段
        builder.addOrderByFields(OrderByField.builder().tableUid(aTable.getUidName()).fieldName("field0001").orderBy(ReportConstants.OrderBy.ASC.getSql()).build());
        builder.addOrderByFields(OrderByField.builder().tableUid(aTable.getUidName()).fieldName("field0010").orderBy(ReportConstants.OrderBy.DESC.getSql()).build());
        builder.addOrderByFields(OrderByField.builder().tableUid(aTable.getUidName()).fieldName("field0002").orderBy(ReportConstants.OrderBy.ASC.getSql()).build());


        // 构建bean
        return builder.build();
    }
}
