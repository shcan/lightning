package com.cans.report.engine.running.manager;

import com.alibaba.fastjson.JSON;
import com.cans.report.engine.bean.MulStatsDesignBean;
import com.cans.report.engine.bean.MulStatsDesignBeanTests;
import com.cans.report.engine.bean.conditionbean.QueryParam;
import com.cans.report.engine.bean.sqlbean.WhereSqlTests;
import com.cans.report.engine.running.manager.impl.MulStatsManagerImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import static com.cans.report.engine.bean.MulStatsDesignBeanTests.getTestMulStatsDesignBean;
import static com.cans.report.engine.running.manager.QueryManagerTests.getTestQueryParam;

/**
 * 测试多表统计查询
 */
@Slf4j
public class MulStatsManagerTests {

    @Test
    void testMulStatsQuery() {
        MulStatsDesignBean mulStatsBean = getTestMulStatsDesignBean();
        QueryParam queryParam = getTestQueryParam(MulStatsDesignBeanTests.randomName);

        MulStatsManager mulStatsManager = new MulStatsManagerImpl();

        log.info("result; {}", JSON.toJSONString(mulStatsManager.mulStats(mulStatsBean, queryParam).getData()));
    }



}
