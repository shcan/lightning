package com.cans.report.engine.running.dao;

import com.cans.report.engine.bean.QueryDesignBean;
import com.cans.report.engine.bean.QueryDesignBeanTests;
import com.cans.report.engine.bean.StatsDesignBean;
import com.cans.report.engine.bean.StatsDesignBeanTests;
import com.cans.report.engine.bean.conditionbean.QueryParam;
import com.cans.report.engine.bean.sqlbean.SqlCriterion;
import com.cans.report.engine.running.dao.impl.ReportSqlExecutorImpl;
import com.cans.report.engine.utils.SqlCriterionUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import static com.cans.report.engine.bean.sqlbean.WhereSqlTests.*;
import static com.cans.report.engine.bean.sqlbean.WhereSqlTests.bTableUid;

/**
 * @author qpy
 * @date 2024-07-25 10:55
 * @description 当前测试类只是拼接sql
 */
@Slf4j
public class ReportSqlExecutorTests {

    /**
     * 测试查询报表拼接sql
     */
    @Test
    public void testQuerySqlExecutorWrapper() {

        // 获取报表
        QueryDesignBean db = QueryDesignBeanTests.getTestQueryDesignBean();
        ReportSqlExecutor reportSqlExecutor = new ReportSqlExecutorImpl();

        // 获取 SqlCriterion 查询条件
        SqlCriterion sqlCriterion = SqlCriterionUtils.buildCriterion(db, new QueryParam());
        
        // 获取执行的sql
        ReportSqlExecutor.SqlExecutorWrapper wrapper = reportSqlExecutor.getSqlExecutorWrapper(sqlCriterion);

        log.info("sql: {}", wrapper);
    }

    /**
     * 测试单表统计拼接sql
     */
    @Test
    public void testStatsSqlExecutorWrapper() {

        // 获取报表
        StatsDesignBean db = StatsDesignBeanTests.getTestStatsDesignBean(aTableDbName, aTableUid, bTableDbName, bTableUid);
        ReportSqlExecutor reportSqlExecutor = new ReportSqlExecutorImpl();

        // 获取 SqlCriterion 查询条件
        SqlCriterion sqlCriterion = SqlCriterionUtils.buildCriterion(db, new QueryParam());

        // 获取执行的sql
        ReportSqlExecutor.SqlExecutorWrapper wrapper = reportSqlExecutor.getSqlExecutorWrapper(sqlCriterion);

        log.info("sql: {}", wrapper);
    }

}
