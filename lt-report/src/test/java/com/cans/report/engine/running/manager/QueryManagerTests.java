package com.cans.report.engine.running.manager;

import com.alibaba.fastjson.JSON;
import com.cans.report.engine.bean.QueryDesignBean;
import com.cans.report.engine.bean.conditionbean.QueryCondition;
import com.cans.report.engine.bean.conditionbean.QueryConditions;
import com.cans.report.engine.bean.conditionbean.QueryParam;
import com.cans.report.engine.bean.configbean.*;
import com.cans.report.engine.bean.sqlbean.WhereSqlTests;
import com.cans.report.engine.constants.ReportConstants;
import com.cans.report.engine.running.manager.impl.QueryManagerImpl;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import static com.cans.report.engine.constants.ReportConstants.*;

/**
 * @author qpy
 * @date 2024-07-30 19:08
 * @description 查询报表测试
 */
@Slf4j
public class QueryManagerTests {
    
    
    @Test
    void testQuery() {

        // 1. 构建 designBean
        QueryDesignBean designBean = getTestQueryDesignBean();
        
        // 2. 构建查询条件
        QueryParam queryParam = getTestQueryParam(WhereSqlTests.aTableUid);

        QueryManager queryManager = new QueryManagerImpl();
        log.info("result; {}", JSON.toJSONString(queryManager.query(designBean, queryParam).getData()));
    }

    public static QueryDesignBean getTestQueryDesignBean() {
        QueryDesignBean.Builder builder = QueryDesignBean.builder();
        builder.id(1L);

        // 1. 添加表
        Table aTable = Table.builder()
                .dbName(WhereSqlTests.aTableDbName)
                .uidName(WhereSqlTests.aTableUid)
                .addField(new Field().setShowName("选部门").setDbName("field0001").setFieldType(ReportConstants.FieldType.TEXT.getKey()).setDbType(ReportConstants.FieldDbType.VARCHAR.getKey()))
                .addField(new Field().setShowName("文本1").setDbName("field0002").setFieldType(ReportConstants.FieldType.TEXT.getKey()).setDbType(ReportConstants.FieldDbType.VARCHAR.getKey()))
                .addField(new Field().setShowName("下拉1").setDbName("field0010").setFieldType(ReportConstants.FieldType.SELECT.getKey()).setDbType(ReportConstants.FieldDbType.VARCHAR.getKey()))
                .addField(new Field().setShowName("单选1").setDbName("field0011").setFieldType(ReportConstants.FieldType.SELECT.getKey()).setDbType(ReportConstants.FieldDbType.VARCHAR.getKey()))
                .addField(new Field().setShowName("数字1").setDbName("field0015").setFieldType(ReportConstants.FieldType.NUMBER.getKey()).setDbType(ReportConstants.FieldDbType.DECIMAL.getKey()))
                .addField(new Field().setShowName("日期1").setDbName("field0013").setFieldType(ReportConstants.FieldType.DATE.getKey()).setDbType(ReportConstants.FieldDbType.DATE.getKey()))
                .addField(new Field().setShowName("日期时间1").setDbName("field0014").setFieldType(ReportConstants.FieldType.DATETIME.getKey()).setDbType(ReportConstants.FieldDbType.DATETIME.getKey()))
                .build();
        Table bTable = Table.builder()
                .dbName(WhereSqlTests.bTableDbName)
                .uidName(WhereSqlTests.bTableUid)
                .addField(new Field().setShowName("字段6").setDbName("field0006").setFieldType(ReportConstants.FieldType.TEXT.getKey()).setDbType(ReportConstants.FieldDbType.VARCHAR.getKey()))
                .addField(new Field().setShowName("字段7").setDbName("field0007").setFieldType(ReportConstants.FieldType.TEXT.getKey()).setDbType(ReportConstants.FieldDbType.VARCHAR.getKey()))
                .build();
        builder.addTable(aTable);
        builder.addTable(bTable);

        // 2. 添加连接表
        JoinTable joinTable = JoinTable.builder().aTableDbName(aTable.getDbName()).aTableUidName(aTable.getUidName())
                .bTableDbName(bTable.getDbName()).bTableUidName(bTable.getUidName())
                .joinType(ReportConstants.JoinType.LEFT.name())
                .joinConditions(Collections.singletonList(JoinCondition.of("id", "formmain_id")))
                .build();
        builder.addJoinTable(joinTable);

        // 添加字段
        builder.addShowField(aTable.getUidName(), "field0001", "选部门");
        builder.addShowField(aTable.getUidName(), "field0010", "下拉1");
        builder.addShowField(aTable.getUidName(), "field0011", "单选1");
        builder.addShowField(aTable.getUidName(), "field0015", "数字1");
        builder.addShowField(aTable.getUidName(), "field0013", "日期1");
        builder.addShowField(aTable.getUidName(), "field0014", "日期时间1");
        
        builder.addShowField(bTable.getUidName(), "field0006", "字段6");
        builder.addShowField(bTable.getUidName(), "field0007", "字段7");


        // 添加排序字段
        builder.addOrderByFields(OrderByField.builder().tableUid(aTable.getUidName()).fieldName("field0001").orderBy(OrderBy.ASC.getSql()).build());
        builder.addOrderByFields(OrderByField.builder().tableUid(aTable.getUidName()).fieldName("field0010").orderBy(OrderBy.ASC.getSql()).build());
        builder.addOrderByFields(OrderByField.builder().tableUid(aTable.getUidName()).fieldName("field0015").orderBy(OrderBy.ASC.getSql()).build());
        
        // 构建bean
        return builder.build();
    }


    /**
     * 通用构建查询条件
     * @return
     */
    public static QueryParam getTestQueryParam(String aTableUidName) {
        return QueryParam.builder()
                .pageNum(1)
                .pageSize(10)
                .queryConditions(
                        QueryConditions.builder().queryConditions(Lists.newArrayList()).build()
                                .addQueryCondition(QueryCondition.builder()
                                        .fieldDbType(ReportConstants.FieldDbType.VARCHAR.getKey())
                                        // 文本类型like
                                        .fieldName("field0002")
                                        .tableUid(aTableUidName)
                                        .fieldType(ReportConstants.FieldType.TEXT.getKey())
                                        .operator(ReportConstants.SqlOperation.LIKE.getSql())
                                        .value("2")
                                        .build())
                                .addQueryCondition(QueryCondition.builder()
                                        .fieldDbType(ReportConstants.FieldDbType.VARCHAR.getKey())
                                        // 日期测试
                                        .fieldName("field0013")
                                        .tableUid(aTableUidName)
                                        .fieldType(ReportConstants.FieldType.DATE.getKey())
                                        .operator(ReportConstants.SqlOperation.GREATER_THAN_OR_EQUAL.getSql())
                                        .value("2024-07-8")
                                        .build())
                                .addQueryCondition(QueryCondition.builder()
                                        .fieldDbType(ReportConstants.FieldDbType.VARCHAR.getKey())
                                        // 日期时间测试
                                        .fieldName("field0014")
                                        .tableUid(aTableUidName)
                                        .fieldType(ReportConstants.FieldType.DATETIME.getKey())
                                        .operator(ReportConstants.SqlOperation.GREATER_THAN_OR_EQUAL.getSql())
                                        .value("2024-07-01   ")
                                        .build())
                                .addQueryCondition(QueryCondition.builder()
                                        .fieldDbType(ReportConstants.FieldDbType.VARCHAR.getKey())
                                        // 数字测试
                                        .fieldName("field0015")
                                        .tableUid(aTableUidName)
                                        .fieldType(ReportConstants.FieldType.NUMBER.getKey())
                                        .operator(ReportConstants.SqlOperation.GREATER_THAN_OR_EQUAL.getSql())
                                        .value("22.2")
                                        .build()
                                )
                ).build();
    }
}
