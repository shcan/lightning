package com.cans.report.engine.bean;

import com.alibaba.fastjson.JSON;
import com.cans.report.engine.bean.configbean.*;
import com.cans.report.engine.constants.ReportConstants;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static com.cans.report.engine.constants.ReportConstants.*;

/**
 * @author qpy
 * @date 2024-07-18 13:20
 * @description
 */
@Slf4j
public class QueryDesignBeanTests {
    
    @Test
    public void test() {
        QueryDesignBean queryDesignBean = getTestQueryDesignBean();
        log.info("{}", JSON.toJSONString(queryDesignBean));
    }
    
    
    public static QueryDesignBean getTestQueryDesignBean() {
        QueryDesignBean.Builder builder = QueryDesignBean.builder();
        builder.id(1L);

        // 1. 添加表
        Table aTable = Table.builder()
                .dbName("formmain_0001")
                .uidName("formmain_0001_0")
                .addField(new Field().setShowName("字段1").setDbName("field0001").setFieldType(FieldType.TEXT.getKey()).setDbType(FieldDbType.VARCHAR.getKey()))
                .addField(new Field().setShowName("字段2").setDbName("field0002").setFieldType(FieldType.TEXT.getKey()).setDbType(FieldDbType.VARCHAR.getKey()))
                .addField(new Field().setShowName("字段3").setDbName("field0003").setFieldType(FieldType.TEXT.getKey()).setDbType(FieldDbType.VARCHAR.getKey()))
                .addField(new Field().setShowName("字段4").setDbName("field0004").setFieldType(FieldType.TEXT.getKey()).setDbType(FieldDbType.VARCHAR.getKey()))
                .addField(new Field().setShowName("字段5").setDbName("field0005").setFieldType(FieldType.TEXT.getKey()).setDbType(FieldDbType.VARCHAR.getKey()))
                .build();
        Table bTable = Table.builder()
                .dbName("formmain_0002")
                .uidName("formmain_0002_1")
                .addField(new Field().setShowName("字段1").setDbName("field0001").setFieldType(FieldType.TEXT.getKey()).setDbType(FieldDbType.VARCHAR.getKey()))
                .addField(new Field().setShowName("字段2").setDbName("field0002").setFieldType(FieldType.TEXT.getKey()).setDbType(FieldDbType.VARCHAR.getKey()))
                .addField(new Field().setShowName("字段3").setDbName("field0003").setFieldType(FieldType.TEXT.getKey()).setDbType(FieldDbType.VARCHAR.getKey()))
                .addField(new Field().setShowName("字段4").setDbName("field0004").setFieldType(FieldType.TEXT.getKey()).setDbType(FieldDbType.VARCHAR.getKey()))
                .addField(new Field().setShowName("字段5").setDbName("field0005").setFieldType(FieldType.TEXT.getKey()).setDbType(FieldDbType.VARCHAR.getKey()))
                .build();
        builder.addTable(aTable);
        builder.addTable(bTable);

        // 2. 添加连接表
        JoinTable joinTable = JoinTable.builder().aTableDbName(aTable.getDbName()).aTableUidName(aTable.getUidName())
                .bTableDbName(bTable.getDbName()).bTableUidName(bTable.getUidName())
                .joinType(JoinType.LEFT.name())
                .joinConditions(Arrays.asList(JoinCondition.of("field0001", "field0001"), JoinCondition.of("field0002", "field0002")))
                .build();
        builder.addJoinTable(joinTable);

        // 添加字段
        builder.addShowField(aTable.getUidName(), "field0001", "字段1");
        builder.addShowField(aTable.getUidName(), "field0002", "字段2");
        builder.addShowField(aTable.getUidName(), "field0003", "字段3");
        builder.addShowField(bTable.getUidName(), "field0001", "字段11");
        builder.addShowField(bTable.getUidName(), "field0002", "字段22");
        builder.addShowField(bTable.getUidName(), "field0003", "字段33");

        // 添加排序字段
        builder.addOrderByFields(OrderByField.builder().tableUid(aTable.getUidName()).fieldName("field0001").orderBy(ReportConstants.OrderBy.ASC.getSql()).build());
        builder.addOrderByFields(OrderByField.builder().tableUid(aTable.getUidName()).fieldName("field0010").orderBy(ReportConstants.OrderBy.DESC.getSql()).build());
        builder.addOrderByFields(OrderByField.builder().tableUid(aTable.getUidName()).fieldName("field0015").orderBy(ReportConstants.OrderBy.ASC.getSql()).build());


        // 构建bean
        return builder.build();
    }
}
