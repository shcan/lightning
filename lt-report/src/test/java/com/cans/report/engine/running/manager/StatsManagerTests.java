package com.cans.report.engine.running.manager;

import com.alibaba.fastjson.JSON;
import com.cans.report.engine.bean.StatsDesignBean;
import com.cans.report.engine.bean.conditionbean.QueryParam;
import com.cans.report.engine.bean.sqlbean.WhereSqlTests;
import com.cans.report.engine.running.manager.impl.StatsManagerImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import static com.cans.report.engine.bean.StatsDesignBeanTests.getTestStatsDesignBean;
import static com.cans.report.engine.running.manager.QueryManagerTests.getTestQueryParam;
import static com.cans.report.engine.bean.sqlbean.WhereSqlTests.*;

/**
 * 统计报表测试
 */
@Slf4j
public class StatsManagerTests {

    /**
     * 简单测试统计报表查询
     */
    @Test
    void testStatsQuery() {
        StatsDesignBean statsDesignBean = getTestStatsDesignBean(aTableDbName, aTableUid, bTableDbName, bTableUid);
        QueryParam queryParam = getTestQueryParam(WhereSqlTests.aTableUid);

        StatsManager statsManager = new StatsManagerImpl();

        log.info("result; {}", JSON.toJSONString(statsManager.stats(statsDesignBean, queryParam).getData()));
    }
}
