package com.cans.report.engine.bean.sqlbean;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@Slf4j
public class WhereSqlTests {

    public static String aTableDbName = "formmain_0031";
    public static String bTableDbName = "formson_0032";
    public static String aTableUid = "formmain_0031_0";
    public static String bTableUid = "formson_0032_1";
    /**
     * 测试sql
     */
    @Test
    void testSql() {
        WhereSql whereSql = new WhereSql();

        whereSql.addTempSql("and    a = :param1 and ");
        whereSql.addTempParam("param1", "1");

        log.info("sql:{}", whereSql.sql());
        whereSql.commitTemp(true);

        whereSql.addTempSql("or b = :param2 and    ");
        whereSql.addTempParam("param2", "2");
        whereSql.commitTemp(true);
        log.info("sql:{}", whereSql.sql());
    }
}
