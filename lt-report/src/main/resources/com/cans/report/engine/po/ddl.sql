
-- 生成MySQL ReportAuth 创建 表的sql
CREATE TABLE `report_auth` (
   `id` bigint(20) NOT NULL comment 'id',
   `create_time` datetime comment '创建时间',
   `last_change_time` datetime comment '修改时间',
   `report_id` bigint(20) comment '报表id',
   `auth_type` int(11) comment '授权类型 1:用户 2:角色 3:部门',
   `auth_id` bigint(20) comment '权限id',
   `sort_num` int(11) comment '排序号',
   primary key (`id`)
) comment = '报表授权';

-- 生成MySQL创建 表的sql
CREATE TABLE `report_design` (
   `id` bigint(20) NOT NULL comment 'id',
   `name` varchar(255) comment '报表名称',
   `report_type` varchar(255) comment '修改时间',
   `report_json` longtext  comment '报表json配置',
   `create_time` datetime comment '创建时间',
   `last_change_time` datetime  comment '修改时间',
   `create_user` bigint comment '创建人',
   `update_user` bigint comment '修改人',
   `delete_flag` int  comment '删除标识',
   `remark` varchar(255) comment '备注',
   PRIMARY KEY (`id`)
) comment = '报表配置';