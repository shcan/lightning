package com.cans.report.config;

import com.baomidou.mybatisplus.extension.ddl.IDdl;
import jakarta.annotation.Resource;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

/**
 * 报表启动配置
 */
@Configuration
public class ReportConfiguration {



    @Component
    public static class MysqlDdl implements IDdl {

        @Resource
        private DataSource dataSource;

        @Override
        public void runScript(Consumer<DataSource> consumer) {
            consumer.accept(dataSource);
        }

        @Override
        public List<String> getSqlFiles() {
            return Collections.singletonList("com/cans/report/engine/po/ddl.sql");
        }
    }
}
