package com.cans.report;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.cans.report.engine.mapper")
public class ReportBoot {
    public static void main(String[] args) {
        new SpringApplication(ReportBoot.class).run(args);
    }
}
