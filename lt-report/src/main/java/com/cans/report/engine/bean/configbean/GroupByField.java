package com.cans.report.engine.bean.configbean;

import java.io.Serializable;

/**
 * @author qpy
 * @date 2024-07-31 11:51
 * @description 分组项字段
 */
public class GroupByField extends Field implements Serializable {
    
    private static final long serialVersionUID = 1L;

    /**
     * 分组类型? 构建
     */
    private String groupBy;
    
}
