package com.cans.report.engine.utils;

import com.cans.report.engine.bean.MulStatsDesignBean;
import com.cans.report.engine.bean.StatsDesignBean;
import com.cans.report.engine.bean.conditionbean.QueryCondition;
import com.cans.report.engine.bean.conditionbean.QueryConditions;
import com.cans.report.engine.bean.configbean.GroupByField;

/**
 * @author qpy
 * @date 2024-08-02 16:36
 * @description 查询条件的util
 */
public class QueryConditionUtils {
    
    
    /**
     * 多表统计查询条件转换成单表统计查询条件
     * @param qcs QueryConditions
     * @param msdb MulStatsDesignBean
     * @param stdb StatsDesignBean
     * @return 具体单表统计的 QueryConditions
     */
    public static QueryConditions mulStatsCondition2StatsCondition(QueryConditions qcs, MulStatsDesignBean msdb, StatsDesignBean stdb) {
        QueryConditions nqcs = new QueryConditions();

        for (QueryCondition qc : qcs.getQueryConditions()) {
            String newTableUid = null, newFieldName = null;
            int index = msdb.getGroupByFieldIndex(qc.getTableUid(), qc.getFieldName());
            if (index != -1) {
                newTableUid = stdb.getGroupByFields().get(index).getTableUidName();
                newFieldName = stdb.getGroupByFields().get(index).getDbName();
            }
            else {
                // 如果是统计项, 只用针对多表统计中一个统计做查询就可以了
                int statsFieldIndex = msdb.getStatsFieldIndex(qc.getTableUid(), qc.getFieldName());
                if (statsFieldIndex != -1) {
                    newTableUid = stdb.getStatsFields().get(statsFieldIndex).getTableUidName();
                    newFieldName = stdb.getStatsFields().get(statsFieldIndex).getDbName();
                }
            }
            // 主要是能替换 tableUid, 和 fieldUid 就可以了
            if (newTableUid != null && newFieldName != null) {
                QueryCondition newQueryCondition = qc.clone();
                newQueryCondition.setTableUid(newTableUid);
                newQueryCondition.setFieldName(newFieldName);
                nqcs.addQueryCondition(newQueryCondition);
            }
        }
        
        return nqcs;
    }
}
