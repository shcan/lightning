package com.cans.report.engine.po;

import com.cans.lightning.base.dto.BaseDto;
import lombok.Getter;
import lombok.Setter;

/**
 * @author qpy
 * @date 2024-07-16 10:01
 * @description 报表授权 对应数据库 report_auth 表
 */
@Getter
@Setter
public class ReportAuth extends BaseDto<Long> {

    /**
     * 报表id
     */
    private Long reportId;

    /**
     * 授权类型 1:用户 2:角色 3:部门...
     */
    private Integer authType;

    /**
     * 授权id
     */
    private Long authId;

    /**
     * 排序号
     */
    private Integer sortNum;
}
