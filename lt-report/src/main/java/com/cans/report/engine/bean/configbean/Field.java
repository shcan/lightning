package com.cans.report.engine.bean.configbean;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.text.MessageFormat;

/**
 * @author qpy
 * @date 2024-07-17 19:20
 * @description 表单字段配置JSON配置
 */
@Getter
@Setter
@Accessors(chain = true)
public class Field implements Serializable, IField {
    
    private static final long serialVersionUID = 1L;

    /**
     * 数据库字段名称
     */
    private String dbName;

    /**
     * 界面展示名称
     */
    private String showName;
    
    /**
     * 表别名称
     */
    private String tableUidName;

    /**
     * 表数据库名称
     */
    private String tableDbName;

    /**
     * 数据库字段的类型 
     * @see com.cans.report.engine.constants.ReportConstants.FieldDbType
     */
    private String dbType;

    /**
     * 字段长度
     */
    private String length;

    /**
     * 控件类型 
     * @see com.cans.report.engine.constants.ReportConstants.FieldType
     */
    private String fieldType;
    
    
    public String whereSql() {
        return MessageFormat.format(" {0}.{1} ", tableUidName, dbName);
    }

    @Override
    public String sql() {
        return getTableUidName() + "." + getDbName();
    }

    @Override
    public String key() {
        return getTableUidName() + "." + getDbName();
    }
}
