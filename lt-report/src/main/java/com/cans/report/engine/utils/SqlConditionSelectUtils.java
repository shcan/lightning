package com.cans.report.engine.utils;

import com.cans.report.engine.bean.conditionbean.QueryCondition;
import com.cans.report.engine.bean.configbean.Field;
import com.cans.report.engine.bean.sqlbean.WhereSql;
import com.cans.report.engine.constants.ReportConstants;

import java.util.List;
import java.util.stream.Collectors;

import static com.cans.report.engine.utils.SqlConditionUtils.*;
import static com.cans.report.engine.utils.SqlConditionValueUtils.*;
import static com.cans.report.engine.constants.ReportConstants.*;

/**
 * @author qpy
 * @date 2024-07-29 17:00
 * @description select类型的实用方法
 */
public class SqlConditionSelectUtils {



    /**
     * queryCondition.value 支持的格式
     *    1. String eg: "id1", "id2", "id3" 
     *    2. Long eg: id1
     *    3. List<Long> eg: [id1, id2, id3]
     *    4. List<String> eg: ["id1", "id2", "id3"]
     *    5. Map<String, Object> eg: {"id": "id1", "name": "是", "value": "0"}
     *    6. List<Map<String, Object>> eg: [{"id": "id1", "name": "是", "value": "0"}, {"id": "id2", "name": "否", "value": "1"}]
     *    最终需要的是解析为一个 List<Long> 或者 List<String>
     * @param whereSql whereSql
     * @param queryCondition queryCondition
     * @param field field
     * @param paramValueKey 命名参数的key
     */
    static void buildSingleWhereSqlSelect(WhereSql whereSql, QueryCondition queryCondition, Field field, String paramValueKey) {

        List<Object> paramList = parseParamValue2List(queryCondition.getValue(), null, "id");
        List<Object> noNullParamList = paramList.stream().filter(pa -> isNullPredicate.test(pa, true)).collect(Collectors.toList());
        // 如果过滤掉存在空的值, 则需要拼接空值的sql
        boolean needNullSql = paramList.size() == 0 || paramList.size() > noNullParamList.size();
        
        List<Object> noNullNewParamList = parseParamValue2DbType(noNullParamList, FieldDbType.getFieldDbType(field.getDbType()));

        // 通用去构建sql条件即可
        buildSingleWhereSqlCommon(whereSql, queryCondition.getOperator(), field, paramValueKey, noNullNewParamList, needNullSql);
    }


}
