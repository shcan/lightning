package com.cans.report.engine.bean;

import com.cans.report.engine.bean.configbean.ConfigBtn;
import com.cans.report.engine.bean.configbean.ConfigCondition;
import com.cans.report.engine.bean.configbean.Field;
import com.cans.report.engine.constants.ReportConstants;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author qpy
 * @date 2024-07-17 11:02
 * @description 抽象报表设计, 主要记录和结构无关的简单报表信息, 通过report_design进行反解析
 */
@Setter
@Getter
public abstract class AbstractDesignBean implements Serializable, Cloneable {
    
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 报表名称
     */
    private String name;
    
    /**
     * 用户配置的筛选条件, 暂不实现
     */
    private ConfigCondition configCondition;

    /**
     * 配置显示哪些按钮 暂不实现
     */
    private ConfigBtn configBtn;

    /**
     * @see ReportConstants.ReportType
     */
    protected String reportType;

    public AbstractDesignBean() {}

    protected AbstractDesignBean(Builder builder) {
        this.id = builder.id;
        this.name = name;
    }

    /**
     * 获取字段
     * @param tableUid tableUid
     * @param dbFieldName dbFieldName
     * @return Field
     */
    public abstract Field getField(String tableUid, String dbFieldName);
    
    public String getReportType() {
        if (reportType != null && ReportConstants.ReportType.getReportType(reportType) != null) {
            return reportType;
        }
        if (this instanceof QueryDesignBean) {
            return ReportConstants.ReportType.QUERY.getKey();
        }
        if (this instanceof StatsDesignBean) {
            return ReportConstants.ReportType.STATISTICS.getKey();
        }
        return null;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        AbstractDesignBean bean = null;
        // 最好不要使用 java.lang.Object.clone 这个默认实现, 因为这个实现, 会基于
        // this数据进行设置  会导致 bean.(List、Map等) = this.bean.(List、Map等) 是同一个对象
        try {
            bean = this.getClass().newInstance();
        } catch (IllegalAccessException | InstantiationException e) {
            throw new RuntimeException(e);
        }
        bean.setId(this.id);
        bean.setName(this.name);
        bean.setReportType(this.reportType);
        bean.setConfigBtn(this.configBtn);
        bean.setConfigCondition(this.configCondition);
        return bean;
    }

    public static class Builder {
        private long id;
        private String name;
        
        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }
    }
    
}
