package com.cans.report.engine.utils;

import com.cans.lightning.utils.StringUtils;
import com.cans.report.engine.bean.AbstractDesignBean;
import com.cans.report.engine.bean.QueryDesignBean;
import com.cans.report.engine.bean.StatsDesignBean;
import com.cans.report.engine.bean.conditionbean.QueryParam;
import com.cans.report.engine.bean.configbean.*;
import com.cans.report.engine.bean.sqlbean.SqlCriterion;
import com.cans.report.engine.constants.ReportConstants;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author qpy
 * @date 2024-07-22 17:29
 * @description sql语句工具类
 */
public class SqlCriterionUtils {

    /**
     * 构建SqlCriterion对象
     * 根据传入不同类型的 AbstractDesignBean, 创建对应的 SqlCriterion 对象 
     * @see ReportConstants.ReportType
     * @return SqlCriterion
     */
    public static <T extends AbstractDesignBean> SqlCriterion buildCriterion(T designBean, QueryParam queryParam) {

        ReportConstants.ReportType reportType = ReportConstants.ReportType.getReportType(designBean.getReportType());
        if (reportType == null) {
            throw new RuntimeException("当前报表 {" + designBean.getName() + "}, class={" + designBean.getClass().getName() + "}, 的报表类型是空值, reportType: " + designBean.getReportType());
        }
        SqlCriterion sqlCriterion = null;
        switch (reportType) {
            case QUERY:
                sqlCriterion = buildQueryCriterion((QueryDesignBean) designBean, queryParam);
                break;
            case STATISTICS:
                sqlCriterion = buildStatisticsCriterion((StatsDesignBean) designBean, queryParam);
                break;
            case MULTI_STATISTICS:
                sqlCriterion = buildMultiStatisticsCriterion();
                break;
            default:
                ;
        }
        return sqlCriterion;
    }

    /**
     * 构建查询的SqlCriterion对象
     * @param designBean designBean
     * @param queryParam queryParam
     */
    private static SqlCriterion buildQueryCriterion(QueryDesignBean designBean, QueryParam queryParam) {
        SqlCriterion sqlCriterion = new SqlCriterion();
        
        // 1. select
        doSelect(sqlCriterion, designBean.getShowFieldMap().values());
        
        // 2. from
        doFrom(sqlCriterion, designBean.getJoinTables(), designBean.getTables());
        
        // 3. where where由具体查询时候动态生成
        
        // 4. order by 查询不需要group by
        
        // 5. order by
        doOrderBy(sqlCriterion, designBean.getOrderByFields());
        
        return sqlCriterion;
    }

    /**
     * 构建统计的SqlCriterion对象
     * @return SqlCriterion
     */
    private static SqlCriterion buildStatisticsCriterion(StatsDesignBean statsDesignBean, QueryParam queryParam) {
        // 1. select
        SqlCriterion sqlCriterion = new SqlCriterion();
        // select 分组项
        doSelect(sqlCriterion, statsDesignBean.getGroupByFields());
        // select 统计项
        doSelect(sqlCriterion, statsDesignBean.getStatsFields());
        
        // 2. from
        doFrom(sqlCriterion, statsDesignBean.getJoinTables(), statsDesignBean.getTables());
        
        // 3. where where由具体查询时候动态生成, 
        // @see com.cans.report.engine.running.dao.impl.ReportSqlExecutorImpl.doSqlFrom
        
        // 4. group by
        // 单表统计, 需要对 GroupBy字段添加group by
        doGroupBy(sqlCriterion, statsDesignBean.getGroupByFields());

        // 5. order by
        doOrderBy(sqlCriterion, statsDesignBean.getOrderByFields());
        
        return sqlCriterion;
    }

    private static void doGroupBy(SqlCriterion sqlCriterion, List<GroupByField> groupByFields) {
        if (groupByFields == null) {
            return;
        }
        for (GroupByField groupByField : groupByFields) {
            sqlCriterion.addGroupBy(groupByField.key(), groupByField.sql());
        }
    }

    private static SqlCriterion buildMultiStatisticsCriterion() {
        
        return new SqlCriterion();
    }

    
    /* ============================ 通用方法 =============================== */

    private static void doSelect(SqlCriterion sqlCriterion, Collection<? extends IField> fieldList) {
        for (IField field : fieldList) {
            sqlCriterion.addSelect(field.key(), field.sql());
        }
    }
    
    
    private static void doFrom(SqlCriterion sqlCriterion, List<JoinTable> joinTables, Map<String, Table> tables) {
        for (JoinTable joinTable : joinTables) {
            sqlCriterion.addJoinTable(joinTable);
        }
    }

    private static void doOrderBy(SqlCriterion sqlCriterion, List<OrderByField> orderByFields) {
        for (OrderByField orderByField : orderByFields) {
            String sql = orderByField.sql();
            if (!StringUtils.isEmpty(sql)) {
                sqlCriterion.addOrderBy(orderByField.key(), orderByField.sql());
            }
        }
    }
}
