package com.cans.report.engine.bean.configbean;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author qpy
 * @date 2024-07-22 16:47
 * @description 连接条件
 */
@Setter
@Getter
public class JoinCondition implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private String aFieldName;
    
    private String bFieldName;
    
    public static JoinCondition of(String aFieldName, String bFieldName) {
        JoinCondition joinCondition = new JoinCondition();
        joinCondition.aFieldName = aFieldName;
        joinCondition.bFieldName = bFieldName;
        return joinCondition;
    }
}
