package com.cans.report.engine.running.manager.impl;

import com.cans.lightning.base.dao.jdbc.PageInfo;
import com.cans.report.engine.bean.StatsDesignBean;
import com.cans.report.engine.bean.conditionbean.QueryParam;
import com.cans.report.engine.bean.sqlbean.SqlCriterion;
import com.cans.report.engine.running.dao.impl.ReportSqlExecutorImpl;
import com.cans.report.engine.running.manager.StatsManager;
import com.cans.report.engine.utils.SqlConditionUtils;
import com.cans.report.engine.utils.SqlCriterionUtils;

/**
 * @author qpy
 * @date 2024-05-20 10:37
 * @description
 */
public class StatsManagerImpl implements StatsManager {
    @Override
    public PageInfo stats(StatsDesignBean statsDesignBean, QueryParam queryParam) {
        return statsBySqlCriterion(statsDesignBean, null, queryParam);
    }


    @Override
    public SqlCriterion buildCriterion(StatsDesignBean statsDesignBean, QueryParam queryParam) {
        SqlCriterion sqlCriterion = SqlCriterionUtils.buildCriterion(statsDesignBean, queryParam);

        // 构建条件
        SqlConditionUtils.buildWhereBeforeQuery(sqlCriterion, queryParam.getQueryConditions(), statsDesignBean);

        return sqlCriterion;
    }

    @Override
    public PageInfo statsBySqlCriterion(StatsDesignBean statsDesignBean, SqlCriterion sqlCriterion, QueryParam queryParam) {
        if (sqlCriterion == null) {
            sqlCriterion = buildCriterion(statsDesignBean, queryParam);
        }
        PageInfo pageInfo = new PageInfo().setPage(queryParam.getPageNum()).setPageSize(queryParam.getPageSize());
        return new ReportSqlExecutorImpl().execute(sqlCriterion, pageInfo);
    }
}
