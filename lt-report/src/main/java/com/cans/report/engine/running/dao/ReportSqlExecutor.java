package com.cans.report.engine.running.dao;

import com.cans.lightning.base.dao.jdbc.PageInfo;
import com.cans.report.engine.bean.sqlbean.SqlCriterion;
import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Map;

/**
 * @author qpy
 * @date 2024-07-22 19:07
 * @description 报表执行SQL的入口
 */
public interface ReportSqlExecutor {

     String SELECT_FIELD_KEY = "f";
    
    /**
     * 执行SQL查询通用接口
     * @param sqlCriterion
     * @return
     */
    Object execute(SqlCriterion sqlCriterion, PageInfo pageInfo);

    /**
     * 获取SQL查询的 sql
     * @return
     */
    SqlExecutorWrapper getSqlExecutorWrapper(SqlCriterion sqlCriterion);
    
    /**
     * sql 执行包装类 
     */
    @Setter
    @Getter
    @ToString
    class SqlExecutorWrapper {

        /**
         * 具体执行的sql
         */
        private StringBuilder sql = new StringBuilder();

        /**
         * 字段映射map
         * select  
         *     formmain_00xx.field_00xx f0 // f0作为映射别名
         *     formmain_00xx.field_00xx f1
         *  
         *  key: f0
         *  value: formmain_00xx.field_00xx
         */
        private Map<String, String> reflectMap = Maps.newHashMap();
    }
}
