package com.cans.report.engine.bean;

import com.cans.report.engine.bean.configbean.*;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author qpy
 * @date 2024-07-31 11:44
 * @description 统计报表配置
 */
@Getter
@Setter
@NoArgsConstructor
public class AbstractStatsDesignBean extends AbstractDesignBean {

    /**
     * 报表绑定表单formmain_00xx的配置
     */
    private Map<String, Table> tables = Maps.newLinkedHashMap();

    /**
     * 连接表条件 from a join b on a.xx = b.xx
     */
    private List<JoinTable> joinTables = new ArrayList<>();
    
    /**
     * 分组项的配置
     */
    private List<GroupByField> groupByFields = Lists.newArrayList();
    
    /**
     * 统计项的配置
     */
    private List<StatsField> statsFields = Lists.newArrayList();

    /**
     * 排序字段
     */
    private List<OrderByField> orderByFields = Lists.newArrayList();
    
    protected AbstractStatsDesignBean(Builder builder) {
        super(builder);
        this.tables = builder.tables;
        this.joinTables = builder.joinTables;
        this.groupByFields = builder.groupByFields;
        this.statsFields = builder.statsFields;
        this.orderByFields = builder.orderByFields;
    }
    
    @Override
    public Field getField(String tableUid, String dbFieldName) {
        Table table = tables.get(tableUid);
        if (table != null) {
            return table.getFieldMap().get(dbFieldName);
        }
        return null;
    }

    @Override
    protected AbstractStatsDesignBean clone() throws CloneNotSupportedException {
        AbstractStatsDesignBean clone = (AbstractStatsDesignBean) super.clone();
        // 使用半浅克隆, 不克隆具体配置对象, (这种克隆方式对于报表没问题, 因为报表只会移除集合中的对象 而不会更改集合中具体某个bean的配置值)
        clone.tables.putAll(this.tables);
        clone.joinTables.addAll(this.joinTables);
        clone.groupByFields.addAll(this.groupByFields);
        clone.statsFields.addAll(this.statsFields);
        clone.orderByFields.addAll(this.orderByFields);
        return clone;
    }

    public static class Builder extends AbstractDesignBean.Builder {
        Map<String, Table> tables = Maps.newLinkedHashMap();
        List<JoinTable> joinTables = new ArrayList<>();
        List<GroupByField> groupByFields = Lists.newArrayList();
        List<StatsField> statsFields = Lists.newArrayList();
        List<OrderByField> orderByFields = Lists.newArrayList();
        
        public Builder addGroupByField(GroupByField groupByField) {
            groupByFields.add(groupByField);
            return this;
        }
        public Builder addGroupByField(List<GroupByField> groupByField) {
            groupByFields.addAll(groupByField);
            return this;
        }
        public Builder addJoinTable(JoinTable joinTable) {
            joinTables.add(joinTable);
            return this;
        }
        public Builder addStatsField(StatsField statsField) {
            statsFields.add(statsField);
            return this;
        }
        
        public Builder addStatsField(List<StatsField> statsField) {
            statsFields.addAll(statsField);
            return this;
        }
        
        public Builder addTable(Table table) {
            tables.put(table.getUidName(), table);
            return this;
        }
        
        public Builder addOrderByFields(OrderByField orderByField) {
            orderByFields.add(orderByField);
            return this;
        }
    }
}
