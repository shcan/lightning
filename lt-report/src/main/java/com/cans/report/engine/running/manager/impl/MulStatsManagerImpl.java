package com.cans.report.engine.running.manager.impl;

import com.cans.lightning.base.dao.jdbc.PageInfo;
import com.cans.report.engine.bean.MulStatsDesignBean;
import com.cans.report.engine.bean.StatsDesignBean;
import com.cans.report.engine.bean.conditionbean.QueryConditions;
import com.cans.report.engine.bean.conditionbean.QueryParam;
import com.cans.report.engine.bean.configbean.GroupByField;
import com.cans.report.engine.bean.configbean.IField;
import com.cans.report.engine.bean.configbean.StatsField;
import com.cans.report.engine.bean.sqlbean.SqlCriterion;
import com.cans.report.engine.running.manager.MulStatsManager;
import com.cans.report.engine.running.manager.StatsManager;
import com.cans.report.engine.utils.QueryConditionUtils;
import com.cans.report.engine.utils.ReportUtils;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author qpy
 * @date 2024-08-02 11:31
 * @description
 */
@Slf4j
public class MulStatsManagerImpl implements MulStatsManager {

    /**
     * 标记: 暂时先这样做 为了测试功能
     */
    private StatsManager statsManager = new StatsManagerImpl();
    
    @Override
    public PageInfo mulStats(MulStatsDesignBean mulStatsDesignBean, QueryParam queryParam) {
        
        // 0. 构建多个统计的查询条件 SqlCriterion
        Map<String, SqlCriterion> sqlCriterionMap = buildSqlCriterion(mulStatsDesignBean, queryParam);
        
        // 1. 调用每个具体统计查询数据, 并做数据合并
        List<Map<String, Object>> mergedData = queryMulStatsData(mulStatsDesignBean, queryParam, sqlCriterionMap);

        // 2. 标记: 后续业务开发中....

        // 暂时功能返回数据  功能强化后续开发
        return new PageInfo().setData(mergedData);
    }

    /**
     * 1. 调用每个具体统计查询数据, 并做数据合并
     * 2. ...
     * @param mulStatsDesignBean mulStatsDesignBean
     * @param queryParam queryParam
     * @param sqlCriterionMap sqlCriterionMap
     * @return List<Map<String, Object>>
     */
    private List<Map<String, Object>> queryMulStatsData(MulStatsDesignBean mulStatsDesignBean,
                                                        QueryParam queryParam,
                                                        Map<String, SqlCriterion> sqlCriterionMap) {

        Map<String, Map<String, Object>> mergeResult = Maps.newLinkedHashMap();
        for (StatsDesignBean stats : mulStatsDesignBean.getStats()) {
            PageInfo pageInfo = statsManager.statsBySqlCriterion(stats, sqlCriterionMap.get(stats.getId().toString()), queryParam);

            Map<IField, IField> stats2MulStatsFields = mulStatsDesignBean.buildStats2MulStatsFieldReflection(stats);
            // 合并数据 统一key-value 为 多表统计的key - value
            mergeData2UnificationKey(stats, stats2MulStatsFields, mergeResult, pageInfo.getData());
        }

        // 查询完成后, 取出 mergeResult.values()的数据 即是所有报表查询的合并数据
        return new ArrayList<>(mergeResult.values());
    }

    /**
     * 构建多个统计的查询条件 SqlCriterion
     * @param mulStatsDesignBean mulStatsDesignBean
     * @param queryParam queryParam
     * @return Map<String, SqlCriterion>
     */
    private Map<String, SqlCriterion> buildSqlCriterion(MulStatsDesignBean mulStatsDesignBean, QueryParam queryParam) {
        Map<String, SqlCriterion> criterionMap = Maps.newHashMap();
        
        for (StatsDesignBean stats : mulStatsDesignBean.getStats()) {
            QueryConditions newQueryConditions = QueryConditionUtils.mulStatsCondition2StatsCondition(queryParam.getQueryConditions(), mulStatsDesignBean, stats);

            // 移除statsDesignBean中不存在的统计项字段
            StatsDesignBean newStats = removeStatsFieldIfNeed(mulStatsDesignBean, stats);

            // 创建sqlCriterion
            SqlCriterion sqlCriterion = statsManager.buildCriterion(newStats, new QueryParam().setPageNum(queryParam.getPageNum()).setPageSize(queryParam.getPageSize()).setQueryConditions(newQueryConditions));
            // id 标识需要另外获取, 尽可能最小代价取值, 多表统计中, 每个单表的id需要唯一
            criterionMap.put(newStats.getId().toString(), sqlCriterion);
        }
        
        return criterionMap;
    }

    /**
     * 如果 mulStatsDesignBean 中不存在统计项的字段, 但是 statsDesignBean 中存在, 则移除 statsDesignBean 中的统计项字段
     * @param mulStatsDesignBean mulStatsDesignBean
     * @param stats stats
     * @return clone的statsDesignBean
     */
    private StatsDesignBean removeStatsFieldIfNeed(MulStatsDesignBean mulStatsDesignBean, StatsDesignBean stats) {
        StatsDesignBean newStats = stats.clone();

        for (StatsField statsField : stats.getStatsFields()) {
            if (mulStatsDesignBean.getStatsFieldIndex(statsField.getTableUidName(), statsField.getDbName()) == -1) {
                newStats.getStatsFields().remove(statsField);
            }
        }

        return newStats;
    }


    /**
     * 多表数据统一合并成 MulStatsBean的 key-value, 只用对行表头做合并即可
     * @param stats stats
     * @param stats2MulStatsFields 多表到多表统计的映射
     * @param mergeResult 合并结果
     * @param dataList 单表查询的数据
     */
    private void mergeData2UnificationKey(StatsDesignBean stats,
                                          Map<IField, IField> stats2MulStatsFields,
                                          Map<String, Map<String, Object>> mergeResult,
                                          List<Map<String, Object>> dataList) {
        for (Map<String, Object> data : dataList) {
            StringBuilder groupKey = new StringBuilder();

            Map<String, Object> row = Maps.newHashMap();
            for (GroupByField groupByField : stats.getGroupByFields()) {
                IField statsField = stats2MulStatsFields.get(groupByField);

                Object singleValue = data.get(groupByField.key());

                ReportUtils.appendGroupByField2Str(groupKey, singleValue);
                row.put(statsField.key(), singleValue);
            }

            String groupKeyStr = groupKey.toString();
            // 需要做这个判断  因为多个表 相同的groupKey需要将数据合并到一起
            if (mergeResult.get(groupKeyStr) != null) {
                row = mergeResult.get(groupKeyStr);
            } else {
                // 放入行值
                mergeResult.put(groupKeyStr, row);
            }
            // 最后将统计项的数据也合并到一起就ok了
            for (StatsField statsField : stats.getStatsFields()) {
                // 需要注意的是, 多表统计里面需要的统计字段才加入返回结果, 否则不加入返回结果
                if (data.containsKey(statsField.key())) {
                    IField statsField2 = stats2MulStatsFields.get(statsField);
                    row.put(statsField2.key(), data.get(statsField.key()));
                }
            }
        }
    }
}
