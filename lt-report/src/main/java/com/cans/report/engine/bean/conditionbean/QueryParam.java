package com.cans.report.engine.bean.conditionbean;

import lombok.*;
import lombok.experimental.Accessors;

/**
 * @author qpy
 * @date 2024-07-19 11:00
 * @description 查询条件
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Accessors(chain = true)
public class QueryParam {

    /**
     * 条件
     */
    private QueryConditions queryConditions;

    /**
     * 页码
     */
    int pageNum;

    /**
     * 每页条数
     */
    int pageSize;
}
