package com.cans.report.engine.bean.configbean;

import com.cans.report.engine.constants.ReportConstants;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author qpy
 * @date 2024-07-17 19:13
 * @description 行表头展示字段
 */
@Setter
@Getter
@Accessors(chain = true)
public class ShowField implements IField, Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * 数据显示格式
     * @see ReportConstants#FORMAT_DATE
     */
    private String format;

    /**
     * 表别名称 唯一
     */
    private String uidTableName;

    /**
     * 表数据库名称
     */
    private String dbFieldName;

    /**
     * 前端展示名称
     */
    private String showName;

    @Override
    public String sql() {
        return key();
    }

    @Override
    public String key() {
        return getUidTableName() + "." + getDbFieldName();
    }
}
