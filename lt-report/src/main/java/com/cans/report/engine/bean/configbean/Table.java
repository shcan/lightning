package com.cans.report.engine.bean.configbean;

import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Map;

/**
 * @author qpy
 * @date 2024-07-17 19:20
 * @description 表单配置的信息的复制
 */
@Getter
@Setter
public class Table implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * 表别名  比如: select * from 数据库名称 表别名, 
     */
    private String uidName;

    /**
     * 数据库表名称 formmain_00xx
     */
    private String dbName;
    
    private Map<String, Field> fieldMap = Maps.newLinkedHashMap();
    
    public static Builder builder() {
        return new Builder();
    }
    
    public static class Builder {
        String uidName;
        String dbName;
        Map<String, Field> fieldMap = Maps.newLinkedHashMap();
        public Builder uidName(String uidName) {
            this.uidName = uidName;
            return this;
        }
        public Builder dbName(String dbName) {
            this.dbName = dbName;
            return this;
        }
        public Builder addField(Field field) {
            field.setTableDbName(this.dbName);
            field.setTableUidName(this.uidName);
            this.fieldMap.put(field.getDbName(), field);
            return this;
        }
        public Table build() {
            Table table = new Table();
            table.setUidName(uidName);
            table.setDbName(dbName);
            table.setFieldMap(fieldMap);
            return table;
        }
    }
}
