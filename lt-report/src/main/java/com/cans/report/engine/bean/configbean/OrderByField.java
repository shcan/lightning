package com.cans.report.engine.bean.configbean;

import com.cans.report.engine.constants.ReportConstants.OrderBy;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author qpy
 * @date 2024-08-01 9:53
 * @description 排序字段
 */
@Setter
@Getter
@Accessors(chain = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderByField implements IField, Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 字段名称
     */
    private String fieldName;

    /**
     * tableUid
     */
    private String tableUid;

    /**
     * 排序方式
     * @see OrderBy
     */
    private String orderBy;
    
    @Override
    public String sql() {
        return OrderBy.getOrderBy(orderBy) == OrderBy.NONE ? "" : key() + " " + OrderBy.getOrderBy(orderBy).getSql();
    }

    @Override
    public String key() {
        return tableUid + "." + fieldName;
    }
}
