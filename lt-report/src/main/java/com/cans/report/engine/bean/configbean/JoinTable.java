package com.cans.report.engine.bean.configbean;

import com.cans.report.engine.constants.ReportConstants;
import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * @author qpy
 * @date 2024-07-22 16:22
 * @description 单个表连接配置信息
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class JoinTable implements Serializable {
    
    private static final long serialVersionUID = 1L;

    /**
     * @see ReportConstants.JoinType
     */
    private String joinType;

    /**
     * a表数据库名称
     */
    private String aTableDbName;
    /**
     * a表唯一别名
     */
    private String aTableUidName;

    /**
     * b表数据库名称
     */
    private String bTableDbName;
    
    /**
     * b表唯一别名
     */
    private String bTableUidName;

    /**
     * 表之间的连接条件
     */
    private List<JoinCondition> joinConditions;
    
}
