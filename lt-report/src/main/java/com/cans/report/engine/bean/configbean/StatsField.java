package com.cans.report.engine.bean.configbean;

import com.cans.report.engine.constants.ReportConstants;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.cans.report.engine.constants.ReportConstants.SqlCalcType;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author qpy
 * @date 2024-07-31 11:52
 * @description 统计项字段
 */
@Setter
@Getter
@Accessors(chain = true)
public class StatsField extends Field implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * 计算类型
     * @see ReportConstants.SqlCalcType
     */
    private String calcType;

    @Override
    public String sql() {
        return Objects.requireNonNull(SqlCalcType.getSqlCalcType(calcType)).getSql() + "(" + super.sql() + ")";
    }


    /**
     * 统计项字段 这个必须重写, 作为唯一标记
     */
    @Override
    public String key() {
        // 统计项字段 这个必须重写, 作为唯一标记
        return Objects.requireNonNull(SqlCalcType.getSqlCalcType(calcType)).getSql() + "_" + super.key();
    }
}
