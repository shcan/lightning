package com.cans.report.engine.exceptions;

/**
 * @author qpy
 * @date 2024-07-29 10:22
 * @description 此类主要是报表单个拼接sql发生数据有问题时, 抛出的异常, 由上层捕获 
 */
public class SingleWhereSqlException extends CommonSqlException {
    
    public SingleWhereSqlException() {
        super();
    }
    
    public SingleWhereSqlException(String message) {
        super(message);
    }
    
    public SingleWhereSqlException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
