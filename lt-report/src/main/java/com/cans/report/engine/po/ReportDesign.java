package com.cans.report.engine.po;

import com.cans.lightning.base.dto.BaseDto;
import lombok.Getter;
import lombok.Setter;

/**
 * @author qpy
 * @date 2024-07-16 9:57
 * @description 报表设计 对应数据库 report_design 表
 */
@Getter
@Setter
public class ReportDesign extends BaseDto<Long> {

    /**
     * 报表标题
     */
    private String name;

    /**
     * 报表类型 QUERY、STATIC、
     */
    private String reportType;

    /**
     * 创建人
     */
    private Long createUser;
    
    /**
     * 更新人
     */
    private Long updateUser;

    /**
     * 删除标志
     */
    private int deleteFlag;

    /**
     * 报表配置信息
     */
    private String reportJson;

    /**
     * 备注
     */
    private String remark;
}
