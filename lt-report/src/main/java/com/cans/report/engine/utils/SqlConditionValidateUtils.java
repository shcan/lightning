package com.cans.report.engine.utils;

import com.cans.lightning.base.exceptions.BusinessException;
import com.cans.report.engine.bean.sqlbean.WhereSql;
import com.cans.report.engine.constants.ReportConstants;
import com.cans.report.engine.exceptions.SingleWhereSqlException;
import org.apache.commons.lang3.StringUtils;

import static com.cans.report.engine.utils.SqlConditionUtils.*;

/**
 * @author qpy
 * @date 2024-07-29 17:05
 * @description 校验条件 为了拆分 SqlConditionUtils 的各个功能, 将各个功能写到对应的class里面
 */
public class SqlConditionValidateUtils {

    /**
     * 必填字段
     */
    static String validateSingleConditionMustField(String filedStr) {
        if (!StringUtils.isBlank(filedStr)) {
            filedStr = filedStr.trim();
            // 1. 操作符校验
            if (ReportConstants.SqlOperation.getSqlOperationBySql(filedStr) != null) {
                return filedStr;
            }
        }
        throw new SingleWhereSqlException("validateSingleConditionMustField: filedStr is null or empty 【" + filedStr + "】");
    }

    /**
     * 校验单个查询条件的值 是否合法
     * @param condFieldValue condFieldValue
     * @return String
     */
    static String validateSingleConditionField(String condFieldValue) {
        if (StringUtils.isBlank(condFieldValue)) {
            return "";
        }
        condFieldValue = condFieldValue.trim();
        if (leftBracket.equalsIgnoreCase(condFieldValue) || rightBracket.equalsIgnoreCase(condFieldValue)) {
            return condFieldValue;
        }

        if (operatorAnd.equalsIgnoreCase(condFieldValue) || operatorOr.equalsIgnoreCase(condFieldValue)) {
            return condFieldValue;
        }

        throw new BusinessException("validateSingleConditionField: illegal value: " + condFieldValue);
    }

    /**
     * 校验空  
     * @param target 目标
     * @param exceptionMsg 异常信息
     */
    static void validateNullAndException(Object target, String exceptionMsg, boolean needThrown) {
        if (target == null) {
            throw new SingleWhereSqlException(exceptionMsg).setNeedThrow(needThrown);
        }
    }

    /**
     * 校验 whereSql=" ( ( ) ) " 等未拼接条件的清空 直接清空 
     * @param whereSql
     * @param clearTemp
     */
    public static boolean validateEmptyWhereSqlAndClearIfNeed(WhereSql whereSql, boolean clearTemp, boolean needThrown) {
        String tempSql = whereSql.getTempSql();
        if (tempSql.replaceAll(leftBracketReg, emptyStr).replaceAll(rightBracketReg, emptyStr).replaceAll(oneSpace, emptyStr).length() == 0){
            if (clearTemp) {
                whereSql.clearTemp();
            }
            if (needThrown) {
                throw new SingleWhereSqlException("validateEmptyWhereSqlAndClearIfNeed: whereSql is empty: " + tempSql);
            }
            return true;
        }
        return false;
    }
}
