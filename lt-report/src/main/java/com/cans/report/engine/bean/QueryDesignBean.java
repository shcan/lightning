package com.cans.report.engine.bean;

import lombok.Getter;
import lombok.Setter;

/**
 * @author qpy
 * @date 2024-07-17 11:03
 * @description
 */
@Getter
@Setter
public class QueryDesignBean extends AbstractQueryDesignBean {
    
    private static final long serialVersionUID = 1L;
    
    public QueryDesignBean() {}
    
    
    private QueryDesignBean(Builder builder) {
        super(builder);
    }
    
    public static Builder builder() {
        return new Builder();
    }

    /**
     * 构建者模式
     */
    public static class Builder extends AbstractQueryDesignBean.Builder {
        
        public QueryDesignBean build() {
            return new QueryDesignBean(this);
        }
    }
}
