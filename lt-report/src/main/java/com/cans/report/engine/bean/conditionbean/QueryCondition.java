package com.cans.report.engine.bean.conditionbean;

import lombok.*;
import lombok.experimental.Accessors;

/**
 * @author qpy
 * @date 2024-07-19 10:17
 * @description 单个查询条件
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
@Builder
public class QueryCondition implements Cloneable {

    /* =========== 对外调用接口 start ====================== */
    /**
     * 返回原始sql  a.field00xx = 1
     * 返回命名sql  a.field00xx = :param_xx
     */
    public String whereSql() {

        return "";
    }

    /**
     * 有的sql a.field00xx is null, 通过 getParam()获取肯定是空, 所以通过这里来判断
     */
    public boolean hasParam() {
        // mark 这种做法不正确, 后续改进
        // 1. 正常情况下, = <> in between 等功能, 都会返回true
        // 2. 某些功能, 比如 like, in, between, 需要判断value是否为空, 如果为空, 则返回false
        // 如果value=null, 传入like in between 等操作, 不应该进行sql 拼接

        // 第2 步骤的情况应该在查询之前做校验, 如果不合法 抛出异常 或者是过滤掉此Condition 不做后端查询
        return this.value != null;
    }

    /**
     * 返回参数对象值
     */
    public Object getParam() {
        return this.value;
    }
    /* =========== 对外调用接口 end ====================== */

    /**
     * 数据库字段列的名称
     */
    protected String fieldName;

    /**
     * 表的uid
     */
    protected String tableUid;

    /**
     * 左括号
     * 默认情况下左、右括号都应该存在值,
     * 标准功能sql拼接时, 对 List<Condition> 每个sql拼接, 左、右括号就从这里取值, 不额外添加左右括号
     */
    protected String leftBracket;

    /**
     * 右括号
     */
    protected String rightBracket;

    /**
     * SQL操作符
     * @see com.cans.report.engine.constants.ReportConstants.SqlOperation
     */
    protected String operator;

    /**
     * 与上一个条件连接符
     */
    protected String andOr;

    /**
     * 控件类型, SqlConditionUtils封装sql时用此字段, 不同字段类型有不同的sql拼接方式
     * @see com.cans.report.engine.constants.ReportConstants.FieldType
     * /@see com.cans.report.engine.bean.configbean.Field#fieldType
     */
    protected String fieldType;

    /**
     * 数据库的字段类型
     * @see com.cans.report.engine.constants.ReportConstants.FieldDbType
     * /@see com.cans.report.engine.bean.configbean.Field#dbType
     */
    protected String fieldDbType;

    /**
     * 参数传入的值
     */
    protected Object value;

    /**
     * 额外左括号
     * 这两个属性目前暂时不会用到
     */
    protected String extraLeftBracket;

    /**
     * 额外右括号
     * 这两个属性目前暂时不会用到
     */
    protected String extraRightBracket;

    @Override
    public QueryCondition clone() {
        try {
            return (QueryCondition) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }
}
