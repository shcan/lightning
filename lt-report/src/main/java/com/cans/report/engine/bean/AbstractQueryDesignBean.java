package com.cans.report.engine.bean;

import com.cans.report.engine.bean.configbean.*;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author qpy
 * @date 2024-07-17 11:03
 * @description 抽象查询bean
 */
@Getter
@Setter
public abstract class AbstractQueryDesignBean extends AbstractDesignBean {
    private static final long serialVersionUID = 1L;

    /**
     * 展示到前端的字段 select xx
     */
    private Map<String, ShowField> showFieldMap = Maps.newLinkedHashMap();

    /**
     * 连接表条件 from a join b on a.xx = b.xx
     */
    private List<JoinTable> joinTables = new ArrayList<>();

    /**
     * 报表绑定表单formmain_00xx的配置
     */
    private Map<String, Table> tables = Maps.newLinkedHashMap();

    /**
     * 排序字段
     */
    private List<OrderByField> orderByFields = Lists.newArrayList();
    
    public AbstractQueryDesignBean() {}
    
    protected AbstractQueryDesignBean(Builder builder) {
        super(builder);
        this.showFieldMap = builder.showFieldMap;
        this.joinTables = builder.joinTables;
        this.tables = builder.tables;
        this.orderByFields = builder.orderByFields;
    }

    @Override
    public Field getField(String tableUid, String dbFieldName) {
        Table table = tables.get(tableUid);
        if (table != null) {
            return table.getFieldMap().get(dbFieldName);
        }
        return null;
    }

    public static class Builder extends AbstractDesignBean.Builder {
        
        Map<String, Table> tables = Maps.newLinkedHashMap();
        Map<String, ShowField> showFieldMap = Maps.newLinkedHashMap();
        List<JoinTable> joinTables = new ArrayList<>();
        List<OrderByField> orderByFields = Lists.newArrayList();
        
        public Builder addShowField(String uidTableName, String dbFieldName, String frontShowName) {
            ShowField showField = new ShowField().setUidTableName(uidTableName).setDbFieldName(dbFieldName).setShowName(frontShowName);
            showFieldMap.put(uidTableName + "." + dbFieldName, showField);
            return this;
        }
        
        public Builder addTable(Table table) {
            tables.put(table.getUidName(), table);
            return this;
        }
        
        public Builder addJoinTable(JoinTable joinTable) {
            joinTables.add(joinTable);
            return this;
        }
        public Builder addOrderByFields(OrderByField orderByField) {
            orderByFields.add(orderByField);
            return this;
        }
    }
}
