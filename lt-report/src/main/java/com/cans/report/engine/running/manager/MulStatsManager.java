package com.cans.report.engine.running.manager;

import com.cans.lightning.base.dao.jdbc.PageInfo;
import com.cans.report.engine.bean.MulStatsDesignBean;
import com.cans.report.engine.bean.conditionbean.QueryParam;

/**
 * @author qpy
 * @date 2024-08-02 11:29
 * @description 多表统计抽象接口
 */
public interface MulStatsManager {

    /**
     * 多表统计数据查询
     * @param mulStatsDesignBean mulStatsDesignBean
     * @param queryParam queryParam
     * @return PageInfo
     */
    PageInfo mulStats(MulStatsDesignBean mulStatsDesignBean, QueryParam queryParam);
}
