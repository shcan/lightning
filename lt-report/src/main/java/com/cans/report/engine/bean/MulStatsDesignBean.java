package com.cans.report.engine.bean;

import com.cans.report.engine.bean.configbean.Field;
import com.cans.report.engine.bean.configbean.GroupByField;
import com.cans.report.engine.bean.configbean.IField;
import com.cans.report.engine.bean.configbean.StatsField;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author qpy
 * @date 2024-08-01 16:23
 * @description 多表统计报表设计
 *      
 *      多表统计相对特殊:
 *          1. 在bean结构设计上, 多表统计设计中, 每个表设计都是一个 StatsDesignBean 对象,
 *              在具体sql查询中也是遍历每个 StatsDesignBean 对象单独执行获取结果
 *          2. 在查询每个 StatsDesignBean sql数据后, 需要将数据进行合并
 *          3. 在合并处理中, 需要本 MulStatsDesignBean 对象存储一个自己的获取字段名称、类型的入口 
 *              默认取 tables.get(0)的字段和table作为映射, 
 *              所以在查询完数据以后, List<Map<String, Object>>数据中的key统一就是 table.get(0)的 tableUid.fieldName
 * 
 */
@Setter
@Getter
@NoArgsConstructor
public class MulStatsDesignBean extends AbstractStatsDesignBean implements Serializable {
    
    private static final long serialVersionUID = 1L;

    /**
     * 多表统计版本中的每个配置项, 都存储一个 StatsDesignBean 对象
     */
    private List<StatsDesignBean> stats = Lists.newArrayList();

    /**
     * 获取表头字段的索引
     * @param tableUid 表的uid
     * @param fieldName fieldName 
     * @return int 表头索引
     */
    public int getGroupByFieldIndex(String tableUid, String fieldName) {
        // 从当前的行表头获取索引
        for (int i = 0; i < this.getGroupByFields().size(); i++) {
            GroupByField groupByField = this.getGroupByFields().get(i);
            if (groupByField.getTableUidName().equals(tableUid) && groupByField.getDbName().equals(fieldName)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 获取统计项字段的索引
     * @param tableUid 表的uid
     * @param fieldName fieldName 
     * @return int 表头索引
     */
    public int getStatsFieldIndex(String tableUid, String fieldName) {
        // 从当前的行表头获取索引
        for (int i = 0; i < this.getStatsFields().size(); i++) {
            StatsField statsField = this.getStatsFields().get(i);
            if (statsField.getTableUidName().equals(tableUid) && statsField.getDbName().equals(fieldName)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 构建统计项和多表统计项的映射关系
     * @param stats
     * @return
     */
    public Map<IField, IField> buildStats2MulStatsFieldReflection(StatsDesignBean stats) {
        Map<IField, IField> result = Maps.newHashMap();
        for (int i = 0; i < stats.getGroupByFields().size(); i++) {
            // 不需要重写 equals 和 hashCode 因为用对象1 == 对象2 直接比较
            result.put(stats.getGroupByFields().get(i), this.getGroupByFields().get(i));
        }
        // 统计项字段 需要同时都存在
        Map<String, StatsField> mulStatsMap = this.getStatsFields().stream().collect(Collectors.toMap(Field::key, a -> a, (a, b) -> a));
        for (int i = 0; i < stats.getStatsFields().size(); i++) {
            if (mulStatsMap.containsKey(stats.getStatsFields().get(i).key())) {
                result.put(stats.getStatsFields().get(i), mulStatsMap.get(stats.getStatsFields().get(i).key()));
            }
        }
        return result;
    }
    
    protected MulStatsDesignBean(Builder builder) {
        super(builder);
        this.stats = builder.stats;
    }
    
    public static Builder builder() {
        return new Builder();
    }


    public static class Builder extends AbstractStatsDesignBean.Builder {
        private final List<StatsDesignBean> stats = Lists.newArrayList();
        
        public Builder addStats(StatsDesignBean statsDesignBean) {
            this.stats.add(statsDesignBean);
            return this;
        }
        
        public MulStatsDesignBean build() {
            return new MulStatsDesignBean(this);
        }
    }
    
}
