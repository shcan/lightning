package com.cans.report.engine.bean;

import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author qpy
 * @date 2024-07-31 13:42
 * @description
 */
@NoArgsConstructor
public class StatsDesignBean extends AbstractStatsDesignBean implements Serializable {
    private static final long serialVersionUID = 1L;

    protected StatsDesignBean(Builder builder) {
        super(builder);
    }

    @Override
    public StatsDesignBean clone()  {
        try {
            return (StatsDesignBean) super.clone();
        } catch (CloneNotSupportedException e) {
            // 不必要抛出去，因为继承了AbstractStatsDesignBean, 该类实现了Cloneable接口，应该支持克隆
            throw new RuntimeException(e);
        }
    }

    public static class Builder extends AbstractStatsDesignBean.Builder {
        public StatsDesignBean build() {
            return new StatsDesignBean(this);
        }
    }
    
    public static Builder builder() {
        return new Builder();
    }
}
