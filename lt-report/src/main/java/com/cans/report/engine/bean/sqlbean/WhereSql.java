package com.cans.report.engine.bean.sqlbean;

import com.cans.report.engine.exceptions.SingleWhereSqlException;
import com.google.common.collect.Maps;

import java.io.Serializable;
import java.util.Map;

import static com.cans.report.engine.utils.SqlConditionUtils.emptyStr;
import static com.cans.report.engine.utils.SqlConditionUtils.oneSpace;
import static com.cans.report.engine.utils.SqlConditionValidateUtils.validateEmptyWhereSqlAndClearIfNeed;

/**
 * @author qpy
 * @date 2024-07-19 13:29
 * @description 拼接where的sql
 */
public class WhereSql implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 判断是否以and/or结尾
     */
    private final static String endJudgeRegex = ".*(and|or|AND|OR){1}\\s*$";

    /**
     * 将结尾清空的正则
     */
    private final static String endReplaceRegex = "\\s*(and|or|AND|OR){1}(\\s*)$";

    private final static String startJudgeRegex = "^\\s*(and|or|AND|OR){1}.*";

    private final static String startReplaceRegex = "^\\s*(and|or|AND|OR){1}\\s*";

    /**
     * sql
     */
    private StringBuilder sql = new StringBuilder();

    /**
     * 参数
     */
    private Map<String, Object> params = Maps.newLinkedHashMap();

    /**
     * 临时sql
     * 存在多WhereSql时, 需要临时存储
     */
    private StringBuilder tempSql = new StringBuilder();

    /**
     * 临时参数
     */
    private Map<String, Object> tempParams = Maps.newLinkedHashMap();

    public WhereSql addTempSql(String sql) {
        this.tempSql.append(sql);
        return this;
    }

    public WhereSql addTempParam(String key, Object value) {
        this.tempParams.put(key, value);
        return this;
    }

    public WhereSql addTempParam(Map<String, Object> params) {
        this.tempParams.putAll(params);
        return this;
    }

    public Map<String, Object> getParams() {
        return this.params;
    }
    
    public String getTempSql() {
        return this.tempSql.toString();
    }
    
    public boolean shouldWhereSql() {
        return this.sql.toString().trim().length() > 0;
    }

    public void clearTemp() {
        this.tempSql.delete(0, this.tempSql.length());
        this.tempParams.clear();
    }

    public String sql() {
        return this.sql.toString();
    }

    public Map<String, Object> params() {
        return this.params;
    }

    /**
     * 提交temp的sql到sql中
     */
    public void commitTemp(boolean clearTemp) {

        // 需要验证下是否有空sql
        if (validateEmptyWhereSqlAndClearIfNeed(this, true, false)) {
            return;
        }

        String stringSql = this.sql.toString();
        String tempStringSql = this.tempSql.toString();
        if (stringSql.matches(startJudgeRegex)) {
            // sql = " and xxx" -> sql = "xxx"
            stringSql = stringSql.replaceFirst(startReplaceRegex, "");
        }
        if (tempStringSql.matches(startJudgeRegex)) {
            // tempSql = " and xxx" -> tempSql = "xxx"
            tempStringSql = tempStringSql.replaceFirst(startReplaceRegex, "");
        }
        if (stringSql.matches(endJudgeRegex)) {
            // sql = "xxx and " -> sql = "xxx"
            stringSql = stringSql.replaceFirst(endReplaceRegex, "");
        }
        if (tempStringSql.matches(endJudgeRegex)) {
            // temp = "xxx and " -> sql = "xxx"
            tempStringSql = tempStringSql.replaceFirst(endReplaceRegex, "");
        }
        this.sql.delete(0, this.sql.length());
        this.sql.append(stringSql).append(stringSql.length() > 0 ? " AND " : " ").append(tempStringSql);
        this.params.putAll(this.tempParams);
        if (clearTemp) {
            this.clearTemp();
        }
    }

}
