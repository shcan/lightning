package com.cans.report.engine.exceptions;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Setter
@Getter
@Accessors(chain = true)
public class CommonSqlException extends RuntimeException {

    private boolean needThrow;

    public CommonSqlException() {
    }

    public CommonSqlException(String message) {
        super(message);
    }

    public CommonSqlException(String message, Throwable cause) {
        super(message, cause);
    }
}
