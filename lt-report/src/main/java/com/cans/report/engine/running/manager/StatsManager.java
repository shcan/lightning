package com.cans.report.engine.running.manager;

import com.cans.lightning.base.dao.jdbc.PageInfo;
import com.cans.report.engine.bean.StatsDesignBean;
import com.cans.report.engine.bean.conditionbean.QueryParam;
import com.cans.report.engine.bean.sqlbean.SqlCriterion;

/**
 * @author qpy
 * @date 2024-05-20 10:34
 * @descriptionv 单表统计实现
 */
public interface StatsManager {

    /**
     * 统计数据查询
     * @param statsDesignBean statsDesignBean
     * @param queryParam queryParam
     * @return PageInfo
     */
    PageInfo stats(StatsDesignBean statsDesignBean, QueryParam queryParam);

    /**
     * 构建查询条件
     * @param statsDesignBean statsDesignBean
     * @param queryParam queryParam
     * @return SqlCriterion
     */
    SqlCriterion buildCriterion(StatsDesignBean statsDesignBean, QueryParam queryParam);


    /**
     * 根据sqlCriterion查询
     * 传入的 sqlCriterion 是一个已经构建好的 select、from、where、order by、group by 等条件的值
     * @param statsDesignBean statsDesignBean
     * @param sqlCriterion sqlCriterion
     * @param queryParam queryParam
     *        (注意:如果sqlCriterion=null则用queryParam来新建SqlCriterion对象, 如果sqlCriterion!=null,只会用到QueryParam的分页参数)
     * @return PageInfo
     */
    PageInfo statsBySqlCriterion(StatsDesignBean statsDesignBean, SqlCriterion sqlCriterion, QueryParam queryParam);
}
