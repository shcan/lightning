package com.cans.report.engine.constants;

/**
 * @author qpy
 * @date 2024-07-17 19:50
 * @description 报表常量
 */
public class ReportConstants {

    /** 显示年月日 */
    public static final String FORMAT_YMD = "yyyy-mm-dd";

    /** 显示年月日 时分 */
    public static final String FORMAT_YMD_HM = "yyyy-mm-dd HH:mm";

    /** 显示年月日 时分秒 */
    public static final String FORMAT_YMD_HMS = "yyyy-mm-dd HH:mm:ss";
    

    
    
    
    /* ================================字段类型相关==================================== */
    /**
     * 数据库字段类型
     */
    public enum FieldDbType {
        //
        VARCHAR("VARCHAR", "文本"), 
        //
        DECIMAL("DECIMAL", "数字"),

        // 3字节 从1000-01-01到9999-12-31
        DATE("DATE", "日期"),
        
        // timestamp和datetime的区别 
        // 1. 4字节 timestamp: '1970-01-01 00:00:01.000000' UTC到'2038-01-19 03:14:07.999999' UTC
        // 2. 8字节 datetime: '1000-01-01 00:00:00.000000'到'9999-12-31 23:59:59.999999'
        TIMESTAMP("TIMESTAMP", "日期时间"),
        //
        DATETIME("DATETIME", "日期时间"),
        //
        LONGTEXT("LONGTEXT", "大文本"),
        //
        LONG("LONG",""), 
        //
        INTEGER("INTEGER","");
        
        private final String key;
        private final String text;

        FieldDbType(String key, String text) {
            this.key = key;
            this.text = text;
        }

        public String getKey() {
            return key;
        }

        public String getText() {
            return text;
        }
        
        public static FieldDbType getFieldDbType(String key) {
            for (FieldDbType fieldDbType : FieldDbType.values()) {
                if (fieldDbType.getKey().equalsIgnoreCase(key)) {
                    return fieldDbType;
                }
            }
            return null;
        }
        
    }

    /**
     * 字段控件类型
     */
    public enum FieldType {
        /** 基本控件 **/
        //文本框
        TEXT("TEXT", "text"),
        // 数字控件
        NUMBER("NUMBER", "number"),
        // 文本域
        TEXTAREA("TEXTAREA", "textarea"),
        //复选框
        CHECKBOX("CHECKBOX", "checkbox"),
        //单选框
        RADIO("RADIO", "radio"),
        //下拉框
        SELECT("SELECT", "select"),
        //日期控件
        DATE("DATE", "date"),
        //日期时间控件
        DATETIME("DATETIME", "datetime");

        private final String key;
        private final String name;

        FieldType(String key, String name) {
            this.key = key;
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public String getKey() {
            return key;
        }
        
        public static FieldType getFieldType(String key) {
            for (FieldType fieldType : FieldType.values()) {
                if (fieldType.getKey().equalsIgnoreCase(key)) {
                    return fieldType;
                }
            }
            return null;
        }
    }

    /* ================================业务相关==================================== */

    /**
     * 报表类型
     */
    public enum ReportType {
        /**
         * 静态报表
         */
        QUERY("QUERY", "查询报表"),
        /**
         * 静态报表
         */
        STATISTICS("STATISTICS", "单表统计"),
        /**
         * 静态报表
         */
        MULTI_STATISTICS("MULTI_STATISTICS", "多表统计");
        
        private final String key;
        private final String text;

        ReportType(String key, String text) {
            this.key = key;
            this.text = text;
        }

        public String getKey() {
            return key;
        }

        public String getText() {
            return text;
        }
        
        public static ReportType getReportType(String key) {
            for (ReportType reportType : ReportType.values()) {
                if (reportType.getKey().equalsIgnoreCase(key)) {
                    return reportType;
                }
            }
            return null;
        }
    }



    /* ================================数据库SQL相关==================================== */
    
    /**
     * 表连接类型
     */
    public enum JoinType {
        /**
         * 内连接
         */
        INNER("inner join", "内连接"),
        /**
         * 外连接
         */
        LEFT("left join", "左外连接"),
        /**
         * 外连接
         */
        RIGHT("right join", "右外连接"),
        /**
         * 外连接
         */
        FULL("full join", "全外连接");
        
        final String sql;
        final String mark;

        JoinType(String sql, String mark) {
            this.sql = sql;
            this.mark = mark;
        }
        public String getSql() {
            return sql;
        }
        public String getMark() {
            return mark;
        }
    }

    /**
     * sql操作符
     */
    public enum SqlOperation {
        /**
         * 相等匹配
         */
        EQUAL("=", "等于"),
        /**
         * 不相等
         */
        NOT_EQUAL("<>", "不等于"),
        /**
         * >
         */
        GREATER_THAN(">", "大于"),
        /**
         * >=
         */
        GREATER_THAN_OR_EQUAL(">=", "大于等于"),
        /**
         * <
         */
        LESS_THAN("<", "小于"),
        /**
         * <=
         */
        LESS_THAN_OR_EQUAL("<=", "小于等于"),
        /**
         * like
         */
        LIKE("like", "模糊"),
        /**
         * not like
         */
        NOT_LIKE("not like", "非模糊查询"),
        /**
         * in
         */
        IN("in", "包含查询"),
        ;
        final String sql;
        final String mark;

        SqlOperation(String sql, String mark) {
            this.sql = sql;
            this.mark = mark;
        }
        public String getSql() {
            return sql;
        }
        public String getMark() {
            return mark;
        }

        /**
         * 通过名称获取
         */
        public static SqlOperation getSqlOperation(String operation) {
            for (SqlOperation sqlOperation : SqlOperation.values()) {
                if (sqlOperation.name().equalsIgnoreCase(operation)) {
                    return sqlOperation;
                }
            }
            return null;
        }

        /**
         * 通过sql获取
         */
        public static SqlOperation getSqlOperationBySql(String sql) {
            for (SqlOperation sqlOperation : SqlOperation.values()) {
                if (sqlOperation.getSql().equalsIgnoreCase(sql)) {
                    return sqlOperation;
                }
            }
            return null;
        }
    }
    
    /**
     * sql统计类型
     */
    public enum SqlCalcType {
        /**
         * 统计
         */
        COUNT("count", "统计"),
        /**
         * 求和
         */
        SUM("sum", "求和"),
        /**
         * 平均
         */
        AVG("avg", "平均"),
        /**
         * 最大
         */
        MAX("max", "最大"),
        /**
         * 最小
         */
        MIN("min", "最小");
        
        final String sql;
        final String mark;

        SqlCalcType(String sql, String mark) {
            this.sql = sql;
            this.mark = mark;
        }
        public String getSql() {
            return sql;
        }
        public String getMark() {
            return mark;
        }
        
        public static SqlCalcType getSqlCalcType(String key) {
            for (SqlCalcType sqlCalcType : SqlCalcType.values()) {
                if (sqlCalcType.getSql().equalsIgnoreCase(key)) {
                    return sqlCalcType;
                }
            }
            return null;
        }
    }

    /**
     * 排序方式
     */
    public enum OrderBy {
        // 不排序
        NONE("none", "无"),
        // 升序
        ASC("asc", "升序"),
        // 降序
        DESC("desc", "降序");
        
        final String sql;
        final String mark;

        OrderBy(String sql, String mark) {
            this.sql = sql;
            this.mark = mark;
        }
        public String getSql() {
            return sql;
        }
        public String getMark() {
            return mark;
        }
        
        public static OrderBy getOrderBy(String key) {
            for (OrderBy orderBy : OrderBy.values()) {
                if (orderBy.getSql().equalsIgnoreCase(key)) {
                    return orderBy;
                }
            }
            return NONE;
        }
    }
    
}
