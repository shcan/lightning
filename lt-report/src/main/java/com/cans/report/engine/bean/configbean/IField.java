package com.cans.report.engine.bean.configbean;

import java.io.Serializable;

/**
 * @author qpy
 * @date 2024-07-31 17:49
 * @description
 */
public interface IField extends Serializable {

   long serialVersionUID = 1L;

    /**
     * 字段的sql
     */
    String sql();

    /**
     * tableUid + "." + fieldName
     */
    String key();
}
