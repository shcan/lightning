package com.cans.report.engine.po;

import com.cans.lightning.base.dto.BaseDto;

/**
 * @author qpy
 * @date 2024-07-16 10:04
 * @description 静态报表 对应数据库 static_report 表, 定时任务调用一次, 然后生成一次
 */
public class StaticReport extends BaseDto<Long> {
    
    
}
