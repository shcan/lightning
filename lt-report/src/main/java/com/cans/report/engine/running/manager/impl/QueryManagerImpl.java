package com.cans.report.engine.running.manager.impl;

import com.cans.lightning.base.dao.jdbc.PageInfo;
import com.cans.report.engine.bean.QueryDesignBean;
import com.cans.report.engine.bean.conditionbean.QueryParam;
import com.cans.report.engine.bean.sqlbean.SqlCriterion;
import com.cans.report.engine.running.dao.impl.ReportSqlExecutorImpl;
import com.cans.report.engine.running.manager.QueryManager;
import com.cans.report.engine.utils.SqlConditionUtils;
import com.cans.report.engine.utils.SqlCriterionUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * @author qpy
 * @date 2024-07-19 11:13
 * @description 查询报表实现
 */
@Slf4j
public class QueryManagerImpl implements QueryManager {
    
    @Override
    public PageInfo query(QueryDesignBean queryDesignBean, QueryParam queryParam) {

        // 1. 拿取条件
        SqlCriterion sqlCriterion = buildCriterion(queryDesignBean, queryParam);

        // 2. 执行查询
        PageInfo pageInfo = new PageInfo().setPage(queryParam.getPageNum()).setPageSize(queryParam.getPageSize());
        return new ReportSqlExecutorImpl().execute(sqlCriterion, pageInfo);
    }
    
    
    
    private SqlCriterion buildCriterion(QueryDesignBean queryDesignBean, QueryParam queryParam) {
        SqlCriterion sqlCriterion = SqlCriterionUtils.buildCriterion(queryDesignBean, queryParam);
        
        // 构建条件
        SqlConditionUtils.buildWhereBeforeQuery(sqlCriterion, queryParam.getQueryConditions(), queryDesignBean);
        
        return sqlCriterion;
    }
}
