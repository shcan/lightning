package com.cans.report.engine.bean.sqlbean;

import com.cans.report.engine.bean.configbean.JoinTable;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Getter;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author qpy
 * @date 2024-07-19 13:24
 * @description SQL查询封装的对象, 查询前统一封装此对象 
 */
@Getter
public class SqlCriterion implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * select字段(key={tableUid.fieldName}, value={tableUid.fieldName})
     */
    private final Map<String, String> selects = Maps.newLinkedHashMap();

    /**
     * from表条件 
     */
    private final List<JoinTable> joinTables = Lists.newArrayList();

    /**
     * WhereSql条件
     */
    private WhereSql whereSql = new WhereSql();

    /**
     * group by 
     */
    private final Map<String, String> groupBy = Maps.newLinkedHashMap();

    /**
     * order by 
     */
    private final Map<String, String> orderBy = Maps.newLinkedHashMap();
    
    public SqlCriterion addSelect(String key, String value) {
        selects.put(key, value);
        return this;
    }
    
    public SqlCriterion addJoinTable(JoinTable joinTable) {
        joinTables.add(joinTable);
        return this;
    }
    
    public SqlCriterion addGroupBy(String key, String value) {
        groupBy.put(key, value);
        return this;
    }
    
    public SqlCriterion addOrderBy(String key, String value) {
        orderBy.put(key, value);
        return this;
    }
    
    public SqlCriterion setWhereSql(WhereSql whereSql) {
        this.whereSql = whereSql;
        return this;
    }
}
