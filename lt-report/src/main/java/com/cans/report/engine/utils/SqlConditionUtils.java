package com.cans.report.engine.utils;

import com.cans.lightning.base.exceptions.BusinessException;
import com.cans.lightning.utils.SqlWildcardUtil;
import com.cans.report.engine.bean.AbstractDesignBean;
import com.cans.report.engine.bean.conditionbean.QueryCondition;
import com.cans.report.engine.bean.conditionbean.QueryConditions;
import com.cans.report.engine.bean.configbean.Field;
import com.cans.report.engine.bean.sqlbean.SqlCriterion;
import com.cans.report.engine.bean.sqlbean.WhereSql;
import com.cans.report.engine.exceptions.CommonSqlException;
import com.cans.report.engine.exceptions.SingleWhereSqlException;
import com.cans.report.engine.exceptions.ValidateException;
import com.cans.report.engine.running.dao.ReportSqlExecutor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;

import java.util.*;
import java.util.function.BiPredicate;
import java.util.regex.Pattern;

import static com.cans.report.engine.constants.ReportConstants.*;
import static com.cans.report.engine.constants.ReportConstants.FieldType.*;
import static com.cans.report.engine.constants.ReportConstants.SqlOperation.*;
import static com.cans.report.engine.utils.SqlConditionValueUtils.*;
import static com.cans.report.engine.utils.SqlConditionSelectUtils.buildSingleWhereSqlSelect;
import static com.cans.report.engine.utils.SqlConditionValidateUtils.*;

/**
 * @author qpy
 * @date 2024-07-25 16:05
 * @description sql条件工具类 所有sql拼接的条件都在这里
 *      功能点:
 *          1. 前端传入条件封装
 *          2. 不同控件 封装成 不同sql命名参数的解析以及加入泛型参数 
 */
@Slf4j
public class SqlConditionUtils {


    /**
     * 在执行真正sql查询之前, 将前端的where封装成List<Condition>, 将whereSql拼装出来
     * @param sqlCriterion sqlCriterion
     * @param queryConditions 查询条件, 包含前端、默认条件、内部参数条件
     * @param designBean designBean
     */
    public static void buildWhereBeforeQuery(SqlCriterion sqlCriterion,
                                                QueryConditions queryConditions,
                                                AbstractDesignBean designBean) {
        if (queryConditions == null || ObjectUtils.isEmpty(queryConditions.getQueryConditions())) {
            return;
        }
        WhereSql newWhereSql = new WhereSql();
        for (int i = 0; i < queryConditions.getQueryConditions().size(); i++) {
            buildSingleWhereSql(i, newWhereSql, queryConditions.getQueryConditions().get(i), designBean);
        }
        if (newWhereSql.shouldWhereSql()) {
            sqlCriterion.getWhereSql().addTempSql(" ( ").addTempSql(newWhereSql.sql()).addTempSql(" ) ");
            if (newWhereSql.getParams() != null) {
                sqlCriterion.getWhereSql().addTempParam(newWhereSql.getParams());
            }
        }
        // 提交temp数据
        sqlCriterion.getWhereSql().commitTemp(true);
    }

    /**
     * 真正执行sql的时候调用, 将成品的sql返回
     * /@see ReportSqlExecutorImpl#doSqlWhere
     */
    public static void buildWhereWhenQuery(ReportSqlExecutor.SqlExecutorWrapper wrapper, SqlCriterion sqlCriterion) {
        if (sqlCriterion.getWhereSql().shouldWhereSql()) {
            wrapper.getSql().append(" WHERE ").append(sqlCriterion.getWhereSql().sql());
        }
    }
    /* =================================静态变量以及私有内部判断类================================= */

    public static final String leftBracket = "(";
    public static final String leftBracketReg = "\\(";
    public static final String rightBracket = ")";
    public static final String rightBracketReg = "\\)";
    public static final String operatorAnd = "and";
    public static final String operatorOr = "or";
    public static final String nullValueStr = "null";
    public static final String oneSpace = " ";
    public static final String emptyStr = "";
    public static final String percentStr = "%";
    // 冒号
    public static final String colon = ":";
    public static final String isNullStr = "is null";
    public static final Pattern dateRegex = Pattern.compile("^\\d{4}-\\d{1,2}-\\d{1,2}$");
    /**
     * 判断是否为空 
     * "null" 字符串作为null判断有争议, 通过 needNullStr 参数控制是否需要判断
     */
    static BiPredicate<Object, Boolean> isNullPredicate = (Object v, Boolean needNullStr) -> 
            v == null || "".equals(v.toString()) || (needNullStr && nullValueStr.equalsIgnoreCase(v.toString().trim()));

    /* =================================私有方法================================= */
    
    
    
    /**
     * 将单个查询条件封装成sql
     * @param index index
     * @param whereSql whereSql
     * @param queryCondition queryCondition
     * @param designBean designBean
     */
    private static void buildSingleWhereSql(int index, WhereSql whereSql, QueryCondition queryCondition, AbstractDesignBean designBean) {

        try {
            // 1. 获取装饰值
            String operator = validateSingleConditionMustField(queryCondition.getOperator());
            String leftBracket = validateSingleConditionField(queryCondition.getLeftBracket());
            String rightBracket = validateSingleConditionField(queryCondition.getRightBracket());
            String andOr = validateSingleConditionField(queryCondition.getAndOr());
            String extraLeftBracket = validateSingleConditionField(queryCondition.getExtraLeftBracket());
            String extraRightBracket = validateSingleConditionField(queryCondition.getExtraRightBracket());
            // 为了减少传参个数, 将整理后的值 重新设置到 queryCondition
            queryCondition.setOperator(operator).setLeftBracket(leftBracket).setRightBracket(rightBracket)
                    .setAndOr(andOr).setExtraLeftBracket(extraLeftBracket).setExtraRightBracket(extraRightBracket);
            
            Field field = designBean.getField(queryCondition.getTableUid(), queryCondition.getFieldName());
            validateNullAndException(field, "field is null or empty 【" + queryCondition.getTableUid() + "." + queryCondition.getFieldName() + "】", true);

            // 2. 添加左右括号
            addStr2WhereSql(whereSql, extraLeftBracket, true);
            addStr2WhereSql(whereSql, leftBracket, true);
            addStr2WhereSql(whereSql, SqlConditionUtils.leftBracket, true);
            
            FieldType fieldType = getFieldType(field.getFieldType());
            // 命名参数的key
            String paramValueKey = String.format("pc_%d_%s_%s", index, field.getTableUidName(), field.getDbName());
            // 3. 不同控件类型调用不同的sql拼接实现
            switch (Objects.requireNonNull(fieldType)) {
                case CHECKBOX:
                case RADIO:
                case SELECT:
                    buildSingleWhereSqlSelect(whereSql, queryCondition, field, paramValueKey);
                    break;
                case DATE:
                case DATETIME:
                    buildSingleWhereSqlDate(whereSql, queryCondition, field, paramValueKey);
                    break;
                case TEXT:
                case NUMBER:
                    buildSingleWhereSqlText(whereSql, queryCondition, field, paramValueKey);
                    break;
                default:
                    // 默认不处理
                    log.warn("buildSingleWhereSql, skip this sql join not throw, default not handle");
                    return;
            }
            
            // 4. 添加右括号
            addStr2WhereSql(whereSql, SqlConditionUtils.rightBracket, true);
            addStr2WhereSql(whereSql, rightBracket, true);
            addStr2WhereSql(whereSql, extraRightBracket, true);
            
            // 5. 如果条件未拼接成, 即 whereSql=" ( ( ) ) " 等字符, 直接清空 continue 
            validateEmptyWhereSqlAndClearIfNeed(whereSql, true, false);
            
            // 后面会做去除的, 随便加上and即可
            addStr2WhereSql(whereSql, SqlConditionUtils.operatorAnd, true);
            whereSql.commitTemp(true);
        }
        catch (SingleWhereSqlException e) {
            // 出现了异常, sql的条件就不会拼接, 导致查询出来了所有数据.  应该加个避免机制
            // 校验异常情况, 如果需要抛出异常, 则抛出异常
            validateCommonSqlException(e);

            whereSql.clearTemp();
            log.warn("buildSingleWhereSql, skip this sql join not throw, SingleWhereSqlException: " + e.getMessage());
        }
        catch (ValidateException e) {
            // 验证出错  
            whereSql.clearTemp();
            log.warn("buildSingleWhereSql, skip this sql join not throw, ValidateException: " + e.getMessage());
        }
        catch (Exception e) {
            // 3. 异常处理
            whereSql.clearTemp();
            log.warn("buildSingleWhereSql, Exception: " + e.getMessage());
            throw new BusinessException("buildSingleWhereSql, Exception: " + e.getMessage(), e);
        }
    }

    /**
     * 构建单个条件的通用方法
     * @param whereSql whereSql
     * @param operatorStr operatorStr
     * @param field field
     * @param paramValueKey 参数map的key
     * @param noNullParam 不为null的参数 (noNullNewParam可能是java.util.List对象)
     * @param needNullSql 拼接null的sql
     */
    static void buildSingleWhereSqlCommon(WhereSql whereSql, String operatorStr, Field field, 
                                          String paramValueKey, Object noNullParam, boolean needNullSql) {
        
        String andOr = "";
        if (!ObjectUtils.isEmpty(noNullParam)) {
            SqlOperation operator = Objects.requireNonNull(SqlOperation.getSqlOperationBySql(operatorStr));
            whereSql.addTempSql(field.whereSql()).addTempSql(oneSpace)
                    .addTempSql(operator.getSql()).addTempSql(oneSpace);
            // 不同类型做不同操作
            if (operator == SqlOperation.IN) {
                whereSql.addTempSql(leftBracket).addTempSql(colon).addTempSql(paramValueKey).addTempSql(rightBracket);
            }
            else {
                noNullParam = noNullParam instanceof List ? ((List)noNullParam).get(0) : noNullParam;
                if (operator == SqlOperation.LIKE || operator == SqlOperation.NOT_LIKE) {
                    // like '%value%', 这个%必须和命名参数放到一起,
                    // 如果不放到一起, 拼接的sql就是: like %'value'%
                    noNullParam = percentStr + SqlWildcardUtil.escape(noNullParam.toString()) + percentStr;
                    whereSql.addTempSql(colon).addTempSql(paramValueKey);
                }
                else {
                    whereSql.addTempSql(colon).addTempSql(paramValueKey);
                }
            }
            whereSql.addTempParam(paramValueKey, noNullParam);
            andOr = oneSpace + operatorOr + oneSpace;
        }
        
        // 添加空值  应该抽象一个方法来处理
        if (needNullSql) {
            whereSql.addTempSql(andOr);
            buildSingleWhereSqlNull(whereSql, field);
        }
    }

    /**
     * 日期、日期时间构建
     * @param whereSql whereSql
     * @param queryCondition queryCondition
     * @param field field
     * @param paramValueKey paramValueKey
     */
    static void buildSingleWhereSqlDate(WhereSql whereSql, QueryCondition queryCondition, Field field, String paramValueKey) {
        // 日期比较特殊, 支持两种格式
        // 格式1. ReportConstants.SqlOperation.EQUAL, ReportConstants.SqlOperation.NOT_EQUAL
        // 这两个格式需要做特殊处理, 都需要添加两个参数, 前一个为开始时间, 后一个为结束时间
        
        // 格式2. > >= < <= GREATER_THAN, GREATER_THAN_OR_EQUAL, LESS_THAN, LESS_THAN_OR_EQUAL
        FieldDbType fieldDbType = FieldDbType.getFieldDbType(field.getDbType());

        assert fieldDbType != null;
        Date date = (Date) parseParamValue2DbType(queryCondition.getParam(), fieldDbType);
        if (date == null) {
            buildSingleWhereSqlNull(whereSql, field);
            return;
        }
        
        // 2. 格式必须是日志或者日期时间
        // yyyy-mm-dd, yyyy-mm-dd yyyy-mm-dd HH-mm-ss 
        // 标记: 这里如果 queryCondition.getParam()是 java.util.Date类型, 需要做额外操作, 目前暂时这样吧
        String dateStr = queryCondition.getParam().toString();
        Date startDate = date;
        Date endDate = date;
        if (dateRegex.matcher(dateStr).find()) {
            startDate = (Date) parseParamValue2DbType(dateStr + " 00:00:00", FieldDbType.DATETIME);
            endDate = (Date) parseParamValue2DbType(dateStr + " 23:59:59", FieldDbType.DATETIME);
        }

        SqlOperation sqlOperation = Objects.requireNonNull(SqlOperation.getSqlOperationBySql(queryCondition.getOperator()));
        switch (sqlOperation) {
            case EQUAL:
            case NOT_EQUAL:
                // = <>, 需要进行两次sql拼接
                // = -> 【>= dateStr and <= dateStr】
                // <> -> 【< dateStr or > dateStr】
                boolean equal = sqlOperation == SqlOperation.EQUAL;
                String joinAndOr = oneSpace + (equal ? operatorAnd : operatorOr) + oneSpace;
                String leftOperation = equal ? GREATER_THAN_OR_EQUAL.getSql() : LESS_THAN.getSql();
                String rightOperation = equal ? LESS_THAN_OR_EQUAL.getSql() : GREATER_THAN.getSql();
                // 构建左边
                buildSingleWhereSqlCommon(whereSql, leftOperation, field, "s" + paramValueKey, startDate, false);
                addStr2WhereSql(whereSql, joinAndOr, false);
                // 构建右边
                buildSingleWhereSqlCommon(whereSql, rightOperation, field, "e" + paramValueKey, endDate, false);
                break;
            case GREATER_THAN_OR_EQUAL:
            case LESS_THAN:
                // >= < 等操作 从 startDate 开始 
                buildSingleWhereSqlCommon(whereSql, queryCondition.getOperator(), field, paramValueKey, startDate, false);
                break;
            case GREATER_THAN:
            case LESS_THAN_OR_EQUAL: 
                // > <= 等操作 从 endDate 开始
                buildSingleWhereSqlCommon(whereSql, queryCondition.getOperator(), field, paramValueKey, endDate, false);
                break;
            default:
                throw new SingleWhereSqlException("type 【" + fieldDbType.getKey() + "】only support 【<>, =, >, >=, <, <=】 , is not support operator 【" + queryCondition.getOperator() + "】");
        }
    }

    static void buildSingleWhereSqlText(WhereSql whereSql, QueryCondition queryCondition, Field field, String paramValueKey) {
        
        // text 暂时不做 \s 分隔  多次查询的情况
        List<Object> params = parseParamValue2List(queryCondition.getParam(), null, null);
        // 标记: 简单构建即可, 后续用到再做优化
        buildSingleWhereSqlCommon(whereSql, queryCondition.getOperator(), field, paramValueKey, params, params.isEmpty());
    }

    /**
     * 构建空值是sql
     * @param whereSql whereSql
     * @param field field
     */
    static void buildSingleWhereSqlNull(WhereSql whereSql, Field field) {
        whereSql.addTempSql(field.whereSql()).addTempSql(oneSpace).addTempSql(isNullStr);
    }

    /**
     * 添加字符串到whereSql都调用此方法
     * @param whereSql whereSql
     * @param str str
     */
    static void addStr2WhereSql(WhereSql whereSql, String str, boolean needSpace) {
        if (!StringUtils.isEmpty(str)) {
            whereSql.addTempSql(str).addTempSql(needSpace ? oneSpace : emptyStr);
        }
    }

    static void validateCommonSqlException(CommonSqlException e) {
        if (e.isNeedThrow()) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
}
