package com.cans.report.engine.bean.conditionbean;

import com.google.common.collect.Lists;
import lombok.*;

import java.util.List;

/**
 * @author qpy
 * @date 2024-07-19 10:11
 * @description 查询的条件
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class QueryConditions {

    /**
     * 传入的查询条件
     */
    private List<QueryCondition> queryConditions = Lists.newArrayList();
    
    
    public QueryConditions addQueryCondition(QueryCondition queryCondition) {
        this.queryConditions.add(queryCondition);
        return this;
    }
}
