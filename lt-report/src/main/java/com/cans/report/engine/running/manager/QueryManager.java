package com.cans.report.engine.running.manager;

import com.cans.lightning.base.dao.jdbc.PageInfo;
import com.cans.report.engine.bean.QueryDesignBean;
import com.cans.report.engine.bean.conditionbean.QueryParam;

/**
 * @author qpy
 * @date 2024-07-19 9:41
 * @description
 */
public interface QueryManager {

    /**
     * 报表查询具体实现的人口
     * @param queryDesignBean
     * @param queryParam
     * @return
     */
    PageInfo query(QueryDesignBean queryDesignBean, QueryParam queryParam);
}
