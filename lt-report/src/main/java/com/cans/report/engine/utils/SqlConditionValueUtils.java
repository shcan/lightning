package com.cans.report.engine.utils;

import com.cans.report.engine.constants.ReportConstants;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateUtils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author qpy
 * @date 2024-07-29 17:08
 * @description 对拼接sql时 参数值的处理
 */
@Slf4j
public class SqlConditionValueUtils {

    static final Pattern dateTimeRegexSeconds = Pattern.compile("^\\s*\\d{4}-\\d{1,2}-\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2}\\s*$");
    static final Pattern dateTimeRegexMinutes = Pattern.compile("^\\s*\\d{4}-\\d{1,2}-\\d{1,2} \\d{1,2}:\\d{1,2}\\s*$");
    static final Pattern dateTimeRegexHours = Pattern.compile("^\\s*\\d{4}-\\d{1,2}-\\d{1,2} \\d{1,2}\\s*$");
    static final Pattern dateRegex = Pattern.compile("^\\s*\\d{4}-\\d{1,2}-\\d{1,2}\\s*$");


    /**
     * 将参数值转换成List
     * @param value 值
     * @param result 结果集
     * @param mapKey 如果参数是map类型，则需要指定mapKey
     * @return List
     */
    static List<Object> parseParamValue2List(Object value, List<Object> result, String mapKey) {
        if (result == null) {
            result = new ArrayList<>();
        }
        if (value == null) {
            return result;
        }
        if (value instanceof String) {
            String[] idList = ((String) value).split(",");
            for (String s : idList) {
                addValue2List(result, s.trim());
            }
        }
        else if (value instanceof Number) {
            addValue2List(result, value);
        }
        else if (value instanceof Map) {
            Map<String, Object> map = (Map<String, Object>) value;
            parseParamValue2List(map.get(mapKey), result, mapKey);
        }
        else if (value instanceof List) {
            List<Object> list = (List<Object>) value;
            if (list.isEmpty()) {
                return result;
            }
            for (Object o : list) {
                parseParamValue2List(o, result, mapKey);
            }
        }
        return result;
    }


    /**
     * 将参数值根据字段类型转换成数据库类型
     * @param noNullParamList 参数值
     * @param fieldDbType 数据库字段类型
     * @return List<Object>
     */
    static List<Object> parseParamValue2DbType(List<Object> noNullParamList, ReportConstants.FieldDbType fieldDbType) {
        List<Object> result = new ArrayList<>();
        
        for (Object noNullParam : noNullParamList) {
            Object parseObj = parseParamValue2DbType(noNullParam, fieldDbType);
            if (parseObj != null) {
                result.add(parseObj);
            }
        }
        return result;
    }

    static Object parseParamValue2DbType(Object noNullParam, ReportConstants.FieldDbType fieldDbType) {
        Object value = null;
        try {
            switch (fieldDbType) {
                case DATE:
                    value = DateUtils.parseDate(noNullParam.toString(), ReportConstants.FORMAT_YMD);
                    break;
                case DATETIME:
                case TIMESTAMP:
                    value = noNullParam instanceof Date ? noNullParam : parseDateTime(noNullParam.toString());
                    break;
                case DECIMAL:
                     value = Double.parseDouble(noNullParam.toString());
                    break;
                case INTEGER:
                    value = Integer.valueOf(noNullParam.toString());
                    break;
                case VARCHAR:    
                default:
                    // 默认当字符串来处理
                    value = noNullParam.toString();
                    break;
            }
        }
        catch (Exception e) {
            log.warn("paramValue 【{}】 convert db type fault. will continue this paramValue", noNullParam);
        }
        return value;
    }

    static void addValue2List(List<Object> result, Object value) {
        result.add(value);
    }

    /**
     * 将字符串转换成日期
     * @param dateStr 日期字符串
     * @throws ParseException 解析报错
     */
    static Date parseDateTime(String dateStr) throws ParseException {
        String format = ReportConstants.FORMAT_YMD_HMS;
        if (dateTimeRegexMinutes.matcher(dateStr).matches()) {
            format = ReportConstants.FORMAT_YMD_HM;
        }
        else if (dateTimeRegexHours.matcher(dateStr).matches()) {
            format = ReportConstants.FORMAT_YMD;
        }
        if (dateRegex.matcher(dateStr).matches()) {
            format = ReportConstants.FORMAT_YMD;
        }
        return DateUtils.parseDate(dateStr.trim(), format);
    }

}
