package com.cans.report.engine.utils;

/**
 * 报表实用工具类
 */
public class ReportUtils {

    private final static String TAG_SPLIT = "@@@@@@_";
    private final static String TAG_STR = "######_";


    /**
     * 添加分组字段
     * @param groupKeyPrefix
     * @param singleValue singleValue
     */
    public static void appendGroupByField2Str(StringBuilder groupKeyPrefix, Object singleValue) {
        // 字符串的"null" 和 空对象的null 分开区分
        groupKeyPrefix.append(singleValue).append((singleValue instanceof String ? TAG_STR : TAG_SPLIT));
    }
}
