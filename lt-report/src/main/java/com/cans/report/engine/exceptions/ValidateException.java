package com.cans.report.engine.exceptions;

/**
 * @author qpy
 * @date 2024-07-29 10:24
 * @description 此异常主要是验证 值、 值类型、 值长度、 等异常数据 抛出给 前端
 */
public class ValidateException extends RuntimeException {
}
