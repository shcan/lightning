package com.cans.report.engine.running.dao.impl;

import com.cans.lightning.base.dao.jdbc.JdbcExecutor;
import com.cans.lightning.base.dao.jdbc.PageInfo;
import com.cans.lightning.base.exceptions.BusinessException;
import com.cans.report.engine.bean.configbean.JoinCondition;
import com.cans.report.engine.bean.configbean.JoinTable;
import com.cans.report.engine.bean.sqlbean.SqlCriterion;
import com.cans.report.engine.constants.ReportConstants;
import com.cans.report.engine.running.dao.ReportSqlExecutor;
import com.cans.report.engine.utils.SqlConditionUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;

import java.util.*;

/**
 * @author qpy
 * @date 2024-07-22 19:08
 * @description 执行报表SQL通用入口
 */
@Slf4j
public class ReportSqlExecutorImpl implements ReportSqlExecutor {
    
    @Override
    public PageInfo execute(SqlCriterion sqlCriterion, PageInfo pageInfo) {

        try {
            SqlExecutorWrapper sew = getSqlExecutorWrapper(sqlCriterion);

            String sql = sew.getSql().toString();
            log.info("sql: {}", sql);
            log.info("param: 【{}】 pageInfo:【{},{}】", sqlCriterion.getWhereSql().params(), pageInfo.getPage(), pageInfo.getPageSize());

            new JdbcExecutor().setSqlLog(true).namedPageQuery(sql, sqlCriterion.getWhereSql().params(), pageInfo);

            // 将映射进行反转换
            convertKey2RealKey(sew.getReflectMap(), pageInfo);
        }
        catch (Exception e) {
            log.error("report execute sql error: ", e);
            throw new BusinessException(e.getMessage(), e);
        }
        return pageInfo;
    }


    /**
     * 将映射进行反转换
     * @param reflectMap reflectMap
     * @param pageInfo pageInfo
     */
    private void convertKey2RealKey(Map<String, String> reflectMap, PageInfo pageInfo) {
        if (!ObjectUtils.isEmpty(pageInfo.getData())) {
            List<Map<String, Object>> newData = new ArrayList<>(pageInfo.getData().size());
            for (Map<String, Object> data : pageInfo.getData()) {
                Map<String, Object> newDataItem = new HashMap<>(data.size());
                for (Map.Entry<String, String> entry : reflectMap.entrySet()) {
                    newDataItem.put(entry.getKey(), data.get(entry.getValue()));
                }
                newData.add(newDataItem);
            }
            pageInfo.setData(newData);
        }
    }

    @Override
    public SqlExecutorWrapper getSqlExecutorWrapper(SqlCriterion sqlCriterion) {
        
        SqlExecutorWrapper sew = new SqlExecutorWrapper();
        
        // 1. select
        doSqlSelect(sew, sqlCriterion);
        
        // 2. from
        doSqlFrom(sew, sqlCriterion);
        
        // 3. where
        doSqlWhere(sew, sqlCriterion);
        
        // 4. group by
        doSqlGroupBy(sew, sqlCriterion);
        
        // 5. order by
        doSqlOrderBy(sew, sqlCriterion);
        
        return sew;
    }

    private void doSqlSelect(SqlExecutorWrapper sew, SqlCriterion sqlCriterion) {
        // 1. 将字段做映射, 减少sql的字符串数量
        int f = 0;
        for (Map.Entry<String, String> selectEntry : sqlCriterion.getSelects().entrySet()) {
            sew.getReflectMap().put(selectEntry.getKey(), ReportSqlExecutor.SELECT_FIELD_KEY + f++);
        }
        
        sew.getSql().append("SELECT ");
        for (Map.Entry<String, String> selectEntry : sqlCriterion.getSelects().entrySet()) {
            sew.getSql().append(selectEntry.getValue()).append(" ").append(sew.getReflectMap().get(selectEntry.getKey())).append(",");
        }
        sew.getSql().deleteCharAt(sew.getSql().length() - 1);
        
    }

    private void doSqlFrom(SqlExecutorWrapper sew, SqlCriterion sqlCriterion) {

        List<JoinTable> joinTables = sqlCriterion.getJoinTables();
        /*
            表连接信息, 极端情况下, 出现table之间连接的死循环
                A -> B
                B -> C
                C -> A
         */
        
        // 1. 校验数据合法性
        validateJoinTable(joinTables);

        sew.getSql().append(" FROM ");
        Set<String> existsTableKey = Sets.newHashSet();

        for (JoinTable joinTable : joinTables) {
            if (!existsTableKey.contains(joinTable.getATableUidName())) {
                sew.getSql().append(joinTable.getATableDbName()).append(" ").append(joinTable.getATableUidName());
                existsTableKey.add(joinTable.getATableUidName());
            }
            // join type
            sew.getSql().append(" ").append(ReportConstants.JoinType.valueOf(joinTable.getJoinType().toUpperCase()).getSql()).append(" ");
            sew.getSql().append(joinTable.getBTableDbName()).append(" ").append(joinTable.getBTableUidName());
            // join condition
            sew.getSql().append(" ON ");
            for (int jc = 0; jc < joinTable.getJoinConditions().size(); jc++) {
                JoinCondition joinCondition = joinTable.getJoinConditions().get(jc);
                if (jc > 0) {
                    sew.getSql().append(" AND ");
                }
                sew.getSql()
                        .append(joinTable.getATableUidName()).append(".").append(joinCondition.getAFieldName())
                        .append(" = ")
                        .append(joinTable.getBTableUidName()).append(".").append(joinCondition.getBFieldName());
            }
        }
    }

    private void doSqlWhere(SqlExecutorWrapper sew, SqlCriterion sqlCriterion) {
        SqlConditionUtils.buildWhereWhenQuery(sew, sqlCriterion);
    }

    private void doSqlGroupBy(SqlExecutorWrapper sew, SqlCriterion sqlCriterion) {
        if (!ObjectUtils.isEmpty(sqlCriterion.getGroupBy())) {
            sew.getSql().append(" GROUP BY ");
            for (Map.Entry<String, String> entry : sqlCriterion.getGroupBy().entrySet()) {
                sew.getSql().append(entry.getValue()).append(",");
            }
            sew.getSql().deleteCharAt(sew.getSql().length() - 1);
        }
    }

    private void doSqlOrderBy(SqlExecutorWrapper sew, SqlCriterion sqlCriterion) {
        String orderByStr = " ORDER BY ";
        if (!ObjectUtils.isEmpty(sqlCriterion.getOrderBy())) {
            sew.getSql().append(orderByStr);
            for (Map.Entry<String, String> entry : sqlCriterion.getOrderBy().entrySet()) {
                sew.getSql().append(entry.getValue()).append(",");
            }
            sew.getSql().deleteCharAt(sew.getSql().length() - 1);
        }
    }

    private void validateJoinTable(List<JoinTable> joinTables) {
        List<JoinTable> illegalJoinTable = Lists.newArrayList();
        if (joinTables.size() > 0) {
            for (JoinTable joinTable : joinTables) {
                if (ObjectUtils.isEmpty(joinTable.getJoinType())
                    || ObjectUtils.isEmpty(ReportConstants.JoinType.valueOf(joinTable.getJoinType().toUpperCase()))
                    || ObjectUtils.isEmpty(joinTable.getATableDbName())
                    || ObjectUtils.isEmpty(joinTable.getATableUidName())
                    || ObjectUtils.isEmpty(joinTable.getBTableDbName())
                    || ObjectUtils.isEmpty(joinTable.getBTableUidName()) 
                    // 连接条件          
                    || ObjectUtils.isEmpty(joinTable.getJoinConditions())
                    || ObjectUtils.isEmpty(joinTable.getJoinConditions().get(0).getAFieldName())
                    || ObjectUtils.isEmpty(joinTable.getJoinConditions().get(0).getBFieldName())) {
                    illegalJoinTable.add(joinTable);
                }
            }
            if (illegalJoinTable.size() > 0) {
                String msg = "连接条件不合法, 每个项目值都不能为空: " + illegalJoinTable;
                log.error(msg);
                throw new RuntimeException(msg);
            }
        }
    }
}
