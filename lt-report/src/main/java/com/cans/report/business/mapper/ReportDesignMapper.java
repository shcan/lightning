package com.cans.report.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cans.report.engine.po.ReportDesign;

/**
 * <p>
 * 报表配置 Mapper 接口
 * </p>
 *
 * @author qpy
 * @since 2024-08-07
 */
public interface ReportDesignMapper extends BaseMapper<ReportDesign> {

}
