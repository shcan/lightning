package com.cans.report.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cans.report.engine.po.ReportAuth;

/**
 * <p>
 * 报表授权 Mapper 接口
 * </p>
 *
 * @author qpy
 * @since 2024-08-07
 */
public interface ReportAuthMapper extends BaseMapper<ReportAuth> {

}
