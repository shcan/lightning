package com.cans.lightning.cache;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * @author: qpy
 * @date: 2023-11-30 18:52
 * @description: 使用 google guava本地缓存的测试
 * 
 * @url https://www.zhihuclub.com/199264.shtml
 */
public class GuavaCacheTests {
    
    private final Log logger = LogFactory.getLog(GuavaCacheTests.class);
    
    @Test
    public void test_LoadingCache() throws ExecutionException {
        LoadingCache<String, String> cache = CacheBuilder
                .newBuilder()
                .build(new CacheLoader<String, String>() {
                    @Override
                    public String load(String key) throws Exception {
                        logger.info("加载缓存: key=" + key);
                        return "no " + key;
                    }
                });
        
        cache.put("i1", "111");
        cache.put("i2", "222");
        
        logger.info("获取缓存: i1=" + cache.get("i1"));
        logger.info("获取缓存: i2=" + cache.get("i2"));
        logger.info("获取缓存: i3=" + cache.get("i3"));
        logger.info("获取缓存: i4=" + cache.get("i4"));
        
    }
    
    
    @Test
    public void test_Callable() throws ExecutionException {

        Cache<String, String> cache = CacheBuilder.newBuilder().build();

        String name = cache.get("name", new Callable<String>() {
            @Override
            public String call() throws Exception {
                return "jack";
            }
        });
        cache.put("id","123");
        
        logger.info("name=" + cache.getIfPresent("name"));        
        logger.info("id=" + cache.getIfPresent("id"));        
        logger.info("address=" + cache.getIfPresent("address"));        
    }

    /**
     * 缓存回收:
     *  1. 容量回收
     *  
     *  2. 定时回收
     *  
     *  3. 基于引用的回收
     *   
     * 移除监听器:
     *  
     *  
     * 缓存回收的时机:
     * 
     * 缓存刷新
     * 
     */
    @Test
    public void test_LoadingCache_other() {

        LoadingCache<String, Object> loadingCache = CacheBuilder
                .newBuilder()
                // 软引用
                .softValues()
                // 初始容量
                .initialCapacity(2000)
                // 最大容量
                .maximumSize(3000)
                // 7天过期
                .expireAfterAccess(7, TimeUnit.DAYS)
                // 移除时的监听器
                .removalListener(notify -> {
                    Object key = notify.getKey();
                    if (notify.wasEvicted()) {
                        logger.info("remove key=" + key + " RemovalCause:" + notify.getCause());
                    }
                })
                .build(new CacheLoader<String, Object>() {

                    @Override
                    public Object load(String key) throws Exception {
                        return key + " 调用 FormBeanLoader去加载值 ";
                    }

                    @Override
                    public Map<String, Object> loadAll(Iterable<? extends String> keys) throws Exception {
                        return new HashMap<>();
                    }
                });
        
        // 最大容量是 3000, 当 i>3000时, 之前的 key 会慢慢移除
        for (int i = 0; i < 4000; i++) {
            loadingCache.put(i + "", i);
        }
    }
    
}
