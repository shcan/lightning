package com.cans.lightning.cache.hotCache;

import com.cans.lightning.LightningApplication;
import com.cans.lightning.base.dto.BaseDto;
import com.cans.lightning.base.dto.Pair;
import com.cans.lightning.business.lowcode.form.cache.hotCache.FormProxyCacheFactory;
import com.cans.lightning.business.lowcode.form.cache.hotCache.HotCacheContext;
import com.cans.lightning.business.lowcode.form.cache.hotCache.HotCacheInitializer;
import com.cans.lightning.business.lowcode.form.cache.hotCache.MapDataLoader;
import com.cans.lightning.cache.ProxyCache;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 测试表单热点缓存
 */
@SpringBootTest
@ContextConfiguration(classes = {LightningApplication.class})
public class FormHotCacheTests {

    @Test
    public void testFormHotCache() {

        ProxyCache proxyCache = new FormProxyCacheFactory().createProxyCache(createContext());

        proxyCache.update(BaseDto.builder().id(10).build(), BaseDto.builder().id(10).build());

        proxyCache.get(10);


        proxyCache.updateBatch(Arrays.asList(Pair.of(BaseDto.builder().id(10).build(), BaseDto.builder().id(10).build()), Pair.of(BaseDto.builder().id(11).build(), BaseDto.builder().id(11).build())));

        proxyCache.getBatch(Arrays.asList(1, 2, 3, 4, 5));

        proxyCache.remove(10);

        proxyCache.get(10);
    }


    public static HotCacheContext createContext() {
        return HotCacheContext.builder()
                .key("qpy")
                .loadIds(Arrays.asList(1, 2, 3, 4, 5))
                .mapDataLoader(new MapDataLoader<Serializable, Serializable>() {
                    @Override
                    public Serializable load(Serializable key) {
                        return null;
                    }

                    @Override
                    public Map<Serializable, Serializable> loadBatch(Collection<Serializable> keys) {
                        return keys.stream().map(key -> BaseDto.builder().id(key).build()).collect(Collectors.toMap(v -> (Serializable) v.getId(), v -> v));
                    }
                })
                .hotCacheInitializer(new HotCacheInitializer<Serializable, Serializable>() {
                    @Override
                    public Map<Serializable, Serializable> load4Init(List<Serializable> keys) {
                        return keys.stream().map(key -> BaseDto.builder().id(key).build()).collect(Collectors.toMap(v -> (Serializable) v.getId(), v -> v));
                    }
                }).build();
    }
}
