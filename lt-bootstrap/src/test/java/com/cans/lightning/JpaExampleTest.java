package com.cans.lightning;

import com.cans.lightning.business.pms.dao.SysUserDao;
import com.cans.lightning.business.pms.entity.SysUser;
import com.cans.lightning.business.pms.service.api.ISysUserService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.security.crypto.password.PasswordEncoder;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author shencan
 * @date 2020/7/26 13:49
 */

@SpringBootTest
public class JpaExampleTest {

    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private SysUserDao sysUserDao;
    @Autowired
    private PasswordEncoder passwordEncoder;


    @Test
    void test1() {

        SysUser sysUser = new SysUser();

        sysUser.setUsername("username");
        sysUser.setNickname("nickname");
        sysUser.setEmail("email");
        sysUser.setPassword("password");


        ExampleMatcher matcher = ExampleMatcher.matching() //构建对象
                .withMatcher("username", ExampleMatcher.GenericPropertyMatcher::startsWith) // 匹配方式根据什么开始
                .withMatcher("nickname", ExampleMatcher.GenericPropertyMatcher::endsWith) // 根据字符结尾
                .withMatcher("email", ExampleMatcher.GenericPropertyMatcher::contains) // 包含查询
                .withMatcher("password", ExampleMatcher.GenericPropertyMatcher::exact); // 精确匹配

    }

    @Test
    void encodeTest(){

        String pad = "123123";

        String encode = passwordEncoder.encode(pad);

        System.out.println(encode);
    }
}
