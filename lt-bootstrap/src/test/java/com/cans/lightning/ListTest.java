package com.cans.lightning;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 类描述
 *
 * @author shenc
 * @date 2022-04-18 20:55
 **/
public class ListTest {

    @Test
    public void listsTest(){
        List<String> contractWordFields = Lists.newArrayList();
        contractWordFields.add("field0001");
        Set<String> ignoreFields = new HashSet<>();
        ignoreFields.add("field0001");
        contractWordFields.removeIf(ignoreFields::contains);
        System.out.println(contractWordFields);
    }



    /**
     * 若是二级明细表，按groupid排序
     * */
    private Comparator<Map<String, Object>> getGroupComparator4View() {
        //保持位置不变
        return (o1, o2) -> 0;
    }

}
