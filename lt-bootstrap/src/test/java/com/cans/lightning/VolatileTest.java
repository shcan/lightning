package com.cans.lightning;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 类描述
 *
 * @author cans
 * @date 2021-08-04 16:29
 **/
public class VolatileTest {


    private boolean b = true;

    {
        System.out.println("静态代码块.....");
    }

    @Test
    public void test() throws InterruptedException {

        Thread t1 = new Thread(() -> {

            int i = 0;

            while (b) {
                System.out.println(i++);
            }

            System.out.println("t1跳出循环");

        });



        Thread t2 = new Thread(() -> {

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("设置为false");
            b = false;

        });


        t1.start();
        t2.start();

        t1.join();
        t2.join();

    }

}
