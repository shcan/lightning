package com.cans.lightning;

/**
 * 类描述
 *
 * @author cans
 * @date 2021-06-16 21:39
 **/
public class Main {


    public static void main(String[] args) {

        MyQueue queue = new MyQueue();

        Thread addThread = new Thread(() -> {
            int i = 0;
            while (true) {

                try {
                    queue.putLast(String.valueOf(i++));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread getThread = new Thread(() -> {
            while (true) {
                try {
                    Thread thread = Thread.currentThread();
                    String first = queue.getFirst();
                    System.out.println("线程: " + thread.getName() + "获取: " + first);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        addThread.start();
        getThread.start();

    }
}
