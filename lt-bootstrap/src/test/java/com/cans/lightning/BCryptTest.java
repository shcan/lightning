package com.cans.lightning;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author shencan
 * @date 2020/12/2 16:47
 */
public class BCryptTest {

    public static String  name = "BCryptTest_name";


    public static void main(String[] args) {

        String pwd = "$2a$10$SZaKZIbcXLQvrJc1FWsNLuA/d6FODyKO5WbntnSIE9AXgHlExwl4.";

        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();


        boolean admin = bCryptPasswordEncoder.matches("admin111", pwd);

        System.out.println(admin);
    }
}
