package com.cans.lightning;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * 类描述
 *
 * @author cans
 * @date 2021-06-16 22:52
 **/
public class Main1 {

    // 房间大小坐标
    static int x, y;
    // 墙的数量
    static int qNum;
    // 墙的坐标
    static List<String> q = new ArrayList<>();
    // 陷阱坐标
    static List<String> xj = new ArrayList<>();
    // 不可到达坐标
    static List<String> no = new ArrayList<>();

    public static void main(String[] args) {


        Scanner sc = new Scanner(System.in);
        // 第一行输入房间的X和Y
        String xy = sc.nextLine();

        String[] xyArr = xy.split(" ");

        x = Integer.parseInt(xyArr[0]);
        y = Integer.parseInt(xyArr[1]);

        qNum = sc.nextInt();

        sc = new Scanner(System.in);
        for (int i = 0; i < qNum; i++) {
            String qLine = sc.nextLine();
            q.add(qLine);
        }

        add(0, 0);
    }


    /**
     * 前进
     * 返回  0:到达死点 1: 到达终点
     */
    public static int add(int xLine, int yLine) {

        if (xLine == x && yLine == y) {
            // 到达终点
            return 1;
        }

        if (xAdd(xLine, yLine)) {
            add(++xLine, yLine);
        } else if (yAdd(xLine, yLine)) {
            add(xLine, ++yLine);
        } else {
            // 到达死点
            return 0;
        }


        return 1;
    }

    public static boolean xAdd(int x, int y) {

        return Boolean.TRUE;

    }

    public static boolean yAdd(int x, int y) {

        return Boolean.TRUE;
    }
}
