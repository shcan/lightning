package com.cans.lightning.config.initializer;

import com.alibaba.fastjson.parser.ParserConfig;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author: qpy
 * @date: 2024-02-03 10:46
 * @description:
 */
public class LightningApplicationContextInitializer implements ApplicationContextInitializer {
    
    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        // 关闭ASM 
        // mark 这个配置没什么用
        ParserConfig.getGlobalInstance().setAsmEnable(false);
    }
}
