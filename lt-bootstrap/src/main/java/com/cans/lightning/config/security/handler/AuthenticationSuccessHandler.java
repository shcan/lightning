package com.cans.lightning.config.security.handler;

import com.cans.lightning.base.dto.ResDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * 配置登陆返回JSON
 *
 * @author shencan
 * @date 2017/10/10
 */
@Component
public class AuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth)
            throws IOException, ServletException {


        ObjectMapper mapper = new ObjectMapper();

        response.setContentType("text/html;charset=UTF-8");
        mapper.writeValue(response.getWriter(), ResDto.success("登陆成功!"));
        response.getWriter().flush();
    }

}
