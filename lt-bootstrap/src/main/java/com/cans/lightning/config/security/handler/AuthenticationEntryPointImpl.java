package com.cans.lightning.config.security.handler;

import com.cans.lightning.base.dto.ResDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

/**
 * 认证失败处理类 返回未授权
 *
 * @author ruoyi
 */
@Component
public class AuthenticationEntryPointImpl implements AuthenticationEntryPoint, Serializable {
    private static final long serialVersionUID = -8970718410437077606L;

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e)
            throws IOException {

        int code = 401;
        String msg = "access: " + request.getRequestURI() + ", error, detail info: " + e.getMessage();

        ResDto<String> fait = ResDto.fait(msg);
        fait.setCode(code);

        ObjectMapper mapper = new ObjectMapper();

        response.setStatus(code);
        response.setContentType("text/html;charset=UTF-8");
        mapper.writeValue(response.getWriter(), fait);
        response.getWriter().flush();
    }
}
