package com.cans.lightning.config.security.handler;

import com.cans.lightning.base.dto.ResDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * 认证失败处理
 *
 * @author shencan
 * @date 2017/10/10
 */
@Component
public class AuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {

        ResDto<String> fait = ResDto.fait(exception.getMessage());

        ObjectMapper mapper = new ObjectMapper();

        response.setContentType("text/html;charset=UTF-8");
        mapper.writeValue(response.getWriter(), fait);
        response.getWriter().flush();

    }
}
