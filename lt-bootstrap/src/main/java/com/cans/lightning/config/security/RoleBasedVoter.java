package com.cans.lightning.config.security;

import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.authorization.AuthorizationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.FilterInvocation;
import jakarta.servlet.http.HttpServletRequest;

import java.util.Collection;
import java.util.function.Supplier;

/**
 * 动态URL接口校验
 *
 * @author cans
 * @date 2021-05-11 15:24
 **/
public class RoleBasedVoter implements AuthorizationManager<FilterInvocation> {

    @Override
    public AuthorizationDecision check(Supplier<Authentication> authentication, FilterInvocation fi) {
        HttpServletRequest request = fi.getRequest();

        String path = request.getServletPath();
        String method = request.getMethod();

        // 判断是否需要权限验证
        if (authentication == null) {
            return new AuthorizationDecision(false);
        }

        return new AuthorizationDecision(true);

        /*Collection<? extends GrantedAuthority> authorities = authentication.get().getAuthorities();

        for (Object attribute : fi.getAttributes()) {
            if (attribute instanceof ConfigAttribute) {
                ConfigAttribute configAttribute = (ConfigAttribute) attribute;
                if (configAttribute.getAttribute() == null) {
                    continue;
                }
                if (supports(configAttribute)) {
                    // Attempt to find a matching granted authority
                    for (GrantedAuthority authority : authorities) {
                        if (configAttribute.getAttribute().equals(authority.getAuthority())) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;*/
    }

    private boolean supports(ConfigAttribute attribute) {
        return true;
    }
}
