package com.cans.lightning.test;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试 spring-security的
 */
@RestController
@RequestMapping("/test/auth")
public class TestAuthController {

    // 列出spring security 方法注释的所有案例
    @PreAuthorize("hasAuthority('test:sys:user:list')")
    @GetMapping("/hasAuthority")
    public String hasAuthority() {
        return "hasAuthority";
    }
    @PreAuthorize("hasAnyAuthority('test:sys:user:list','test:sys:user:add')")
    @GetMapping("/hasAnyAuthority")
    public String hasAnyAuthority() {
        return "hasAnyAuthority";
    }
    @PreAuthorize("hasRole('admin')")
    @GetMapping("/hasRole")
    public String hasRole() {
        return "hasRole";
    }
    @PreAuthorize("hasAnyRole('admin','user')")
    @GetMapping("/hasAnyRole")
    public String hasAnyRole() {
        return "hasAnyRole";
    }
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/isAuthenticated")
    public String isAuthenticated() {
        return "isAuthenticated";
    }
    @PreAuthorize("isAnonymous()")
    @GetMapping("/isAnonymous")
    public String isAnonymous() {
        return "isAnonymous";
    }
    @PreAuthorize("isRememberMe()")
    @GetMapping("/isRememberMe")
    public String isRememberMe() {
        return "isRememberMe";
    }
    @PreAuthorize("permitAll()")
    @GetMapping("/permitAll")
    public String permitAll() {
        return "permitAll";
    }
    @PreAuthorize("denyAll()")
    @GetMapping("/denyAll")
    public String denyAll() {
        return "denyAll";
    }
    @PreAuthorize("hasIpAddress('127.0.0.1')")
    @GetMapping("/hasIpAddress")
    public String hasIpAddress() {
        return "hasIpAddress";
    }

}
