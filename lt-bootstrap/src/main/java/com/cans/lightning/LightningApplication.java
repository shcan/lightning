package com.cans.lightning;

import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.metadata.TableInfoHelper;
import com.cans.lightning.base.entity.BaseEntity;
import com.cans.lightning.business.core.utils.ContextUtil;
import org.apache.ibatis.builder.MapperBuilderAssistant;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author 31733
 */
@EnableAsync
@SpringBootApplication
public class LightningApplication implements ApplicationRunner {


    public static void main(String[] args) {
        ConfigurableApplicationContext ac = SpringApplication.run(LightningApplication.class, args);
        ContextUtil.setApplicationContext(ac);
    }


    @Override
    public void run(ApplicationArguments args) throws Exception {
        TableInfoHelper.initTableInfo(new MapperBuilderAssistant(new MybatisConfiguration(), ""), BaseEntity.class) ;
    }
}
