package com.cans.lightning.formrun.bean;

import com.alibaba.fastjson.annotation.JSONType;
import com.cans.lightning.business.core.bean.BaseBean;
import com.cans.lightning.business.lowcode.form.bean.AppFormBean;
import lombok.Getter;
import lombok.Setter;

/**
 * @author cans
 * @date 2023/11/28
 **/
@Getter
@Setter
@JSONType(ignores = {"allTableBean", "nextFiendName", "currentViewId"})
public class AppFormDataMasterBean extends BaseBean {

    public AppFormDataMasterBean(){
    }

    public AppFormDataMasterBean(AppFormBean formDefinition){
        this.formDefinition = formDefinition;
    }

    /**
     * 表单定义
     */
    private AppFormBean formDefinition;

    /**
     * 视图权限
     */
    private String authViewId;
    /**
     * 主表数据
     */
    private MainTableDataBean mainTableData;

}
