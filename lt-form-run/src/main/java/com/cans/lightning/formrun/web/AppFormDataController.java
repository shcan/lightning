package com.cans.lightning.formrun.web;

import com.cans.lightning.base.dto.ResDto;
import com.cans.lightning.business.lowcode.dto.AppFormRowDataDto;
import com.cans.lightning.formrun.bean.AppFormDataMasterBean;
import com.cans.lightning.formrun.service.api.IAppFormDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 表单数据运行接口
 *
 * @author shencan
 * @date 2020/6/14 22:14
 */
@RestController
@RequestMapping("/run/app_form")
public class AppFormDataController {

    @Autowired
    private IAppFormDataService appFormDataService;

    @PostMapping("/saveOrUpdate")
    public ResDto<String> saveOrUpdate(@RequestBody AppFormRowDataDto formDataDto){
        appFormDataService.saveOrUpdate(formDataDto);
        return ResDto.success();
    }

    @PostMapping("/createOrEdit")
    public ResDto<AppFormDataMasterBean> createOrEdit(@RequestBody AppFormRowDataDto formDataDto){
        return ResDto.success(appFormDataService.createOrEdit(formDataDto));
    }

    @PostMapping("/deleteById")
    public ResDto<String> deleteById(@RequestBody AppFormRowDataDto formDataDto){
        appFormDataService.deleteById(formDataDto);
        return ResDto.success();
    }

    @PostMapping("/getPageData")
    public ResDto<Object> getPageData(@RequestBody AppFormRowDataDto formDataDto){
        return ResDto.success(appFormDataService.getPageData(formDataDto));
    }

    @GetMapping("/getListShowField/{formId}/{bindId}")
    public ResDto<Object> getListShowField( @PathVariable("formId") String formId,@PathVariable("bindId") String bindId){
        return ResDto.success(appFormDataService.getListShowField(formId,bindId));
    }

    @GetMapping("/getListShowField/{formId}")
    public ResDto<Object> getListShowField( @PathVariable("formId") String formId){
        return ResDto.success(appFormDataService.getListShowField(formId,null));
    }
}
