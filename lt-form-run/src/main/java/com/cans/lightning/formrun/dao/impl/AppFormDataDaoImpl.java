package com.cans.lightning.formrun.dao.impl;

import com.baomidou.mybatisplus.extension.toolkit.SqlRunner;
import com.cans.lightning.business.lowcode.enums.AppFormSysField;
import com.cans.lightning.formrun.dao.api.IAppFormDataDao;
import com.google.common.collect.Lists;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author cans
 * @date 2023/11/25
 **/
@Repository("appFormDataDao")
public class AppFormDataDaoImpl implements IAppFormDataDao {

    @Override
    public List<Map<String, Object>> selectDataList(String tableName) {
        return SqlRunner.db().selectList("SELECT * FROM " + tableName + " order by " + AppFormSysField.CREATE_TIME + " desc");
    }

    @Override
    public void deleteFormData(String dbTableName, Map<String, String> param) {
        List<String> idINList = Lists.newArrayList();
        for (String id : param.keySet()) {
            idINList.add(String.format("#{%s}", id));
        }
        String join = Strings.join(idINList, ',');
        SqlRunner.db().update("DELETE FROM " + dbTableName + " WHERE id in ( " + join + " )", param);
    }

    @Override
    public Map<String, Object> getMasterDataById(String tableName, String dataId) {
        return SqlRunner.db().selectOne("SELECT * FROM " + tableName + " where id = {0} ;",dataId);
    }
}
