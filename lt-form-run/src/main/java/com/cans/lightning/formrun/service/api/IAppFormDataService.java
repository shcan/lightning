package com.cans.lightning.formrun.service.api;

import com.cans.lightning.business.lowcode.dto.AppFormBindDto;
import com.cans.lightning.business.lowcode.dto.AppFormRowDataDto;
import com.cans.lightning.formrun.bean.AppFormDataMasterBean;

import java.util.List;
import java.util.Map;

public interface IAppFormDataService {

    void saveOrUpdate(AppFormRowDataDto formDataDto);

    void deleteById(AppFormRowDataDto formDataDto);

    List<Map<String, Object>> getPageData(AppFormRowDataDto formDataDto);

    AppFormDataMasterBean createOrEdit(AppFormRowDataDto formDataDto);

    List<Map<String,String>> getListShowField(String formId,String bindId);
}
