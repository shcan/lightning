package com.cans.lightning.formrun.bean;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
public class MainTableDataBean {

    private String id;

    private Map<String,FieldValueBean> fieldValue;

}
