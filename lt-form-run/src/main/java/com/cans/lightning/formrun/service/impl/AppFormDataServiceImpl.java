package com.cans.lightning.formrun.service.impl;

import com.baomidou.mybatisplus.extension.toolkit.SqlRunner;
import com.cans.lightning.business.core.service.UUIDAutoGen;
import com.cans.lightning.business.lowcode.form.bean.FormBindBean;
import com.cans.lightning.business.lowcode.form.bean.FormFieldBean;
import com.cans.lightning.formrun.bean.AppFormDataMasterBean;
import com.cans.lightning.formrun.service.api.IAppFormDataService;
import com.cans.lightning.formrun.bean.FieldValueBean;
import com.cans.lightning.formrun.dao.api.IAppFormDataDao;
import com.cans.lightning.business.lowcode.dto.AppFormRowDataDto;
import com.cans.lightning.business.lowcode.dto.AppFormFieldValueDto;
import com.cans.lightning.business.lowcode.dto.AppFormDto;
import com.cans.lightning.business.lowcode.enums.AppFormSysField;
import com.cans.lightning.business.lowcode.enums.FieldTypeEnum;
import com.cans.lightning.business.lowcode.form.design.FormDesignManager;
import com.cans.lightning.business.lowcode.form.bean.AppFormBean;
import com.cans.lightning.business.lowcode.service.api.IAppFormDefinitionService;
import com.cans.lightning.formrun.bean.MainTableDataBean;
import com.google.common.base.Joiner;
import com.google.common.collect.Maps;
import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 应用信息
 *
 * @author shencan
 * @date 2020/6/14 20:53
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class AppFormDataServiceImpl implements IAppFormDataService {
    private static final Logger logger = LoggerFactory.getLogger(AppFormDataServiceImpl.class);

    @Qualifier("appFormDataDao")
    @Autowired
    private IAppFormDataDao appFormDataDao;
    @Autowired
    private IAppFormDefinitionService appFormDefinitionService;
    @Autowired
    private FormDesignManager appFormBeanManager;
    @Autowired
    private UUIDAutoGen idAutoGen;
    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public void saveOrUpdate(AppFormRowDataDto formDataDto) {
        AppFormDto formDefinitionServiceInfo = appFormDefinitionService.getInfo(formDataDto.getFormId());
        Pair<String, Map<String, Object>> sqlPair;
        if(formDataDto.getDataId() == null){
            sqlPair = this.getInsertSql(formDefinitionServiceInfo.getMainTable().getDbTableName(), formDataDto.getFieldValue());
        }else{
            sqlPair = this.getUpdateSql(formDataDto.getDataId(),formDefinitionServiceInfo.getMainTable().getDbTableName(), formDataDto.getFieldValue());
        }

        if(sqlPair == null){
            return;
        }
        namedParameterJdbcTemplate.update(sqlPair.getLeft(), sqlPair.getValue());

    }

    @Override
    public void deleteById(AppFormRowDataDto formDataDto) {
        AppFormDto formDefinitionServiceInfo = appFormDefinitionService.getInfo(formDataDto.getFormId());
        Map<String, String> param = new HashMap<>(1);
        String[] idArr = formDataDto.getDataId().split(",");
        int i = 0;
        for (String id : idArr) {
            param.put("id" + "_" + i++, id);
        }
        appFormDataDao.deleteFormData(formDefinitionServiceInfo.getMainTable().getDbTableName(), param);
    }

    @Override
    public List<Map<String, Object>> getPageData(AppFormRowDataDto formDataDto) {
        AppFormDto formDefinitionServiceInfo = appFormDefinitionService.getInfo(formDataDto.getFormId());
        return appFormDataDao.selectDataList(formDefinitionServiceInfo.getMainTable().getDbTableName());
    }

    @Override
    public AppFormDataMasterBean createOrEdit(AppFormRowDataDto formDataDto) {
        AppFormBean formBean = appFormBeanManager.getFormBean(formDataDto.getFormId());
        AppFormDataMasterBean appFormDataMasterBean = new AppFormDataMasterBean(formBean);
        String dataId = formDataDto.getDataId();
        appFormDataMasterBean.setMainTableData(this.loadFormMasterData(formDataDto.getFormId(),dataId));
        return appFormDataMasterBean;
    }

    @Override
    public List<Map<String,String>> getListShowField(String formId,String bindId) {
        AppFormBean formBean = appFormBeanManager.getFormBean(formId);
        List<FormBindBean> formBindList = formBean.getFormBindList();

        if(StringUtils.isEmpty(bindId)){
            FormBindBean formBindBean = formBindList.get(0);
            if(formBindBean != null){
                bindId = formBindBean.getId();
            }
        }

        List<Map<String,String>> res = Lists.newArrayList();
        for (FormBindBean formBindBean : formBindList) {
            if(Objects.equals(formBindBean.getId(),bindId)){
                String fieldListShow = formBindBean.getFieldListShow();
                String[] dbFieldNameArr = fieldListShow.split(",");
                for (String dbFieldName : dbFieldNameArr) {
                    Map<String, FormFieldBean> fieldMap = formBean.getFieldMap();
                    FormFieldBean formFieldBean = fieldMap.get(dbFieldName);
                    Map<String,String> map = Maps.newHashMap();
                    map.put("id",formFieldBean.getId());
                    map.put("fieldName",formFieldBean.getFieldName());
                    map.put("dbFieldName",formFieldBean.getDbFieldName());
                    res.add(map);
                }
            }
        }
        return res;
    }

    private MainTableDataBean loadFormMasterData(String formId,String dataId) {
        MainTableDataBean mainTableDataBean = new MainTableDataBean();
        if(StringUtils.isEmpty(dataId)){
            return mainTableDataBean;
        }
        AppFormBean formBean = appFormBeanManager.getFormBean(formId);
        mainTableDataBean.setId(dataId);
        Map<String, Object> masterDataMap = appFormDataDao.getMasterDataById(formBean.getMainTableBean().getTableName(), dataId);
        Map<String, FieldValueBean> fieldValue = new HashMap<>();
        masterDataMap.forEach((k, v) -> {
            FieldValueBean fieldValueBean = new FieldValueBean();
            if(v!=null){
                fieldValueBean.setDisplayValue(v.toString());
                fieldValueBean.setSourceValue(v.toString());
            }
            fieldValue.put(k, fieldValueBean);
        });
        mainTableDataBean.setFieldValue(fieldValue);
        return mainTableDataBean;
    }

    private Pair<String, Map<String, Object>> getUpdateSql(String dataId,String dbTableName, List<AppFormFieldValueDto> fieldValues) {
        Map<String, Object> params = Maps.newLinkedHashMap();


        for (AppFormFieldValueDto fieldValue : fieldValues) {
            if(fieldValue == null){
                continue;
            }
            if(AppFormSysField.CREATE_TIME.equals(fieldValue.getDbFieldName())){
                continue;
            }
            params.put(fieldValue.getDbFieldName(), this.parseToDbValue(fieldValue));
        }
        StringBuilder sql = new StringBuilder("UPDATE " + dbTableName);
        if(params.isEmpty()){
            return null;
        }
        sql.append(" SET ");
        for (String key : params.keySet()) {
            sql.append(key).append(" = :").append(key).append(" ").append(" ,");
        }
        sql.deleteCharAt(sql.length() - 1);
        sql.append(" WHERE id = :id");
        params.put("id", dataId);
        params.put(AppFormSysField.LAST_CHANGE_TIME, new Date());
        return Pair.of(sql.toString(), params);
    }
    private Pair<String, Map<String, Object>> getInsertSql(String dbTableName, List<AppFormFieldValueDto> fieldValues) {
        Map<String, Object> params = Maps.newLinkedHashMap();
        for (AppFormFieldValueDto fieldValue : fieldValues) {
            if(fieldValue == null){
                continue;
            }
            params.put(fieldValue.getDbFieldName(), this.parseToDbValue(fieldValue));
        }
        params.put(AppFormSysField.ID, idAutoGen.nextId());
        params.put(AppFormSysField.CREATE_TIME, new Date());
        params.put(AppFormSysField.LAST_CHANGE_TIME, new Date());
        Set<String> fieldNames = params.keySet();
        String sql = "INSERT INTO " + dbTableName + " ( " + Joiner.on(",").join(fieldNames) + " ) "
                + "VALUES " + "( " + Joiner.on(",").join(fieldNames.stream().map(i -> String.format(":%s", i)).collect(Collectors.toList())) + " )";
        return Pair.of(sql, params);
    }

    private Object parseToDbValue(AppFormFieldValueDto fieldValue) {
        if (fieldValue.getValue() == null) {
            return null;
        }
        FieldTypeEnum fieldTypeEnum = FieldTypeEnum.getByKey(fieldValue.getDbFieldDataType());
        if (fieldTypeEnum == null) {
            throw new IllegalStateException("Unexpected value: " + fieldValue.getDbFieldDataType());
        }
        Object dbValue = fieldValue.getValue();
        switch (fieldTypeEnum) {
            case VARCHAR:
                break;
            case INT:
                break;
            case DATE:
                SimpleDateFormat smf = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    dbValue = smf.parse(fieldValue.getValue());
                } catch (ParseException e) {
                    dbValue = null;
                    logger.error("日期格式化失败,value" + fieldValue.getValue(), e);
                }
                break;
            case DATE_TIME:
                break;
            case FILE:
                break;
            case DECIMAL:
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + fieldValue.getDbFieldDataType());
        }
        return dbValue;
    }

}
