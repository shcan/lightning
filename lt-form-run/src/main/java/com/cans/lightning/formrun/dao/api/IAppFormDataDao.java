package com.cans.lightning.formrun.dao.api;

import java.util.List;
import java.util.Map;

public interface IAppFormDataDao {

    List<Map<String, Object>> selectDataList(String tableName);

    void deleteFormData(String dbTableName, Map<String, String> param);

    Map<String,Object> getMasterDataById(String formName, String dataId);

}
