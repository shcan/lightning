package com.cans.lightning.formrun.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FieldValueBean {

    private String sourceValue;

    private String displayValue;
}
