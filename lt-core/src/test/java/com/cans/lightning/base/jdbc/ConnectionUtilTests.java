package com.cans.lightning.base.jdbc;

import com.cans.lightning.base.dao.jdbc.ConnectionUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

/**
 * 测试真实获取连接
 */
@Slf4j
public class ConnectionUtilTests {

    // @Test
    void pressureTest() throws SQLException {
        // 由于最大连接配置的4个, 所以到第5次循环因为获取不到连接 线程被阻塞
        for (int i = 0; i < 100; i++) {
            log.info("get number {} connection: {}", i, ConnectionUtil.getConnection());
        }
    }
}
