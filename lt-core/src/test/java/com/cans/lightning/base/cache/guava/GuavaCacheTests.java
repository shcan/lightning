package com.cans.lightning.base.cache.guava;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Slf4j
public class GuavaCacheTests {

    /*
        key5 was removal, reason: SIZE
        key14 was removal, reason: SIZE
        key19 was removal, reason: SIZE
        key20 was removal, reason: SIZE
        key21 was removal, reason: SIZE
        key1 was removal, reason: SIZE
        key26 was removal, reason: SIZE
        key27 was removal, reason: SIZE
        ......... 不是按照顺序插入, 时间最长未使用来移除的?
        key41 was removal, reason: SIZE
        key42 was removal, reason: SIZE
        key52 was removal, reason: SIZE
        key57 was removal, reason: SIZE
        key46 was removal, reason: SIZE
     */
    @Test
    public void testGuavaCache() throws ExecutionException, InterruptedException {
        LoadingCache<String, Optional<Object>> cache = getCache();
        for (int i = 0; i < 150; i++) {
            Thread.sleep(30);
            cache.get("key" + i);
        }
    }

    public LoadingCache<String, Optional<Object>> getCache() {
        LoadingCache<String, Optional<Object>> loadingCache = CacheBuilder
                .newBuilder()
                .softValues()
                .initialCapacity(100)
                .maximumSize(100)
                .expireAfterAccess(7, TimeUnit.DAYS)
                .removalListener(removalNotification -> {
                    log.info("{} was removal, reason: {}", removalNotification.getKey(), removalNotification.getCause());
                })
                // 需要指定数据加载器
                .build(new CacheLoader<String, Optional<Object>>() {
                    @Override
                    public Optional<Object> load(String key) throws Exception {
                        // log.info("loading key {}", key);
                        return Optional.ofNullable("loadingValue_" + key);
                    }

                    @Override
                    public Map<String, Optional<Object>> loadAll(Iterable<? extends String> keys) throws Exception {
                        List<String> keyList = new ArrayList<>();
                        Map<String, Optional<Object>> map = Maps.newHashMap();
                        keys.forEach(key -> {
                            map.put(key, Optional.ofNullable("loadingValue_" + key));
                            keyList.add(key);
                        });
                        // log.info("loading all key {}", keyList);
                        return map;
                    }
                });
        return loadingCache;
    }
}
