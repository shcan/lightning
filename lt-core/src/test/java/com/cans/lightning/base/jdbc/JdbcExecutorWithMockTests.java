package com.cans.lightning.base.jdbc;


import com.cans.lightning.base.dao.jdbc.JdbcExecutor;
import com.cans.lightning.base.dao.jdbc.PageInfo;
import com.cans.lightning.base.exceptions.BusinessException;
import com.google.common.collect.Maps;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class JdbcExecutorWithMockTests {

    @InjectMocks
    private JdbcExecutor jdbcExecutor;

    @Mock
    private Connection connection;

    @Mock
    private PreparedStatement preparedStatement;

    @Mock
    private ResultSet resultSet;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        when(connection.prepareStatement(any())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);
        jdbcExecutor.setConn(connection);
    }

    @Test
    public void namedPageQuery_ValidInput_Success() throws Exception {
        String namedSql = "select * from user where name = :name";
        Map<String, Object> params = Maps.newHashMap();
        params.put("name", "test");
        PageInfo pageInfo = new PageInfo();
        pageInfo.setPageSize(10);
        pageInfo.setPage(1);

        /*when(DialectUtil.getSqlWithOffsetLimit(any(), any(), any(), any())).thenAnswer(invocation -> {
            String sql = invocation.getArgument(0);
            List<Object> param = invocation.getArgument(1);
            int offset = invocation.getArgument(2);
            int limit = invocation.getArgument(3);
            return new AbstractMap.SimpleEntry<>(sql, param);
        });*/

        when(resultSet.next()).thenReturn(true).thenReturn(false);
        when(resultSet.getObject("id")).thenReturn(1L);
        when(resultSet.getObject("name")).thenReturn("test");

        PageInfo result = jdbcExecutor.namedPageQuery(namedSql, params, pageInfo);

        assertEquals(1, result.getData().size());
        assertEquals(1, result.getData().get(0).get("id"));
        assertEquals("test", result.getData().get(0).get("name"));
        assertEquals(1, pageInfo.getPage());
        assertEquals(10, pageInfo.getPageSize());
    }

    @Test
    public void namedPageQuery_BusinessException_ThrowsException() throws Exception {
        String namedSql = "select * from user where name = :name";
        Map<String, Object> params = Maps.newHashMap();
        params.put("name", "test");
        PageInfo pageInfo = new PageInfo();
        pageInfo.setPageSize(10);
        pageInfo.setPage(1);

        doThrow(new BusinessException("Business error")).when(connection).prepareStatement(any());

        jdbcExecutor.namedPageQuery(namedSql, params, pageInfo);
    }

    @Test
    void namedPageQuery_SQLException_ThrowsException() throws Exception {
        String namedSql = "select * from user where name = :name";
        Map<String, Object> params = Maps.newHashMap();
        params.put("name", "test");
        PageInfo pageInfo = new PageInfo();
        pageInfo.setPageSize(10);
        pageInfo.setPage(1);

        doThrow(new SQLException("SQL error")).when(connection).prepareStatement(any());

        jdbcExecutor.namedPageQuery(namedSql, params, pageInfo);
    }

    @Test
    void namedPageQuery_NoRowsFound_EmptyData() throws Exception {
        String namedSql = "select * from user where name = :name";
        Map<String, Object> params = Maps.newHashMap();
        params.put("name", "test");
        PageInfo pageInfo = new PageInfo();
        pageInfo.setPageSize(10);
        pageInfo.setPage(1);

        /*when(DialectUtil.getSqlWithOffsetLimit(any(), any(), any(), any())).thenAnswer(invocation -> {
            String sql = invocation.getArgument(0);
            List<Object> param = invocation.getArgument(1);
            int offset = invocation.getArgument(2);
            int limit = invocation.getArgument(3);
            return new AbstractMap.SimpleEntry<>(sql, param);
        });*/

        when(resultSet.next()).thenReturn(false);

        PageInfo result = jdbcExecutor.namedPageQuery(namedSql, params, pageInfo);

        assertTrue(result.getData().isEmpty());
    }
}
