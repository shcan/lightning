package com.cans.lightning.base.cache.simple;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

@Slf4j
public class SimpleCglibTests {


    /**
     * cglib动态代理是基于继承的方式代理,
     * 当前创建的 TestClass的代理, 代理对象继承了 TestClass
     * 并且执行的时候 先走到 intercept 方法, 然后执行 invokeSuper 方法,
     */
    @Test
    public void test() {
        // org.springframework.cglib.proxy.Enhancer
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(TestClass.class);
        enhancer.setCallback(new MethodInterceptorImpl());
        TestClass testClass = (TestClass)enhancer.create();
        testClass.test();
    }

    /**
     * cglib动态代理, 执行的时候 使用自定义实列来进行调用
     * 比如缓存中先走到代理对象, 然后根据判断来执行具体对象的方法
     */
    @Test
    public void testSelf() {
        // org.springframework.cglib.proxy.Enhancer
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(TestClass.class);
        enhancer.setCallback(new MethodInterceptorImpl(new TestClass()));
        TestClass testClass = (TestClass)enhancer.create();
        testClass.test();
    }


    class MethodInterceptorImpl implements MethodInterceptor {

        private TestClass testClass;

        public MethodInterceptorImpl() {}

        public MethodInterceptorImpl(TestClass testClass) {
            this.testClass = testClass;
        }

        @Override
        public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
            try {
                log.info("before intercept ");
                if (testClass != null) {
                    log.info("call targetClass:{}, not default super call", testClass);
                    return methodProxy.invoke(testClass, objects);
                }
                return methodProxy.invokeSuper(o, objects);
            }
            finally {
                log.info("after intercept {}", method);
            }
        }
    }

    public static class TestClass {

        public TestClass() {
        }
        public void test() {
            log.info("test ------");
        }
    }
}
