package com.cans.lightning.base.jdbc;

import com.alibaba.fastjson.JSON;
import com.cans.lightning.base.dao.jdbc.JdbcExecutor;
import com.cans.lightning.base.dao.jdbc.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author qpy
 * @date 2024-07-25 17:37
 * @description
 */
@Slf4j
class JdbcExecutorTests {


    /**
     * 命名参数测试
     */
    @Test
    void namedPrepareTest() {
        String testsSql = "select * from t_user where id in (:id) and name = :name";
        Map<String, Object> params = new HashMap<String, Object>(){{
            put("id", Arrays.asList(1,2,3));
            put("name", "张三");
        }};

        Map.Entry<String, List<Object>> jdbcSql = new JdbcExecutor().namedPrepare2JdbcPrepare(testsSql, params);
        log.info("jdbcSql: k:{}, v:{}", jdbcSql.getKey(), jdbcSql.getValue());
    }

    @Test
    void namedPageQueryTest() throws SQLException {
        String testsSql = "SELECT formmain_0021_0.field0003 f0,formmain_0021_0.field0004 f1,formmain_0021_0.field0005 f2,formmain_0021_0.field0006 f3,formmain_0021_0.field0010 f4 " +
                "FROM formmain_0021 formmain_0021_0 " +
                "left join formson_0022 formson_0022_1 " +
                "on  formson_0022_1.formmain_id = formmain_0021_0.id where   (  ( formmain_0021_0.field0010  = :sys_con_0 )  )  ";
        Map<String, Object> params = new HashMap<String, Object>(){{
            put("sys_con_0", "-1439696448845754541");
        }};
        PageInfo pageInfo = new PageInfo().setPage(1).setPageSize(10);
        new JdbcExecutor().setSqlLog(true).namedPageQuery(testsSql, params, pageInfo);

        log.info("pageInfo.data: {}", JSON.toJSONString(pageInfo.getData()));
    }
}
