package com.cans.lightning.utils;

import java.util.HashSet;
import java.util.Set;

public class SqlWildcardUtil {

    private static Set<String> mysqlSet = new HashSet<String>();
    static {
        mysqlSet.add("%");
        mysqlSet.add("_");
        mysqlSet.add("\\");
    }

    /**
     *  escape mysql wildcard
     */
    public static String escape(String sqlStr) {
        StringBuilder sb = new StringBuilder(sqlStr);
        for (int i = 0; i < sb.length(); i++) {
            if (mysqlSet.contains(sb.substring(i, i + 1))) {
                sb.insert(i, "\\");
                i++;
            }
        }
        return sb.toString();
    }
}
