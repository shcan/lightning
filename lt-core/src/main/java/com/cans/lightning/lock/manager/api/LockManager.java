package com.cans.lightning.lock.manager.api;

import com.cans.lightning.lock.bean.LockInfo;
import com.cans.lightning.lock.exception.InLockException;

public interface LockManager {

    /**
     * 加锁
     *
     * @param resourceId
     * @return
     */
    LockInfo lock(String resourceId,String userId) throws InLockException;

    /**
     * 解锁
     *
     * @param resourceId
     */
    void unLock(String resourceId);
}
