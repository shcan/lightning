package com.cans.lightning.lock.manager.impl;

import com.cans.lightning.cache.redis.RedisCache;
import com.cans.lightning.lock.bean.LockInfo;
import com.cans.lightning.lock.exception.InLockException;
import com.cans.lightning.lock.manager.api.LockManager;
import org.springframework.stereotype.Component;

import org.springframework.beans.factory.annotation.Autowired;
import java.util.concurrent.TimeUnit;

/**
 * @author cans
 * @date 2024/3/30
 **/
@Component
public class LockManagerImpl implements LockManager {

    @Autowired
    private RedisCache redisCache;

    @Override
    public LockInfo lock(String resourceId, String userId) throws InLockException {
        LockInfo lockInfo = new LockInfo();
        lockInfo.init(resourceId, userId);
        boolean lockResult = redisCache.setCacheObjectIfAbsent(resourceId, lockInfo, 30 * 60, TimeUnit.SECONDS);
        if (!lockResult) {
            LockInfo inLock = redisCache.getCacheObject(resourceId);
            // 后续优化提示
            throw new InLockException("当前表单正在被" + inLock.getUserId() + "编辑");
        }
        return lockInfo;
    }

    @Override
    public void unLock(String resourceId) {
        redisCache.deleteObject(resourceId);
    }
}
