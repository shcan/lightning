package com.cans.lightning.lock.exception;

/**
 * 锁被占用异常
 *
 * @author cans
 * @date 2024/3/30
 **/
public class InLockException extends RuntimeException {
    public InLockException(String s) {
        super(s);
    }

    public InLockException() {
        super();
    }
}
