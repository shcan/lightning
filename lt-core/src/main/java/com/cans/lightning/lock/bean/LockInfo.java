package com.cans.lightning.lock.bean;

import com.cans.lightning.utils.NumberUtils;

/**
 * 锁信息
 *
 * @author cans
 * @date 2024/3/30
 **/
public class LockInfo {
    /**
     * 锁ID
     */
    private Long id;

    /**
     * 锁持有者
     */
    private String userId;

    /**
     * 锁定资源ID
     */
    private String resourceId;

    /**
     * 加锁时间搓
     */
    private Long createTimeStamp;

    public LockInfo init(String userId,String resourceId){
        this.setId(NumberUtils.genId());
        this.setUserId(userId);
        this.setResourceId(resourceId);
        this.setCreateTimeStamp(System.nanoTime());
        return this;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public Long getCreateTimeStamp() {
        return createTimeStamp;
    }

    public void setCreateTimeStamp(Long createTimeStamp) {
        this.createTimeStamp = createTimeStamp;
    }
}
