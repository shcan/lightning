package com.cans.lightning.cache.hotCache;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.ByteUtil;
import com.cans.lightning.base.dto.Pair;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisScriptingCommands;
import org.springframework.data.redis.connection.ReturnType;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.ObjectUtils;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;

/**
 * redis集群热点缓存操作
 */
@Slf4j
public class HotCacheRedisCluster implements HotCacheRedis{

    private final static String slotPrefix = "{";
    private final static String slotSuffix = "}";
    public final static String reloadAllFlag = "!@#reloadAll#@!";

    /**
     * redis lua脚本的sha1值
     */
    private String pullSha1;
    private String pushSha1;

    private StringRedisTemplate rt;
    /**
     * 初始化门槛 当没有实例时, 需要阻塞
     */
    private static CountDownLatch intiLatch = new CountDownLatch(1);
    private static volatile HotCacheRedisCluster instance;

    public HotCacheRedisCluster(StringRedisTemplate stringRedisTemplate) {
        this.rt = stringRedisTemplate;

        initScript();

        instance = this;
        intiLatch.countDown();
    }

    private void initScript() {
        long time = System.currentTimeMillis();
        // 每台节点会执行脚本 lettuce 实现点在这
        // io.lettuce.core.cluster.RedisAdvancedClusterAsyncCommandsImpl.executeOnNodes
        try (RedisConnection clusterConnection = rt.getConnectionFactory().getConnection()) {
            RedisScriptingCommands redisScriptingCommands = clusterConnection
                    .scriptingCommands();
            long time2 = System.currentTimeMillis();
            log.info("initScript before: create connection time:{}", time2 - time);

            String pullLuaStr = IoUtil.read(new InputStreamReader(Objects.requireNonNull(this.getClass().getResourceAsStream("pullChange.lua"))));
            String pushLuaStr = IoUtil.read(new InputStreamReader(Objects.requireNonNull(this.getClass().getResourceAsStream("pushChange.lua"))));

            this.pullSha1 = redisScriptingCommands.scriptLoad(pullLuaStr.getBytes());
            this.pushSha1 = redisScriptingCommands.scriptLoad(pushLuaStr.getBytes());
            log.info("scriptLoad cost:{} pullSha1:{}, pushSha1:{}", (System.currentTimeMillis() - time2), pullSha1, pushSha1);
        }
    }

    @Override
    public Long getVersion(String redisKey) {
        String version = rt.opsForValue().get(k(redisKey, VERSION_KEY));
        if (!ObjectUtils.isEmpty(version)) {
            return Long.parseLong(version);
        }
        return 0L;
    }

    @Override
    public List<Pair<Long, String>> pullChange(String redisKey, Long localVersion) {
        // pull 返回的是List<String>
        // List(0)=version, List(1)=fstSerialBase64Str
        // List(2)=version, List(3)=fstSerialBase64Str
        // .... 以此类推
        try {
            // 1. 读取redis数据变更
            long time = System.currentTimeMillis();
            List<Object> result = rt.execute(new RedisCallback<List<Object>>() {
                @Override
                public List<Object> doInRedis(RedisConnection conn) throws DataAccessException {
                    return conn.scriptingCommands().evalSha(pullSha1, ReturnType.MULTI, 2,
                            // keys
                            tb(k(redisKey, VERSION_KEY)), tb(k(redisKey, DATA_KEY)),
                            // argv
                            tb(localVersion), tb(reloadAllFlag));
                }
            });
            if (log.isDebugEnabled()) {
                log.info("pullChange key:{} cost:{}, result:{}", redisKey, System.currentTimeMillis() - time, result);
            }
            if (ObjectUtils.isEmpty(result)) {
                return Collections.emptyList();
            }

            // 2. 拿到数据变更 返回数据
            int forSize = result.size() / 2;
            List<Pair<Long, String>> res = new ArrayList<>(result.size() / 2);
            for (int i = 0; i < forSize; i++) {
                Object version = result.get(i * 2);
                Object fstSerialBase64Str = result.get(i * 2 + 1);
                Long versionL = c2l(version);
                String fstSerialBase64StrS = c2s(fstSerialBase64Str);
                if (ObjectUtils.isEmpty(versionL) || ObjectUtils.isEmpty(fstSerialBase64StrS)) {
                    log.warn("pullChange error key:{} version:{} fstSerialBase64Str:{}", redisKey, version, fstSerialBase64Str);
                    // redis数据有问题 直接重新加载所有
                    Long version1 = getVersion(redisKey);
                    res.clear();
                    res.add(Pair.of(version1, reloadAllFlag));
                    return res;
                }
                res.add(Pair.of(versionL, fstSerialBase64StrS));
            }
            return res;
        }
        catch (Exception e) {
            log.error("pullChange error key:{}", redisKey, e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public Long pushChange(String redisKey, String fstSerialBase64Str) {
        // push 直接调用redis进行设置 就ok, lua中进行版本累加 不会有并发问题
        // 等到get的时候, 自动将变更进行读取
        try {
            long time = System.currentTimeMillis();
            // RedisTemplate 自动关闭连接 无需人为操作
            Long v = rt.execute(new RedisCallback<Long>() {
                @Override
                public Long doInRedis(RedisConnection conn) throws DataAccessException {
                    return conn.scriptingCommands().evalSha(pushSha1, ReturnType.INTEGER, 2,
                            // keys
                            tb(k(redisKey, VERSION_KEY)), tb(k(redisKey, DATA_KEY)),
                            // argv
                            tb(fstSerialBase64Str), tb(HOT_CACHE_MAX_TTL));
                }
            });
            if (log.isDebugEnabled()) {
                log.info("pushChange key:{} cost:{}, result:{}", redisKey, System.currentTimeMillis() - time, v);
            }
            return v;
        }
        catch (Exception e) {
            log.error("pushChange error key:{}", redisKey, e);
            throw new RuntimeException(e);
        }
    }

    private static Long c2l(Object version) {
        if (version == null) {
            return null;
        }
        if (version instanceof byte[]) {
            // 将字节数组转换成数字
            byte[] bs = (byte[]) version;
            long x = 0;
            for (int i = 0; i < bs.length; i++) {
                x += (long) ((bs[i] - '0') * Math.pow(10, bs.length - i - 1));
            }
            return x;
        }
        return Long.parseLong(version.toString());
    }

    private static String c2s(Object base64) {
        if (base64 == null) {
            return null;
        }
        return new String((byte[]) base64);
    }

    private String k(String redisKey, String vd) {
        return slotPrefix + redisKey + slotSuffix + vd;
    }

    private byte[] tb(Object o) {
        if (o instanceof String) {
            return ((String) o).getBytes();
        }
        else if (o instanceof Number) {
            return String.valueOf(o).getBytes();
        }
        throw new RuntimeException("unsupported type:" + o.getClass());
    }

    public static HotCacheRedisCluster getInstance() {
        while (instance == null) {
            try {
                intiLatch.await();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        return instance;
    }
}
