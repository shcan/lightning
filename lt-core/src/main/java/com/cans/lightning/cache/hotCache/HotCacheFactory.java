package com.cans.lightning.cache.hotCache;

import com.cans.lightning.cache.hotCache.proxy.HotCacheInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.framework.Advised;
import org.springframework.aop.support.AopUtils;
import org.springframework.aop.target.SingletonTargetSource;
import org.springframework.cglib.proxy.Enhancer;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 热点缓存工厂  所有缓存均会由此单例工厂创建缓存对象
 */
@Slf4j
public class HotCacheFactory {

    private static volatile HotCacheFactory instance;

    private Map<String, HotCacheInterceptor> registerHotCache = new ConcurrentHashMap<>();

    /**
     * 创建热点缓存代理
     * @param target 目标热点缓存
     * @param key 缓存key
     * @return 代理对象
     * @param <T>
     */
    public <T> T createHotCacheProxy(T target, String key) {
        if (!(target instanceof HotCache)) {
            log.warn("target is not HotCache instance. {}", target.getClass().getName());
            return target;
        }

        // 非代理对象
        if (!AopUtils.isAopProxy(target)) {
            return normalCreateHotCacheProxy(target, key);
        }
        return proxyCreateHotCacheProxy(target, key);
    }

    private <T> T normalCreateHotCacheProxy(T target, String key) {
        HotCacheInterceptor hotCacheInterceptor = new HotCacheInterceptor((HotCache) target);

        // 缓存信息注册本身
        register2Self(hotCacheInterceptor);
        // 注册到 Mbean
        register2Mbean(hotCacheInterceptor);

        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(target.getClass());
        enhancer.setCallback(hotCacheInterceptor);
        log.info("createHotCacheProxy success. {}, key {}", target.getClass().getName(), key);
        return (T) enhancer.create();
    }

    private <T> T proxyCreateHotCacheProxy(T target, String key) {
        if (!(target instanceof Advised)) {
            throw new RuntimeException("target is not Advised instance. " + target.getClass().getName());
        }
        try {
            Advised advised = (Advised) target;
            Object target1 = advised.getTargetSource().getTarget();
            target1 = normalCreateHotCacheProxy(target1, key);
            advised.setTargetSource(new SingletonTargetSource(target1));
            return (T) target1;
        }
        catch (Exception e) {
            log.error("proxyCreateHotCacheProxy error. {}", e.getMessage());
            throw new RuntimeException("proxyCreateHotCacheProxy error. " + e.getMessage());
        }
    }

    private synchronized void register2Mbean(HotCacheInterceptor hotCacheInterceptor) {

        try {
            MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
            ObjectName oName = new ObjectName("com.cans.HotCache:" + this.getClass().getSimpleName() + ",cacheName:" + hotCacheInterceptor.getCacheKey());
            mBeanServer.registerMBean(new HotCacheMbean(), oName);
        }
        catch (Exception e) {
            log.error("register2Mbean error. {}", e.getMessage());
        }
    }

    private synchronized void register2Self(HotCacheInterceptor hotCacheInterceptor) {
        if (registerHotCache.get(hotCacheInterceptor.getCacheKey()) != null) {
            log.warn("registerHotCache already exist. {}", hotCacheInterceptor.getCacheKey());
            throw new RuntimeException("registerHotCache already exist. " + hotCacheInterceptor.getCacheKey());
        }
        registerHotCache.put(hotCacheInterceptor.getCacheKey(), hotCacheInterceptor);
    }

    public static HotCacheFactory getInstance() {
        if (instance == null) {
            synchronized (HotCacheFactory.class) {
                if (instance == null) {
                    instance = new HotCacheFactory();
                }
            }
        }
        return instance;
    }

}
