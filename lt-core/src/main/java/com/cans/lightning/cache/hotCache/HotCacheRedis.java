package com.cans.lightning.cache.hotCache;

import com.cans.lightning.base.dto.Pair;

import java.util.List;

/**
 * 热点缓存 redis实用工具
 */
public interface HotCacheRedis {

    String VERSION_KEY = ":v";

    String DATA_KEY = ":d";

    // 10分钟
    long HOT_CACHE_MAX_TTL = 10 * 60L;

    /**
     * 获取当前缓存的版本
     * @param redisKey redisKey
     * @return 版本
     */
    Long getVersion(String redisKey);

    /**
     * 获取当前缓存的变更记录
     * @param redisKey redisKey
     * @return 变更记录
     */
    List<Pair<Long, String>> pullChange(String redisKey, Long localVersion);

    /**
     * 推送变更记录
     * @param redisKey redisKey
     * @param fstSerialBase64Str fst序列号 base64字符串
     */
    Long pushChange(String redisKey, String fstSerialBase64Str);
}
