package com.cans.lightning.cache;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class CacheActionQueue<K extends Serializable> implements Serializable {

    List<CacheChangeAction<K, K>> changeList = new CopyOnWriteArrayList<>();

    public void add(CacheChangeAction<K, K> cacheChangeAction) {
        changeList.add(cacheChangeAction);
    }

    public List<CacheChangeAction<K, K>> getChangeList() {
        return changeList;
    }

    public boolean isEmpty() {
        return changeList.isEmpty();
    }
}
