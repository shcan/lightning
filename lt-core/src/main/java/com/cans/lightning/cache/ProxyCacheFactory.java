package com.cans.lightning.cache;

import java.io.Serializable;

/**
 * 缓存创建接口
 */
public interface ProxyCacheFactory<T extends CacheContext> {

    /**
     * 创建缓存
     * @param creatContext 创建缓存的上下文参数
     * @return 创建后的缓存对象
     */
   ProxyCache<? extends Serializable, ? extends Serializable> createProxyCache(T creatContext);
}
