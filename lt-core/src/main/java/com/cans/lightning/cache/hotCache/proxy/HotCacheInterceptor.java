package com.cans.lightning.cache.hotCache.proxy;

import com.cans.lightning.base.dto.BaseDto;
import com.cans.lightning.base.dto.Pair;
import com.cans.lightning.cache.CacheActionQueue;
import com.cans.lightning.cache.CacheChangeAction;
import com.cans.lightning.cache.CacheSerialUtil;
import com.cans.lightning.cache.enums.ActionType;
import com.cans.lightning.cache.hotCache.HotCache;
import com.cans.lightning.cache.hotCache.HotCacheRedisCluster;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.optional.qual.Present;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.util.ClassUtils;
import org.springframework.util.ObjectUtils;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.StampedLock;
import java.util.stream.Collectors;

/**
 * 热点缓存代理
 */
@Slf4j
public class HotCacheInterceptor<T extends HotCache> implements MethodInterceptor {

    private final static String ADD_METHOD = "add";
    private final static String GET_METHOD = "get";
    private final static String GET_BATCH_METHOD = "getBatch";
    private final static String FIND_METHOD = "find";
    private final static String UPDATE_METHOD = "update";
    private final static String UPDATE_BATCH_METHOD = "updateBatch";
    private final static String UPDATE_CHANGE_METHOD = "updateChange";
    private final static String REMOVE_METHOD = "remove";
    private final static String REMOVE_BATCH_METHOD = "removeBatch";

    private T hotCache;

    private final AtomicBoolean isInit = new AtomicBoolean(false);

    private final StampedLock lock = new StampedLock();

    private HotCacheCounter hotCacheCounter;

    private AtomicLong localVersion = new AtomicLong();

    /**
     * 如果开启了事务, 事务提交之后才做变更
     */
    private final ThreadLocal<CacheActionQueue> transactionCommit = new ThreadLocal<>();

    /**
     * 如果不需要存储到redis  将值清空
     */
    private boolean needStoreObj2Redis = false;

    @Getter
    private String cacheDefinition;
    @Getter
    private String cacheKey;

    public HotCacheInterceptor(T hotCache) {
        this.hotCache = hotCache;
        this.hotCacheCounter = new HotCacheCounter();
        this.cacheDefinition = hotCache.getCacheDefine();
        this.cacheKey = hotCache.getKey();
        // 执行缓存的初始化 加载数据
        initCacheIfNecessary();
    }

    public HotCacheInterceptor(T hotCache, boolean needStoreObj2Redis) {
        this(hotCache);
        this.needStoreObj2Redis = needStoreObj2Redis;
    }

    @Override
    public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {

        String methodName = method.getName();
        switch (methodName) {
            case ADD_METHOD:
                dealAddData((Serializable) obj);
                hotCacheCounter.getAddCount().increment();
                return null;
            case GET_METHOD:
                FIND_METHOD:
                GET_BATCH_METHOD:
                dealGetData(args, proxy, methodName);
                hotCacheCounter.getReadCount().increment();
                return null;
            case UPDATE_METHOD:
                dealUpdateData((Serializable) args[0], (Serializable) args[1]);
                hotCacheCounter.getUpdateCount().increment();
                return null;
            case UPDATE_BATCH_METHOD:
                dealUpdateBatchData((List<Pair<Serializable, Serializable>>) args[0]);
                hotCacheCounter.getUpdateCount().increment();
                return null;
            case REMOVE_METHOD:
                dealRemoveData((Serializable) args[0]);
                hotCacheCounter.getDeleteCount().increment();
                return null;
            case REMOVE_BATCH_METHOD:
                dealRemoveBatchData((List<Serializable>) args[0]);
                hotCacheCounter.getDeleteCount().increment();
                return null;
        }
        return null;
    }

    /// 人口方法start ====================
    private Object dealGetData(Object[] args, MethodProxy proxy, String methodName) {

        // 检查redis数据变化情况
        checkDataChangeFromRedis();

        Object result = null;
        long stamp = lock.tryOptimisticRead();
        if (stamp != 0) {
            result = doGetNow(args, proxy, methodName);
        }

        // if !lock.validate(stamp) == false, 说明有线程加了写锁, 则加读锁获取, 或者等待写锁
        // 一般不会发生此情况 只有在 checkDataChangeFromRedis()的时候, 会加锁
        if (stamp == 0L || !lock.validate(stamp)) {
            try {
                int retry = 1;
                stamp = lock.tryReadLock(1000L, TimeUnit.MILLISECONDS);
                while (stamp == 0) {
                    if (retry > 5) {
                        log.warn("retry time out retry:{} cacheKey:{}", retry, cacheKey);
                        throw new RuntimeException("retry time out " + retry + " key:" + cacheKey);
                    }
                    stamp = lock.tryReadLock(1000L, TimeUnit.MILLISECONDS);
                    retry++;
                }
                result = doGetNow(args, proxy, methodName);
            }
            catch (Exception e) {
                log.error("dealGetData error", e);
                throw new RuntimeException(e);
            }
            finally {
                if (lock.validate(stamp)) {
                    lock.unlockRead(stamp);
                }
            }
        }
        return result;
    }

    private void dealAddData(Serializable obj) {
        doUpdateAction(Lists.newArrayList(CacheChangeAction.builder().actionType(ActionType.Add).key(key(obj)).newV(obj).build()));
    }

    private void dealUpdateData(Serializable oldV, Serializable newV) {
        doUpdateAction(Lists.newArrayList(CacheChangeAction.builder().actionType(ActionType.Update).key(key(oldV)).oldV(oldV).newV(newV).build()));
    }

    private void dealUpdateBatchData(List<Pair<Serializable, Serializable>> updateList) {
        if (ObjectUtils.isEmpty(updateList)) {
            return;
        }
        doUpdateAction(updateList.stream().map(pair -> CacheChangeAction.builder().actionType(ActionType.Update).key(key(pair.getK())).oldV(pair.getK()).newV(pair.getV()).build()).collect(Collectors.toList()));
    }

    private void dealRemoveData(Serializable arg) {
        doUpdateAction(Lists.newArrayList(CacheChangeAction.builder().actionType(ActionType.Remove).key(key(arg)).build()));
    }

    private void dealRemoveBatchData(List<Serializable> arg) {
        if (ObjectUtils.isEmpty(arg)) {
            return;
        }
        doUpdateAction(arg.stream().map(obj -> CacheChangeAction.builder().actionType(ActionType.Remove).key(key(obj)).build()).collect(Collectors.toList()));
    }

    /// 人口方法end ====================

    private void checkDataChangeFromRedis() {
        // 一个线程检查一次
        // ....
        long localVersion = this.localVersion.get();
        Long redisVersion = HotCacheRedisCluster.getInstance().getVersion(cacheKey);
        // 1. 版本相同 不用做变更
        if (localVersion == redisVersion) {
            return;
        }
        // 2. 本地版本>redis版本的时候  直接重新加载
        else if (localVersion > redisVersion) {
            long stamp = lock.writeLock();
            try {
                doReloadNow();
            }
            catch (Exception e) {
                log.error("checkDataChangeFromRedis error key:{}", cacheKey, e);
                throw new RuntimeException(e);
            }
            finally {
                if (lock.validate(stamp)) {
                    lock.unlock(stamp);
                }
            }
        }

        // 3. redisVersion > localVersion 需要拉取redis的变更数据
        long time = System.currentTimeMillis();
        long stamp = -1;
        try {
            stamp = lock.writeLock();
            long oldVersion = this.localVersion.get();
            List<Pair<Long, String>> changeList = HotCacheRedisCluster.getInstance().pullChange(cacheKey, oldVersion);
            if (ObjectUtils.isEmpty(changeList)) {
                log.warn("checkDataChangeFromRedis no change key:{} localV:{}, redisV:{}", cacheKey, oldVersion, redisVersion);
                return;
            }
            // 检测是否 reload
            if (changeList.size() == 1) {
                if (HotCacheRedisCluster.reloadAllFlag.equals(changeList.get(0).getV())) {
                    log.info("checkDataChangeFromRedis reloadAll key:{}", cacheKey);
                    doReloadNow();
                    this.localVersion.set(changeList.get(0).getK());
                    return;
                }
            }

            long newVersion = -1L;
            // 正常变更数据变化
            try {
                for (Pair<Long, String> pair : changeList) {
                    Long version = pair.getK();
                    String fstSerialBase64Str = pair.getV();
                    if (version == null || ObjectUtils.isEmpty(fstSerialBase64Str)) {
                        log.warn("checkDataChangeFromRedis error key:{} version:{} fstSerialBase64Str:{}", cacheKey, version, fstSerialBase64Str);
                        continue;
                    }
                    // 1. 获取redis数据
                    CacheActionQueue<Serializable> action = (CacheActionQueue) CacheSerialUtil.deserialize2_base64(fstSerialBase64Str);
                    newVersion = version;
                    doUpdateNow(action);
                }
                this.localVersion.set(newVersion);
            }
            catch (Exception e) {
                log.error("checkDataChangeFromRedis error key:{} ", cacheKey, e);
                throw e;
            }
            if (log.isInfoEnabled()) {
                log.info("checkDataChangeFromRedis key:{} cost:{}ms", cacheKey, System.currentTimeMillis() - time);
            }
        }
        catch (Exception e) {
            log.warn("checkDataChangeFromRedis error key:{}", cacheKey, e);
            throw new RuntimeException(e);
        }
        finally {
            if (lock.validate(stamp)) {
                lock.unlock(stamp);
            }
        }
    }

    private Object doGetNow(Object[] objects, MethodProxy methodProxy, String methodName) {
        try {
            Object result = methodProxy.invoke(hotCache, objects);
            // 如果当前线程在事务中做了变更, 则需要获取 transactionCommit 里面的值来决定是否拿到值
            if (transactionCommit.get() != null) {
                return hotCache.mergeTransactionData(result, transactionCommit.get().getChangeList(), methodName, objects);
            }
            return result;
        }
        catch (Throwable e) {
            log.error("doGetNow error key:{}", cacheKey, e);
            throw new RuntimeException(e);
        }
    }

    private Serializable key(Object o) {
        if (o == null) {
            return null;
        }
        if (o instanceof String || ClassUtils.isPrimitiveOrWrapper(o.getClass())) {
            return (Serializable) o;
        }
        if (o instanceof BaseDto) {
            return (Serializable) ((BaseDto) o).getId();
        }
        throw new RuntimeException("key not support type:" + o.getClass());
    }

    /**
     * 所有更新的变更 都需要先走这里变更
     * @param actions 变更列表
     */
    private void doUpdateAction(List<CacheChangeAction> actions) {
        CacheActionQueue queue = transactionCommit.get();
        boolean registerSync = (queue == null);
        if (registerSync) {
            queue = new CacheActionQueue();
            queue.getChangeList().addAll(actions);
            transactionCommit.set(queue);
        }

        // 如果开启了事务, 则事务提交之后 再进行更新热点缓存
        if (TransactionSynchronizationManager.isActualTransactionActive()
                && TransactionSynchronizationManager.isSynchronizationActive()) {
            if (registerSync) {
                // 注册提交时间
                TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronization() {
                    @Override
                    public void afterCompletion(int status) {
                        if (status == TransactionSynchronization.STATUS_COMMITTED) {
                            updateActionAbstract(transactionCommit.get());
                        }
                    }
                });

            }
        }
        else {
            updateActionAbstract(queue);
        }
    }

    private void updateActionAbstract(CacheActionQueue<Serializable> actionQueue) {
        // 未开启事务
        try {
            doUpdate(actionQueue);
        }
        catch (Exception e) {
            log.error("doUpdateAction error key:{}", cacheKey, e);
            throw new RuntimeException(e);
        }
        finally {
            transactionCommit.remove();
        }
    }

    private void doUpdate(CacheActionQueue<Serializable> actionQueue) {
        // 1. 更新本地
        // 其实不用更新本地, 直接将值放入 redis 后续 checkDataChangeFromRedis() 再做更新
        // ...

        // 2. 更新redis
        long time = System.currentTimeMillis();
        try {
            if (!this.needStoreObj2Redis) {
                actionQueue.getChangeList().forEach(a -> {
                    a.setOldV(null);
                    a.setNewV(null);
                });
                HotCacheRedisCluster.getInstance().pushChange(cacheKey, CacheSerialUtil.serialize2_base64(actionQueue));
            }
        }
        catch (Exception e) {
            log.error("doUpdate error key:{}", cacheKey, e);
            throw new RuntimeException(e);
        }
        finally {
            if (log.isInfoEnabled()) {
                log.info("doUpdate key:{} cost:{}ms", cacheKey, System.currentTimeMillis() - time);
            }
        }
    }

    private void doUpdateNow(CacheActionQueue<Serializable> actionQueue) {
        if (ObjectUtils.isEmpty(actionQueue.getChangeList())) {
            return;
        }
        if (log.isInfoEnabled()) {
            log.info("doUpdateNow key:{} actionList:{}", cacheKey, actionQueue.getChangeList().stream().map(a -> a.getKey() + "_" + a.getActionType()).collect(Collectors.toList()));
        }
        List<List<CacheChangeAction<Serializable, Serializable>>> partition = Lists.partition(actionQueue.getChangeList(), 100);
        for (List<CacheChangeAction<Serializable, Serializable>> action : partition) {
            hotCache.updateChange(action);
        }
    }

    private void doReloadNow() {
        if (log.isDebugEnabled()) {
            log.info("doReloadNow key:{}", cacheKey);
        }
        long start = System.currentTimeMillis();
        Long redisVersion = HotCacheRedisCluster.getInstance().getVersion(cacheKey);
        try {
            hotCache.reload();
        }
        catch (Exception e) {
            log.error("doReloadNow error key:{}", cacheKey, e);
            throw new RuntimeException(e);
        }
        finally {
            long cost = System.currentTimeMillis() - start;
            hotCacheCounter.setLastReloadTime(cost);

            localVersion.set(redisVersion);
            if (log.isDebugEnabled()) {
                log.info("doReloadNow key:{} cost:{}", cacheKey, cost);
            }
        }
    }


    private void initCacheIfNecessary() {
        if (isInit.get()) {
            return;
        }
        CompletableFuture.runAsync(() -> {
            long stamp = lock.writeLock();
            try {
                if (isInit.get()) {
                    return;
                }
                long start = System.currentTimeMillis();

                hotCache.init();
                isInit.set(true);
                hotCacheCounter.setLastReloadTime(System.currentTimeMillis() - start);
            }
            finally {
                lock.unlock(stamp);
            }
        });
    }
}
