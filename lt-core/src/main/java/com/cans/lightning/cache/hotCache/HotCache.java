package com.cans.lightning.cache.hotCache;

import com.cans.lightning.base.dto.Pair;
import com.cans.lightning.cache.CacheActionQueue;
import com.cans.lightning.cache.CacheChangeAction;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 热点缓存抽象接口, 所有热点缓存需要实现此接口, 此接口的实例会创建 Cglib代理
 * 然后被 com.cans.lightning.cache.hotCache.proxy.HotCacheProxy 进行代理
 * 注意: 使用guava的本地缓存, 需要实现此接口HotCache后, 自己创建guava缓存的字段, 然后自定义存储逻辑
 */
public interface HotCache<K extends Serializable, V extends Serializable> {

    /////////////// 数据定义接口
    /**
     * 获取缓存
     * @param key key
     * @return value
     */
    V get(K key);

    /**
     * 批量获取
     * @param keys keys
     * @return Map
     */
    Map<K, V> getBatch(Collection<K> keys);

    /**
     * 暴露给 com.cans.lightning.cache.hotCache.HotProxyCache
     * 调用到此位置 其实是走的 cglib代理 拦截
     * 然后调用 HotProxyCache#updateChange
     * @param k k
     * @param v v
     */
    default void update(K k, V v) {}

    /**
     * 更新缓存,
     * 注意: 由cglib代理对象拼接数据, 执行具体的更新方法
     * @param cacheChangeActions 缓存更新事件
     */
    void updateChange(List<CacheChangeAction<K, V>> cacheChangeActions);

    /**
     * 批量更新缓存
     * @param updateList 更新的list
     * @deprecated HotCache#updateChange instead
     * @see HotCache#updateChange instead
     */
    @Deprecated
    default void updateBatch(List<Pair<K, V>> updateList) {}

    /**
     * 删除缓存
     * @param key 移除的key
     * @deprecated HotCache#updateChange instead
     * @see HotCache#updateChange instead
     */
    @Deprecated
    default void remove(K key) {}

    /**
     * 批量移除
     * @param keys 移除的keys
     * @deprecated HotCache#updateChange instead
     * @see HotCache#updateChange instead
     */
    @Deprecated
    default void removeBatch(Collection<K> keys) {}

    /**
     * 重新加载缓存
     */
    void reload();

    default void init(){}


    default Object mergeTransactionData(Object result,
                                      List<CacheChangeAction<Serializable, Serializable>> transactionObj,
                                      String methodName,
                                      Object[] args) {
        return result;
    }

    /////////////// 信息定义接口

    /**
     * 缓存的key
     * 此key == redis中的key
     */
    String getKey();

    /**
     * 此缓存的描述
     */
    String getCacheDefine();
}
