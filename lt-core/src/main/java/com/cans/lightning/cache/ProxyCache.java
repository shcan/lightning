package com.cans.lightning.cache;

import com.cans.lightning.base.dto.Pair;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 所有其他缓存都基于此接口
 * 1. 热点缓存
 * 2. redis缓存
 */
public interface ProxyCache <K extends Serializable, V extends Serializable> {

    /**
     * 获取缓存
     * @param key key
     * @return value
     */
    V get(K key);

    /**
     * 批量获取
     * @param keys keys
     * @return Map
     */
    Map<K, V> getBatch(Collection<K> keys);

    /**
     * 更新缓存
     * @param key 更新的key
     * @param value 更新的value
     */
    void update(K key, V value);

    /**
     * 批量更新缓存
     * @param updateList 更新的list
     */
    void updateBatch(List<Pair<K, V>> updateList);

    /**
     * 删除缓存
     * @param key 移除的key
     */
    void remove(K key);

    /**
     * 批量移除
     * @param keys 移除的keys
     */
    void removeBatch(Collection<K> keys);

    /**
     * 重新加载缓存
     */
    void reload();

}
