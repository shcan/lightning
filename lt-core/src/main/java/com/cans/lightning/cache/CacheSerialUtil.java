package com.cans.lightning.cache;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.SerializationException;
import org.apache.commons.lang3.StringUtils;
import org.nustaq.serialization.FSTConfiguration;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * 缓存序列号的专用工具类
 */
@Slf4j
public class CacheSerialUtil {

    private static final byte[] EMPTY_IN_BYTES = new byte[0];
    private static final byte[] EMPTY_OUT_BYTES = new byte[0];

    /*private static final ThreadLocal<FSTConfiguration> fstThreadLocal = new ThreadLocal<FSTConfiguration>() {
        @Override
        protected FSTConfiguration initialValue() {
            return FSTConfiguration.createDefaultConfiguration();
        }
    };*/


    /**
     * 序列化成base64
     * @param obj 原始对象
     * @return base64序列号的内容
     */
    public static String serialize2_base64(Object obj) {
        if (obj == null) {
            throw new NullPointerException("obj is null");
        }
        try {
            // FSTConfiguration fst = fstThreadLocal.get();
            ByteArrayOutputStream bs = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(bs);
            oos.writeObject(obj);
            byte[] objBytes = bs.toByteArray();
            oos.close();
            // clearFSTOutputStreamBuffer(fst);
            return Base64.encodeBase64String(objBytes);
        }
        catch (Exception e) {
            log.error("serialize2_base64 error", e);
            throw new SerializationException(e);
        }
    }

    /**
     * 反序列化成对象
     * @param base64Str redis中的base64
     * @return 序列化后的对象
     */
    public static Object deserialize2_base64(String base64Str) {
        if (StringUtils.isBlank(base64Str)) {
            return base64Str;
        }
        try {
            if (base64Str.length() % 4 == 0 && Base64.isBase64(base64Str)) {
                // FSTConfiguration fst = fstThreadLocal.get();
                byte[] objBytes = Base64.decodeBase64(base64Str);
                ObjectInputStream ooi = new ObjectInputStream(new ByteArrayInputStream(objBytes));
                Object object = ooi.readObject();
                ooi.close();
                // clearFSTInStreamBuffer(fst);
                return object;
            }
        }
        catch (Exception e) {
            log.error("deserialize2_base64 error", e);
            throw new SerializationException(e);
        }
        return base64Str;
    }


    private static void clearFSTOutputStreamBuffer(FSTConfiguration configuration) {
        configuration.getObjectOutput(EMPTY_OUT_BYTES);
        configuration.clearCaches();
    }

    private static void clearFSTInStreamBuffer(FSTConfiguration configuration) {
        configuration.getObjectOutput(EMPTY_IN_BYTES);
        configuration.clearCaches();
    }
}
