package com.cans.lightning.cache.hotCache;

import com.cans.lightning.base.dto.Pair;
import com.cans.lightning.cache.ProxyCache;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 热点缓存代理
 * 表单的FormBean使用此热点缓存
 */
public class HotProxyCache<K extends Serializable, V extends Serializable> implements ProxyCache<K, V> {

    private HotCache<K, V> target;

    public HotProxyCache(HotCache<K, V> target) {
        this.target = target;
    }

    @Override
    public V get(K key) {
        return target.get(key);
    }

    @Override
    public Map<K, V> getBatch(Collection<K> keys) {
        return target.getBatch(keys);
    }

    @Override
    public void update(K key, V value) {
        target.update(key, value);
    }

    @Override
    public void updateBatch(List<Pair<K, V>> updateList) {
        target.updateBatch(updateList);
    }

    @Override
    public void remove(K key) {
        target.remove(key);
    }

    @Override
    public void removeBatch(Collection<K> keys) {
        target.removeBatch(keys);
    }

    @Override
    public void reload() {
        target.reload();
    }
}
