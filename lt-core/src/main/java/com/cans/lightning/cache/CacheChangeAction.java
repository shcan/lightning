package com.cans.lightning.cache;

import com.cans.lightning.cache.enums.ActionType;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 缓存变更
 */
@Setter
@Getter
@Builder
public class CacheChangeAction<K extends Serializable, V extends Serializable> implements Serializable{

    private static final long serialVersionUID = 1L;

    private K key;

    private V oldV;

    private V newV;

    private ActionType actionType;
}
