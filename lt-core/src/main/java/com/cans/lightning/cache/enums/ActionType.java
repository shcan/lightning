package com.cans.lightning.cache.enums;

public enum ActionType {

    Add("add"),
    Update("update"),
    Remove("remove");

    private String action;

    ActionType(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }

    public static ActionType getActionType(String action) {
        for (ActionType type : ActionType.values()) {
            if (type.getAction().equals(action)) {
                return type;
            }
        }
        return null;
    }
}
