package com.cans.lightning.cache.hotCache.proxy;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.concurrent.atomic.LongAdder;

@Getter
@Setter
public class HotCacheCounter {

    /**
     * 记录统计开始时间
     */
    private Date startDate = new Date();
    /**
     * 添加次数
     */
    private final LongAdder addCount = new LongAdder();
    /**
     * 删除次数
     */
    private final LongAdder deleteCount = new LongAdder();
    /**
     * 更新次数
     */
    private final LongAdder updateCount = new LongAdder();
    /**
     * 读数量
     */
    private final LongAdder readCount = new LongAdder();
    /**
     * 数据未命中
     */
    private final LongAdder missCount = new LongAdder();
    /**
     * 最近一次加载
     */
    private Long lastReloadTime;



}
