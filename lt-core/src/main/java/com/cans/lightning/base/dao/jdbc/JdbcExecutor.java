package com.cans.lightning.base.dao.jdbc;

import com.cans.lightning.base.dao.jdbc.dialect.Dialect;
import com.cans.lightning.base.dao.jdbc.dialect.DialectUtil;
import com.cans.lightning.base.exceptions.BusinessException;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterUtils;
import org.springframework.jdbc.core.namedparam.ParsedSql;

import java.sql.*;
import java.util.*;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author qpy
 * @date 2024-07-25 17:37
 * @description Jdbc原始执行便利试用类
 * 功能点: 
 *      1. 命名参数封装
 *      2. sql分页封装
 *      3. 数据库池管理 操作数据库信息
 */
@Slf4j
public class JdbcExecutor {

    /**
     * 是否自动提交
     */
    private boolean autoCommit = true;

    /**
     * 用于统计嵌套调用深度, 如果自动提交时, stackDepth=0 则关闭
     * 所有暴露的public方法都可以加上
     */
    private AtomicInteger stackDepth = new AtomicInteger(0);

    /**
     * 持有的连接
     */
    protected volatile Connection conn;

    /**
     * sql日志开关
     */
    private boolean sqlLog = false;

    /**
     * 命名参数封装成jdbc识别的?号 参数
     * 
     * @param namedSql name命名参数
     * @param params 命名参数的值
     * @return key: jdbc参数化sql, value: 参数列表
     */
    public Map.Entry<String, List<Object>> namedPrepare2JdbcPrepare(String namedSql, Map<String, Object> params) {

        MapSqlParameterSource parameterSource = new MapSqlParameterSource(params);
        ParsedSql parseSql = NamedParameterUtils.parseSqlStatement(namedSql);
        // parameterSource不传也可以
        String parameterSql = NamedParameterUtils.substituteNamedParameters(parseSql, parameterSource);
        Object[] objects = NamedParameterUtils.buildValueArray(parseSql, parameterSource, null);

        return new AbstractMap.SimpleEntry<>(parameterSql, new ArrayList<>(Arrays.asList(objects)));
    }


    /**
     * 执行分页查询
     * @param namedSql 命名sql
     * @param params 参数key value
     * @param pageInfo 分页信息
     * @return 分页查询结果
     */
    public PageInfo namedPageQuery(String namedSql, Map<String, Object> params, PageInfo pageInfo) throws BusinessException, SQLException {
        
        try {
            // 0. 嵌套调用深度 +1
            stackDepthWrite(1);

            // 1. 解析命名参数
            Map.Entry<String, List<Object>> jdbcSql = namedPrepare2JdbcPrepare(namedSql, params);

            // 2. 排序, 分组....
            //

            // 3. 分页封装
            jdbcSql = doJdbcPageWarp(jdbcSql, pageInfo);

            // 4. 获取连接, 执行sql
            execute(jdbcSql.getKey(), jdbcSql.getValue(), pageInfo);

        }
        catch (Exception e) {
            log.error("execute namedPageQuery method. error: 【{}】", e.getMessage());
            throw e;
        }
        finally {
            stackDepthWrite(-1);
            close();
        }
        return pageInfo;
    }

    /**
     * 原生sql查询操作
     * @param sql jdbc 带? 参数的sql
     * @param jdbcParams 参数值
     * @param pageInfo 分页信息, 查询后结果放入 pageInfo对象
     */
    public void execute(String sql, List<Object> jdbcParams, PageInfo pageInfo) throws SQLException {

        try {
            stackDepthWrite(1);

            // select 和 update delete insert 分开操作

            // 暂时写死 支持绝大部分SQL了
            String prefix = sql.substring(0, 6).toLowerCase();
            if (prefix.startsWith("select") || prefix.startsWith("show")) {
                query(sql, jdbcParams, pageInfo);
            }
            else if (prefix.startsWith("update") || prefix.startsWith("delete") || prefix.startsWith("insert")) {
                update(sql, jdbcParams, pageInfo);
            }
            else {
                throw new BusinessException("not support sql type, type: 【"+ prefix +"】");
            }
        }
        catch (Exception e) {
            log.error("execute method. error: 【{}】", e.getMessage());
            throw e;
        }
        finally {
            stackDepthWrite(-1);
            close();
        }
    }

    /**
     * 更新操作 INSERT | UPDATE | DELETE
     * @see JdbcExecutor#execute 建议使用此通用查询方法
     * @param sql jdbc 带? 参数的sql
     * @param jdbcParams 参数值
     * @param pageInfo 分页信息, 查询后结果放入 pageInfo对象
     */
    public void update(String sql, List<Object> jdbcParams, PageInfo pageInfo) {


    }

    /**
     * 查询操作 INSERT | UPDATE | DELETE
     * @see JdbcExecutor#execute 建议使用此通用查询方法
     * @param sql jdbc 带? 参数的sql
     * @param jdbcParams 参数值
     * @param pageInfo 分页信息, 查询后结果放入 pageInfo对象
     */
    public void query(String sql, List<Object> jdbcParams, PageInfo pageInfo) throws SQLException {

        try {
            stackDepthWrite(1);

            // 1. 优化SQL
            String optimizeSql = optimizeCountSqlIfNecessary(sql);
            // 2. 预语句创建
            PreparedStatement pst = getConnection().prepareStatement(optimizeSql);

            // 打印sql
            printSqlIfNecessary(sql, jdbcParams, pageInfo);

            // 3. 参数化设置
            doPreparedStatementSetting(pst, jdbcParams);

            // 4. 真正执行sql查询
            ResultSet rs = pst.executeQuery();

            // 5. 结果写入到 PageInfo
            pageInfo.setData(doResultSet2List(rs));
        }
        catch (Exception e) {
            log.error("query method. error: 【sql: {}】", sql);
            log.error("query method. error: 【msg: {}】", e.getMessage());
            throw e;
        }
        finally {
            stackDepthWrite(-1);
            close();
        }
    }

    /**
     * 进行参数封装
     * @param jdbcSql 带有 ? 参数的 jdbc sql
     * @param pageInfo 分页信息
     * @return
     */
    private Map.Entry<String, List<Object>> doJdbcPageWarp(Map.Entry<String, List<Object>> jdbcSql, PageInfo pageInfo) {
        // 1. 获取方言, 调用方言进行分页
        Dialect dialect = DialectUtil.getDialect();
        Map.Entry<String, List<Object>> limitSql = dialect.getLimitSql(jdbcSql.getKey(), pageInfo.getPage(), pageInfo.getPageSize());
        jdbcSql.getValue().addAll(limitSql.getValue());
        return new AbstractMap.SimpleEntry<>(limitSql.getKey(), jdbcSql.getValue());
    }


    private void stackDepthWrite(int addOrSub) {
        stackDepth.addAndGet(addOrSub);
    }

    /**
     * 如果有必要, 进行SQL优化
     * @param sql jdbc sql
     * @return 优化后的jdbc sql
     */
    private String optimizeCountSqlIfNecessary(String sql) {
        // count(*) 判断
        String judgeSql = "select count(*)";
        if (sql.length() < judgeSql.length()
                || !sql.substring(0, judgeSql.length()).toLowerCase().equals(judgeSql)) {
            return sql;
        }

        // todo 需要对 select count(*)的sql做优化
        return sql;
    }

    /**
     * 参数化设置
     * @param pst PreparedStatement
     * @param jdbcParams 参数
     */
    private void doPreparedStatementSetting(PreparedStatement pst, List<Object> jdbcParams) throws SQLException {
        if (jdbcParams == null) {
            return;
        }
        for (int i = 0; i < jdbcParams.size(); i++) {
            Object paramObj = jdbcParams.get(i);
            // 需要对Date类型进行特殊处理?
            if (paramObj instanceof java.util.Date && !(paramObj instanceof java.sql.Date)) {
                paramObj = new java.sql.Date(((java.util.Date) paramObj).getTime());
            }
            pst.setObject(i + 1, paramObj);
        }
    }

    private List<Map<String, Object>> doResultSet2List(ResultSet rs) throws SQLException {
        if (rs == null) {
            throw new BusinessException("ResultSet is null");
        }
        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            List<Map<String, Object>> result = new ArrayList<>();
            while (rs.next()) {
                Map<String, Object> row = Maps.newLinkedHashMap();
                for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                    row.put(rsmd.getColumnLabel(i), rs.getObject(i));
                }
                result.add(row);
            }
            return result;
        }
        finally {
            rs.close();
        }
    }

    /**
     * 关闭连接
     */
    public void close() {
        close(false);
    }

    public void close(boolean force) {
        if (!autoCommit && !force) {
            return;
        }
        if (stackDepth.get() == 0) {
            if (conn != null) {
                try {
                    conn.close();
                    log.info("【this hash @{}】 close {} success 【Connection hash @{}】",
                            Integer.toHexString(this.hashCode()),
                            conn.getClass().getName(),
                            Integer.toHexString(conn.hashCode()));
                    conn = null;
                }
                catch (Exception e) {
                    log.error("close connection error: 【{}】", e.getMessage());
                    throw new BusinessException(e.getMessage(), e);
                }
            }
        }
    }

    public void setConn(Connection conn) {
        if (this.conn != null) {
            close();
        }
        this.conn = conn;
    }


    public JdbcExecutor setSqlLog(boolean sqlLog) {
        this.sqlLog = sqlLog;
        return this;
    }

    /* ===================== 以下是私有方法 ========================= */
    private Connection getConnection() {
        if (conn == null) {
            // JdbcExecutor本身不支持多线程, 当然也可以在多线程使用
            // 但是还是建议使用单线程, 尽管不会出现锁的问题
            synchronized (this) {
                if (conn == null) {
                    try {
                        conn = ConnectionUtil.getConnection();
                    } catch (Exception e) {
                        log.error("get connection error: 【{}】", e.getMessage());
                        throw new BusinessException(e.getMessage(), e);
                    }
                }
            }
        }
        return conn;
    }

    private void printSqlIfNecessary(String sql, List<Object> jdbcParams, PageInfo pi) {
        if (sqlLog) {
            log.info("pageInfo:【{}】, execute sql: 【{}】", (pi != null ? pi.getPage() + "," + pi.getPageSize() : ""), sql);
            // log.info("params: 【{}】", jdbcParams);
        }
    }

}
