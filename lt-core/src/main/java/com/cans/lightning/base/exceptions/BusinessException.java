package com.cans.lightning.base.exceptions;

/**
 * @author: qpy
 * @date: 2024-01-31 17:11
 * @description:
 */
public class BusinessException extends RuntimeException {

    /** 打印到日志的code码 */
    private String      code;

    private boolean     fullPage;

    public BusinessException(String msg) {
        super(msg);
    }

    public BusinessException(String msg, Throwable throwable) {
        super(msg, throwable);
    }
}
