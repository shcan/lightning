package com.cans.lightning.base.web;

import com.cans.lightning.base.dto.IBaseDto;
import com.cans.lightning.base.dto.ResDto;
import com.cans.lightning.base.entity.BaseEntity;
import com.cans.lightning.base.service.api.IBaseService;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * WEB - 接口基类
 *
 * @author shencan
 * @date 2020/6/14 21:31
 */
public abstract class BaseController<T extends BaseEntity, D extends IBaseDto<ID>, ID> {

    @PutMapping
    public ResDto<D> save(@RequestBody D d) {
        this.getBaseService().saveOrUpdateDto(d);
        return ResDto.success(d);
    }

    @GetMapping("/{id}")
    public ResDto<D> getById(@PathVariable("id") ID id) {
        if(StringUtils.isEmpty(id)){
            return null;
        }
        return ResDto.success(this.getBaseService().getDtoById(id));
    }


    @GetMapping("/all")
    public ResDto<List<D>> findAll() {
        return ResDto.success(this.getBaseService().findAllToDto());
    }

    @DeleteMapping
    public ResDto<String> deletes(@RequestBody ID[] id) {
        this.getBaseService().deletes(id);
        return ResDto.success("删除完成");
    }

    @DeleteMapping("/{id}")
    public ResDto<String> deletes(@PathVariable("id") ID id) {
        this.getBaseService().deleteById(id);
        return ResDto.success("删除完成");
    }

    /**
     * 获取DAO实现
     *
     * @return
     */
    public abstract IBaseService<T, D, ID> getBaseService();
}
