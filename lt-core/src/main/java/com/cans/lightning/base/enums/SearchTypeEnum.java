package com.cans.lightning.base.enums;

/**
 * @author shencan
 * @date 2020/12/20 12:11
 */
public enum SearchTypeEnum {

    /**
     * 大于
     */
    GT,
    /**
     * 小于
     */
    LT,
    /**
     * 等于
     */
    EQ,
    /**
     * 模糊查询
     */
    LIKE
}
