package com.cans.lightning.base.dto;

import com.cans.lightning.base.enums.SearchTypeEnum;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author shencan
 * @date 2020/12/20 11:58
 */
@Getter
@Setter
public class SearchFilter {

    private List<SearchBean> searchBeans;

    private SearchTypeEnum searchType;

    private List<SearchFilter> searchFilters;


    public SearchFilter addSearchBean(SearchBean searchBean) {

        this.searchBeans.add(searchBean);

        return this;

    }


    public SearchFilter addSearchFilter(SearchFilter searchFilter) {

        this.searchFilters.add(searchFilter);

        return this;
    }


}
