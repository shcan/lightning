package com.cans.lightning.base.dao.jdbc.dialect;

import java.util.List;
import java.util.Map;

/**
 * @author qpy
 * @date 2024-07-26 10:50
 * @description 方言操作的抽象接口
 */
public interface Dialect {

    /**
     * 获取分页sql
     * @param sql sql
     * @param page 第几页
     * @param pageSize 每页大小
     * @return 带有分页参数的sql, 以及分页值的参数列表
     */
    Map.Entry<String, List<Object>> getLimitSql(String sql, int page, int pageSize);

}
