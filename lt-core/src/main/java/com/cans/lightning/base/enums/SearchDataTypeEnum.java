package com.cans.lightning.base.enums;

/**
 * 查询条件数据类型
 *
 * @author shencan
 * @date 2020/12/20 12:11
 */
public enum SearchDataTypeEnum {

    /**
     * 数字类型
     */
    NUMBER,
    /**
     * 日期格式
     */
    DATE_TIME,
    /**
     * 字符串
     */
    STRING,
    /**
     * boolean类型
     */
    BOOLEAN
}
