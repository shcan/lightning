package com.cans.lightning.base.entity;



import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;
import java.util.Date;

/**
 * 实体基类
 *
 * @author shencan
 * @date 2020/6/14 21:07
 */
public class BaseEntity implements Serializable {

    /**
     * 主键 uuid
     **/
   @TableId(value = "id",type = IdType.ASSIGN_ID)
    private String id;
    /**
     * 创建时间
     **/
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private Date createTime = new Date();

    /**
     * 最后修改时间
     **/
    @TableField(value = "last_change_time",fill = FieldFill.DEFAULT)
    private Date lastChangeTime = new Date();

    /**
     * 创建人
     */
//    private String createUser;
//
//
//    public String getCreateUser() {
//        return createUser;
//    }
//
//    public void setCreateUser(String createUser) {
//        this.createUser = createUser;
//    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    public Date getLastChangeTime() {
        return lastChangeTime;
    }

    public void setLastChangeTime(Date lastChangeTime) {
        this.lastChangeTime = lastChangeTime;
    }

}
