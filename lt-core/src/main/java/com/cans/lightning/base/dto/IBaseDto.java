package com.cans.lightning.base.dto;

import java.io.Serializable;

/**
 * DTO 基类
 *
 * @author shencan
 * @date 2020/12/3 09:46
 */
public interface IBaseDto<PK> extends Serializable {

    /**
     * 获取ID
     * @return
     */
    PK getId();

    /**
     * 设置ID
     * @param id
     */
    void setId(PK id);

}
