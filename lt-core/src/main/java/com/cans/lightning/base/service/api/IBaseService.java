package com.cans.lightning.base.service.api;

import java.util.List;

/**
 * 基础服务 - 基类
 *
 * @author shencan
 * @date 2020/6/14 21:05
 */
public interface IBaseService<T,D,ID> {

    /**
     * 保存或更新
     *
     * @param d
     * @return
     */
    void saveOrUpdateDto(D d) ;

    /**
     * 保存或更新
     *
     * @param t
     * @return
     */
    void saveOrUpdate(T t);


    /**
     * 保存或更新
     *
     * @param t
     * @return
     */
    void insertList(List<T> t);

    /**
     * 保存或更新
     *
     * @param t
     * @return
     */
    void insertDtoList(List<D> d);

    /**
     * 通过ID获取
     *
     * @param id
     * @return
     */
    T getById(ID id);

    /**
     * 通过ID获取
     *
     * @param id
     * @return
     */
    D getDtoById(ID id);

    /**
     * 通过ID获取
     *
     * @param ids
     * @return
     */
    List<T> getByIds(List<ID> ids);

    /**
     * 通过ID删除
     *
     * @param id
     */
    void deleteById(ID id);

    /**
     * 批量删除
     *
     * @param ids id集合
     */
    void deletes(ID[] ids);


    /**
     * 通过bean删除
     *
     * @param t
     */
    void delete(T t);

    /**
     * 查询所有
     *
     * @return
     */
    List<T> findAll();

    /**
     * 查询所有
     *
     * @return
     */
    List<D> findAllToDto();

    D toDto(T t);

    T toEntity(D d);

}
