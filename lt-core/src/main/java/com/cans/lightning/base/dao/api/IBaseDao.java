package com.cans.lightning.base.dao.api;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cans.lightning.base.entity.BaseEntity;


/**
 * @author cans
 * @date 2022/10/30
 **/
public interface IBaseDao<E extends BaseEntity> extends BaseMapper<E> {

}
