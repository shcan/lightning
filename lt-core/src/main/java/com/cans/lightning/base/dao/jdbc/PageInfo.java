package com.cans.lightning.base.dao.jdbc;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author qpy
 * @date 2024-07-26 10:45
 * @description
 */
@Setter
@Getter
@Accessors(chain = true)
public class PageInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private int page = 1;
    private int pageSize = 20;
    private List<Map<String, Object>> data = Lists.newArrayList();
    /**
     * 查询结果后的总数
     */
    private int total;

    /**
     * 自定义查询的参数
     */
    private Map<String, Object> params;

    /**
     * 多一次总数查询
     */
    private boolean showTotal = false;
    
}
