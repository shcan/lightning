package com.cans.lightning.base.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Pair<K extends Serializable,V extends  Serializable> implements Serializable {

    private K k;
    private V v;

    public static <K extends Serializable,V extends  Serializable> Pair<K,V> of(K k, V v){
        return new Pair<>(k,v);
    }

}
