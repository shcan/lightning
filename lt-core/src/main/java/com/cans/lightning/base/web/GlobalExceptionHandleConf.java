package com.cans.lightning.base.web;

import com.cans.lightning.base.dto.ResDto;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 全局异常捕获
 *
 * @author shencan
 * @date 2020/9/20 14:23
 */
@Slf4j
@ControllerAdvice
@ResponseBody
public class GlobalExceptionHandleConf {

    /**
     * 顶级异常处理
     *
     * @param e 异常信息
     * @return
     */
    @ExceptionHandler(Exception.class)
    public ResDto handlerException(Exception e) {

        log.error("全局异常捕获: ", e);

        return ResDto.fait(e.getMessage());
    }

    @ExceptionHandler(ServletException.class)
    public ResDto handlerServletException(ServletException e) {
        log.error("handlerServletException: {}", e.getMessage(),e);
        return ResDto.fait(e.getMessage());
    }

    /**
     * 无权访问
     */
    @ExceptionHandler({AccessDeniedException.class})
    public ResDto handlerRuntimeException(HttpServletRequest servletRequest, AccessDeniedException e) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        log.warn("access denied, user:{}, ip:{}, uri:{} msg: {}", auth != null ? auth.getName() : null,
                servletRequest.getRemoteAddr(), servletRequest.getRequestURI(), e.getMessage());
        return ResDto.fait(e.getMessage());
    }
}
