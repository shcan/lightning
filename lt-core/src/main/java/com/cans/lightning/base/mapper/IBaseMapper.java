package com.cans.lightning.base.mapper;

import com.cans.lightning.base.dto.BaseDto;
import com.cans.lightning.base.entity.BaseEntity;

import java.util.List;

/**
 * @author shencan
 * @date 2020/12/3 09:45
 */
public interface IBaseMapper<E extends BaseEntity, D extends BaseDto<PK>,PK> {


    /**
     * 转换为实体
     *
     * @param d dto
     * @return
     */
    E toEntity(D d);


    /**
     * 转换为DTO
     *
     * @param e 实体
     * @return
     */
    D toDto(E e);

    /**
     * 批量装换Entity
     *
     * @param ds
     * @return
     */
    List<E> toEntitys(List<D> ds);

    /**
     * 批量转换 Dto
     *
     * @param es
     * @return
     */
    List<D> toDtos(List<E> es);
}
