package com.cans.lightning.base.dao.jdbc.dialect;

import com.cans.lightning.base.constant.DatabaseConfig;

/**
 * @author qpy
 * @date 2024-07-26 17:53
 * @description 获取Dialect实用工具
 */
public class DialectUtil {

    private static Dialect dialect;

    public static Dialect getDialect() {
        if (dialect == null) {
            dialect = doGetDialectFromConfig();
        }
        return dialect;
    }

    private static Dialect doGetDialectFromConfig() {
        // todo 数据库的环境, 后续适配 从配置获取
        DatabaseConfig.Dialect dialectEnum = DatabaseConfig.Dialect.MySQL;
        switch (dialectEnum) {
            case Oracle:
                return new OracleDialect();
            case SQL_Server:
                return new SqlServerDialect();
            case MySQL:
            default:
                return new MySQLDialect();
        }
    }
}
