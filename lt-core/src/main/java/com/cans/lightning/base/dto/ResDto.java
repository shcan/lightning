package com.cans.lightning.base.dto;

import java.io.Serializable;

/**
 * @author shencan
 * Date: 2017/10/3
 * Description: controller层传输类
 */
public class ResDto<T> implements Serializable {

    private static final int SUCCESS = 1;
    private static final int FAIT = 0;

    /**
     * 是否成功
     */
    private Integer code;

    /**
     * 错误信息
     */
    private String message;

    /**
     * 返回结构
     */
    private T data;

    public static <T> ResDto<T> success(T t) {
        ResDto<T> res = new ResDto<>();
        res.setCode(SUCCESS);
        res.setData(t);
        return res;
    }

    /**
     * 返回成功信息
     *
     * @param <T>
     * @return
     */
    public static <T> ResDto<T> success() {
        ResDto<T> res = new ResDto<>();
        res.setCode(SUCCESS);
        return res;
    }

    public static <T> ResDto<T> fait(String message) {
        ResDto<T> res = new ResDto<>();
        res.setCode(FAIT);
        res.setMessage(message);
        return res;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
