package com.cans.lightning.base.dao.jdbc.dialect;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author qpy
 * @date 2024-07-26 10:57
 * @description
 */
public class MySQLDialect implements Dialect {

    @Override
    public Map.Entry<String, List<Object>> getLimitSql(String sql, int page, int pageSize) {
        StringBuilder pageSql = new StringBuilder(sql.length() + 12).append(sql).append(" limit ?, ? ");
        List<Object> pageParam = Arrays.asList((page - 1) * pageSize, pageSize);
        return new AbstractMap.SimpleEntry<>(pageSql.toString(), pageParam);
    }
}
