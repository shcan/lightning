package com.cans.lightning.base.enums;

/**
 * @author: qpy
 * @date: 2024-02-04 16:29
 * @description:
 */
public enum CacheType {
    
    REDIS,
    LOCAL;
}
