package com.cans.lightning.base.dto;

import com.cans.lightning.base.enums.SearchDataTypeEnum;
import com.cans.lightning.base.enums.SearchTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * JPA 查询组装类
 *
 * @author shencan
 * @date 2020/12/20 10:18
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SearchBean {

    /**
     * 属性名称
     */
    private String attributeName;

    /**
     * 匹配规则
     */
    private SearchTypeEnum searchType;

    /**
     * 数据类型
     */
    private SearchDataTypeEnum dataType;

    /**
     * 属性值
     */
    private String attributeValue;


}
