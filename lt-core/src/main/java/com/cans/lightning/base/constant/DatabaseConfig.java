package com.cans.lightning.base.constant;

import lombok.Getter;

public class DatabaseConfig {

    @Getter
    public enum Dialect {
        MySQL("MySQL", "MySQL Database"),
        Oracle("Oracle", "Oracle Database"),
        SQL_Server("SQL Server", "SQL Server Database");
        String key;
        String mark;

        Dialect(String key, String mark) {
            this.key = key;
            this.mark = mark;
        }
    }
}
