package com.cans.lightning.base.dao.jdbc;

import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author qpy
 * @date 2024-07-26 17:36
 * @description 为了方便调试数据库  临时加的获取数据库连接
 */
@Slf4j
public class ConnectionUtil {
    
    private static volatile DataSource holderDataSource;

    public static Connection getConnection() throws SQLException {
        return getDataSource().getConnection();
    }
    
    
    private static DataSource getDataSource() {
        if (holderDataSource == null) {
            synchronized (ConnectionUtil.class) {
                if (holderDataSource == null) {
                    try {
                        holderDataSource = createDataSource();
                    } catch (Exception e) {
                        // 在这里处理异常，比如记录日志，并抛出自定义异常
                        log.error("Failed to create DataSource", e);
                        throw new RuntimeException("Failed to initialize DataSource", e);
                    }
                }
            }
        }
        return holderDataSource;
    }

    private static DataSource createDataSource() {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setUsername("root");
        dataSource.setPassword("123456");
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setJdbcUrl("jdbc:mysql://127.0.0.1:3306/seeyon_82sp1_0509?useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull&allowMultiQueries=true&useSSL=false&serverTimezone=UTC");
        dataSource.setMinimumIdle(2);
        dataSource.setMaximumPoolSize(5);
        return dataSource;
    }
}
