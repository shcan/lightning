package com.cans.lightning.base.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 自定义分页参数
 *
 * @author shencan
 * @date 2020/6/21 22:58
 */
@Getter
@Setter
public class MyPageRequest implements Serializable {

    private int page;

    private int size;

    private String sort;

    private String orderBy;

    /**
     * 查询字段匹配器
     */
    private SearchFilter searchFilter;
}
