package com.cans.lightning.base.dto;

import lombok.*;

import java.util.Date;

/**
 * @author shencan
 * @date 2021/1/16 22:08
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BaseDto<PK> implements IBaseDto<PK> {

    /**
     * ID
     */
    private PK id;
    /**
     * 创建时间
     **/
    private Date createTime = new Date();

    /**
     * 最后修改时间
     **/
    private Date lastChangeTime  = new Date();
}
