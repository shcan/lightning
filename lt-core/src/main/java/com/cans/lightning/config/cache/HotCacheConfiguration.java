package com.cans.lightning.config.cache;

import com.cans.lightning.cache.hotCache.HotCacheRedis;
import com.cans.lightning.cache.hotCache.HotCacheRedisCluster;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;

@Configuration
@AutoConfigureAfter(RedisAutoConfiguration.class)
public class HotCacheConfiguration {

    @Bean
    public HotCacheRedis hotCacheRedis(StringRedisTemplate stringRedisTemplate) {
        return new HotCacheRedisCluster(stringRedisTemplate);
    }
}
