package com.cans.lightning.config.system;

/**
 * 存储规则配置
 *
 * @author cans
 * @date 2021-04-22 11:11
 **/
public enum StorageTypeEnum {

    SIMPLE,
    /**
     * 默认:保存在服务器指定文件夹
     */
    DEFAULT,
    /**
     * 使用mongodb
     */
    MONGODB;
}
