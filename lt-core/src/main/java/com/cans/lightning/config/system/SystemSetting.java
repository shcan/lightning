package com.cans.lightning.config.system;

import com.cans.lightning.config.system.StorageTypeEnum;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 系统参数配置
 *
 * @author shencan
 * @date 2020/6/26 17:50
 */
@Component
@ConfigurationProperties(prefix = "lightning.system")
public class SystemSetting {

    /**
     * 文件基础目录
     */
    private String basePath;

    /**
     * 存储类型 {@link StorageTypeEnum} SIMPLE(默认): 普通放在服务器上 MONGODB: 存放在MONGODB上
     */
    private String storageType = "SIMPLE";

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    public String getStorageType() {
        return storageType;
    }

    public void setStorageType(String storageType) {
        this.storageType = storageType;
    }
}
