--
-- 将数据放入redis里面
--
local versionKey = KEYS[1];
local dataKey = KEYS[2];
local data = ARGV[1];
local dataTtl = tonumber(ARGV[2]);

redis.call("SETNX", versionKey, 0)
-- 版本+1
local addVersion = redis.call("INCR", versionKey)
-- 将变更数据+版本号 设置到redis里面
-- 并加上超时
redis.call("SETEX", dataKey .. addVersion, dataTtl, data)

-- 这个新的版本 返回没用处 先返回吧
return "" .. addVersion
