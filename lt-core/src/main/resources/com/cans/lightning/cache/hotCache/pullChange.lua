-- 读取redis缓存的变化
-- @date 2024-08-19
-- keys[1]是redis的 版本key
-- keys[2]是redis的 变化base64 key
-- argv[1] 是当前节点的缓存key
-- argv[2] 是 reloadAll的标识符
local versionKey = KEYS[1];
local dataKey = KEYS[2];
local localVersion = tonumber(ARGV[1]);
local isReloadAll = ARGV[2];

-- 获取redis的数据版本号
local redisVersion = redis.call('GET', versionKey);

-- 拼接数据返回
local result = {};
-- 数据没变化
if (redisVersion == nil) then
    return result;
end

redisVersion = tonumber(redisVersion)
-- 版本相同 数据没变化
if (redisVersion == localVersion) then
    return result;
end

-- 将redis每个版本的数据都获取
local tempVersion = localVersion;
while (tempVersion < redisVersion) do
    tempVersion = tempVersion + 1;

    -- 获取每个版本的数据变化
    local dataKeyV = dataKey .. tempVersion;
    local data = redis.call('GET', dataKeyV);

    -- 如果相差版本太多, 直接返回重新加载数据
    if (data == nil or (isReloadAll == data)) then
        local localResult = {};
        localResult[1] = "" .. redisVersion;
        localResult[2] = isReloadAll;
        return localResult;
    end

    -- 将每个版本的数据  都放入返回数据中
    local len = #result;
    result[len + 1] = "" .. tempVersion;
    result[len + 2] = data;
end

return result;






