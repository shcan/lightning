package com.cans.lightning.business.lowcode.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.cans.lightning.base.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 应用表单视图-单项
 *
 * @author cans
 * @date 2021/12/5
 **/
@Getter
@Setter
@TableName("app_form_view_item")
public class AppFormViewItem extends BaseEntity {

    /**
     * 所属应用
     */
    private String formId;

    /**
     * 所属视图
     */
    private String viewId;

    /**
     * 展示类型
     */
    private String showType;

    /**
     * 关联项
     */
    private String refObj;

    /**
     * 排序号
     */
    private Long sortId;


}
