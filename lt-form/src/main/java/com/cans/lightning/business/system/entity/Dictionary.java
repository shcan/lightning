package com.cans.lightning.business.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cans.lightning.base.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 数据字典
 *
 * @author cans
 * @date 2021-03-29 15:21
 **/
@Getter
@Setter
@TableName("sys_dictionary")
public class Dictionary extends BaseEntity {

    /**
     * 编号
     */
   @TableField("code")
    private String code;

    /**
     * 名称
     */
   @TableField("name")
    private String name;

    /**
     * 排序号
     */
   @TableField("idx")
    private Integer idx;
}
