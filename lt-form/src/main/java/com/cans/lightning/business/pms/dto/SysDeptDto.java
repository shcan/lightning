package com.cans.lightning.business.pms.dto;

import com.cans.lightning.base.dto.BaseDto;
import lombok.Getter;
import lombok.Setter;

/**
 * @author shencan
 * @date 2020/12/3 15:21
 */
@Getter
@Setter
public class SysDeptDto extends BaseDto<String> {

    private String id;
}
