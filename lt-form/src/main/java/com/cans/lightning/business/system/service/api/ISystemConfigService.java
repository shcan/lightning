package com.cans.lightning.business.system.service.api;

import com.cans.lightning.base.service.api.IBaseService;
import com.cans.lightning.business.system.dto.SystemConfigDto;
import com.cans.lightning.business.system.entity.SystemConfig;


/**
 * 系统配置
 *
 * @author shencan
 * @date 2020/6/14 20:39
 */
public interface ISystemConfigService extends IBaseService<SystemConfig,SystemConfigDto, String> {

    /**
     * 根据KDY获取实例
     * @param configKey 配置key
     * @return
     */
    SystemConfig getByKeyCode(String configKey);
}
