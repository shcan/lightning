package com.cans.lightning.business.lowcode.entity;

import com.cans.lightning.base.entity.BaseEntity;
import com.cans.lightning.business.lowcode.enums.FieldTypeEnum;
import lombok.Getter;
import lombok.Setter;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 字段信息
 *
 * @author cans
 * @date 2021/12/5
 **/
@Getter
@Setter
@TableName("app_field")
public class AppTableField extends BaseEntity {

    /**
     * 表单ID
     */
    private String formId;

    /**
     * 表格ID
     */
    private String tableId;

    /**
     * 字段名称
     */
    private String fieldName;

    /**
     * 数据库字段名
     */
    private String dbFieldName;

    /**
     * 字段描述
     */
    private String fieldDesc;

    /**
     * 字段长度
     */
    private Integer length;

    /**
     * 字段类型
     */
    private String fieldType;

    /**
     * @see FieldTypeEnum
     * 数据库字段类型
     */
    private String dbFieldType;

    /**
     * 排序号
     */
    private Long sortId;

    /**
     * 输入框宽度
     */
    private Integer width;

    /**
     * 字段索引
     */
    private Integer fieldIndex;
}
