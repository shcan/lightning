package com.cans.lightning.business.lowcode.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.cans.lightning.base.mapper.IBaseMapper;
import com.cans.lightning.base.service.impl.BaseServiceImpl;
import com.cans.lightning.business.lowcode.dao.AppFormBindDao;
import com.cans.lightning.business.lowcode.dto.AppFormBindDto;
import com.cans.lightning.business.lowcode.entity.AppFormBind;
import com.cans.lightning.business.lowcode.form.bean.FormBindBean;
import com.cans.lightning.business.lowcode.form.bean.ViewBean;
import com.cans.lightning.business.lowcode.form.bean.ViewItemBean;
import com.cans.lightning.business.lowcode.mapper.AppFormBindMapper;
import com.cans.lightning.business.lowcode.service.api.IAppFormBindService;
import jakarta.annotation.Resource;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 表单视图
 *
 * @author shencan
 * @date 2020/6/14 20:53
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class AppFormBindServiceImpl extends BaseServiceImpl<AppFormBind,  AppFormBindDto, String> implements IAppFormBindService {

    private static final Logger logger = LoggerFactory.getLogger(AppFormBindServiceImpl.class);

    @Resource
    private AppFormBindDao appFormBindDao;
    @Resource
    private AppFormBindMapper appFormBindMapper;

    @Override
    public BaseMapper<AppFormBind> getDaoImpl() {
        return appFormBindDao;
    }

    @Override
    public IBaseMapper<AppFormBind, AppFormBindDto, String> getMapperImpl() {
        return appFormBindMapper;
    }

    @Override
    public List<AppFormBindDto> findByFormId(String formId) {
        LambdaQueryWrapper<AppFormBind> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AppFormBind::getFormId, formId).orderByAsc(AppFormBind::getSortId);
        List<AppFormBind> select = appFormBindDao.selectList(queryWrapper);
        return appFormBindMapper.toDtos(select);
    }

}
