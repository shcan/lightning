package com.cans.lightning.business.lowcode.mapper;

import com.cans.lightning.base.mapper.IBaseMapper;
import com.cans.lightning.business.lowcode.dto.AppFormDto;
import com.cans.lightning.business.lowcode.entity.AppForm;
import org.mapstruct.Mapper;
import org.springframework.util.StringUtils;

/**
 * @author shencan
 * @date 2020/12/2 19:06
 */
@Mapper(componentModel = "spring")
public interface AppFormMapper extends IBaseMapper<AppForm, AppFormDto,String> {

    /**
     * 转换为Entity
     *
     * @param dto
     * @return
     */
    @Override
    AppForm toEntity(AppFormDto dto);


    /**
     * 通过id获取实体
     *
     * @param id
     * @return
     */
    default AppForm fromId(String id) {
        if (StringUtils.isEmpty(id)) {
            return null;
        }
        AppForm entity = new AppForm();
        entity.setId(id);
        return entity;
    }
}
