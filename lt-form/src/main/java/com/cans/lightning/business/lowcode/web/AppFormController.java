package com.cans.lightning.business.lowcode.web;

import com.cans.lightning.base.dto.ResDto;
import com.cans.lightning.base.service.api.IBaseService;
import com.cans.lightning.base.web.BaseController;
import com.cans.lightning.business.lowcode.dto.AppFormDto;
import com.cans.lightning.business.lowcode.entity.AppForm;
import com.cans.lightning.business.lowcode.form.bean.AppFormBean;
import com.cans.lightning.business.lowcode.service.api.IAppFormDefinitionService;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * 表单
 *
 * @author shencan
 * @date 2020/6/14 22:14
 */
@RestController
@RequestMapping("/app_form")
public class AppFormController extends BaseController<AppForm, AppFormDto, String> {

    @Autowired
    private IAppFormDefinitionService appFormService;

    @Override
    public IBaseService<AppForm, AppFormDto, String> getBaseService() {
        return appFormService;
    }

    @GetMapping("/getByAppId/{appId}")
    public ResDto getByAppId(@PathVariable("appId") String appId) {
        return ResDto.success(appFormService.getByAppId(appId));
    }

    @GetMapping("/getFormBean/{formId}")
    public ResDto<AppFormBean> getFormBean(@PathVariable("formId") String formId) {
        return ResDto.success(appFormService.getFormBean(formId));
    }

    @GetMapping("/getInfo/{id}")
    public ResDto getInfo(@PathVariable("id") String id) {
        return ResDto.success(appFormService.getInfo(id));
    }

    @PostMapping("/saveToDb")
    public ResDto saveToDb(@RequestBody AppFormDto appFormDto){
        appFormService.saveToDb(appFormDto);
        return ResDto.success();
    }

    /**
     * 获取下一个字段索引ID
     * @param formId
     * @return
     */
    @GetMapping("/getNextFieldIndex/{formId}")
    public ResDto<String> getNextFieldIndex(@PathVariable("formId") String formId) {
        return ResDto.success(appFormService.getNextFieldIndex(formId));
    }
}
