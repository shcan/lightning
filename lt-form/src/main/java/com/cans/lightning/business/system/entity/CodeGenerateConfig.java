package com.cans.lightning.business.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cans.lightning.base.entity.BaseEntity;
import com.cans.lightning.business.system.enums.ResetRuleEnum;
import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;
import java.time.ZonedDateTime;

/**
 * 编码生成规则配置
 *
 * @author cans
 * @date 2021-04-22 10:50
 **/
@Getter
@Setter
@TableName("sys_code_generate_config")
public class CodeGenerateConfig extends BaseEntity {

    /**
     * 编号
     */
    @TableField("code")
    private String code;

    /**
     * 名称
     */
    @TableField("name")
    private String name;

    /**
     * 前缀
     */
    @TableField("prefix")
    private String prefix;

    /**
     * 年
     */
    @TableField("year")
    private Integer year;

    /**
     * 月
     */
    @TableField("month")
    private Integer month;

    /**
     * 日
     */
    @TableField("day")
    private Integer day;

    /**
     * 流水号长度
     */
    @TableField("number_length")
    private Integer numberLength;

    /**
     * 重置序号规则,使用枚举见 {@link ResetRuleEnum}
     */
    @TableField("reset_rule")
    private Integer resetRule;

    /**
     * 初始号
     */
    @TableField("init_number")
    private BigInteger initNumber;

    /**
     * 当前流水号
     */
    @TableField("current_number")
    private BigInteger currentNumber;

    /**
     * 增量
     */
    @TableField("increment")
    private Integer increment;

    /**
     * 最后一次生成时间,用于重置编号
     */
    @TableField("last_generate_date_time")
    private ZonedDateTime lastGenerateDateTime;
}
