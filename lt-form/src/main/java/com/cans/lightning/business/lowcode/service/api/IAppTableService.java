package com.cans.lightning.business.lowcode.service.api;

import com.cans.lightning.base.service.api.IBaseService;
import com.cans.lightning.business.lowcode.dto.AppTableDto;
import com.cans.lightning.business.lowcode.entity.AppTable;

import java.util.List;


/**
 * 应用信息
 *
 * @author shencan
 * @date 2020/6/14 20:39
 */
public interface IAppTableService extends IBaseService<AppTable, AppTableDto, String> {

    List<AppTable> findByFormId(String id);

    List<AppTableDto> getInfoByFormId(String formId);

    void saveOrUpdate(AppTableDto appTableDto);

    AppTableDto getManTableByFormId(String formId);

    List<AppTableDto> getSonTablesByFormId(String formId);

    String getNextFieldIndex(String formId);
}
