package com.cans.lightning.business.system.dto;

import com.cans.lightning.base.dto.BaseDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 与前端ZORRO对应
 *
 * @author shencan
 * @date 2020/12/3 15:02
 */
@Getter
@Setter
public class FileInfoDto extends BaseDto<String> {

    public static String PATH = "api/file/img/";

    private String id;

    /**
     * UID
     */
    private String uid;

    /**
     * 文件名
     */
    private String name;

    /**
     * 状态
     */
    private String status;

    /**
     * 访问连接
     */
    private String url;

}
