package com.cans.lightning.business.lowcode.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.cans.lightning.base.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 应用表单表视图
 *
 * @author cans
 * @date 2021/12/5
 **/
@Getter
@Setter
@TableName("app_form_view")
public class AppFormView extends BaseEntity {

    /**
     * 所属应用
     */
    private String appId;

    /**
     * 所属表单
     */
    private String formId;

    /**
     * 视图名称
     */
    private String viewName;

    /**
     * 字段绑定信息
     */
    private String fieldBind;

    /**
     * 排序号
     */
    private Long sortId;

}
