package com.cans.lightning.business.pms.web;

import com.cans.lightning.base.dto.ResDto;
import com.cans.lightning.base.service.api.IBaseService;
import com.cans.lightning.base.web.BaseController;
import com.cans.lightning.business.pms.dto.SysMenuDto;
import com.cans.lightning.business.pms.dto.SysMenuShowDto;
import com.cans.lightning.business.pms.dto.SysMenuTreeDto;
import com.cans.lightning.business.pms.entity.SysMenu;
import com.cans.lightning.business.pms.service.api.ISysMenuService;
import org.springframework.data.domain.Page;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

/**
 * 权限 - 菜单接口
 *
 * @author shencan
 * @date 2020/6/14 22:14
 */
@RestController
@RequestMapping("/sys-menu")
public class SysMenuController extends BaseController<SysMenu, SysMenuDto, String> {

    @Autowired
    private ISysMenuService sysMenuService;

    @GetMapping("/page-list")
    public Page<SysMenuDto> pageList(Integer page, Integer size, String orderBy, String order) {
        return null;
    }


    @GetMapping("/find-children/{parentId}")
    public ResDto<List<SysMenu>> findChildren(@PathVariable("parentId") String parentId) {
        return ResDto.success(sysMenuService.getByParentId(parentId));
    }

    @PostMapping("/save")
    public ResDto<SysMenuDto> save(@RequestBody SysMenuDto sysMenuDto) {
        SysMenu sysMenu = sysMenuService.toEntity(sysMenuDto);
        if (!StringUtils.isEmpty(sysMenuDto.getParentId())) {
            SysMenu parent = sysMenuService.getById(sysMenuDto.getParentId());
            sysMenu.setParentId(parent.getId());
        }
        sysMenuService.saveOrUpdate(sysMenu);
        return ResDto.success();
    }

    @GetMapping("/menu-tree")
    public ResDto menuTree() {
        List<SysMenuTreeDto> resList = sysMenuService.getTree();
        return ResDto.success(resList);
    }

    @GetMapping("/tree-list")
    public ResDto treeList() {
        return ResDto.success(sysMenuService.tableTree());
    }

    @GetMapping("/get-user-menu-tree")
    public ResDto getUserMenuTree() {
        List<SysMenuShowDto> menuList = sysMenuService.getUserMenuTree();
        return ResDto.success(menuList);
    }


    @Override
    public IBaseService<SysMenu, SysMenuDto, String> getBaseService() {
        return sysMenuService;
    }
}
