package com.cans.lightning.business.system.service.api;

/**
 * 编号生成服务
 *
 * @author shencan
 * @date 2020/6/14 20:39
 */
public interface ICodeGenerateService {

    /**
     * 根据配置code生成编号
     *
     * @param configCode
     * @return
     */
    String generateCode(String configCode);

    String generateBySystemConfigKey(String configKey);
}
