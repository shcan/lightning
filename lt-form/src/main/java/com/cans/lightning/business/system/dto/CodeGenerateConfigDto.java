package com.cans.lightning.business.system.dto;

import com.cans.lightning.base.dto.BaseDto;
import com.cans.lightning.business.system.enums.ResetRuleEnum;
import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;

/**
 * 编号生成规则配置
 *
 * @author cans
 * @date 2021-03-29 15:21
 **/
@Getter
@Setter
public class CodeGenerateConfigDto extends BaseDto<String> {


    /**
     * 编号
     */
    private String code;

    /**
     * 名称
     */
    private String name;

    /**
     * 前缀
     */
    private String prefix;

    /**
     * 年
     */
    private Integer year;

    /**
     * 月
     */
    private Integer month;

    /**
     * 日
     */
    private Integer day;

    /**
     * 流水号长度
     */
    private Integer numberLength;

    /**
     * 重置序号规则,使用枚举见 {@link ResetRuleEnum}
     */
    private Integer resetRule;

    /**
     * 当前流水号
     */
    private BigInteger currentNumber;

    /**
     * 初始流水号
     */
    private BigInteger initNumber;

    /**
     * 增量
     */
    private Integer increment;

}
