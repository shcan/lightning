package com.cans.lightning.business.lowcode.mapper;

import com.cans.lightning.base.mapper.IBaseMapper;
import com.cans.lightning.business.lowcode.dto.AppDefinitionDto;
import com.cans.lightning.business.lowcode.entity.AppDefinition;
import org.mapstruct.Mapper;
import org.springframework.util.StringUtils;

/**
 * @author shencan
 * @date 2020/12/2 19:06
 */
@Mapper(componentModel = "spring")
public interface AppDefinitionMapper extends IBaseMapper<AppDefinition, AppDefinitionDto,String> {

    /**
     * 转换为Entity
     *
     * @param dto
     * @return
     */
    @Override
    AppDefinition toEntity(AppDefinitionDto dto);


    /**
     * 通过id获取实体
     *
     * @param id
     * @return
     */
    default AppDefinition fromId(String id) {
        if (StringUtils.isEmpty(id)) {
            return null;
        }
        AppDefinition entity = new AppDefinition();
        entity.setId(id);
        return entity;
    }
}
