package com.cans.lightning.business.lowcode.form.loader.formBean;

import com.cans.lightning.business.core.utils.ContextUtil;
import com.cans.lightning.business.lowcode.dto.AppFormViewDto;
import com.cans.lightning.business.lowcode.entity.AppFormViewItem;
import com.cans.lightning.business.lowcode.form.bean.AppFormBean;
import com.cans.lightning.business.lowcode.form.bean.ViewBean;
import com.cans.lightning.business.lowcode.form.bean.ViewItemBean;
import com.cans.lightning.business.lowcode.form.loader.AbstractBeanLoader;
import com.cans.lightning.business.lowcode.service.api.IAppFormViewService;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 加载视图信息
 *
 * @author cans
 * @date 2023/11/29
 **/
@Component
public class BeanViewLoader extends AbstractBeanLoader<AppFormBean> {

    @Autowired
    private IAppFormViewService formViewService;

    @Override
    public AppFormBean loadOne(AppFormBean appFormBean) {
        List<AppFormViewDto> formViewList = formViewService.findByFormId(appFormBean.getId());
        List<ViewBean> viewBeanList = Lists.newArrayList();
        if (CollectionUtils.isEmpty(formViewList)) {
            ViewBean viewBean = new ViewBean();
            viewBean.setId(ContextUtil.nextID());
            viewBean.setFormId(appFormBean.getId());
            viewBean.setViewName("视图1");
            appFormBean.setViewBean(viewBean);
            appFormBean.updateCurrentViewId(viewBean.getId());
            viewBeanList.add(viewBean);
            appFormBean.setViewList(viewBeanList);
            return appFormBean;
        }
        AppFormViewDto appFormView = formViewList.get(0);
        String currentViewId = appFormBean.getCurrentViewId();
        if (currentViewId == null) {
            currentViewId = appFormView.getId();
            appFormBean.updateCurrentViewId(currentViewId);
        }

        List<AppFormViewItem> viewItemList = formViewService.findItemByFormId(appFormBean.getId());
        Map<String, List<AppFormViewItem>> viewIdItemMap = viewItemList.stream().collect(Collectors.groupingBy(AppFormViewItem::getViewId));
        for (AppFormViewDto appFormViewDto : formViewList) {
            ViewBean viewBean = new ViewBean().create(appFormViewDto);
            viewBean.setViewItemList(this.getViewItemBeanList(viewIdItemMap.get(viewBean.getId())));
            viewBeanList.add(viewBean);
        }
        appFormBean.setViewList(viewBeanList);
        appFormBean.setViewBean(viewBeanList.get(0));
        return appFormBean;
    }

    private List<ViewItemBean> getViewItemBeanList(List<AppFormViewItem> viewItemList){
        List<ViewItemBean> res = Lists.newArrayList();
        if(CollectionUtils.isEmpty(viewItemList)){
            return res;
        }
        for (AppFormViewItem appFormViewItem : viewItemList) {
            ViewItemBean viewItemBean = new ViewItemBean().create(appFormViewItem);
            res.add(viewItemBean);
        }
        return res;
    }
}
