package com.cans.lightning.business.lowcode.dto;

import com.cans.lightning.base.dto.BaseDto;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 应用表单信息
 *
 * @author cans
 * @date 2021/12/5
 **/
@Getter
@Setter
public class AppFormDto extends BaseDto<String> {

    /**
     * 所属应用
     */
    private String appId;

    /**
     * 表单名称
     */
    private String formName;

    /**
     * 字段游标
     */
    private Integer fieldIndex;

    /**
     * 明细表信息
     */
    private List<AppTableDto> sonTables;

    /**
     * 主表信息
     */
    private AppTableDto mainTable;
}
