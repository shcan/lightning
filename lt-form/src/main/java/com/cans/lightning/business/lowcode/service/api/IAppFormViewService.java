package com.cans.lightning.business.lowcode.service.api;

import com.cans.lightning.base.service.api.IBaseService;
import com.cans.lightning.business.lowcode.dto.AppFormViewDto;
import com.cans.lightning.business.lowcode.entity.AppFormView;
import com.cans.lightning.business.lowcode.entity.AppFormViewItem;
import com.cans.lightning.business.lowcode.form.bean.ViewBean;

import java.util.List;


/**
 * 表单视图
 *
 * @author shencan
 * @date 2020/6/14 20:39
 */
public interface IAppFormViewService extends IBaseService<AppFormView, AppFormViewDto, String> {

    List<AppFormViewDto> findByFormId(String id);

    void saveOrUpdate(ViewBean viewBean);

    void saveOrUpdate(AppFormViewItem appFormViewItem);

    List<AppFormViewItem> findItemByFormId(String formId);
}
