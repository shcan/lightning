package com.cans.lightning.business.lowcode.form.loader.formBean;

import com.cans.lightning.business.lowcode.dto.AppFormDto;
import com.cans.lightning.business.lowcode.form.bean.AppFormBean;
import com.cans.lightning.business.lowcode.form.loader.AbstractBeanLoader;
import com.cans.lightning.business.lowcode.service.api.IAppFormDefinitionService;
import org.springframework.stereotype.Component;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * formBean 基础信息加载
 *
 * @author cans
 * @date 2023/11/29
 **/
@Component
public class BeanBaseLoader extends AbstractBeanLoader<AppFormBean> {

    @Autowired
    private IAppFormDefinitionService formDefinitionService;

    @Override
    public AppFormBean loadOne(AppFormBean appFormBean) {
        AppFormDto appFormDto = formDefinitionService.getDtoById(appFormBean.getId());
        appFormBean.setAppId(appFormDto.getAppId());
        appFormBean.setFormName(appFormDto.getFormName());
        appFormBean.setFieldIndex(appFormDto.getFieldIndex());
        return appFormBean;
    }
}
