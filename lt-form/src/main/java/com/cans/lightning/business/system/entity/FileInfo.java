package com.cans.lightning.business.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.cans.lightning.base.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;


/**
 * @author shencan
 * @date 2020/6/26 17:03
 */
@Getter
@Setter
@TableName("sys_file_info")
public class FileInfo extends BaseEntity {

    /**
     * 存储类型
     */
    private String storageType;

    private String filePath;

    private String fileName;

    private String fileType;

    private String username;

}
