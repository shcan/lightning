package com.cans.lightning.business.lowcode.form.bean;

import com.alibaba.fastjson.annotation.JSONType;
import com.cans.lightning.business.core.bean.BaseBean;
import com.cans.lightning.business.core.utils.ContextUtil;
import com.cans.lightning.business.lowcode.dto.AppTableFieldDto;
import com.cans.lightning.business.lowcode.enums.AppIndexEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author cans
 * @date 2023/11/28
 **/
@JSONType(ignores = {"allTableBean", "nextFiendName", "currentViewId"})
public class AppFormBean extends BaseBean {

    public static final String CURRENT_VIEW_ID = "currentViewId";

    public AppFormBean() {
    }

    public AppFormBean(String formId) {
        this.setId(formId);
    }

    /**
     * 表单名称
     */
    private String formName;

    /**
     * 应用ID
     */
    private String appId;
    /**
     * 字段游标
     */
    private Integer fieldIndex = 1;

    /**
     * 主表
     */
    private MainTableBean mainTableBean;

    /**
     * 明细表
     */
    private List<SonTableBean> sonTableList;

    /**
     * 视图信息
     */
    private List<ViewBean> viewList;

    /**
     * 表单绑定信息
     */
    private List<FormBindBean> formBindList;

    /**
     * 当前的视图
     */
    private ViewBean viewBean = new ViewBean();

    /**
     * 表单权限
     */
    private List<FormAuthBean> formAuthList;

    /**
     * key 数据库字段名
     */
    private Map<String, FormFieldBean> fieldMap;

    /**
     * 当前展示的信息
     * 1. 当前展示的视图 currentViewId
     */
    private Map<String, String> currentInfo = Maps.newHashMap();


    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public Integer getFieldIndex() {
        return fieldIndex;
    }

    public void setFieldIndex(Integer fieldIndex) {
        this.fieldIndex = fieldIndex;
    }

    public MainTableBean getMainTableBean() {
        return mainTableBean;
    }

    public void setMainTableBean(MainTableBean mainTableBean) {
        this.mainTableBean = mainTableBean;
    }

    public List<SonTableBean> getSonTableList() {
        return sonTableList;
    }

    public void setSonTableList(List<SonTableBean> sonTableList) {
        this.sonTableList = sonTableList;
    }

    public List<ViewBean> getViewList() {
        return viewList;
    }

    public void setViewList(List<ViewBean> viewList) {
        this.viewList = viewList;
    }

    public List<FormBindBean> getFormBindList() {
        return formBindList;
    }

    public void setFormBindList(List<FormBindBean> formBindList) {
        this.formBindList = formBindList;
    }

    public ViewBean getViewBean() {
        return viewBean;
    }

    public void setViewBean(ViewBean viewBean) {
        this.viewBean = viewBean;
    }

    public List<FormAuthBean> getFormAuthList() {
        return formAuthList;
    }

    public void setFormAuthList(List<FormAuthBean> formAuthList) {
        this.formAuthList = formAuthList;
    }

    public Map<String, FormFieldBean> getFieldMap() {
        return fieldMap;
    }

    public void setFieldMap(Map<String, FormFieldBean> fieldMap) {
        this.fieldMap = fieldMap;
    }

    public Map<String, String> getCurrentInfo() {
        return currentInfo;
    }

    public void setCurrentInfo(Map<String, String> currentInfo) {
        this.currentInfo = currentInfo;
    }

    @JsonIgnore
    public String getCurrentViewId() {
        return currentInfo.get(AppFormBean.CURRENT_VIEW_ID);
    }

    public String updateCurrentViewId(String viewId) {
        return currentInfo.put(AppFormBean.CURRENT_VIEW_ID,viewId);
    }

    @JsonIgnore
    public String getNextFiendName() {
        Integer fieldIndex = this.getFieldIndex();
        if (fieldIndex == null) {
            fieldIndex = 1;
        }
        this.setFieldIndex(fieldIndex + 1);
        return AppIndexEnum.FIELD.getKey() + Strings.padStart(String.valueOf(fieldIndex), 5, '0');
    }

    /**
        mark 记录调用栈报错:
            [
                "com.cans.lightning.business.lowcode.manager.form.bean.AppFormBean.getAllTableBean(l:109)",
                "com.alibaba.fastjson.serializer.ASMSerializer_2_AppFormBean.writeNormal(l:-1)",
                "com.alibaba.fastjson.serializer.ASMSerializer_2_AppFormBean.write(l:-1)",          writer=com.alibaba.fastjson.serializer.ASMSerializer_2_AppFormBean
                "com.alibaba.fastjson.serializer.JSONSerializer.write(l:281)",
                "com.alibaba.fastjson.JSON.toJSONBytes(l:731)",
                "com.alibaba.fastjson.JSON.toJSONBytes(l:702)",
                "com.alibaba.fastjson.JSON.toJSONBytes(l:629)",
                "com.alibaba.fastjson.JSON.toJSONBytes(l:618)",
                "com.alibaba.fastjson.support.spring.GenericFastJsonRedisSerializer.serialize(l:24)",
                "org.springframework.data.redis.core.AbstractOperations.rawValue(l:127)",
                "org.springframework.data.redis.core.DefaultValueOperations.set(l:254)",
                "com.cans.lightning.cache.redis.RedisCache.setCacheObject(l:58)",
                "com.cans.lightning.business.lowcode.manager.form.FormDesignManager.addLockAndForm2Cache(l:109)",
            ]
        原因: tableBeanLists.addAll(this.getSonTableList()) 空指针异常
     */
    @JsonIgnore
    public List<TableBean> getAllTableBean() {
        List<TableBean> tableBeanLists = Lists.newArrayList();
        if(this.getMainTableBean() != null){
            tableBeanLists.add(this.getMainTableBean());
        }
        if(!CollectionUtils.isEmpty(this.getSonTableList())){
            tableBeanLists.addAll(this.getSonTableList());
        }
        return tableBeanLists;
    }


    /**
     * 表中添加字段
     *
     * @param fieldDto 字段
     * @return
     */
    public FormFieldBean addField(AppTableFieldDto fieldDto) {
        Map<String, TableBean> tableBeanMap = this.getAllTableBean().stream().collect(Collectors.toMap(TableBean::getId, Function.identity()));
        TableBean tableBean = tableBeanMap.get(fieldDto.getTableId());
        List<FormFieldBean> fieldList = tableBean.getFieldList();
        FormFieldBean formFieldBean = new FormFieldBean().create(fieldDto);
        formFieldBean.setId(ContextUtil.nextID());
        formFieldBean.setDbFieldName(this.getNextFiendName());
        fieldList.add(formFieldBean);
        return formFieldBean;
    }


    @Override
    public AppFormBean clone() throws CloneNotSupportedException {
        AppFormBean cloneFormBean = (AppFormBean) super.clone();
        cloneFormBean.setFormName(this.formName);
        cloneFormBean.setAppId(this.appId);
        cloneFormBean.setFieldIndex(this.fieldIndex);
        
        // 需要深度clone()的对象
        cloneFormBean.setMainTableBean(this.mainTableBean == null ? null : this.mainTableBean.clone(cloneFormBean));
        if (this.sonTableList != null) {
            List<SonTableBean> sonTableBeans = new ArrayList<>();
            for (SonTableBean sonTableBean : this.sonTableList) {
                sonTableBeans.add(sonTableBean.clone(cloneFormBean));
            }
            cloneFormBean.setSonTableList(sonTableBeans);
        }
        if (this.viewList != null) {
            List<ViewBean> viewList = Lists.newArrayList();
            for (ViewBean viewBean : this.viewList) {
                viewList.add(viewBean.clone(cloneFormBean));
            }
            cloneFormBean.setViewList(viewList);
        }
        cloneFormBean.setViewBean(this.viewBean.clone(cloneFormBean));
        if (this.formAuthList != null) {
            List<FormAuthBean> formAuthList = Lists.newArrayList();
            for (FormAuthBean formAuthBean : this.formAuthList) {
                formAuthList.add(formAuthBean.clone(cloneFormBean));
            }
            cloneFormBean.setFormAuthList(formAuthList);
        }
        if (this.fieldMap != null) {
            Map<String, FormFieldBean> fieldMap = Maps.newHashMap();
            fieldMap.putAll(this.fieldMap);
            cloneFormBean.setFieldMap(fieldMap);
        }
        Map<String, String> currentInfo = Maps.newHashMap();
        currentInfo.putAll(this.currentInfo);
        cloneFormBean.setCurrentInfo(currentInfo);
        return cloneFormBean;
    }
}
