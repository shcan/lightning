package com.cans.lightning.business.lowcode.form.design.editingcache;

import com.cans.lightning.business.lowcode.form.bean.AppFormBean;
import com.cans.lightning.business.lowcode.form.constant.FormCacheConstants;
import com.cans.lightning.cache.redis.RedisCache;
import org.springframework.context.ApplicationContext;

import java.util.concurrent.TimeUnit;

/**
 * @author: qpy
 * @date: 2024-02-04 13:46
 * @description:
 */
public class RedisFormEditingCache<K, V> implements FormEditingCache<K, V> {

    private RedisCache redisCache;
    
    private final String prefix;
    private final TimeUnit timeUnit;
    private final Integer timeout;
    
    public RedisFormEditingCache(String prefix, TimeUnit timeUnit, Integer timeout) {
        this.prefix = prefix;
        this.timeUnit = timeUnit;
        this.timeout = timeout;
    }
    
    @Override
    public boolean put(K formId, V formBean) {
        redisCache.setCacheObject(prefix + formId, formBean, timeout, timeUnit);
        return true;
    }

    @Override
    public boolean remove(K formId) {
        return redisCache.deleteObject(prefix + formId);
    }

    @Override
    public void init(ApplicationContext ioc) {
        this.redisCache = ioc.getBean(RedisCache.class);
    }

    @Override
    public String toString() {
        return "RedisFormEditingCache"+ this.hashCode() + "@{" +
                ", prefix='" + prefix + '\'' +
                ", timeUnit=" + timeUnit +
                ", timeout=" + timeout +
                ", redisCache=" + (redisCache == null ? "空" : redisCache.getClass().getName() + "@" + redisCache.hashCode()) +
                '}';
    }
}
