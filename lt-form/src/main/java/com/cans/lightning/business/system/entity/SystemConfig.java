package com.cans.lightning.business.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cans.lightning.base.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 系统配置
 *
 * @author cans
 * @date 2021-04-06 14:48
 **/
@Getter
@Setter
@TableName("sys_config")
public class SystemConfig  extends BaseEntity {

    /**
     * 参数key值
     */
   @TableField("key_code")
    private String keyCode;

    /**
     * 参数名称
     */
   @TableField("name")
    private String name;

    /**
     * 参数值
     */
   @TableField("value")
    private String value;

    /**
     * 是否系统预设
     */
   @TableField("pre_set")
    private Integer preSet;

    /**
     * 备注
     */
   @TableField("memo")
    private String memo;

    /**
     * 排序号
     */
   @TableField("idx")
    private Integer idx;
}
