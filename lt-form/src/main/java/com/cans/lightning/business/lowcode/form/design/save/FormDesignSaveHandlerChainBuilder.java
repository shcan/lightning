package com.cans.lightning.business.lowcode.form.design.save;

import com.cans.lightning.business.lowcode.form.bean.AppFormBean;
import com.cans.lightning.business.lowcode.form.design.save.handler.*;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author cans
 * @date 2023/11/29
 **/
@Component
public class FormDesignSaveHandlerChainBuilder {

    @Autowired
    private FormDesignBaseSaveHandler formBeanTableLoader;
    @Autowired
    private FormDesignMainTableSaveHandler formDesignMainTableSaveHandler;
    @Autowired
    private FormDesignSonTableSaveHandler formDesignSonTableSaveHandler;
    @Autowired
    private FormDesignEndSaveHandler formDesignEndSaveHandler;
    @Autowired
    private FormDesignViewSaveHandler formDesignViewSaveHandler;
    @Resource
    private FormDesignBindSaveHandler formDesignBindSaveHandler;

    public AbstractBeanSaveHandler<AppFormBean> getSaveHandlerChain(){
        AbstractBeanSaveHandler<AppFormBean> saveHandler = formBeanTableLoader
                .nextHandler(formDesignMainTableSaveHandler)
                .nextHandler(formDesignSonTableSaveHandler)
                .nextHandler(formDesignViewSaveHandler)
                .nextHandler(formDesignBindSaveHandler);

        saveHandler.nextHandler(formDesignEndSaveHandler);
        return formBeanTableLoader;
    }
}
