package com.cans.lightning.business.system.web;

import com.cans.lightning.base.dto.ResDto;
import com.cans.lightning.business.system.dto.DictionaryItemDto;
import com.cans.lightning.business.system.service.api.IDictionaryService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

/**
 * 数据字典
 *
 * @author shencan
 * @date 2020/6/21 20:16
 */
@RestController
@RequestMapping("/dict")
public class DictionaryController {

    @Autowired
    private IDictionaryService dictionaryService;

    /**
     * 根据字典主表CODE获取下挂明细项
     *
     * @param dictCode 字典主表CODE
     * @return
     */
    @GetMapping("/get-dict-item/{dictCode}")
    public ResDto<List<DictionaryItemDto>> getDictItem(@PathVariable("dictCode") String dictCode) {
        return ResDto.success(dictionaryService.getDictItems(dictCode));
    }
}

