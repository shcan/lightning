package com.cans.lightning.business.pms.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cans.lightning.base.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Set;

/**
 * 用户
 *
 * @author shencan
 * @date 2020/6/14 20:31
 */
@Getter
@Setter
@TableName("sys_user")
public class SysUser extends BaseEntity implements UserDetails {

    private String username;

    private Integer sex;

    private Integer age;

    private String nickname;

    private String phoneNum;

    private String email;

    private String memo;

    private String password;


    private String avatarId;

    private Integer status;

    @TableField(exist = false)
    private SysDept sysDept;

    /**
     * 是否是超级管理员
     */
    private Integer superMan;

    @TableField(exist = false)
    private Set<SysRole> roles;

    private LocalDateTime loginTime;

    private LocalDateTime expireTime;

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return true;
    }

    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }


    @Override
    public String toString() {
        return "SysUser{" +
                "username='" + username + '\'' +
                ", sex=" + sex +
                ", age=" + age +
                ", password='" + password + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) {
            return true;
        }

        if (!(o instanceof SysUser)) {
            return false;
        }

        return this.getId().equals(((SysUser) o).getId());

    }


    @Override
    public int hashCode() {

        int result = 17;

        result = 31 * result + (getId() == null ? 0 : getId().hashCode());

        return result;
    }


}
