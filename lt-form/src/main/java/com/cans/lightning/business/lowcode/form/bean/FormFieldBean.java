package com.cans.lightning.business.lowcode.form.bean;

import com.cans.lightning.business.core.bean.BaseBean;
import com.cans.lightning.business.lowcode.dto.AppTableFieldDto;
import lombok.Getter;
import lombok.Setter;

/**
 * @author cans
 * @date 2023/11/28
 **/
@Getter
@Setter
public class FormFieldBean extends BaseBean {

    private String formId;

    private String tableId;
    /**
     * 字段名称
     */
    private String fieldName;
    /**
     * 数据库字段名
     */
    private String dbFieldName;

    /**
     * 字段类型  TODO: 可改为枚举
     */
    private String fieldDataType;

    /**
     * 字段长度
     */
    private Integer fieldLength;

    /**
     * 输入框宽度
     */
    private Integer width;

    /**
     * 小数位
     */
    private Integer scale;

    /**
     * 数据库数据类型
     */
    private String dbFieldDataType;

    /**
     * 是否已被删除
     */
    private Boolean delete = Boolean.FALSE;

    private String value = null;

    private boolean sysField = false;

    /**
     * 排序号
     */
    private Long sortIndex;

    public FormFieldBean create(AppTableFieldDto field) {
        this.setId(field.getId());
        // 字段名称
        this.setFieldName(field.getFieldName());
        // 数据库字段名称
        this.setDbFieldName(field.getDbFieldName());
        // 宽度
        this.setWidth(field.getWidth());
        // 字段类型  TODO: 可改为枚举
        this.setFieldDataType(field.getFieldDataType());
        //  字段长度
        this.setFieldLength(field.getLength());
        // 小数位
        this.setScale(null);
        // 数据库数据类型
        this.setDbFieldDataType(field.getDbFieldDataType());
        // 排序号
        this.setSortIndex(field.getFieldIndex());
        return this;
    }
    
    
    public FormFieldBean clone(AppFormBean formBean) throws CloneNotSupportedException {
        FormFieldBean ffb = (FormFieldBean) super.clone();
        ffb.setFormId(this.formId);
        ffb.setTableId(this.tableId);
        ffb.setFieldName(this.fieldName);
        ffb.setDbFieldName(this.dbFieldName);
        ffb.setFieldDataType(this.fieldDataType);
        ffb.setFieldLength(this.fieldLength);
        ffb.setWidth(this.width);
        ffb.setScale(this.scale);
        ffb.setDbFieldDataType(this.dbFieldDataType);
        ffb.setDelete(this.delete);
        ffb.setValue(this.value);
        ffb.setSortIndex(this.sortIndex);
        return ffb;
    }
    
}
