package com.cans.lightning.business.lowcode.service.api;

import com.cans.lightning.base.service.api.IBaseService;
import com.cans.lightning.business.lowcode.dto.AppDefinitionDto;
import com.cans.lightning.business.lowcode.entity.AppDefinition;


/**
 * 应用信息
 *
 * @author shencan
 * @date 2020/6/14 20:39
 */
public interface IAppDefinitionService extends IBaseService<AppDefinition, AppDefinitionDto, String> {

}
