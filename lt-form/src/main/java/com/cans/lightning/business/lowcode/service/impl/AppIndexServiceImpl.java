package com.cans.lightning.business.lowcode.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.cans.lightning.business.lowcode.dao.AppIndexDao;
import com.cans.lightning.business.lowcode.entity.AppIndex;
import com.cans.lightning.business.lowcode.enums.AppIndexEnum;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author cans
 * @date 2022/11/6
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class AppIndexServiceImpl {

    @Autowired
    private AppIndexDao appIndexDao;

    public Long getNextMainTableIndex(){
        return this.getNextFieldIndex(AppIndexEnum.MAIN_TABLE.getKey());
    }

    public Long getNextSonTableIndex(){
        return this.getNextFieldIndex(AppIndexEnum.SON_TABLE.getKey());
    }

    public Long getNextFieldIndex(String tableId) {
        LambdaQueryWrapper<AppIndex> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AppIndex::getName, tableId).orderByAsc(AppIndex::getCreateTime);
        AppIndex appIndex = appIndexDao.selectOne(queryWrapper);
        if(appIndex == null){
            appIndex = new AppIndex();
            appIndex.setName(tableId);
            appIndex.setValue(0L);
            appIndexDao.insert(appIndex);
        }else{
            appIndex.setValue(appIndex.getValue()+ 1);
            appIndexDao.updateById(appIndex);
        }
        return appIndex.getValue();
    }
}
