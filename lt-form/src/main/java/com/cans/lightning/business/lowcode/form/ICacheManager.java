package com.cans.lightning.business.lowcode.form;

public interface ICacheManager<T> {

    T get(String key);

    void invalidate(String key);
}
