package com.cans.lightning.business.pms.service.impl;

import com.cans.lightning.business.pms.dto.SysMenuShowDto;
import com.cans.lightning.cache.redis.RedisCache;
import org.springframework.stereotype.Component;

import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

/**
 * 菜单的缓存
 */
@Component
public class MenuCache {

    public static String MENU_KEY = "user_menu_tree:";

    @Autowired
    private RedisCache redisCache;


    /**
     * 获取缓存信息
     *
     * @param userId 人员ID
     * @return
     */
    public List<SysMenuShowDto> getUserMenuTree(String userId) {
        String redisKey = MENU_KEY + userId;

        if (redisCache.hasKey(redisKey)) {
            return redisCache.getCacheList(redisKey);
        }

        return null;
    }

}
