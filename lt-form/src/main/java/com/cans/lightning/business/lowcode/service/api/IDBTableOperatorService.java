package com.cans.lightning.business.lowcode.service.api;

import com.cans.lightning.business.lowcode.dto.AppTableDto;
import com.cans.lightning.business.lowcode.dto.AppTableFieldDto;

public interface IDBTableOperatorService {

    void createTable(AppTableDto tableDto);

    void createField(AppTableDto tableDto,AppTableFieldDto fieldDto);

    void ifNotExitCreate(AppTableDto appTableDto, AppTableFieldDto fieldDto);
}
