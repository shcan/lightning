package com.cans.lightning.business.pms.dto;

import com.cans.lightning.base.dto.BaseDto;
import com.cans.lightning.business.pms.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * @author shencan
 * @date 2020/10/17 23:47
 */
@Getter
@Setter
public class SysUserDto extends BaseDto<String> implements UserDetails {

    /**
     * ID
     */
    private String id;
    /**
     * 用户名
     */
    private String username;

    private Integer sex;

    private Integer age;

    private String nickname;

    private String phoneNum;

    private String email;

    private String memo;

    private String password;

    private String avatarId;

    private Integer status;

    private LocalDateTime loginTime;

    private LocalDateTime expireTime;
    /**
     * 角色
     */
    private Set<SysRoleDto> roles;

    /**
     * 角色ID
     */
    private Set<String> roleIds;

    public SysUserDto create(SysUser user){
        BeanUtils.copyProperties(user,this);
        return this;
    }

    @JsonIgnore
    @Override
    public List getAuthorities() {
        return Collections.EMPTY_LIST;
    }
}
