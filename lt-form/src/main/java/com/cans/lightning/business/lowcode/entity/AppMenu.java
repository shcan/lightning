package com.cans.lightning.business.lowcode.entity;

import com.cans.lightning.base.entity.BaseEntity;
import com.cans.lightning.business.lowcode.enums.AppMenuTYpeEnum;
import lombok.Getter;
import lombok.Setter;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 应用菜单
 *
 * @author cans
 * @date 2021/12/5
 **/
@Getter
@Setter
@TableName("app_menu")
public class AppMenu extends BaseEntity {

    /**
     * 所属应用
     */
    private String appId;
    /**
     * 菜单类型
     *
     * @see AppMenuTYpeEnum
     */
    private Integer appMenuType;

    /**
     * 菜单绑定ID,根据menuType一起进行判定
     */
    private String bindingId;

    /**
     * 路由
     */
    private String url;
    /**
     * 层级
     */
    private Integer level;
    /**
     * 排序号
     */
    private Integer sortId;
    /**
     * 0: 应用 1: 新建  2: 列表
     */
    private Integer type;

    /**
     * 菜单名
     */
    private String name;

    /**
     * 打开方式
     */
    private String openType;
}
