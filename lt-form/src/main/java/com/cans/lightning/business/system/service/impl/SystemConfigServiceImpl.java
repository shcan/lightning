package com.cans.lightning.business.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.cans.lightning.base.mapper.IBaseMapper;
import com.cans.lightning.base.service.impl.BaseServiceImpl;
import com.cans.lightning.business.system.dao.SystemConfigDao;
import com.cans.lightning.business.system.dto.SystemConfigDto;
import com.cans.lightning.business.system.entity.SystemConfig;
import com.cans.lightning.business.system.mapper.SystemConfigMapperI;
import com.cans.lightning.business.system.service.api.ISystemConfigService;
import lombok.extern.slf4j.Slf4j;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * 系统配置服务
 *
 * @author shencan
 * @date 2020/6/14 20:53
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class SystemConfigServiceImpl extends BaseServiceImpl<SystemConfig, SystemConfigDto, String> implements ISystemConfigService {

    @Autowired
    private SystemConfigDao systemConfigDao;
    @Autowired
    private SystemConfigMapperI systemConfigMapper;


    @Override
    public BaseMapper<SystemConfig> getDaoImpl() {
        return systemConfigDao;
    }

    @Override
    public IBaseMapper<SystemConfig, SystemConfigDto, String> getMapperImpl() {
        return systemConfigMapper;
    }

    @Override
    public SystemConfig getByKeyCode(String configKey) {
        LambdaQueryWrapper<SystemConfig> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SystemConfig::getKeyCode, configKey);
        return systemConfigDao.selectOne(queryWrapper);
    }
}
