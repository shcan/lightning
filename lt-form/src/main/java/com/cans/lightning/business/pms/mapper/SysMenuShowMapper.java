package com.cans.lightning.business.pms.mapper;

import com.cans.lightning.base.mapper.IBaseMapper;
import com.cans.lightning.business.pms.dto.SysMenuShowDto;
import com.cans.lightning.business.pms.entity.SysMenu;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * @author shencan
 * @date 2020/12/3 22:55
 */
@Mapper(componentModel = "spring")
public interface SysMenuShowMapper extends IBaseMapper<SysMenu, SysMenuShowDto,String> {

    @Override
    @Mapping(source = "menuName", target = "text")
    @Mapping(source = "url", target = "link")
    @Mapping(target = "children", ignore = true)
    @Mapping(target = "menuType", defaultValue = "0")
    SysMenuShowDto toDto(SysMenu sysMenu);
}
