package com.cans.lightning.business.lowcode.dto;

import com.cans.lightning.base.dto.BaseDto;
import com.cans.lightning.business.lowcode.enums.TableTypeEnum;
import com.cans.lightning.business.lowcode.form.bean.FormFieldBean;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 应用表单表信息
 *
 * @author cans
 * @date 2021/12/5
 **/
@Getter
@Setter
public class AppTableDto extends BaseDto<String> {

    /**
     * 所属应用
     */
    private String appId;

    /**
     * 所属表单
     */
    private String formId;

    /**
     * 字段游标
     */
    private Long fieldIndex = 0L;

    /**
     * 主表/明细表 {@link TableTypeEnum}
     */
    private String tableType;

    /**
     * 主表名
     */
    private String tableName;

    /**
     * 对应数据库表名
     */
    private String dbTableName;

    /**
     * 字段列表
     */
    private List<AppTableFieldDto> fieldArr = Lists.newArrayList();

    public AppTableDto create(FormFieldBean formFieldBean) {
        return null;
    }
}
