package com.cans.lightning.business.pms.mapper;

import com.cans.lightning.base.mapper.IBaseMapper;
import com.cans.lightning.business.pms.dto.SysUserDto;
import com.cans.lightning.business.pms.entity.SysUser;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.util.StringUtils;

/**
 * @author shencan
 * @date 2020/12/2 19:06
 */
@Mapper(componentModel = "spring", uses = SysRoleMapper.class)
public interface SysUserMapper extends IBaseMapper<SysUser, SysUserDto,String> {

    /**
     * 转换为Entity
     *
     * @param dto
     * @return
     */
    @Override
    SysUser toEntity(SysUserDto dto);

    @Override
    @Mapping(target = "roles",ignore = true)
    @Mapping(target = "password",ignore = true)
    SysUserDto toDto(SysUser sysUser);

    /**
     * 通过id获取实体
     *
     * @param id
     * @return
     */
    default SysUser fromId(String id) {
        if (StringUtils.isEmpty(id)) {
            return null;
        }
        SysUser entity = new SysUser();
        entity.setId(id);
        return entity;
    }
}
