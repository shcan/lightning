package com.cans.lightning.business.lowcode.form.bean;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cans.lightning.business.core.bean.BaseBean;
import com.cans.lightning.business.lowcode.entity.AppFormViewItem;
import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

/**
 * @author cans
 * @date 2023/12/10
 **/
@Getter
@Setter
public class ViewItemBean extends BaseBean {

    private String showType;

    private Map<String,Object> refObj;

    /**
     * 排序号
     */
    private Long sortId;

    public ViewItemBean create(AppFormViewItem appFormViewItem) {
        this.setId(appFormViewItem.getId());
        this.showType = appFormViewItem.getShowType();
        JSONObject jsonObject = JSON.parseObject(appFormViewItem.getRefObj());
        Integer line = jsonObject.getInteger("line");
        Integer row = jsonObject.getInteger("row");
        Map<String,Object> map = JSON.parseObject(appFormViewItem.getRefObj(), Map.class);
        map.put("line",line);
        map.put("row",row);
        this.refObj = map;
        this.sortId = appFormViewItem.getSortId();
        return this;
    }
    
    public ViewItemBean clone(AppFormBean formBean) throws CloneNotSupportedException {
        ViewItemBean vib = (ViewItemBean) super.clone();
        vib.setShowType(this.showType);
        vib.setSortId(this.sortId);

        if (this.refObj != null) {
            Map<String, Object> refObj = Maps.newHashMap();
            refObj.putAll(this.refObj);
            vib.setRefObj(refObj);
        }
        return vib;
    }
}
