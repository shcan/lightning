package com.cans.lightning.business.lowcode.form.loader.formBean;

import com.cans.lightning.business.lowcode.form.bean.AppFormBean;
import com.cans.lightning.business.lowcode.form.loader.AbstractBeanLoader;
import com.cans.lightning.business.lowcode.form.bean.FormFieldBean;
import com.cans.lightning.business.lowcode.form.bean.TableBean;
import com.google.common.collect.Maps;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * FormBean 加载结束
 *
 * @author cans
 * @date 2023/11/29
 **/
@Component
public class FormBeanEndLoader extends AbstractBeanLoader<AppFormBean> {

    @Override
    public AppFormBean loadOne(AppFormBean appFormBean) {
        return appFormBean;
    }

}
