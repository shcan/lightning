package com.cans.lightning.business.lowcode.dto;

import com.cans.lightning.base.dto.BaseDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 应用表单绑定
 *
 * @author cans
 * @date 2021/12/5
 **/
@Getter
@Setter
public class AppFormBindDto extends BaseDto<String> {
    /**
     * 所属应用
     */
    private String appId;

    /**
     * 所属表单
     */
    private String formId;

    /**
     * 视图名称
     */
    private String bindName;

    /**
     * 列表显示项
     */
    private String fieldListShow;

    /**
     * 排序设置
     */
    private String sortSetting;

    /**
     * 自定义筛选项
     */
    private String filterField;

    /**
     * 授权信息
     */
    private String authInfo;

}
