package com.cans.lightning.business.lowcode.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.cans.lightning.base.entity.BaseEntity;
import com.cans.lightning.business.lowcode.form.bean.FormBindBean;
import lombok.Getter;
import lombok.Setter;

/**
 * 应用表单表绑定
 *
 * @author cans
 * @date 2021/12/5
 **/
@Getter
@Setter
@TableName("app_form_bind")
public class AppFormBind extends BaseEntity {

    /**
     * 所属应用
     */
    private String appId;

    /**
     * 所属表单
     */
    private String formId;

    /**
     * 视图名称
     */
    private String bindName;

    /**
     * 列表显示项
     */
    private String fieldListShow;

    /**
     * 排序设置
     */
    private String sortSetting;

    /**
     * 自定义筛选项
     */
    private String filterField;

    /**
     * 授权信息
     */
    private String authInfo;

    private Long sortId;

    public AppFormBind transferFrom(FormBindBean formBindBean) {
        this.setId(formBindBean.getId());
        this.setBindName(formBindBean.getBindName());
        this.setFieldListShow(formBindBean.getFieldListShow());
        this.setFilterField(formBindBean.getFilterField());
        this.setSortSetting(formBindBean.getSortSetting());
        this.setAuthInfo(formBindBean.getAuthInfo());
        return this;
    }
}
