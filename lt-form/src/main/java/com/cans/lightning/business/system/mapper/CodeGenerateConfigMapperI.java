package com.cans.lightning.business.system.mapper;

import com.cans.lightning.base.mapper.IBaseMapper;
import com.cans.lightning.business.system.dto.CodeGenerateConfigDto;
import com.cans.lightning.business.system.entity.CodeGenerateConfig;
import org.mapstruct.Mapper;

/**
 * @author shencan
 * @date 2020/12/3 15:25
 */
@Mapper(componentModel = "Spring")
public interface CodeGenerateConfigMapperI extends IBaseMapper<CodeGenerateConfig, CodeGenerateConfigDto,String> {

    @Override
    CodeGenerateConfigDto toDto(CodeGenerateConfig entity);
}
