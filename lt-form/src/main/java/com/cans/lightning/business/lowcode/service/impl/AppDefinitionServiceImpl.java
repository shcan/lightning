package com.cans.lightning.business.lowcode.service.impl;

import com.cans.lightning.base.mapper.IBaseMapper;
import com.cans.lightning.base.service.impl.BaseServiceImpl;
import com.cans.lightning.business.lowcode.dao.AppDefinitionDao;
import com.cans.lightning.business.lowcode.dto.AppDefinitionDto;
import com.cans.lightning.business.lowcode.entity.AppDefinition;
import com.cans.lightning.business.lowcode.mapper.AppDefinitionMapper;
import com.cans.lightning.business.lowcode.service.api.IAppDefinitionService;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * 应用信息
 *
 * @author shencan
 * @date 2020/6/14 20:53
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class AppDefinitionServiceImpl extends BaseServiceImpl<AppDefinition, AppDefinitionDto, String> implements IAppDefinitionService {

    @Autowired
    private AppDefinitionDao appDefinitionDao;
    @Autowired
    private AppDefinitionMapper appDefinitionMapper;

    @Override
    public BaseMapper<AppDefinition> getDaoImpl() {
        return appDefinitionDao;
    }

    @Override
    public IBaseMapper<AppDefinition, AppDefinitionDto, String> getMapperImpl() {
        return appDefinitionMapper;
    }
}
