package com.cans.lightning.business.system.mapper;

import com.cans.lightning.base.mapper.IBaseMapper;
import com.cans.lightning.business.system.dto.SystemConfigDto;
import com.cans.lightning.business.system.entity.SystemConfig;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * @author shencan
 * @date 2020/12/3 15:25
 */
@Mapper(componentModel = "Spring")
public interface SystemConfigMapperI extends IBaseMapper<SystemConfig, SystemConfigDto, String> {

    @Override
    @Mapping(source = "preSet",target = "preSet",defaultValue = "0")
    SystemConfig toEntity(SystemConfigDto systemConfigDto);
}
