package com.cans.lightning.business.pms.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author shencan
 * @date 2020/9/20 22:22
 */
@Getter
@Setter
public class SysMenuTreeDto {

    /**
     * 名称
     */
    private String title;

    /**
     * 主键
     */
    private String key;
    /**
     * 主键
     */
    private String value;

    /**
     * 是否叶子节点
     */
    private Boolean isLeaf;

    /**
     * 下级菜单
     */
    List<SysMenuTreeDto> children;

}
