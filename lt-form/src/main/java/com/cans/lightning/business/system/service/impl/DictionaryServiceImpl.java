package com.cans.lightning.business.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.cans.lightning.base.mapper.IBaseMapper;
import com.cans.lightning.base.service.impl.BaseServiceImpl;
import com.cans.lightning.business.system.dao.DictionaryDao;
import com.cans.lightning.business.system.dto.DictionaryDto;
import com.cans.lightning.business.system.dto.DictionaryItemDto;
import com.cans.lightning.business.system.entity.Dictionary;
import com.cans.lightning.business.system.mapper.DictionaryMapperI;
import com.cans.lightning.business.system.service.api.IDictionaryItemService;
import com.cans.lightning.business.system.service.api.IDictionaryService;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

/**
 * 数据字典服务
 *
 * @author shencan
 * @date 2020/6/14 20:53
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class DictionaryServiceImpl extends BaseServiceImpl<Dictionary, DictionaryDto, String> implements IDictionaryService {

    @Autowired
    private DictionaryDao dictionaryDao;
    @Autowired
    private DictionaryMapperI dictionaryMapper;
    @Autowired
    private IDictionaryItemService dictionaryItemService;


    @Override
    public List<DictionaryItemDto> getDictItems(String dictCode) {
        Dictionary dictionary = this.getByCode(dictCode);
        return dictionaryItemService.findByDictId(dictionary.getId());
    }


    @Override
    public Dictionary getByCode(String dictCode) {
        LambdaQueryWrapper<Dictionary> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Dictionary::getCode, dictCode);
        return dictionaryDao.selectOne(queryWrapper);
    }

    @Override
    public BaseMapper<Dictionary> getDaoImpl() {
        return dictionaryDao;
    }

    @Override
    public IBaseMapper<Dictionary, DictionaryDto, String> getMapperImpl() {
        return dictionaryMapper;
    }

}
