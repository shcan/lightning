package com.cans.lightning.business.lowcode.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cans.lightning.base.mapper.IBaseMapper;
import com.cans.lightning.base.service.impl.BaseServiceImpl;
import com.cans.lightning.business.lowcode.dao.AppTableFieldDao;
import com.cans.lightning.business.lowcode.dto.AppTableFieldDto;
import com.cans.lightning.business.lowcode.entity.AppTableField;
import com.cans.lightning.business.lowcode.mapper.AppTableFieldMapper;
import com.cans.lightning.business.lowcode.service.api.IAppTableFieldService;
import com.google.common.collect.Lists;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.data.web.config.QuerydslWebConfiguration;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 应用信息
 *
 * @author shencan
 * @date 2020/6/14 20:53
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class AppTableFieldServiceImpl extends BaseServiceImpl<AppTableField, AppTableFieldDto, String> implements IAppTableFieldService {

    @Autowired
    private AppTableFieldDao appTableFieldDao;
    @Autowired
    private AppTableFieldMapper appTableFieldMapper;


    @Override
    public BaseMapper<AppTableField> getDaoImpl() {
        return appTableFieldDao;
    }

    @Override
    public IBaseMapper<AppTableField, AppTableFieldDto, String> getMapperImpl() {
        return appTableFieldMapper;
    }

    @Override
    public void saveOrUpdate(List<AppTableFieldDto> fieldArr) {
        if (CollectionUtils.isEmpty(fieldArr)) {
            return;
        }
        long sortId = 0;
        for (AppTableFieldDto appTableFieldDto : fieldArr) {
            if (appTableFieldDto.getDelete()) {
                this.deleteById(appTableFieldDto.getId());
            } else {
                appTableFieldDto.setSortId(sortId++);
                this.saveOrUpdateDto(appTableFieldDto);
            }
        }
    }

    @Override
    public List<AppTableFieldDto> getByFormId(String formId) {
        LambdaQueryWrapper<AppTableField> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AppTableField::getFormId, formId).orderByAsc(AppTableField::getSortId);
        return appTableFieldMapper.toDtos(appTableFieldDao.selectList(queryWrapper));
    }

    @Override
    public List<AppTableFieldDto> getByTableIds(List<String> tableIdList) {
        if (CollectionUtils.isEmpty(tableIdList)) {
            return Lists.newArrayList();
        }
        LambdaQueryWrapper<AppTableField> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(AppTableField::getTableId, tableIdList).orderByAsc(AppTableField::getSortId);
        return appTableFieldMapper.toDtos(appTableFieldDao.selectList(queryWrapper));
    }

    @Override
    public List<AppTableFieldDto> getByTableId(String tableId) {
        LambdaQueryWrapper<AppTableField> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AppTableField::getTableId, tableId).orderByAsc(AppTableField::getSortId);
        List<AppTableField> tableFields = appTableFieldDao.selectList(queryWrapper);
        return appTableFieldMapper.toDtos(tableFields);
    }

    @Override
    public String getNextFieldIndex(String tableId) {
        LambdaQueryWrapper<AppTableField> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AppTableField::getTableId, tableId).orderByAsc(AppTableField::getFieldIndex);
        Page<AppTableField> page = appTableFieldDao.selectPage(Page.of(0,1),queryWrapper);
        List<AppTableField> list = page.getRecords();
        if (CollectionUtils.isEmpty(list)) {
            return "0001";
        }
        AppTableField appTableField = list.get(0);
        return String.format("%04d", appTableField.getFieldIndex());
    }
}
