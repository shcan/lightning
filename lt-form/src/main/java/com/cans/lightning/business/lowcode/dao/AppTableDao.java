package com.cans.lightning.business.lowcode.dao;

import com.cans.lightning.base.dao.api.IBaseDao;
import com.cans.lightning.business.lowcode.entity.AppTable;
import org.apache.ibatis.annotations.Mapper;

/**
 * 表
 *
 * @author shencan
 * @date 2020/6/14 20:33
 */
@Mapper
public interface AppTableDao extends IBaseDao<AppTable> {
}
