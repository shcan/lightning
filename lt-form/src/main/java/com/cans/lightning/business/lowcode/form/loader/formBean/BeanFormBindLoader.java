package com.cans.lightning.business.lowcode.form.loader.formBean;

import com.cans.lightning.business.lowcode.dto.AppFormBindDto;
import com.cans.lightning.business.lowcode.form.bean.AppFormBean;
import com.cans.lightning.business.lowcode.form.bean.FormBindBean;
import com.cans.lightning.business.lowcode.form.bean.FormFieldBean;
import com.cans.lightning.business.lowcode.form.loader.AbstractBeanLoader;
import com.cans.lightning.business.lowcode.service.api.IAppFormBindService;
import jakarta.annotation.Resource;
import org.apache.commons.compress.utils.Lists;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 加载视图信息
 *
 * @author cans
 * @date 2023/11/29
 **/
@Component
public class BeanFormBindLoader extends AbstractBeanLoader<AppFormBean> {

    @Resource
    private IAppFormBindService formBindService;

    @Override
    public AppFormBean loadOne(AppFormBean appFormBean) {
        Map<String, FormFieldBean> fieldMap = appFormBean.getFieldMap();
        List<AppFormBindDto> formBindDtoList = formBindService.findByFormId(appFormBean.getId());
        List<FormBindBean> formBindList = Lists.newArrayList();
        for (AppFormBindDto appFormBindDto : formBindDtoList) {
            FormBindBean formBindBean = new FormBindBean().transferFrom(appFormBindDto);
            String fieldListShow = formBindBean.getFieldListShow();
            formBindBean.setFieldListShowDesc(this.getShowDesc(fieldListShow, fieldMap, formBindBean));
            formBindList.add(formBindBean);
        }
        appFormBean.setFormBindList(formBindList);
        return appFormBean;
    }

    private String getShowDesc(String fieldListShow, Map<String, FormFieldBean> fieldMap, FormBindBean formBindBean) {
        StringBuilder fieldListShowDesc = new StringBuilder();
        if(fieldListShow != null){
            String[] fieldNameArr = fieldListShow.split(",");
            for (String fieldName : fieldNameArr) {
                FormFieldBean formFieldBean = fieldMap.get(fieldName);
                if(formFieldBean != null){
                    fieldListShowDesc.append(formFieldBean.getFieldName()).append(",");
                }
            }
        }
        if(!fieldListShowDesc.isEmpty()){
            fieldListShowDesc.deleteCharAt(fieldListShowDesc.length() - 1);
        }
        return fieldListShowDesc.toString();
    }

}
