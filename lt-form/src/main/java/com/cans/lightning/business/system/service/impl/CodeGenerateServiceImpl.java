package com.cans.lightning.business.system.service.impl;

import com.cans.lightning.business.system.entity.SystemConfig;
import com.cans.lightning.business.system.service.api.ICodeGenerateConfigService;
import com.cans.lightning.business.system.service.api.ICodeGenerateService;
import com.cans.lightning.business.system.service.api.ISystemConfigService;
import com.cans.lightning.utils.StringUtils;
import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * 编码生成服务
 *
 * @author shencan
 * @date 2020/6/14 20:53
 */
@Service
public class CodeGenerateServiceImpl implements ICodeGenerateService {

    @Autowired
    private ICodeGenerateConfigService codeGenerateConfigService;
    @Autowired
    private ISystemConfigService systemConfigService;

    @Override
    public String generateCode(String configCode) {

        if (StringUtils.isEmpty(configCode)) {
            return null;
        }

        synchronized (configCode.intern()) {
            return codeGenerateConfigService.generateCode(configCode);
        }

    }

    @Override
    public String generateBySystemConfigKey(String configKey) {

        if (StringUtils.isEmpty(configKey)) {
            return null;
        }

        SystemConfig systemConfig = systemConfigService.getByKeyCode(configKey);

        if (systemConfig == null) {
            throw new RuntimeException("根据configKey[" + configKey + "]未查询到系统配置");
        }

        String configCode = systemConfig.getValue();

        synchronized (configCode.intern()) {
            return codeGenerateConfigService.generateCode(configCode);
        }

    }
}
