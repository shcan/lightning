package com.cans.lightning.business.lowcode.web;

import com.cans.lightning.base.dto.ResDto;
import com.cans.lightning.base.service.api.IBaseService;
import com.cans.lightning.base.web.BaseController;
import com.cans.lightning.business.lowcode.dto.AppMenuDto;
import com.cans.lightning.business.lowcode.dto.AppMenuSaveDto;
import com.cans.lightning.business.lowcode.entity.AppMenu;
import com.cans.lightning.business.lowcode.service.api.IAppMenuService;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

/**
 * 菜单
 *
 * @author shencan
 * @date 2020/6/14 22:14
 */
@RestController
@RequestMapping("/app_menu")
public class AppMenuController extends BaseController<AppMenu, AppMenuDto, String> {

    @Autowired
    private IAppMenuService appMenuService;

    @Override
    public IBaseService<AppMenu, AppMenuDto, String> getBaseService() {
        return appMenuService;
    }

    @PostMapping("save_app_menu")
    public ResDto<String> saveAppMenu(@RequestBody AppMenuSaveDto saveDto){
        appMenuService.saveOrUpdateDtoList(saveDto.getAppId(),saveDto.getMenuList());
        return ResDto.success();
    }

    @GetMapping("get_app_menus/{appId}")
    public ResDto<List<AppMenuDto>> getAppMenuList(@PathVariable("appId") String appId){
        return ResDto.success(appMenuService.getAppMenuList(appId));
    }
}
