package com.cans.lightning.business.pms.mapper;

import com.cans.lightning.base.mapper.IBaseMapper;
import com.cans.lightning.business.pms.dto.SysDeptDto;
import com.cans.lightning.business.pms.entity.SysDept;
import org.mapstruct.Mapper;
import org.springframework.util.StringUtils;

/**
 * @author shencan
 * @date 2020/12/2 19:06
 */
@Mapper(componentModel = "spring", uses = SysRoleMapper.class)
public interface SysDeptMapper extends IBaseMapper<SysDept, SysDeptDto,String> {

    /**
     * 转换为Entity
     *
     * @param dto
     * @return
     */
    @Override
    SysDept toEntity(SysDeptDto dto);


    /**
     * 通过id获取实体
     *
     * @param id
     * @return
     */
    default SysDept fromId(String id) {
        if (StringUtils.isEmpty(id)) {
            return null;
        }
        SysDept entity = new SysDept();
        entity.setId(id);
        return entity;
    }
}
