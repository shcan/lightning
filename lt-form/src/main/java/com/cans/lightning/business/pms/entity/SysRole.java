package com.cans.lightning.business.pms.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cans.lightning.base.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

/**
 * @author shencan
 * @date 2020/9/20 14:54
 */
@Getter
@Setter
@TableName("sys_role")
public class SysRole extends BaseEntity {

    /**
     * 角色编号
     */
    private String roleCode;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 状态 1:启用 0:未启用
     */
    private Integer status;

    /**
     * 备注
     */
    private String memo;

    /**
     * 角色对应的用户
     */
    @TableField(exist = false)
    private Set<SysUser> users;

    /**
     * 角色对应的菜单
     */
    @TableField(exist = false)
    private Set<SysMenu> menus;

    /**
     * 角色有权限的接口
     */
    @TableField(exist = false)
    private Set<SysAccessMethod> accessMethods;


    @Override
    public boolean equals(Object o) {

        if (o == this) {
            return true;
        }

        if (!(o instanceof SysRole)) {
            return false;
        }

        return this.getId().equals(((SysRole) o).getId());

    }


    @Override
    public int hashCode() {

        int result = 27;

        result = 31 * result + (getId() == null ? 0 : getId().hashCode());

        return result;
    }
}
