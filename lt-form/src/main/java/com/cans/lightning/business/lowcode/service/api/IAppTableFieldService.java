package com.cans.lightning.business.lowcode.service.api;

import com.cans.lightning.base.service.api.IBaseService;
import com.cans.lightning.business.lowcode.dto.AppTableFieldDto;
import com.cans.lightning.business.lowcode.entity.AppTableField;

import java.util.List;


/**
 * 应用信息
 *
 * @author shencan
 * @date 2020/6/14 20:39
 */
public interface IAppTableFieldService extends IBaseService<AppTableField, AppTableFieldDto, String> {

    void saveOrUpdate(List<AppTableFieldDto> fieldArr);

    List<AppTableFieldDto> getByFormId(String id);

    List<AppTableFieldDto> getByTableIds(List<String> collect);

    List<AppTableFieldDto> getByTableId(String tableId);

    String getNextFieldIndex(String formId);
}
