package com.cans.lightning.business.core.service;

import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * @author cans
 * @date 2023/11/25
 **/
@Component("uuidAutoGen")
public class UUIDAutoGen {

    public String nextID(String params) {
        return nextId();
    }
    public String nextId() {
        return UUID.randomUUID().toString().replace("-","");
    }
}
