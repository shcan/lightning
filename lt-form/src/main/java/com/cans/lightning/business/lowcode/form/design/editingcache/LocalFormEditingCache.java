package com.cans.lightning.business.lowcode.form.design.editingcache;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.springframework.context.ApplicationContext;

import java.util.concurrent.TimeUnit;

/**
 * @author: qpy
 * @date: 2024-02-04 13:45
 * @description:
 */
public class LocalFormEditingCache<K, V> implements FormEditingCache<K, V> {

    private final Cache<K, V> formBeanEditCache = CacheBuilder.newBuilder()
            .maximumSize(15000) // 设置缓存容量
            .expireAfterWrite(30, TimeUnit.MINUTES) // 设置缓存项的过期时间
            .build();


    @Override
    public boolean put(K formId, V formBean) {
        formBeanEditCache.put(formId, formBean);
        return true;
    }

    @Override
    public boolean remove(K formId) {
        formBeanEditCache.invalidate(formId);
        return true;
    }

    @Override
    public void init(ApplicationContext ioc) {
        
    }

    @Override
    public String toString() {
        return "LocalFormEditingCache" + this.hashCode() + "@{" +
                "formBeanEditCache=" + "com.google.common.cache.Cache" +
                '}';
    }
}
