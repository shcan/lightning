package com.cans.lightning.business.system.mapper;

import com.cans.lightning.base.mapper.IBaseMapper;
import com.cans.lightning.business.system.dto.FileInfoDto;
import com.cans.lightning.business.system.entity.FileInfo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * @author shencan
 * @date 2020/12/3 15:25
 */
@Mapper(componentModel = "Spring")
public interface FileInfoMapperI extends IBaseMapper<FileInfo, FileInfoDto,String> {

    @Override
    @Mapping(source = "fileName",target = "name")
    @Mapping(source = "id",target = "uid")
    @Mapping(target = "url",expression = "java( new String(fileInfoDto.PATH).concat(fileInfo.getId()) )")
    FileInfoDto toDto(FileInfo fileInfo);
}
