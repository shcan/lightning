package com.cans.lightning.business.lowcode.form.bean;

import com.cans.lightning.business.lowcode.dto.AppTableDto;
import lombok.Getter;
import lombok.Setter;

/**
 * @author cans
 * @date 2023/11/28
 **/
@Getter
@Setter
public class MainTableBean extends TableBean {

    public MainTableBean create(AppTableDto appTableDto) {
        if(appTableDto == null){
            return this;
        }
        this.setId(appTableDto.getId());
        return this;
    }

    @Override
    public MainTableBean clone(AppFormBean formBean) throws CloneNotSupportedException {
        MainTableBean mtb =  (MainTableBean) super.clone(formBean);
        // ... 
        
        return mtb;
    }
}
