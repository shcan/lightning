package com.cans.lightning.business.system.dto;

import com.cans.lightning.base.dto.BaseDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 数据字典单项
 *
 * @author cans
 * @date 2021-03-29 15:21
 **/
@Getter
@Setter
public class DictionaryItemDto extends BaseDto<String> {

    private String id;

    /**
     * 关联主项
     */
    private DictionaryDto dictionary;

    /**
     * 关联的主项ID
     */
    private String dictionaryId;

    /**
     * 标示
     */
    private String label;

    /**
     * 描述
     */
    private String value;

    /**
     * 排序号
     */
    private Integer idx;
}
