package com.cans.lightning.business.pms.dao;

import com.cans.lightning.business.pms.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author shencan
 * @date 2020/6/14 20:33
 */
@Mapper
public interface SysUserDao extends BaseMapper<SysUser> {
}
