package com.cans.lightning.business.lowcode.form.design.save.handler;

import com.cans.lightning.business.lowcode.entity.AppForm;
import com.cans.lightning.business.lowcode.form.bean.AppFormBean;
import com.cans.lightning.business.lowcode.form.design.save.context.AppFormDesignSaveContext;
import com.cans.lightning.business.lowcode.form.design.save.AbstractBeanSaveHandler;
import com.cans.lightning.business.lowcode.service.api.IAppFormDefinitionService;
import org.springframework.stereotype.Component;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * formBean基础信息保存入库
 *
 * @author cans
 * @date 2023/11/29
 **/
@Component
public class FormDesignBaseSaveHandler extends AbstractBeanSaveHandler<AppFormBean> {

    @Autowired
    private IAppFormDefinitionService formDefinitionService;


    @Override
    public AppFormBean saveOne(AppFormDesignSaveContext<AppFormBean> saveContext) {
        AppFormBean appFormBean = saveContext.getData();
        AppForm appForm = this.extractAppform(appFormBean);
        formDefinitionService.saveOrUpdate(appForm);
        return appFormBean;
    }

    private AppForm extractAppform(AppFormBean appFormBean) {
        AppForm appForm = new AppForm();
        appForm.setId(appFormBean.getId());
        appForm.setAppId(appFormBean.getAppId());
        appForm.setFormName(appFormBean.getFormName());
        appForm.setFieldIndex(appFormBean.getFieldIndex());
        return appForm;
    }
}
