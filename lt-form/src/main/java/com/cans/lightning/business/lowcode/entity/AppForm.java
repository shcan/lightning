package com.cans.lightning.business.lowcode.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.cans.lightning.base.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 应用表单信息
 *
 * @author cans
 * @date 2021/12/5
 **/
@Getter
@Setter
@TableName("app_form")
public class AppForm extends BaseEntity {

    /**
     * 所属应用
     */
    private String appId;

    /**
     * 表单名称
     */
    private String formName;

    /**
     * 字段游标
     */
    private Integer fieldIndex;

}
