package com.cans.lightning.business.lowcode.form.cache.hotCache;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

/**
 * 具体的加载缓存接口
 * @param <K>
 * @param <V>
 */
public interface MapDataLoader<K extends Serializable, V extends Serializable> {

    /**
     * 加载数据
     */
    V load(K key);

    /**
     * 批量加载
     */
    Map<K, V> loadBatch(Collection<K> keys);

    // ...

}
