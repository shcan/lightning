package com.cans.lightning.business.pms.service.api;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.cans.lightning.base.service.api.IBaseService;
import com.cans.lightning.business.pms.dto.SysUserDto;
import com.cans.lightning.business.pms.entity.SysUser;
import org.springframework.security.core.userdetails.UserDetailsService;


/**
 * @author shencan
 * @date 2020/6/14 20:39
 * @description
 */
public interface ISysUserService extends IBaseService<SysUser,SysUserDto, String>, UserDetailsService {

    /**
     * 根据用户名获取用户
     *
     * @param username
     * @return
     */
    SysUser getByUsername(String username);

    SysUser getByEmail(String email);

    /**
     * 获取当前登陆用户信息
     *
     * @return
     */
    SysUser getCurrentUser();

    /**
     * 更新用户详情信息
     *
     * @param userDto 用户DTO
     * @return
     */
    SysUserDto updateInfo(SysUserDto userDto);

    SysUserDto toDto(SysUser user);

    /**
     * 重置密码
     *
     * @param email 邮箱地址
     * @param password 密码
     * @param code 验证码
     */
    void resetPassword(String email, String password, String code);

    IPage findPage(IPage of);
}
