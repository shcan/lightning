package com.cans.lightning.business.lowcode.dto;

import com.cans.lightning.base.dto.BaseDto;
import com.cans.lightning.business.lowcode.form.bean.FormFieldBean;
import lombok.Getter;
import lombok.Setter;

/**
 * 字段信息
 *
 * @author cans
 * @date 2021/12/5
 **/
@Getter
@Setter
public class AppTableFieldDto extends BaseDto<String> {

    /**
     * 表单ID
     */
    private String formId;

    /**
     * 表格ID
     */
    private String tableId;

    /**
     * 字段名称
     */
    private String fieldName;

    /**
     * 数据库字段名
     */
    private String dbFieldName;

    /**
     * 字段描述
     */
    private String fieldDesc;

    /**
     * 字段长度
     */
    private Integer length;

    /**
     * 字段类型
     */
    private String fieldDataType;

    /**
     * 数据库字段类型
     */
    private String dbFieldDataType;

    /**
     * 是否已被删除
     */
    private Boolean delete = Boolean.FALSE;

    /**
     * 排序号
     */
    private Long sortId;

    /**
     * 输入框宽度
     */
    private Integer width;

    /**
     * 字段索引
     */
    private Long fieldIndex;

    public AppTableFieldDto create(FormFieldBean formFieldBean) {
        this.setId(formFieldBean.getId());
        this.formId = formFieldBean.getFormId();
        this.setTableId(formFieldBean.getTableId());
        // 字段名称
        this.fieldName = formFieldBean.getFieldName();
        // 数据库字段名
        this.dbFieldName = formFieldBean.getDbFieldName();
        // 字段描述
        this.fieldDesc = formFieldBean.getFieldName();
        // 字段长度
        this.length = formFieldBean.getFieldLength();
        // 字段类型
        this.fieldDataType = formFieldBean.getFieldDataType();
        // 数据库字段类型
        this.dbFieldDataType = formFieldBean.getDbFieldDataType();
        // 是否已被删除
        this.delete = formFieldBean.getDelete();
        // 排序号
        this.sortId = formFieldBean.getSortIndex();
        // 输入框宽度
        this.width = formFieldBean.getWidth();
        return this;
    }
}
