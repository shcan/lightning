package com.cans.lightning.business.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cans.lightning.base.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 数据字典单项
 *
 * @author cans
 * @date 2021-03-29 15:21
 **/
@Getter
@Setter
@TableName("sys_dictionary_item")
public class DictionaryItem extends BaseEntity {

    /**
     * 关联主项
     */
   @TableField("dictionary_id")
    private String dictionaryId;

    /**
     * 标示
     */
   @TableField("label")
    private String label;

    /**
     * 描述
     */
   @TableField("value")
    private String value;

    /**
     * 排序号
     */
   @TableField("idx")
    private Integer idx;
}
