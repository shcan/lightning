package com.cans.lightning.business.lowcode.form.loader.formBean;

import com.cans.lightning.business.lowcode.dto.AppTableDto;
import com.cans.lightning.business.lowcode.enums.FieldTypeEnum;
import com.cans.lightning.business.lowcode.form.bean.*;
import com.cans.lightning.business.lowcode.form.constant.FormSystemFieldConstants;
import com.cans.lightning.business.lowcode.form.loader.tableBean.MainTableBeanLoaderBuilder;
import com.cans.lightning.business.lowcode.form.loader.AbstractBeanLoader;
import com.cans.lightning.business.lowcode.service.api.IAppTableService;
import com.google.common.collect.Maps;
import org.springframework.stereotype.Component;

import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * formBean 表结构加载
 *  |-- 主表结构加载
 *  |-- 明细表结构加载
 *
 * @author cans
 * @date 2023/11/29
 **/
@Component
public class BeanTableLoader extends AbstractBeanLoader<AppFormBean> {

    @Autowired
    private IAppTableService appTableService;
    @Autowired
    private MainTableBeanLoaderBuilder mainTableBeanLoaderBuilder;

    @Override
    public AppFormBean loadOne(AppFormBean appFormBean) {
        AppTableDto appTableDto = appTableService.getManTableByFormId(appFormBean.getId());
        MainTableBean mainTableBean = mainTableBeanLoaderBuilder.getLoader().runLoad(new MainTableBean().create(appTableDto));
        appFormBean.setMainTableBean(mainTableBean);
        List<AppTableDto> sonTableDtoList = appTableService.getSonTablesByFormId(appFormBean.getId());
        List<SonTableBean> sonTableBeanList = sonTableDtoList.stream().map(i -> new SonTableBean().create(i)).collect(Collectors.toList());
        appFormBean.setSonTableList(sonTableBeanList);

        this.loadFieldMap(appFormBean);
        return appFormBean;
    }

    private void loadFieldMap(AppFormBean appFormBean) {
        Map<String, FormFieldBean> res = Maps.newHashMap();
        List<TableBean> allTableBean = appFormBean.getAllTableBean();
        for (TableBean tableBean : allTableBean) {
            for (FormFieldBean formFieldBean : tableBean.getFieldList()) {
                formFieldBean.setTableId(tableBean.getId());
                res.put(formFieldBean.getDbFieldName(), formFieldBean);
            }
        }
        // 加载系统字段
        loadSystemFields(appFormBean, res);
    }

    private static void loadSystemFields(AppFormBean appFormBean, Map<String, FormFieldBean> res) {
        // 创建时间
        FormFieldBean createTimeBean = new FormFieldBean();
        createTimeBean.setSysField(true);
        createTimeBean.setId(FormSystemFieldConstants.CREATE_TIME);
        createTimeBean.setFieldName("创建时间");
        createTimeBean.setDbFieldDataType(FieldTypeEnum.DATE_TIME.getKey());
        createTimeBean.setDbFieldName(FormSystemFieldConstants.CREATE_TIME);
        res.put(FormSystemFieldConstants.CREATE_TIME, createTimeBean);
        // 修改时间
        FormFieldBean lastChangeBean = new FormFieldBean();
        lastChangeBean.setSysField(true);
        lastChangeBean.setId(FormSystemFieldConstants.LAST_CHANGE_TIME);
        lastChangeBean.setFieldName("修改时间");
        lastChangeBean.setDbFieldDataType(FieldTypeEnum.DATE_TIME.getKey());
        lastChangeBean.setDbFieldName(FormSystemFieldConstants.LAST_CHANGE_TIME);
        res.put(FormSystemFieldConstants.LAST_CHANGE_TIME, lastChangeBean);
        // 创建人
        FormFieldBean createUserBean = new FormFieldBean();
        createUserBean.setSysField(true);
        createUserBean.setId(FormSystemFieldConstants.CREATE_USER);
        createUserBean.setFieldName("创建人");
        createUserBean.setDbFieldDataType(FieldTypeEnum.VARCHAR.getKey());
        createUserBean.setDbFieldName(FormSystemFieldConstants.CREATE_USER);
        res.put(FormSystemFieldConstants.CREATE_USER, createUserBean);
        appFormBean.setFieldMap(res);
    }
}
