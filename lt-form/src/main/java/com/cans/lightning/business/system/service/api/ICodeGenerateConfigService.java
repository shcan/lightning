package com.cans.lightning.business.system.service.api;

import com.cans.lightning.base.service.api.IBaseService;
import com.cans.lightning.business.system.dto.CodeGenerateConfigDto;
import com.cans.lightning.business.system.entity.CodeGenerateConfig;


/**
 * 编码生成规则
 *
 * @author shencan
 * @date 2020/6/14 20:39
 */
public interface ICodeGenerateConfigService extends IBaseService<CodeGenerateConfig, CodeGenerateConfigDto, String> {

    /**
     * 根据编号获取
     *
     * @param code 编号
     * @return
     */
    CodeGenerateConfig getByCode(String code);

    /**
     * 根据配置code生成编号
     *
     * @param code 配置code
     * @return
     */
    String generateCode(String code);
}
