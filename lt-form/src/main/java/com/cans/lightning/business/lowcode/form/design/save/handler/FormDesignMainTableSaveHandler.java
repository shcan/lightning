package com.cans.lightning.business.lowcode.form.design.save.handler;

import com.cans.lightning.business.lowcode.dto.AppTableDto;
import com.cans.lightning.business.lowcode.dto.AppTableFieldDto;
import com.cans.lightning.business.lowcode.enums.FieldTypeEnum;
import com.cans.lightning.business.lowcode.enums.TableTypeEnum;
import com.cans.lightning.business.lowcode.form.AppIndexManager;
import com.cans.lightning.business.lowcode.form.bean.AppFormBean;
import com.cans.lightning.business.lowcode.form.bean.FormFieldBean;
import com.cans.lightning.business.lowcode.form.bean.MainTableBean;
import com.cans.lightning.business.lowcode.form.design.save.AbstractBeanSaveHandler;
import com.cans.lightning.business.lowcode.form.design.save.context.AppFormDesignSaveContext;
import com.cans.lightning.business.lowcode.service.api.IAppTableFieldService;
import com.cans.lightning.business.lowcode.service.api.IAppTableService;
import com.cans.lightning.utils.StringUtils;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * formBean -- 主表信息保存入库
 *
 * @author cans
 * @date 2023/11/29
 **/
@Component
public class FormDesignMainTableSaveHandler extends AbstractBeanSaveHandler<AppFormBean> {

    @Autowired
    private IAppTableService appTableService;
    @Autowired
    private AppIndexManager appIndexManager;
    @Autowired
    private IAppTableFieldService appTableFieldService;

    @Override
    public AppFormBean saveOne(AppFormDesignSaveContext<AppFormBean> saveContext) {
        AppFormBean appFormBean = saveContext.getData();
        AppTableDto appTable = this.extractedAppTable(appFormBean);
        appTableService.saveOrUpdate(appTable);
        return appFormBean;
    }

    private AppTableDto extractedAppTable(AppFormBean appFormBean) {
        MainTableBean mainTableBean = appFormBean.getMainTableBean();
        AppTableDto mainTable = new AppTableDto();
        mainTable.setFieldArr(Lists.newArrayList());
        mainTable.setId(mainTableBean.getId());
        mainTable.setFormId(appFormBean.getId());
        mainTable.setTableType(TableTypeEnum.MAIN.getKey());
        String dbTableName = mainTableBean.getDbTableName();
        if(StringUtils.isEmpty(dbTableName)){
            dbTableName = appIndexManager.getNextMainTableName();
        }
        mainTable.setDbTableName(dbTableName);
        mainTable.setAppId(appFormBean.getAppId());
        mainTable.setTableName(dbTableName);
        mainTable.getFieldArr().addAll(this.getMainTableFieldList(appFormBean));
        return mainTable;

    }

    /**
     * 获取主表的字段信息
     *
     * @param appFormBean 表单对象
     * @return
     */
    private List<AppTableFieldDto> getMainTableFieldList(AppFormBean appFormBean) {
        List<AppTableFieldDto> resList = Lists.newArrayList();
        Map<String, FormFieldBean> fieldMap = appFormBean.getFieldMap();
        MainTableBean mainTableBean = appFormBean.getMainTableBean();
        Map<String, List<FormFieldBean>> tableGroup = fieldMap.values().stream().filter(i-> Objects.nonNull(i.getTableId())).collect(Collectors.groupingBy(FormFieldBean::getTableId));
        List<FormFieldBean> fieldList = tableGroup.get(mainTableBean.getId());
        if(CollectionUtils.isEmpty(fieldList)){
            return resList;
        }
        for (FormFieldBean formFieldBean : fieldList) {
            AppTableFieldDto appTableField = new AppTableFieldDto();
            appTableField.setId(formFieldBean.getId());
            // 表单ID
            appTableField.setFormId(appFormBean.getId());
            // 表格ID
            appTableField.setTableId(mainTableBean.getId());
            // 字段名称
            appTableField.setFieldName(formFieldBean.getFieldName());
            // 数据库字段名
            appTableField.setDbFieldName(formFieldBean.getDbFieldName());
            // 字段描述
            appTableField.setFieldDesc(formFieldBean.getFieldName());
            // 字段长度
            appTableField.setLength(formFieldBean.getFieldLength());
            // 字段类型
            appTableField.setFieldDataType(formFieldBean.getFieldDataType());
            /**
             * @see FieldTypeEnum
             * 数据库字段类型
             */
            appTableField.setDbFieldDataType(formFieldBean.getDbFieldDataType());
            // 排序号
            appTableField.setSortId(formFieldBean.getSortIndex());
            // 输入框宽度
            appTableField.setWidth(formFieldBean.getWidth());
            // 是否被删除
            appTableField.setDelete(formFieldBean.getDelete());
            // 字段索引
            resList.add(appTableField);
        }
        return resList;
    }


}
