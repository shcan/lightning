package com.cans.lightning.business.lowcode.form.constant;

import java.util.concurrent.TimeUnit;

/**
 * @author: qpy
 * @date: 2024-01-31 13:58
 * @description:
 */
public class FormCacheConstants {

    /**
     * 缓存粒度
     */
    public static final TimeUnit CACHE_TIME_UNIT = TimeUnit.SECONDS;
    
    /**
     * 表单缓存的前缀
     */
    public final static String FORM_PREFIX = "form:";

    /**
     * 此key用来存储 当前缓存的表单, 当多节点时, 需要存储一个版本号, 来表示缓存是否过期(todo: 后期做)
     */
    public final static String caching = FORM_PREFIX + "caching:";
    
    /**
     * 编辑中的表单的缓存前缀
     */
    public final static String EDITING = FORM_PREFIX + "editing:";

    /**
     * 编辑中的表单缓存时间
     */
    public final static int EDITING_TIME = 5 * 60;
    
    /**
     * 编辑中的表单的 编辑信息   比如编辑的所属人等
     */
    public final static String EDITING_INFO = FORM_PREFIX + "editing_info:";
    
}
