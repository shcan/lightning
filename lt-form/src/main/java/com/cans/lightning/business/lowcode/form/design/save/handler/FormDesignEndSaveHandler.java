package com.cans.lightning.business.lowcode.form.design.save.handler;

import com.cans.lightning.business.lowcode.form.bean.AppFormBean;
import com.cans.lightning.business.lowcode.form.design.save.AbstractBeanSaveHandler;
import com.cans.lightning.business.lowcode.form.design.save.context.AppFormDesignSaveContext;
import org.springframework.stereotype.Component;

/**
 * formBean -- 保存结束handler
 * 暂时什么也不做
 *
 * @author cans
 * @date 2023/11/29
 **/
@Component
public class FormDesignEndSaveHandler extends AbstractBeanSaveHandler<AppFormBean> {

    @Override
    public AppFormBean saveOne(AppFormDesignSaveContext<AppFormBean> saveContext) {
        AppFormBean appFormBean = saveContext.getData();
        return appFormBean;
    }
}
