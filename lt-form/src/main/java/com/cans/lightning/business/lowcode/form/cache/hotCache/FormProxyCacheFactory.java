package com.cans.lightning.business.lowcode.form.cache.hotCache;

import com.cans.lightning.cache.ProxyCache;
import com.cans.lightning.cache.ProxyCacheFactory;
import com.cans.lightning.cache.hotCache.HotCacheFactory;
import com.cans.lightning.cache.hotCache.HotProxyCache;

import java.io.Serializable;

public class FormProxyCacheFactory implements ProxyCacheFactory<HotCacheContext> {
    @Override
    public ProxyCache<? extends Serializable, ? extends Serializable> createProxyCache(HotCacheContext ctx) {
        // 创建热点缓存
        FormHotCache formHotCache = new FormHotCache<>(ctx.getKey(), ctx.getMapDataLoader(), ctx.getHotCacheInitializer(), ctx);

        // 创建代理对象
        formHotCache = HotCacheFactory.getInstance().createHotCacheProxy(formHotCache, ctx.getKey());

        return new HotProxyCache<Serializable, Serializable>(formHotCache);
    }
}
