package com.cans.lightning.business.system.dao;

import com.cans.lightning.business.system.entity.SystemConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统配置
 *
 * @author shencan
 * @date 2020/6/14 20:33
 */
@Mapper
public interface SystemConfigDao extends BaseMapper<SystemConfig> {
}
