package com.cans.lightning.business.pms.web;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cans.lightning.base.dto.MyPageRequest;
import com.cans.lightning.base.dto.ResDto;
import com.cans.lightning.base.service.api.IBaseService;
import com.cans.lightning.base.web.BaseController;
import com.cans.lightning.business.pms.dto.SysRoleDto;
import com.cans.lightning.business.pms.entity.SysRole;
import com.cans.lightning.business.pms.service.api.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 角色接口
 *
 * @author shencan
 * @date 2020/6/14 22:14
 */
@RestController
@RequestMapping("/sys-role")
public class SysRoleController extends BaseController<SysRole, SysRoleDto, String> {

    @Autowired
    private ISysRoleService sysRoleService;


    @PostMapping("/pageList")
    public ResDto pageList(@RequestBody MyPageRequest pageRequest) {
        return ResDto.success(sysRoleService.findPage(Page.of(pageRequest.getPage(), pageRequest.getSize())));
    }

    @Override
    public IBaseService<SysRole, SysRoleDto, String> getBaseService() {
        return sysRoleService;
    }
}
