package com.cans.lightning.business.lowcode.service.api;

public interface IDBTableDoOperatorService extends IDBTableOperatorService {
    /**
     * 数据库类型
     *
     * @return
     */
    int getDbType();
}
