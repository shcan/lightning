package com.cans.lightning.business.pms.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * 用户登录对象
 *
 * @author ruoyi
 */
@Getter
@Setter
public class LoginBody {
    /**
     * 用户名
     */
    private String username;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 用户密码
     */
    private String password;

    /**
     * 验证码
     */
    private String code;

    /**
     * TOKEN key
     */
    private String key;

    /**
     * TOKEN
     */
    private String token;

}
