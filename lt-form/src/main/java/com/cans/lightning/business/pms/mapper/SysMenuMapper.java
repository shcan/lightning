package com.cans.lightning.business.pms.mapper;

import com.cans.lightning.base.mapper.IBaseMapper;
import com.cans.lightning.business.pms.dto.SysMenuDto;
import com.cans.lightning.business.pms.entity.SysMenu;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.util.StringUtils;

/**
 * @author shencan
 * @date 2020/12/2 19:06
 */
@Mapper(componentModel = "spring", uses = SysRoleMapper.class)
public interface SysMenuMapper extends IBaseMapper<SysMenu, SysMenuDto,String> {

    /**
     * 转换为Entity
     *
     * @param dto
     * @return
     */
    @Override
    SysMenu toEntity(SysMenuDto dto);


    @Override
    @Mapping(source = "parentId", target = "parentId")
    SysMenuDto toDto(SysMenu sysMenu);

    /**
     * 通过id获取实体
     *
     * @param id
     * @return
     */
    default SysMenu fromId(String id) {
        if (StringUtils.isEmpty(id)) {
            return null;
        }
        SysMenu entity = new SysMenu();
        entity.setId(id);
        return entity;
    }
}
