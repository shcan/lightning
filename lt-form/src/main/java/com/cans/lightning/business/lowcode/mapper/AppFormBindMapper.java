package com.cans.lightning.business.lowcode.mapper;

import com.cans.lightning.base.mapper.IBaseMapper;
import com.cans.lightning.business.lowcode.dto.AppFormBindDto;
import com.cans.lightning.business.lowcode.entity.AppFormBind;
import com.cans.lightning.business.lowcode.entity.AppFormView;
import org.mapstruct.Mapper;
import org.springframework.util.StringUtils;

/**
 * @author shencan
 * @date 2020/12/2 19:06
 */
@Mapper(componentModel = "spring")
public interface AppFormBindMapper extends IBaseMapper<AppFormBind, AppFormBindDto, String> {

    /**
     * 转换为Entity
     *
     * @param dto
     * @return
     */
    @Override
    AppFormBind toEntity(AppFormBindDto dto);


    /**
     * 通过id获取实体
     *
     * @param id
     * @return
     */
    default AppFormBind fromId(String id) {
        if (StringUtils.isEmpty(id)) {
            return null;
        }
        AppFormBind entity = new AppFormBind();
        entity.setId(id);
        return entity;
    }
}
