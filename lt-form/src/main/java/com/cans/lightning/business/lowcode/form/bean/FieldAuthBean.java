package com.cans.lightning.business.lowcode.form.bean;

import com.cans.lightning.business.core.bean.BaseBean;
import lombok.Getter;
import lombok.Setter;

/**
 * @author cans
 * @date 2023/11/28
 **/
@Getter
@Setter
public class FieldAuthBean extends BaseBean {

    private String fieldId;

    private String auth;

    private DefaultValueOperatorBean defaultValueOperator;


    public FieldAuthBean clone(AppFormBean formBean) throws CloneNotSupportedException {
        FieldAuthBean fab =  (FieldAuthBean) super.clone();
        // ... 

        return fab;
    }
    
}
