package com.cans.lightning.business.pms.dto;

import com.cans.lightning.base.dto.BaseDto;
import com.cans.lightning.base.dto.IBaseDto;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

/**
 * @author shencan
 * @date 2020/12/2 22:03
 */
@Getter
@Setter
@EqualsAndHashCode
public class SysRoleDto extends BaseDto<String> {

    private String id;

    /**
     * 角色编号
     */
    private String roleCode;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 备注
     */
    private String memo;

    /**
     * 菜单
     */
    Set<SysMenuDto> menus;

    /**
     * 菜单的ID
     */
    Set<String> menuIds;

    /**
     * 接口
     */
    private Set<SysAccessMethodDto> accessMethods;
    /**
     * 接口的ID
     */
    Set<String> accessMethodIds;

}
