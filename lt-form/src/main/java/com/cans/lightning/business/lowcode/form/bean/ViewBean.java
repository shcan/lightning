package com.cans.lightning.business.lowcode.form.bean;

import com.alibaba.fastjson.JSON;
import com.cans.lightning.business.core.bean.BaseBean;
import com.cans.lightning.business.lowcode.dto.AppFormViewDto;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

/**
 * @author cans
 * @date 2023/11/28
 **/
@Getter
@Setter
public class ViewBean extends BaseBean {

    /**
     * 视图名称
     */
    private String viewName;

    /**
     * 表单ID
     */
    private String formId;

    /**
     * 视图单项和字段绑定信息
     */
    private Map<String, String> fieldBind = Maps.newHashMap();
    /**
     * 视图单项
     */
    private List<ViewItemBean> viewItemList = Lists.newArrayList();

    public ViewBean create(AppFormViewDto appFormViewDto) {
        this.setId(appFormViewDto.getId());
        this.viewName = appFormViewDto.getViewName();
        this.formId = appFormViewDto.getFormId();
        this.setFieldBind(JSON.parseObject(appFormViewDto.getFieldBind(),Map.class));
        return this;
    }

    public ViewBean clone(AppFormBean formBean) throws CloneNotSupportedException {
        ViewBean vb = (ViewBean) super.clone();
        vb.setViewName(this.viewName);
        vb.setFormId(this.formId);
        
        Map<String, String> fieldBind = Maps.newHashMap();
        fieldBind.putAll(this.fieldBind);
        vb.setFieldBind(fieldBind);

        List<ViewItemBean> viewItemBeanList = Lists.newArrayList();
        for (ViewItemBean viewItemBean : this.viewItemList) {
            viewItemBeanList.add(viewItemBean.clone(formBean));
        }
        vb.setViewItemList(viewItemBeanList);
        return vb;
    }
}
