package com.cans.lightning.business.lowcode.form.loader.formBean;

import com.cans.lightning.business.lowcode.form.bean.AppFormBean;
import com.cans.lightning.business.lowcode.form.loader.AbstractBeanLoader;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author cans
 * @date 2023/11/29
 **/
@Component
public class FormBeanLoaderBuilder {

    @Autowired
    private BeanBaseLoader formBeanBaseLoader;
    @Autowired
    private BeanTableLoader formBeanTableLoader;
    @Autowired
    private BeanViewLoader beanViewLoader;
    @Autowired
    private FormBeanEndLoader formBeanEndLoader;
    @Resource
    private BeanFormBindLoader formBindLoader;


    public AbstractBeanLoader<AppFormBean> getLoader(){
        AbstractBeanLoader<AppFormBean> appFormBeanAbstractBeanLoader = formBeanBaseLoader.nextLoader(formBeanTableLoader)
                .nextLoader(beanViewLoader)
                .nextLoader(formBindLoader);


        appFormBeanAbstractBeanLoader.nextLoader(formBeanEndLoader);
        return formBeanBaseLoader;
    }
}
