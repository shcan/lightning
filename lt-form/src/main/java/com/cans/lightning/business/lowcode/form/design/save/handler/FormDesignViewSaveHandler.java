package com.cans.lightning.business.lowcode.form.design.save.handler;

import com.cans.lightning.business.lowcode.form.bean.AppFormBean;
import com.cans.lightning.business.lowcode.form.bean.ViewBean;
import com.cans.lightning.business.lowcode.form.design.save.AbstractBeanSaveHandler;
import com.cans.lightning.business.lowcode.form.design.save.context.AppFormDesignSaveContext;
import com.cans.lightning.business.lowcode.service.api.IAppFormViewService;
import org.springframework.stereotype.Component;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * formBean -- 主表信息保存入库
 *
 * @author cans
 * @date 2023/11/29
 **/
@Component
public class FormDesignViewSaveHandler extends AbstractBeanSaveHandler<AppFormBean> {

    @Autowired
    private IAppFormViewService formViewService;

    @Override
    public AppFormBean saveOne(AppFormDesignSaveContext<AppFormBean> saveContext) {
        AppFormBean appFormBean = saveContext.getData();
        ViewBean viewBean = appFormBean.getViewBean();
        viewBean.setFormId(appFormBean.getId());
        formViewService.saveOrUpdate(viewBean);
        return appFormBean;
    }


}
