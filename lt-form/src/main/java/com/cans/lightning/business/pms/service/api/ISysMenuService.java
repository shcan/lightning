package com.cans.lightning.business.pms.service.api;

import com.cans.lightning.base.service.api.IBaseService;
import com.cans.lightning.business.pms.dto.SysMenuDto;
import com.cans.lightning.business.pms.dto.SysMenuShowDto;
import com.cans.lightning.business.pms.dto.SysMenuTreeDto;
import com.cans.lightning.business.pms.entity.SysMenu;
import com.cans.lightning.business.pms.entity.SysUser;

import java.util.List;


/**
 * 权限 - 菜单
 *
 * @author shencan
 * @date 2020/6/14 20:39
 */
public interface ISysMenuService  extends IBaseService<SysMenu, SysMenuDto, String> {

    /**
     * 获取用户有权限的菜单树结构
     *
     * @return
     */
    List<SysMenuShowDto> getUserMenuTree();

    /**
     * 获取用户有权限的菜单树结构
     *
     * @return
     */
    List<SysMenuShowDto> getUserMenuTree(SysUser currentUser);

    void removeUserMenuTree(SysUser currentUser);

    void removeUserMenuTree();

    /**
     * 获取树结构
     * @return
     */
    List<SysMenuTreeDto> getTree();

    List<SysMenu> getByParentId(String parentId);

    List<SysMenuDto> tableTree();
}
