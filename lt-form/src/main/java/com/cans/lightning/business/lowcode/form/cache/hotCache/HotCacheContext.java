package com.cans.lightning.business.lowcode.form.cache.hotCache;

import com.cans.lightning.cache.CacheContext;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;
import java.util.function.Consumer;

@Setter
@Getter
@Builder
public class HotCacheContext<T> implements CacheContext {

    // 缓存key
    private String key;

    // 缓存加载器
    private MapDataLoader<? extends Serializable, ? extends Serializable> mapDataLoader;

    private List<T> loadIds;

    private HotCacheInitializer<? extends Serializable, ? extends Serializable> hotCacheInitializer;

    public void runTask(Consumer<List<T>> task) {
        task.accept(loadIds);
    }

}
