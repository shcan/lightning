package com.cans.lightning.business.lowcode.enums;

import com.google.common.collect.Lists;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 字段类型类型
 *
 * @author cans
 * @date 2021-04-22 11:11
 **/
public enum AppIndexEnum {

    MAIN_TABLE("man", "主表"),
    SON_TABLE("son", "从表"),
    FIELD("field", "字段");

    private final String key;
    private final String value;

    AppIndexEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    /**
     * 通过值得到value
     *
     * @param key
     * @return
     */
    public static String getValueByKey(String key) {
        if (!StringUtils.isEmpty(key)) {
            for (AppIndexEnum e : AppIndexEnum.values()) {
                if (e.getKey().equals(key)) {
                    return e.getValue();
                }
            }
        }
        return null;
    }

    /**
     * 通过值得到value
     *
     * @param key
     * @return
     */
    public static AppIndexEnum getByKey(String key) {
        if (!StringUtils.isEmpty(key)) {
            for (AppIndexEnum e : AppIndexEnum.values()) {
                if (e.getKey().equals(key)) {
                    return e;
                }
            }
        }
        return null;
    }

    public static String getKeyByValue(String value) {
        if (!StringUtils.isEmpty(value)) {
            for (AppIndexEnum e : AppIndexEnum.values()) {
                if (e.getValue().equals(value)) {
                    return e.getKey();
                }
            }
        }
        return null;
    }

    public static List<Map<String, Object>> list() {

        List<Map<String, Object>> list = Lists.newArrayList();

        for (AppIndexEnum e : AppIndexEnum.values()) {
            HashMap<String, Object> map = new HashMap<>(1);
            map.put("key", e.getKey());
            map.put("value", e.getValue());
            list.add(map);
        }

        return list;

    }
}
