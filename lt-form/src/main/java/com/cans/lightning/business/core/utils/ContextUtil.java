package com.cans.lightning.business.core.utils;

import com.cans.lightning.business.core.service.UUIDAutoGen;
import org.springframework.context.ApplicationContext;

/**
 * @author cans
 * @date 2023/12/10
 **/
public class ContextUtil {
    private static ApplicationContext ac;

    public static <T> T getBean(String beanName, Class<T> clazz) {
        return ac.getBean(beanName, clazz);
    }

    public static void setApplicationContext(ApplicationContext applicationContext) {
        ac = applicationContext;
    }

    public static String nextID(){
        return ContextUtil.getBean("uuidAutoGen", UUIDAutoGen.class).nextId();
    }
}
