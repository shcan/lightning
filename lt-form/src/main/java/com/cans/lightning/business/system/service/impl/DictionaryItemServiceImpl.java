package com.cans.lightning.business.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.cans.lightning.base.mapper.IBaseMapper;
import com.cans.lightning.base.service.impl.BaseServiceImpl;
import com.cans.lightning.business.system.dao.DictionaryItemDao;
import com.cans.lightning.business.system.dto.DictionaryItemDto;
import com.cans.lightning.business.system.entity.DictionaryItem;
import com.cans.lightning.business.system.mapper.DictionaryItemMapperI;
import com.cans.lightning.business.system.service.api.IDictionaryItemService;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

/**
 * 数据字典服务
 *
 * @author shencan
 * @date 2020/6/14 20:53
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class DictionaryItemServiceImpl extends BaseServiceImpl<DictionaryItem, DictionaryItemDto, String> implements IDictionaryItemService {

    @Autowired
    private DictionaryItemDao dictionaryItemDao;
    @Autowired
    private DictionaryItemMapperI dictionaryItemMapper;


    @Override
    public BaseMapper<DictionaryItem> getDaoImpl() {
        return dictionaryItemDao;
    }

    @Override
    public IBaseMapper<DictionaryItem, DictionaryItemDto, String> getMapperImpl() {
        return dictionaryItemMapper;
    }

    @Override
    public List<DictionaryItemDto> findByDictId(String dictionaryId) {
        LambdaQueryWrapper<DictionaryItem> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(DictionaryItem::getDictionaryId, dictionaryId);
        List<DictionaryItem> itemList = dictionaryItemDao.selectList(queryWrapper);
        return dictionaryItemMapper.toDtos(itemList);
    }
}
