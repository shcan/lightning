package com.cans.lightning.business.lowcode.form.cache.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author qpy
 * @date 2024-03-06 13:27
 * @description 表单缓存日志, 记录了表单start~end的时间
 */
public class FormCacheLogger {
    private final static Logger logger = LoggerFactory.getLogger(FormCacheLogger.class);

    /**
     * 是否开启调试信息【输出更多内容】
     */
    private static final boolean DEBUG_ENABLE = true;

    /**
     * 存储当前线程的开始时间 markStart() 里面存进去
     */
    private static final ConcurrentHashMap<Thread, ConcurrentHashMap<String, Long>> loggerNames = new ConcurrentHashMap<>();
    
    enum LogType {
        /** 调试 */
        DEBUG,
        /** 调试 */
        INFO,
        /** 慢日志 */
        SLOW
    }
    
    /**
     * 没有返回结果的函数接口
     * @date 2024/3/6 15:56
     * @since v1.0 
     * @author qpy
     */
    
    @FunctionalInterface
    public interface Runner {
        void run() throws Exception;
    }
    
    /**
     * 带有返回值结果的函数接口
     * @date 2024/3/6 15:58
     * @since v1.0
     * @author qpy
     */
    
    @FunctionalInterface
    public interface Caller<V> {
        V call() throws Exception; 
    }

    /**
     * 记录时间的开始
     * @param stepName 关键字的名称, 好分辨的名称即可
     * @date 2024/3/6 13:55
     * @since v1.0
     * @author qpy
     */
    public static void markStartLog(String stepName) {
        markStartLog(stepName, LogType.INFO);
    }

    /**
     * 
     * @param stepName 关键字的名称, 好分辨的名称即可
     * @param args 其他参数, 会在时间后面打印出来
     * @date 2024/3/6 14:00
     * @since v1.0
     * @author qpy
     */
    public static void markEndLog(String stepName, @Nullable Object... args) {
        markEndLog(stepName, LogType.INFO, args);
    }
    
    /**
     * 执行 runner 并打印执行时间
     * @param taskName 任务名称
     * @param runner 运行器
     * @date 2024/3/6 15:58
     * @since v1.0
     * @author qpy
     */
    public static void monitor(String taskName, Runner runner) {
        long start = System.currentTimeMillis();
        try {
            runner.run();
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
        finally {
            logger.info("monitor: " + taskName + ", cost: " + (System.currentTimeMillis() - start));
        }
    }
    
    /**
     * 执行 caller 并打印执行时间
     * @param taskName
     * @param caller 
     * @return 
     * @date 2024/3/6 16:00
     * @since 
     * @author qpy
     */
    public static <V>  V monitor(String taskName, Caller<V> caller) {
        long start = System.currentTimeMillis();
        try {
            return caller.call();
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
        finally {
            logger.info("monitor: " + taskName + ", cost: " + (System.currentTimeMillis() - start));
        }
    }
    
    
    private static void markStartLog(String stepName, LogType logType) {
        if (logType != LogType.SLOW) {
            logger.info("step: " + stepName + " start.");
        }
        // 如果以前已经markStart 先将之前markStart的日志进行输出
        markEndLog(stepName);
        
        loggerNames.computeIfAbsent(Thread.currentThread(), k -> new ConcurrentHashMap<>())
                .put(stepName, System.currentTimeMillis());
    }
    
    
    private static void markEndLog(String stepName, LogType logType, @Nullable Object... args) {
        ConcurrentHashMap<String, Long> currentThreadMap = loggerNames.get(Thread.currentThread());
        if (currentThreadMap == null) {
            return;    
        }
        Long start = currentThreadMap.remove(stepName);
        if (currentThreadMap.isEmpty()) {
            loggerNames.remove(Thread.currentThread());
        }
        if (start != null) {
            long cost = System.currentTimeMillis() - start;
            if (!DEBUG_ENABLE && logType == LogType.SLOW && cost < 10_000) {
                return;
            }
            StringBuilder logContent = new StringBuilder("step: ").append(stepName).append(", cost: ").append(cost);
            if (args != null) {
                if (args.length % 2 == 0) {
                    int len = args.length / 2;
                    for (int i = 0; i < len; i++) {
                        logContent.append(",").append(args[2 * i]).append(":").append(args[2 * i + 1]);
                    }
                }
                else {
                    logContent.append("args: [");
                    for (int i = 0; i < args.length; i++) {
                        if (i > 0) {
                            logContent.append(", ");
                        }
                        logContent.append(args[i]);
                    }
                    logContent.append("]");
                }
            }
            logger.info(logContent.toString());
        }
    }
    
}
