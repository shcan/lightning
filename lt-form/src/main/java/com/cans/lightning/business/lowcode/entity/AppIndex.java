package com.cans.lightning.business.lowcode.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.cans.lightning.base.entity.BaseEntity;
import com.cans.lightning.business.lowcode.enums.AppIndexEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * 应用游标信息
 *
 * @author cans
 * @date 2021/12/5
 **/
@Getter
@Setter
@TableName("app_index")
public class AppIndex extends BaseEntity {

    /**
     * 名称 {@link AppIndexEnum}
     */
    private String name;

    /**
     * 目前游标
     */
    private Long value;

}
