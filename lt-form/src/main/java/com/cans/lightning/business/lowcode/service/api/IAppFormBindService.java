package com.cans.lightning.business.lowcode.service.api;

import com.cans.lightning.base.service.api.IBaseService;
import com.cans.lightning.business.lowcode.dto.AppFormBindDto;
import com.cans.lightning.business.lowcode.dto.AppFormViewDto;
import com.cans.lightning.business.lowcode.entity.AppFormBind;
import com.cans.lightning.business.lowcode.entity.AppFormView;
import com.cans.lightning.business.lowcode.entity.AppFormViewItem;
import com.cans.lightning.business.lowcode.form.bean.ViewBean;

import java.util.List;


/**
 * 表单视图
 *
 * @author shencan
 * @date 2020/6/14 20:39
 */
public interface IAppFormBindService extends IBaseService<AppFormBind, AppFormBindDto, String> {

    List<AppFormBindDto> findByFormId(String id);

}
