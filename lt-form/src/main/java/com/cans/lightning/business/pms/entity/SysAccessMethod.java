package com.cans.lightning.business.pms.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.cans.lightning.base.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * API权限控制-方法
 *
 * @author cans
 * @date 2021-05-07 14:08
 **/
@Getter
@Setter
@TableName("sys_access_method")
public class SysAccessMethod extends BaseEntity {

    /**
     * 所属权限控制类
     */
    private SysAccessClass sysAccessClass;

    /**
     * 方法标识
     */
    private String methodMark;

    /**
     * 方法名称
     */
    private String methodName;

    /**
     * 请求URL
     */
    private String url;

    /**
     * 请求方式
     */
    private String requestMethod;
}
