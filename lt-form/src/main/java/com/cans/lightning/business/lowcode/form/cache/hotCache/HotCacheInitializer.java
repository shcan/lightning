package com.cans.lightning.business.lowcode.form.cache.hotCache;

import java.util.List;
import java.util.Map;

@FunctionalInterface
public interface HotCacheInitializer<K, V> {

    Map<K, V> load4Init(List<K> keys);
}
