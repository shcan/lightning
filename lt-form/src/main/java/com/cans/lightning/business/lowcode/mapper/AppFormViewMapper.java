package com.cans.lightning.business.lowcode.mapper;

import com.cans.lightning.base.mapper.IBaseMapper;
import com.cans.lightning.business.lowcode.dto.AppFormViewDto;
import com.cans.lightning.business.lowcode.entity.AppFormView;
import org.mapstruct.Mapper;
import org.springframework.util.StringUtils;

/**
 * @author shencan
 * @date 2020/12/2 19:06
 */
@Mapper(componentModel = "spring")
public interface AppFormViewMapper extends IBaseMapper<AppFormView, AppFormViewDto,String> {

    /**
     * 转换为Entity
     *
     * @param dto
     * @return
     */
    @Override
    AppFormView toEntity(AppFormViewDto dto);


    /**
     * 通过id获取实体
     *
     * @param id
     * @return
     */
    default AppFormView fromId(String id) {
        if (StringUtils.isEmpty(id)) {
            return null;
        }
        AppFormView entity = new AppFormView();
        entity.setId(id);
        return entity;
    }
}
