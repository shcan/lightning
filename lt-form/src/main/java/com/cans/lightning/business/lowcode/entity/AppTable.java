package com.cans.lightning.business.lowcode.entity;

import com.cans.lightning.base.entity.BaseEntity;
import com.cans.lightning.business.lowcode.enums.TableTypeEnum;
import lombok.Getter;
import lombok.Setter;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 应用表单表信息
 *
 * @author cans
 * @date 2021/12/5
 **/
@Getter
@Setter
@TableName("app_table")
public class AppTable extends BaseEntity {

    /**
     * 所属应用
     */
    private String appId;

    /**
     * 所属表单
     */
    private String formId;

    /**
     * 主表/明细表 {@link TableTypeEnum}
     */
    private String tableType;

    /**
     * 主表名
     */
    private String tableName;

    /**
     * 对应数据库表名
     */
    private String dbTableName;
}
