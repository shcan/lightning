package com.cans.lightning.business.lowcode.web;

import com.cans.lightning.base.dto.ResDto;
import com.cans.lightning.base.service.api.IBaseService;
import com.cans.lightning.base.web.BaseController;
import com.cans.lightning.business.lowcode.dto.AppDefinitionDto;
import com.cans.lightning.business.lowcode.entity.AppDefinition;
import com.cans.lightning.business.lowcode.service.api.IAppDefinitionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * 应用管理
 *
 * @author shencan
 * @date 2020/6/14 22:14
 */
@RestController
@RequestMapping("/app_definition")
public class AppDefinitionController extends BaseController<AppDefinition, AppDefinitionDto, String> {

    @Autowired
    private IAppDefinitionService appDefinitionService;

    @GetMapping("/all")
    public ResDto findAll() {
        return ResDto.success(appDefinitionService.findAll());
    }

    @Override
    public IBaseService<AppDefinition, AppDefinitionDto, String> getBaseService() {
        return appDefinitionService;
    }
}
