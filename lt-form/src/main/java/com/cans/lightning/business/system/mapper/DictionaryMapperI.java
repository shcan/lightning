package com.cans.lightning.business.system.mapper;

import com.cans.lightning.base.mapper.IBaseMapper;
import com.cans.lightning.business.system.dto.DictionaryDto;
import com.cans.lightning.business.system.entity.Dictionary;
import org.mapstruct.Mapper;

/**
 * @author shencan
 * @date 2020/12/3 15:25
 */
@Mapper(componentModel = "Spring")
public interface DictionaryMapperI extends IBaseMapper<Dictionary, DictionaryDto,String> {

    @Override
    DictionaryDto toDto(Dictionary entity);
}
