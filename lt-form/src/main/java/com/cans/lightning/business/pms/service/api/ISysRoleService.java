package com.cans.lightning.business.pms.service.api;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cans.lightning.base.service.api.IBaseService;
import com.cans.lightning.business.pms.dto.SysRoleDto;
import com.cans.lightning.business.pms.entity.SysRole;


/**
 * @author shencan
 * @date 2020/6/14 20:39
 */
public interface ISysRoleService extends IBaseService<SysRole, SysRoleDto, String> {

    Page<SysRoleDto> findPage(Page<SysRole> pageRequest);
}
