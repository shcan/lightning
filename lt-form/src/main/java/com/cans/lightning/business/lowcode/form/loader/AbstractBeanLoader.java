package com.cans.lightning.business.lowcode.form.loader;

/**
 * 责任链模式加载Bean对象
 *
 * @author cans
 * @date 2023/11/29
 **/
public abstract class AbstractBeanLoader<T> {

    private AbstractBeanLoader<T> nextLoader;

    public AbstractBeanLoader<T> nextLoader(AbstractBeanLoader<T> nextLoader) {
        this.nextLoader = nextLoader;
        return nextLoader;
    }

    public final T runLoad(T appFormBean) {
        T load = this.loadOne(appFormBean);
        if (this.nextLoader == null) {
            return load;
        }
        return this.nextLoader.runLoad(load);
    }

    public abstract T loadOne(T appFormBean);
}
