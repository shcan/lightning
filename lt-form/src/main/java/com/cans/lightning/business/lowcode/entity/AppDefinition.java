package com.cans.lightning.business.lowcode.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.cans.lightning.base.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 应用定义
 *
 * @author cans
 * @date 2021/12/5
 **/
@Getter
@Setter
@TableName("app_definition")
public class AppDefinition extends BaseEntity {
    /**
     * 包名称
     */
    private String name;
}
