package com.cans.lightning.business.pms.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cans.lightning.business.pms.entity.SysMenu;
import org.apache.ibatis.annotations.Mapper;

/**
 * 权限 - 菜单
 *
 * @author shencan
 * @date 2020/6/14 20:33
 */
@Mapper
public interface SysMenuDao extends BaseMapper<SysMenu> {

}
