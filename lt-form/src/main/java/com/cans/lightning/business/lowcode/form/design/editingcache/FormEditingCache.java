package com.cans.lightning.business.lowcode.form.design.editingcache;

import com.cans.lightning.business.lowcode.form.bean.AppFormBean;
import org.springframework.context.ApplicationContext;

/**
 * @author: qpy
 * @date: 2024-02-04 13:44
 * @description: 表单编辑缓存
 */
public interface FormEditingCache<K, V> {


    /**
     * 添加缓存
     * @param formId    表单id
     * @param formBean  表单对象
     * @return true
     */
    boolean put(K formId, V formBean);

    /**
     * 移除缓存
     * @param formId    表单id
     * @return true
     */
    boolean remove(K formId);
    
    void init(ApplicationContext ioc);
}
