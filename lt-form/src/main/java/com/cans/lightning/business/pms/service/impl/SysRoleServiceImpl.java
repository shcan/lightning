package com.cans.lightning.business.pms.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cans.lightning.base.mapper.IBaseMapper;
import com.cans.lightning.base.service.impl.BaseServiceImpl;
import com.cans.lightning.business.pms.dao.SysRoleDao;
import com.cans.lightning.business.pms.dto.SysRoleDto;
import com.cans.lightning.business.pms.entity.SysRole;
import com.cans.lightning.business.pms.mapper.SysRoleMapper;
import com.cans.lightning.business.pms.service.api.ISysRoleService;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author shencan
 * @date 2020/6/14 20:53
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysRoleServiceImpl extends BaseServiceImpl<SysRole,SysRoleDto, String> implements ISysRoleService {

    @Autowired
    private SysRoleDao sysRoleDao;
    @Autowired
    private SysRoleMapper sysRoleMapper;


    @Override
    public BaseMapper<SysRole> getDaoImpl() {
        return sysRoleDao;
    }

    @Override
    public IBaseMapper<SysRole, SysRoleDto, String> getMapperImpl() {
        return sysRoleMapper;
    }

    @Override
    public Page<SysRoleDto> findPage(Page<SysRole> pageRequest) {
        QueryWrapper<SysRole> queryWrapper = new QueryWrapper<>();
        Page<SysRole> res = sysRoleDao.selectPage(pageRequest,queryWrapper);
        Page<SysRoleDto> objectPage = Page.of(res.getCurrent(), res.getSize(), res.getTotal());
        objectPage.setRecords( this.sysRoleMapper.toDtos(res.getRecords()));
        return objectPage;
    }
}
