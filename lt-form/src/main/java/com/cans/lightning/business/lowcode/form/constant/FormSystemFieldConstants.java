package com.cans.lightning.business.lowcode.form.constant;

/**
 * @author: qpy
 * @date: 2024-01-31 13:58
 * @description:
 */
public class FormSystemFieldConstants {

    /**
     * 创建时间
     */
    public static final String CREATE_TIME = "create_time";
    /**
     * 创建时间
     */
    public static final String LAST_CHANGE_TIME = "last_change_time";
    /**
     * 创建时间
     */
    public static final String CREATE_USER = "create_user";
    
}
