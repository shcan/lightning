package com.cans.lightning.business.lowcode.form.design.save;


import com.cans.lightning.business.lowcode.form.design.save.context.AppFormDesignSaveContext;

/**
 * @author cans
 * @date 2023/12/1
 **/
public abstract class AbstractBeanSaveHandler<T> {

    private AbstractBeanSaveHandler<T> nextHandler;

    public AbstractBeanSaveHandler<T> nextHandler(AbstractBeanSaveHandler<T> nextHandler) {
        this.nextHandler = nextHandler;
        return nextHandler;
    }

    public final T runSaveHandle(AppFormDesignSaveContext<T> saveContext) {
        T load = this.saveOne(saveContext);
        if (this.nextHandler == null) {
            return load;
        }
        return this.nextHandler.runSaveHandle(saveContext);
    }

    public abstract T saveOne(AppFormDesignSaveContext<T> saveContext);
}
