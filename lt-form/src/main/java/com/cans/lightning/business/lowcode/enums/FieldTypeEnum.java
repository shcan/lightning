package com.cans.lightning.business.lowcode.enums;

import com.google.common.collect.Lists;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 字段类型类型
 *
 * @author cans
 * @date 2021-04-22 11:11
 **/
public enum FieldTypeEnum {

    VARCHAR("varchar", "文本"),
    INT("int", "数字"),
    DATE("date", "日期"),
    DATE_TIME("date_time", "日期时间"),
    FILE("file", "文件"),
    DECIMAL("DECIMAL", "浮点数");

    private final String key;
    private final String value;

    FieldTypeEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    /**
     * 通过值得到value
     *
     * @param key
     * @return
     */
    public static String getValueByKey(String key) {
        if (!StringUtils.isEmpty(key)) {
            for (FieldTypeEnum e : FieldTypeEnum.values()) {
                if (e.getKey().equals(key)) {
                    return e.getValue();
                }
            }
        }
        return null;
    }

    /**
     * 通过值得到value
     *
     * @param key
     * @return
     */
    public static FieldTypeEnum getByKey(String key) {
        if (!StringUtils.isEmpty(key)) {
            for (FieldTypeEnum e : FieldTypeEnum.values()) {
                if (e.getKey().equals(key)) {
                    return e;
                }
            }
        }
        return null;
    }

    public static String getKeyByValue(String value) {
        if (!StringUtils.isEmpty(value)) {
            for (FieldTypeEnum e : FieldTypeEnum.values()) {
                if (e.getValue().equals(value)) {
                    return e.getKey();
                }
            }
        }
        return null;
    }

    public static List<Map<String, Object>> list() {

        List<Map<String, Object>> list = Lists.newArrayList();

        for (FieldTypeEnum e : FieldTypeEnum.values()) {
            HashMap<String, Object> map = new HashMap<>(1);
            map.put("key", e.getKey());
            map.put("value", e.getValue());
            list.add(map);
        }

        return list;

    }
}
