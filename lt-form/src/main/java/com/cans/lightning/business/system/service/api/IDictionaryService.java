package com.cans.lightning.business.system.service.api;

import com.cans.lightning.base.service.api.IBaseService;
import com.cans.lightning.business.system.dto.DictionaryDto;
import com.cans.lightning.business.system.dto.DictionaryItemDto;
import com.cans.lightning.business.system.entity.Dictionary;

import java.util.List;


/**
 * 数据字典服务
 *
 * @author shencan
 * @date 2020/6/14 20:39
 */
public interface IDictionaryService extends IBaseService<Dictionary,DictionaryDto, String> {


    List<DictionaryItemDto> getDictItems(String dictCode);

    Dictionary getByCode(String dictCode);
}
