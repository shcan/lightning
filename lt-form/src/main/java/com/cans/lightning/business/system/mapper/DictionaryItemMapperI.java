package com.cans.lightning.business.system.mapper;

import com.cans.lightning.base.mapper.IBaseMapper;
import com.cans.lightning.business.system.dto.DictionaryItemDto;
import com.cans.lightning.business.system.entity.DictionaryItem;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * @author shencan
 * @date 2020/12/3 15:25
 */
@Mapper(componentModel = "Spring")
public interface DictionaryItemMapperI extends IBaseMapper<DictionaryItem, DictionaryItemDto,String> {

    @Override
    @Mapping(source = "dictionaryId", target = "dictionaryId")
    DictionaryItemDto toDto(DictionaryItem entity);


    @Override
    @Mapping(source = "dictionaryId", target = "dictionaryId")
    DictionaryItem toEntity(DictionaryItemDto dictionaryItemDto);
}
