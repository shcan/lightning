package com.cans.lightning.business.lowcode.dao;

import com.cans.lightning.base.dao.api.IBaseDao;
import com.cans.lightning.business.lowcode.entity.AppDefinition;
import org.apache.ibatis.annotations.Mapper;

/**
 * 应用定义
 *
 * @author shencan
 * @date 2020/6/14 20:33
 */
@Mapper
public interface AppDefinitionDao extends IBaseDao<AppDefinition> {
}
