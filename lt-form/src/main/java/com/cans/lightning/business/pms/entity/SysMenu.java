package com.cans.lightning.business.pms.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.cans.lightning.base.entity.BaseEntity;
import com.cans.lightning.business.pms.enums.MenuTYpeEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * 权限管理 - 菜单
 *
 * @author shencan
 * @date 2020/9/20 18:44
 */
@Getter
@Setter
@TableName("sys_menu")
public class SysMenu extends BaseEntity {

    /**
     * 菜单编号
     */
    private String menuCode;

    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 翻译
     */
    private String i18n;

    /**
     * 路由
     */
    private String url;

    /**
     * 图标
     */
    private String icon;

    /**
     * 排序号
     */
    private Integer idx;

    /**
     * 菜单类型
     *
     * @see MenuTYpeEnum
     */
    private Integer menuType;

    /**
     * 菜单绑定ID,根据menuType一起进行判定
     */
    private String bindingId;

    /**
     * 父ID
     */
    private String parentId;

    /**
     * 打开方式
     */
    private String openType;


    @Override
    public boolean equals(Object o) {

        if (o == this) {
            return true;
        }

        if (!(o instanceof SysMenu)) {
            return false;
        }

        return this.getId().equals(((SysMenu) o).getId());

    }


    @Override
    public int hashCode() {

        int result = 37;

        result = 31 * result + (getId() == null ? 0 : getId().hashCode());

        return result;
    }
}
