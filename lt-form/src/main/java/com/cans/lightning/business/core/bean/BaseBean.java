package com.cans.lightning.business.core.bean;

import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/**
 * @author cans
 * @date 2023/11/28
 **/
@Getter
@Setter
public class BaseBean implements Serializable {

    protected final static Logger logger = LoggerFactory.getLogger(BaseBean.class);
    
    private String id;

    @Override
    public BaseBean clone() throws CloneNotSupportedException {
        BaseBean baseBean = null;
        try {
            baseBean = this.getClass().newInstance();
            baseBean.setId(this.getId());
        } 
        catch (Exception e) {
            logger.error("clone()报错: " + e.getMessage());
            throw new RuntimeException(e);
        }
        return baseBean;
    }
}
