package com.cans.lightning.business.system.service.api;

import com.cans.lightning.base.service.api.IBaseService;
import com.cans.lightning.business.system.dto.FileInfoDto;
import com.cans.lightning.business.system.entity.FileInfo;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.OutputStream;


/**
 * @author shencan
 * @date 2020/6/14 20:39
 * @description
 */
public interface IFileInfoService extends IBaseService<FileInfo, FileInfoDto, String> {


    /**
     * 文件上床
     *
     * @param multipartFile 接收的文件信息
     * @return
     * @throws IOException
     */
    FileInfo upload(MultipartFile multipartFile) throws IOException;

    /**
     * 展示图片
     *
     * @param outputStream 输出流
     * @param fileId       文件的ID
     * @return
     */
    void view(OutputStream outputStream, String fileId) throws IOException;

}
