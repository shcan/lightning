package com.cans.lightning.business.lowcode.vo;

import com.cans.lightning.business.core.bean.BaseBean;
import com.cans.lightning.business.lowcode.form.bean.*;
import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

/**
 * @author: qpy
 * @date: 2024-02-02 11:17
 * @description:
 */
@Setter
@Getter
public class AppFormBeanVo extends BaseBean {

    public static final String CURRENT_VIEW_ID = "currentViewId";

    /**
     * 表单名称
     */
    private String formName;

    /**
     * 应用ID
     */
    private String appId;
    /**
     * 字段游标
     */
    private Integer fieldIndex = 1;

    /**
     * 主表
     */
    private MainTableBean mainTableBean;

    /**
     * 明细表
     */
    private List<SonTableBean> sonTableList;

    /**
     * 视图信息
     */
    private List<ViewBean> viewList;

    /**
     * 当前的视图
     */
    private ViewBean viewBean = new ViewBean();

    /**
     * 表单权限
     */
    private List<FormAuthBean> formAuthList;

    /**
     * key 数据库字段名
     */
    private Map<String, FormFieldBean> fieldMap;

    /**
     * 当前展示的信息
     * 1. 当前展示的视图 currentViewId
     */
    private Map<String, String> currentInfo = Maps.newHashMap(); 

}
