package com.cans.lightning.business.lowcode.form.bean;

import com.cans.lightning.business.core.bean.BaseBean;
import com.cans.lightning.business.lowcode.dto.AppFormBindDto;
import lombok.Getter;
import lombok.Setter;

/**
 * @author cans
 * @date 2024/9/15 14:35
 * @description 表单绑定信息
 */
@Getter
@Setter
public class FormBindBean extends BaseBean {

    /**
     * 绑定名称
     */
    private String bindName;

    /**
     * 列表显示项(数据库字段名)
     */
    private String fieldListShow;

    /**
     * 列表显示项(描述)
     */
    private String fieldListShowDesc;

    /**
     * 排序设置
     */
    private String sortSetting;

    /**
     * 自定义筛选项
     */
    private String filterField;

    /**
     * 授权信息
     */
    private String authInfo;

    public FormBindBean transferFrom(AppFormBindDto appFormBindDto) {
        this.setId(appFormBindDto.getId());
        this.setBindName(appFormBindDto.getBindName());
        this.setFieldListShow(appFormBindDto.getFieldListShow());
        this.setSortSetting(appFormBindDto.getSortSetting());
        this.setFilterField(appFormBindDto.getFilterField());
        this.setAuthInfo(appFormBindDto.getAuthInfo());
        return this;
    }
}

