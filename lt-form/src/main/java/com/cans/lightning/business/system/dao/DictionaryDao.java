package com.cans.lightning.business.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cans.lightning.business.system.entity.Dictionary;
import org.apache.ibatis.annotations.Mapper;

/**
 * 数据字典
 *
 * @author shencan
 * @date 2020/6/14 20:33
 */
@Mapper
public interface DictionaryDao extends BaseMapper<Dictionary> {
}
