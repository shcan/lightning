package com.cans.lightning.business.lowcode.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 表单一条数据数据
 *
 * @author cans
 * @date 2023/11/25
 **/
@Getter
@Setter
public class AppFormRowDataDto {
    /**
     * 表单ID
     */
    String formId;
    /**
     * 数据ID
     */
    String dataId;
    /**
     * 表单数据
     */
    List<AppFormFieldValueDto> fieldValue;
}
