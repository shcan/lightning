package com.cans.lightning.business.system.dto;

import com.cans.lightning.base.dto.BaseDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 系统配置DTO
 *
 * @author cans
 * @date 2021-04-06 14:55
 **/
@Getter
@Setter
public class SystemConfigDto extends BaseDto<String> {

    /**
     * 参数key值
     */
    private String keyCode;

    /**
     * 参数名称
     */
    private String name;

    /**
     * 参数值
     */
    private String value;

    /**
     * 是否系统预设 1: 是 0: 否
     */
    private Integer preSet;

    /**
     * 备注
     */
    private String memo;

    /**
     * 排序号
     */
    private Integer idx;
}
