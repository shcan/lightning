package com.cans.lightning.business.system.dto;

import com.cans.lightning.base.dto.BaseDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 数据字典DTO
 *
 * @author cans
 * @date 2021-03-29 15:21
 **/
@Getter
@Setter
public class DictionaryDto extends BaseDto<String> {

    private String id;
    /**
     * 编号
     */
    private String code;

    /**
     * 名称
     */
    private String name;

    /**
     * 排序号
     */
    private Integer idx;

}
