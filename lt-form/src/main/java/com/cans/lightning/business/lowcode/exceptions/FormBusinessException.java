package com.cans.lightning.business.lowcode.exceptions;

import com.cans.lightning.base.exceptions.BusinessException;

public class FormBusinessException extends BusinessException {

    public FormBusinessException(String msg) {
        super(msg);
    }

    public FormBusinessException(String msg, Throwable throwable) {
        super(msg, throwable);
    }
}
