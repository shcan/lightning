package com.cans.lightning.business.system.handler.api;

import com.cans.lightning.business.system.entity.FileInfo;

import java.io.IOException;
import java.io.OutputStream;

/**
 * 图片预览处理器
 *
 * @author shenc
 * @date 2021-09-20 20:21
 **/
public interface PictureViewHandler {


    /**
     * 处理类型
     * @return
     */
    String type();

    /**
     * 预览图片
     *
     * @param outputStream
     * @param fileInfo
     * @return
     */
    void view(OutputStream outputStream, FileInfo fileInfo) throws IOException;
}
