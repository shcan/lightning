package com.cans.lightning.business.system.web;

import com.cans.lightning.business.system.service.api.ICodeGenerateConfigService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * 编号生成规则配置
 *
 * @author shencan
 * @date 2020/6/21 20:16
 */
@RestController
@RequestMapping("/code-generate-config")
public class CodeGenerateConfigController {

    @Autowired
    private ICodeGenerateConfigService codeGenerateConfigService;
}
