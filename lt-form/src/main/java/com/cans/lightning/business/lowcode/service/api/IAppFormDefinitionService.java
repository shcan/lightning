package com.cans.lightning.business.lowcode.service.api;

import com.cans.lightning.base.service.api.IBaseService;
import com.cans.lightning.business.lowcode.dto.AppFormDto;
import com.cans.lightning.business.lowcode.entity.AppForm;
import com.cans.lightning.business.lowcode.form.bean.AppFormBean;

import java.util.List;


/**
 * 应用信息
 *
 * @author shencan
 * @date 2020/6/14 20:39
 */
public interface IAppFormDefinitionService extends IBaseService<AppForm, AppFormDto, String> {

    List<AppFormDto> getByAppId(String appId);

    void saveToDb(AppFormDto appFormDto);

    AppFormDto getInfo(String id);

    String getNextFieldIndex(String formId);

    AppFormBean getFormBean(String formId);

}
