package com.cans.lightning.business.lowcode.dto;

import com.cans.lightning.base.dto.BaseDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 游标信息
 *
 * @author cans
 * @date 2021/12/5
 **/
@Getter
@Setter
public class AppIndexDto extends BaseDto<String> {

    /**
     * 名称
     */
    private String name;

    /**
     * 目前游标
     */
    private Long value;

}
