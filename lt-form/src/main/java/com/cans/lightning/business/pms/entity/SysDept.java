package com.cans.lightning.business.pms.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.cans.lightning.base.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * @author shencan
 * @date 2020/7/26 15:48
 */
@Getter
@Setter
@TableName( "sys_dept")
public class SysDept extends BaseEntity {


    /**
     * 部门编号
     */
    private String deptCode;

    /**
     * 部门名称
     */
    private String deptName;


    @Override
    public boolean equals(Object o) {

        if (o == this) {
            return true;
        }

        if (!(o instanceof SysDept)) {
            return false;
        }

        return this.getId().equals(((SysDept) o).getId());

    }


    @Override
    public int hashCode() {

        int result = 47;

        result = 31 * result + (getId() == null ? 0 : getId().hashCode());

        return result;
    }
}
