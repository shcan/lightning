package com.cans.lightning.business.lowcode.mapper;

import com.cans.lightning.base.mapper.IBaseMapper;
import com.cans.lightning.business.lowcode.dto.AppMenuDto;
import com.cans.lightning.business.lowcode.entity.AppMenu;
import org.mapstruct.Mapper;
import org.springframework.util.StringUtils;

/**
 * @author shencan
 * @date 2020/12/2 19:06
 */
@Mapper(componentModel = "spring")
public interface AppMenuMapper extends IBaseMapper<AppMenu, AppMenuDto,String> {

    /**
     * 转换为Entity
     *
     * @param dto
     * @return
     */
    @Override
    AppMenu toEntity(AppMenuDto dto);


    /**
     * 通过id获取实体
     *
     * @param id
     * @return
     */
    default AppMenu fromId(String id) {
        if (StringUtils.isEmpty(id)) {
            return null;
        }
        AppMenu entity = new AppMenu();
        entity.setId(id);
        return entity;
    }
}
