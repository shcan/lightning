package com.cans.lightning.business.lowcode.dto;

import com.cans.lightning.business.lowcode.enums.FieldTypeEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author cans
 * @date 2023/11/25
 **/
@Getter
@Setter
public class AppFormFieldValueDto {

    /**
     * 字段名称
     */
    private String dbFieldName;
    /**
     * @see FieldTypeEnum
     * 数据库字段类型
     */
    private String dbFieldDataType;
    /**
     * 字段值
     */
    private String value;

    /**
     * 字段值
     */
    private String dbValue;

}
