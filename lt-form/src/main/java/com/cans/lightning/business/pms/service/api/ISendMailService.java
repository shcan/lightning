package com.cans.lightning.business.pms.service.api;


import jakarta.mail.MessagingException;

/**
 * 类描述
 *
 * @author cans
 * @date 2021-05-19 14:53
 **/
public interface ISendMailService {

     boolean sendMail(String toMail,String subject,String content) throws MessagingException;
}
