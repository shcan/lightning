package com.cans.lightning.business.lowcode.mapper;

import com.cans.lightning.base.mapper.IBaseMapper;
import com.cans.lightning.business.lowcode.dto.AppTableFieldDto;
import com.cans.lightning.business.lowcode.entity.AppTableField;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.util.StringUtils;

/**
 * @author shencan
 * @date 2020/12/2 19:06
 */
@Mapper(componentModel = "spring")
public interface AppTableFieldMapper extends IBaseMapper<AppTableField, AppTableFieldDto,String> {

    /**
     * 转换为Entity
     *
     * @param dto
     * @return
     */
    @Override
    @Mapping(source = "fieldDataType",target = "fieldType")
    @Mapping(source = "dbFieldDataType",target = "dbFieldType")
    AppTableField toEntity(AppTableFieldDto dto);

    @Override
    @Mapping(source = "fieldType",target = "fieldDataType")
    @Mapping(source = "dbFieldType",target = "dbFieldDataType")
    AppTableFieldDto toDto(AppTableField appTableField);

    /**
     * 通过id获取实体
     *
     * @param id
     * @return
     */
    default AppTableField fromId(String id) {
        if (StringUtils.isEmpty(id)) {
            return null;
        }
        AppTableField entity = new AppTableField();
        entity.setId(id);
        return entity;
    }
}
