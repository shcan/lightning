package com.cans.lightning.business.system.dao;

import com.cans.lightning.business.system.entity.FileInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 文件信息
 *
 * @author shencan
 * @date 2020/6/14 20:33
 */
@Mapper
public interface FileInfoDao extends BaseMapper<FileInfo> {
}
