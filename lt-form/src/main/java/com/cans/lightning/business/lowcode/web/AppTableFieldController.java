package com.cans.lightning.business.lowcode.web;

import com.cans.lightning.base.service.api.IBaseService;
import com.cans.lightning.base.web.BaseController;
import com.cans.lightning.business.lowcode.dto.AppTableFieldDto;
import com.cans.lightning.business.lowcode.entity.AppTableField;
import com.cans.lightning.business.lowcode.service.api.IAppTableFieldService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * 字段
 *
 * @author shencan
 * @date 2020/6/14 22:14
 */
@RestController
@RequestMapping("/app_table_field")
public class AppTableFieldController extends BaseController<AppTableField, AppTableFieldDto, String> {

    @Autowired
    private IAppTableFieldService appTableFieldService;

    @Override
    public IBaseService<AppTableField, AppTableFieldDto, String> getBaseService() {
        return appTableFieldService;
    }
}
