package com.cans.lightning.business.lowcode.form.loader.tableBean;

import com.cans.lightning.business.lowcode.form.bean.MainTableBean;
import com.cans.lightning.business.lowcode.form.loader.AbstractBeanLoader;
import org.springframework.stereotype.Component;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author cans
 * @date 2023/11/29
 **/
@Component
public class MainTableBeanLoaderBuilder {

    @Autowired
    private MainTableBeanLoader mainTableBeanLoader;

    public AbstractBeanLoader<MainTableBean> getLoader(){
        return mainTableBeanLoader;
    }
}
