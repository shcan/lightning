package com.cans.lightning.business.pms.enums;

import com.google.common.collect.Lists;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 菜单类型枚举
 *
 * @author cans
 * @date 2021-04-22 11:11
 **/
public enum MenuTYpeEnum {

    /**
     * 系统菜单
     */
    SYSTEM(0, "系统菜单"),
    /**
     * 低代码APP
     */
    LOW_CODE_APP(1, "低代码APP"),

    /**
     * 表单应用绑定
     */
    LOW_CODE_FORM_BINDING(2, "表单应用绑定");

    private final Integer key;
    private final String value;

    MenuTYpeEnum(Integer key, String value) {
        this.key = key;
        this.value = value;
    }

    public Integer getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    /**
     * 通过值得到value
     *
     * @param key
     * @return
     */
    public static String getValueByKey(Integer key) {
        if (!StringUtils.isEmpty(key)) {
            for (MenuTYpeEnum e : MenuTYpeEnum.values()) {
                if (e.getKey().equals(key)) {
                    return e.getValue();
                }
            }
        }
        return null;
    }

    /**
     * 通过值得到value
     *
     * @param key
     * @return
     */
    public static MenuTYpeEnum getByKey(Integer key) {
        if (!StringUtils.isEmpty(key)) {
            for (MenuTYpeEnum e : MenuTYpeEnum.values()) {
                if (e.getKey().equals(key)) {
                    return e;
                }
            }
        }
        return null;
    }

    public static Integer getKeyByValue(String value) {
        if (!StringUtils.isEmpty(value)) {
            for (MenuTYpeEnum e : MenuTYpeEnum.values()) {
                if (e.getValue().equals(value)) {
                    return e.getKey();
                }
            }
        }
        return null;
    }

    public static List<Map<String, Object>> list() {

        List<Map<String, Object>> list = Lists.newArrayList();

        for (MenuTYpeEnum e : MenuTYpeEnum.values()) {
            HashMap<String, Object> map = new HashMap<>(1);
            map.put("key", e.getKey());
            map.put("value", e.getValue());
            list.add(map);
        }

        return list;

    }
}
