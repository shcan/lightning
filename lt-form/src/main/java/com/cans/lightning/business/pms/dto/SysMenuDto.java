package com.cans.lightning.business.pms.dto;

import com.cans.lightning.base.dto.BaseDto;
import com.cans.lightning.business.lowcode.enums.AppMenuTYpeEnum;
import com.cans.lightning.business.pms.entity.SysMenu;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import java.util.List;

/**
 * @author shencan
 * @date 2020/9/20 22:22
 */
@Getter
@Setter
public class SysMenuDto extends BaseDto<String> {

    /**
     * ID
     */
    private String id;
    /**
     * 菜单编号
     */
    private String menuCode;

    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 路由
     */
    private String url;

    /**
     * 图标
     */
    private String icon;

    /**
     * 父ID
     */
    private String parentId;
    /**
     * 菜单类型
     *
     * @see AppMenuTYpeEnum
     */
    private Integer appMenuType;

    /**
     * 菜单绑定ID,根据AppMenuTYpeEnum一起进行判定
     */
    private String bindingId;

    /**
     * 排序号
     */
    private Integer idx;

    /**
     * 下挂菜单
     */
    private List<SysMenuDto> children;

    public SysMenu toEntity(){
        SysMenu sysMenu = new SysMenu();
        BeanUtils.copyProperties(this,sysMenu);
        return sysMenu;
    }

    public SysMenuDto create(SysMenu sysMenu){
        BeanUtils.copyProperties(sysMenu,this);
        return this;
    }
}
