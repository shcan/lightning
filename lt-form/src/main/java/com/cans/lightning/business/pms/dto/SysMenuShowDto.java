package com.cans.lightning.business.pms.dto;

import com.cans.lightning.base.dto.BaseDto;
import com.cans.lightning.business.lowcode.dto.AppDefinitionDto;
import com.cans.lightning.business.lowcode.dto.AppMenuDto;
import com.cans.lightning.business.pms.entity.SysMenu;
import com.cans.lightning.business.pms.enums.MenuTYpeEnum;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import java.util.List;

/**
 * @author shencan
 * @date 2020/9/20 22:22
 */
@Getter
@Setter
public class SysMenuShowDto extends BaseDto<String> {

    /**
     * ID
     */
    private String id;
    /**
     * 菜单编号
     */
    private String menuCode;

    /**
     * 菜单名称
     */
    private String text;

    /**
     * 翻译
     */
    private String i18n;

    /**
     * 路由
     */
    private String link;

    /**
     * 图标
     */
    private String icon;
    /**
     * 菜单类型
     *
     * @see MenuTYpeEnum
     */
    private Integer menuType;

    /**
     * 菜单绑定ID,根据menuType一起进行判定
     */
    private String bindingId;

    /**
     * 打开方式
     */
    private String openType;

    /**
     * 下挂菜单
     */
    private List<SysMenuShowDto> children;

    public SysMenuShowDto create(SysMenu menu) {
        this.setMenuType(MenuTYpeEnum.SYSTEM.getKey());
        BeanUtils.copyProperties(menu,this);
        return this;
    }

    public SysMenuShowDto create(AppDefinitionDto appDefinitionDto) {
        BeanUtils.copyProperties(appDefinitionDto,this);
        this.setMenuType(MenuTYpeEnum.LOW_CODE_APP.getKey());
        this.setBindingId(appDefinitionDto.getId());
        this.setText(appDefinitionDto.getName());
        return this;
    }

    public SysMenuShowDto create(AppMenuDto appMenu) {
        this.setId(appMenu.getId());
        this.setText(appMenu.getName());
        this.setBindingId(appMenu.getBindingId());
        this.setLink(appMenu.getUrl());
        this.setOpenType(appMenu.getOpenType());
        this.setMenuType(MenuTYpeEnum.LOW_CODE_FORM_BINDING.getKey());
        return this;
    }
}
