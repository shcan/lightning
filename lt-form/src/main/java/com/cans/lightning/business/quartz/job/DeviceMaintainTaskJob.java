package com.cans.lightning.business.quartz.job;

import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * 设备维保任务生成JOB
 *
 * @author cans
 * @date 2021-03-12 09:33
 **/
@Slf4j
public class DeviceMaintainTaskJob extends QuartzJobBean {


    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        // 任务名称,对应的计划code
        String name = jobExecutionContext.getJobDetail().getKey().getName();
        // 通过计划code生成维保计划
        // 业务逻辑 ...
        log.error("根据任务name[" + name + "]生成维保计划....");

    }
}
