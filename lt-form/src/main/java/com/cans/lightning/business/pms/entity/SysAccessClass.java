package com.cans.lightning.business.pms.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.cans.lightning.base.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * API权限控制-类
 *
 * @author cans
 * @date 2021-05-07 14:08
 **/
@Getter
@Setter
@TableName("sys_access_class")
public class SysAccessClass extends BaseEntity {

    /**
     * 类标识(注解描述)
     */
    private String classMark;

    /**
     * 全限定类名
     */
    private String className;

    /**
     * 非限定类名
     */
    private String shortClassName;
}
