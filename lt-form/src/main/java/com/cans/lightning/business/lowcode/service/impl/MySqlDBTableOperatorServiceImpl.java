package com.cans.lightning.business.lowcode.service.impl;

import com.baomidou.mybatisplus.extension.toolkit.SqlRunner;
import com.cans.lightning.business.lowcode.dto.AppTableDto;
import com.cans.lightning.business.lowcode.dto.AppTableFieldDto;
import com.cans.lightning.business.lowcode.enums.FieldTypeEnum;
import com.cans.lightning.business.lowcode.service.api.IDBTableDoOperatorService;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 数据库表操作Service
 *
 * @author cans
 * @date 2022/11/6
 **/
@Service
public class MySqlDBTableOperatorServiceImpl implements IDBTableDoOperatorService {

    private static final Logger logger = LoggerFactory.getLogger(MySqlDBTableOperatorServiceImpl.class);

    /**
     * 创建数据库表
     *
     * @param tableDto 表信息
     */
    @Override
    public void createTable(AppTableDto tableDto) {
        String dbTableName = tableDto.getDbTableName();
        Map<String, Object> selectOne = SqlRunner.db().selectOne("SHOW TABLES LIKE '" + dbTableName + "' ");
        boolean existTable = selectOne != null;
        if (existTable) {
            logger.info("表[" + dbTableName + "]已存在");
            return;
        }

        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("CREATE TABLE ").append(dbTableName).append(" ( ");
        sqlBuilder.append("  `id` varchar(36) COLLATE utf8mb4_bin NOT NULL, ");

        List<AppTableFieldDto> fieldArr = tableDto.getFieldArr().stream().filter(item -> item.getId() != null && !item.getDelete()).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(fieldArr)) {
            for (AppTableFieldDto fieldDto : fieldArr) {
                sqlBuilder.append(this.getAppendFieldSql(fieldDto));
                sqlBuilder.append(" COLLATE utf8mb4_bin DEFAULT NULL, ");
            }
        }
        sqlBuilder.append("  `create_time` datetime NOT NULL, ");
        sqlBuilder.append("  `last_change_time` datetime NOT NULL, ");
        sqlBuilder.append("  `create_user` varchar(36) DEFAULT NULL, ");
        sqlBuilder.append("  PRIMARY KEY (`id`) USING BTREE " +
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;");

        SqlRunner.db().update(sqlBuilder.toString(), Maps.newHashMap());
    }

    @Override
    public void createField(AppTableDto tableDto, AppTableFieldDto fieldDto) {
        String sqlBuilder = "ALTER TABLE " + tableDto.getDbTableName() + " ADD COLUMN " +
                this.getAppendFieldSql(fieldDto)
                + " DEFAULT NULL COMMENT '" + fieldDto.getFieldName() + "' ";
        SqlRunner.db().update(sqlBuilder, Maps.newHashMap());
    }

    @Override
    public void ifNotExitCreate(AppTableDto appTableDto, AppTableFieldDto fieldDto) {
        long l = SqlRunner.db().selectCount("SELECT count(1) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='" + appTableDto.getTableName() + "' AND COLUMN_NAME='" + fieldDto.getDbFieldName() + "';");
        if ( l == 0) {
            this.createField(appTableDto, fieldDto);
        }
    }

    @Override
    public int getDbType() {
        return 1;
    }

    private String getAppendFieldSql(AppTableFieldDto fieldDto) {
        StringBuilder sqlBuilder = new StringBuilder();
        Pair<String, Integer> fieldType = this.getFieldType(fieldDto);
        String dbType = fieldType.getLeft();
        Integer length = fieldType.getRight();
        sqlBuilder.append("  `").append(fieldDto.getDbFieldName()).append("` ").append(dbType);
        if (length != null) {
            sqlBuilder.append("(").append(length).append(")");
        }
        return sqlBuilder.toString();
    }

    private Pair<String, Integer> getFieldType(AppTableFieldDto fieldDto) {
        FieldTypeEnum typeEnum = FieldTypeEnum.getByKey(fieldDto.getDbFieldDataType());
        if (typeEnum == null) {
            throw new RuntimeException("不支持的字段类型: " + fieldDto.getDbFieldDataType());
        }
        String dbType = "";
        Integer length = fieldDto.getLength();
        switch (typeEnum) {
            case VARCHAR:
                dbType = "varchar";
                length = length == null ? 200 : length;
                break;
            case INT:
                dbType = "int";
                break;
            case DATE:
                dbType = "date";
                length = null;
                break;
            case FILE:
                dbType = "varchar";
                length = 36;
                break;
            case DECIMAL:
                dbType = "decimal";
                break;
            case DATE_TIME:
                dbType = "datetime";
                length = null;
                break;
        }
        return Pair.of(dbType, length);
    }
}
