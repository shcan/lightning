package com.cans.lightning.business.lowcode.form.design.editingcache;

import com.cans.lightning.base.enums.CacheType;
import com.cans.lightning.business.lowcode.form.constant.FormCacheConstants;
import com.cans.lightning.utils.StringUtils;
import org.springframework.context.ApplicationContext;

/**
 * @author: qpy
 * @date: 2024-02-04 14:24
 * @description:
 */
public class FormEditingCacheFactory {

    /**
     * 
     * @param prefix
     * @param type
     * @param ioc
     * @return
     */
    public static <K, V> FormEditingCache<K, V> createEditCache(String prefix, String type, ApplicationContext ioc) {

        CacheType cacheType = null;
        if (StringUtils.isEmpty(type)) {
            cacheType = CacheType.LOCAL;                
        } 
        else {
            cacheType = CacheType.valueOf(type.toUpperCase());
        }
        
        FormEditingCache<K, V> editingCache = null;
        switch (cacheType) {
            case REDIS:
                editingCache = new RedisFormEditingCache<K, V>(prefix, FormCacheConstants.CACHE_TIME_UNIT, FormCacheConstants.EDITING_TIME);
                editingCache.init(ioc);
                break;
            case LOCAL:
            default:
                editingCache = new LocalFormEditingCache<>();
                break;
        }
        return editingCache;
    }
}
