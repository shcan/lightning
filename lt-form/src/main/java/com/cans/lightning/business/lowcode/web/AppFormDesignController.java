package com.cans.lightning.business.lowcode.web;

import com.cans.lightning.base.dto.ResDto;
import com.cans.lightning.business.lowcode.dto.AppTableFieldDto;
import com.cans.lightning.business.lowcode.form.bean.AppFormBean;
import com.cans.lightning.business.lowcode.form.design.FormDesignManager;
import com.cans.lightning.business.lowcode.form.design.MergeData;
import com.cans.lightning.business.lowcode.service.api.IAppFormDefinitionService;
import com.cans.lightning.business.lowcode.vo.AppFormBeanVo;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;
import java.util.concurrent.ExecutionException;

/**
 * 表单
 *
 * @author shencan
 * @date 2020/6/14 22:14
 */
@RestController
@RequestMapping("/app_form_design")
public class AppFormDesignController  {

    @Autowired
    private IAppFormDefinitionService appFormService;
    @Autowired
    private FormDesignManager formDesignManager;

    @GetMapping("/editForm/{id}")
    public ResDto<AppFormBeanVo> designBean(@PathVariable("id") String id) {
        return ResDto.success(formDesignManager.editForm(id));
    }

    @PostMapping("/mergeBaseInfo")
    public ResDto<String> mergeBaseInfo(@RequestBody MergeData mergeData){
        try {
            formDesignManager.mergeBaseInfo(mergeData);
        } catch (RuntimeException e) {
            return ResDto.fait(e.getMessage());
        }
        return ResDto.success();
    }

    @GetMapping("/removeEdit/{formId}")
    public ResDto<String> removeEdit( @PathVariable("formId") String formId){
        formDesignManager.removeEdit(formId);
        return ResDto.success();
    }

    @GetMapping("/loadFormBind/{formId}")
    public ResDto<Object> loadFormBind( @PathVariable("formId") String formId){
        return ResDto.success(formDesignManager.loadFormBind(formId));
    }

    @GetMapping("/loadFormField/{formId}")
    public ResDto<Object> loadFormField( @PathVariable("formId") String formId){
        return ResDto.success(formDesignManager.loadFormField(formId));
    }


    @PostMapping("/saveToDb")
    public ResDto<String> saveToDb(@RequestBody AppFormBean appFormBean) throws ExecutionException {
        formDesignManager.saveToDb(appFormBean);
        formDesignManager.removeEdit(appFormBean.getId());
        return ResDto.success();
    }

    /**
     * 获取下一个字段索引ID
     * @param formId
     * @return
     */
    @GetMapping("/getNextFieldIndex/{formId}")
    public ResDto<String> getNextFieldIndex(@PathVariable("formId") String formId) {
        return ResDto.success(appFormService.getNextFieldIndex(formId));
    }

    @PostMapping("/addField")
    public ResDto<AppTableFieldDto> addField(@RequestBody AppTableFieldDto tableFieldDto) throws ExecutionException {
        return ResDto.success(formDesignManager.addField(tableFieldDto));
    }
}
