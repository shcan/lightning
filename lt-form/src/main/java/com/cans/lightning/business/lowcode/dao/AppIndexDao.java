package com.cans.lightning.business.lowcode.dao;

import com.cans.lightning.base.dao.api.IBaseDao;
import com.cans.lightning.business.lowcode.entity.AppIndex;
import org.apache.ibatis.annotations.Mapper;

/**
 * 游标
 *
 * @author shencan
 * @date 2020/6/14 20:33
 */
@Mapper
public interface AppIndexDao extends IBaseDao<AppIndex> {
}
