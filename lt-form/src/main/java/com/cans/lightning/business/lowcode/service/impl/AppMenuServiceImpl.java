package com.cans.lightning.business.lowcode.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.cans.lightning.base.mapper.IBaseMapper;
import com.cans.lightning.base.service.impl.BaseServiceImpl;
import com.cans.lightning.business.lowcode.dao.AppMenuDao;
import com.cans.lightning.business.lowcode.dto.AppMenuDto;
import com.cans.lightning.business.lowcode.entity.AppMenu;
import com.cans.lightning.business.lowcode.mapper.AppMenuMapper;
import com.cans.lightning.business.lowcode.service.api.IAppMenuService;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 应用信息
 *
 * @author shencan
 * @date 2020/6/14 20:53
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class AppMenuServiceImpl extends BaseServiceImpl<AppMenu, AppMenuDto, String> implements IAppMenuService {

    @Autowired
    private AppMenuDao appMenuDao;
    @Autowired
    private AppMenuMapper AppMenuMapper;


    @Override
    public BaseMapper<AppMenu> getDaoImpl() {
        return appMenuDao;
    }

    @Override
    public IBaseMapper<AppMenu, AppMenuDto, String> getMapperImpl() {
        return AppMenuMapper;
    }


    @Override
    public void saveOrUpdateDto(AppMenuDto AppMenuDto) {
        AppMenu AppMenu = this.toEntity(AppMenuDto);
        super.saveOrUpdate(AppMenu);

    }

    @Override
    public List<AppMenuDto> getAppMenuList(String appId) {
        LambdaQueryWrapper<AppMenu> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AppMenu::getAppId, appId).orderByAsc(AppMenu::getSortId);
        return AppMenuMapper.toDtos(appMenuDao.selectList(queryWrapper));
    }

    @Override
    public void saveOrUpdateDtoList(String appId,List<AppMenuDto> appMenuDtoList) {
        this.refreshSortId(appMenuDtoList);
        List<AppMenuDto> insertList = appMenuDtoList.stream().filter(item -> item.getId() == null).collect(Collectors.toList());
        List<AppMenuDto> updateList = appMenuDtoList.stream().filter(item -> item.getId() != null).toList();
        LambdaQueryWrapper<AppMenu> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.select(AppMenu::getId);
        queryWrapper.eq(AppMenu::getAppId, appId).orderByAsc(AppMenu::getSortId);
        List<String> dbIds = appMenuDao.selectList(queryWrapper).stream().map(AppMenu::getId).toList();
        List<String> saveIds = updateList.stream().map(AppMenuDto::getId).toList();
        for (String dbId : dbIds) {
            if (!saveIds.contains(dbId)) {
                appMenuDao.deleteById(dbId);
            }
        }
        this.insertDtoList(insertList);
        for (AppMenuDto appMenuDto : updateList) {
            this.saveOrUpdateDto(appMenuDto);
        }
    }

    /**
     * 刷新排序号
     * @param appMenuDtoList
     */
    private void refreshSortId(List<AppMenuDto> appMenuDtoList) {
        int i = 0;
        for (AppMenuDto appMenuDto : appMenuDtoList) {
            appMenuDto.setSortId(i++);
        }
    }

    @Override
    public List<AppMenuDto> getUserAppMenu(String appId) {
        LambdaQueryWrapper<AppMenu> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AppMenu::getAppId, appId).orderByAsc(AppMenu::getSortId);
        return this.getMapperImpl().toDtos(appMenuDao.selectList(queryWrapper));
    }
}
