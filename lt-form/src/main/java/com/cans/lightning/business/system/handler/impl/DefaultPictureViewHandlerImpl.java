package com.cans.lightning.business.system.handler.impl;

import com.cans.lightning.business.system.entity.FileInfo;
import com.cans.lightning.config.system.StorageTypeEnum;
import com.cans.lightning.business.system.handler.api.PictureViewHandler;
import com.cans.lightning.business.system.utils.FileUtils;
import org.springframework.stereotype.Component;

import java.io.OutputStream;

/**
 * 默认的批片预览处理类
 *
 * @author shenc
 * @date 2021-09-20 20:25
 **/
@Component
public class DefaultPictureViewHandlerImpl implements PictureViewHandler {


    @Override
    public String type() {
        return StorageTypeEnum.DEFAULT.name();
    }

    @Override
    public void view(OutputStream outputStream, FileInfo fileInfo) {

        FileUtils.viewPicture(outputStream, fileInfo.getFilePath());
    }
}
