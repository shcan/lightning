package com.cans.lightning.business.lowcode.form.design;

import com.cans.lightning.business.lowcode.dto.AppTableFieldDto;
import com.cans.lightning.business.lowcode.form.bean.AppFormBean;
import com.cans.lightning.business.lowcode.form.bean.FormBindBean;
import com.cans.lightning.business.lowcode.vo.AppFormBeanVo;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * @author cans
 * @date 2023/11/28
 * @description: 表单设计、保存入口类
 **/
public interface FormDesignManager {

    /**
     * 获取表单缓存
     * @param id
     * @return
     */
    AppFormBean getFormBean(String id);

    /**
     * 表单设计入口
     * @param formId
     * @return
     * @throws ExecutionException
     */
    AppFormBeanVo editForm(String formId);

    /**
     * 合并编辑中的表单缓存
     * @param mergeData
     * @throws ExecutionException
     */
    void mergeBaseInfo(MergeData mergeData) throws RuntimeException;

    /**
     * 移除编辑中的表单
     * @param formId
     */
    void removeEdit(String formId);

    /**
     * 获取编辑中的表单
     * @param formId
     * @return
     * @throws ExecutionException
     */
    AppFormBean getEditBean(String formId) throws ExecutionException;

    /**
     * 添加字段
     * @param tableFieldDto
     * @return
     * @throws ExecutionException
     */
    AppTableFieldDto addField(AppTableFieldDto tableFieldDto) throws ExecutionException;


    /**
     * 保存编辑中的表单到后端
     * @param appFormBean
     * @throws ExecutionException
     */
    void saveToDb(AppFormBean appFormBean) throws ExecutionException;

    List<FormBindBean> loadFormBind(String formId);

    Object loadFormField(String formId);
}
