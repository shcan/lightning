package com.cans.lightning.business.lowcode.web;

import com.cans.lightning.base.service.api.IBaseService;
import com.cans.lightning.base.web.BaseController;
import com.cans.lightning.business.lowcode.dto.AppTableDto;
import com.cans.lightning.business.lowcode.entity.AppTable;
import com.cans.lightning.business.lowcode.service.api.IAppTableService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * 表
 *
 * @author shencan
 * @date 2020/6/14 22:14
 */
@RestController
@RequestMapping("/app_table")
public class AppTableController extends BaseController<AppTable, AppTableDto, String> {

    @Autowired
    private IAppTableService appTableService;

    @Override
    public IBaseService<AppTable, AppTableDto, String> getBaseService() {
        return appTableService;
    }
}
