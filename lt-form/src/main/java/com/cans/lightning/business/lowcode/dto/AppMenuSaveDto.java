package com.cans.lightning.business.lowcode.dto;

import com.cans.lightning.base.dto.BaseDto;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 应用表单表信息
 *
 * @author cans
 * @date 2021/12/5
 **/
@Getter
@Setter
public class AppMenuSaveDto {

    /**
     * 所属应用
     */
    private String appId;

    /**
     * 菜单
     */
    private List<AppMenuDto> menuList;
}
