package com.cans.lightning.business.lowcode.form.bean;

import com.cans.lightning.business.core.bean.BaseBean;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cans
 * @date 2023/11/28
 **/
@Getter
@Setter
public class TableBean extends BaseBean {
    /**
     * 表单ID
     */
    String formId;
    /**
     * 表名称
     */
    private String tableName;

    /**
     * 数据库表名称
     */
    private String dbTableName;

    /**
     * 字段列表
     */
    private List<FormFieldBean> fieldList = new ArrayList<>();


    /**
     * 需要自定义clone() 方法, 因为有的属性是有AppFormBean的, 所以每次clone()需要传入AppFormBean进来
     * @param formBean AppFormBean对象
     * @return
     */
    public TableBean clone(AppFormBean formBean) throws CloneNotSupportedException {
        TableBean tableBean = (TableBean) super.clone();
        // ..... 业务逻辑
        tableBean.setFormId(this.formId);
        tableBean.setTableName(this.tableName);
        tableBean.setDbTableName(this.dbTableName);
        
        if (!ObjectUtils.isEmpty(this.fieldList)) {
            List<FormFieldBean> formFieldBeanList = new ArrayList<>();
            for (FormFieldBean formFieldBean : this.fieldList) {
                formFieldBeanList.add(formFieldBean.clone(formBean));
            }
            tableBean.setFieldList(formFieldBeanList);
        }
        return tableBean;
    }
}
