package com.cans.lightning.business.lowcode.form.bean;

import com.cans.lightning.business.lowcode.dto.AppTableDto;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author cans
 * @date 2023/11/28
 **/
public class SonTableBean extends TableBean {

    public SonTableBean create(AppTableDto tableDto) {
        if (tableDto == null) {
            return this;
        }
        this.setId(tableDto.getId());
        // 表名称
        this.setTableName(tableDto.getTableName());
        // 数据库表名称
        this.setDbTableName(tableDto.getDbTableName());
        // 字段
        List<FormFieldBean> formFieldBeanList = tableDto.getFieldArr().stream().map(i -> new FormFieldBean().create(i)).collect(Collectors.toList());
        this.setFieldList(formFieldBeanList);
        return this;
    }

    @Override
    public SonTableBean clone(AppFormBean formBean) throws CloneNotSupportedException {
        SonTableBean stb =  (SonTableBean) super.clone(formBean);
        // ... 

        return stb;
    }
}
