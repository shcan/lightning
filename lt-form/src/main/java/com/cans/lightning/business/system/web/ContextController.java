package com.cans.lightning.business.system.web;

import com.cans.lightning.base.dto.ResDto;
import com.cans.lightning.business.core.utils.ContextUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 上下文
 *
 * @author shencan
 * @date 2020/6/21 20:16
 */
@RestController
@RequestMapping("/context")
public class ContextController {

    @GetMapping("/getNextId")
    public ResDto<String> getNextId(){
        return ResDto.success(ContextUtil.nextID());
    }

}
