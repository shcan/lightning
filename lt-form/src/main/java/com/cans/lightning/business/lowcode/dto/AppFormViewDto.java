package com.cans.lightning.business.lowcode.dto;

import com.cans.lightning.base.dto.BaseDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 应用表单信息
 *
 * @author cans
 * @date 2021/12/5
 **/
@Getter
@Setter
public class AppFormViewDto extends BaseDto<String> {
    /**
     * 所属应用
     */
    private String appId;

    private String formId;

    private String fieldBind;
    /**
     * 视图名称
     */
    private String viewName;
}
