package com.cans.lightning.business.lowcode.form;

import com.cans.lightning.business.lowcode.enums.AppIndexEnum;
import com.cans.lightning.business.lowcode.service.impl.AppIndexServiceImpl;
import com.google.common.base.Strings;
import org.springframework.stereotype.Component;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author cans
 * @date 2022/11/6
 **/
@Component
public class AppIndexManager {

    public static final String TABLE_PREFIX =  "form";

    @Autowired
    private AppIndexServiceImpl appIndexService;

    public String getNextMainTableName(){
       return TABLE_PREFIX + AppIndexEnum.MAIN_TABLE.getKey() + "_" + Strings.padStart(String.valueOf(appIndexService.getNextMainTableIndex()),5,'0');
    }

    public String getNextSonTableName(){
        return TABLE_PREFIX + AppIndexEnum.SON_TABLE.getKey() + "_" + Strings.padStart(String.valueOf(appIndexService.getNextSonTableIndex()),5,'0');
    }

    public Long getNextFieldIndex(String tableId){
        return appIndexService.getNextFieldIndex(tableId);
    }
}
