package com.cans.lightning.business.pms.web;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cans.lightning.base.dto.MyPageRequest;
import com.cans.lightning.base.dto.ResDto;
import com.cans.lightning.base.service.api.IBaseService;
import com.cans.lightning.base.web.BaseController;
import com.cans.lightning.business.pms.dto.SysUserDto;
import com.cans.lightning.business.pms.entity.SysUser;
import com.cans.lightning.business.pms.service.api.ISysUserService;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * 用户接口
 *
 * @author shencan
 * @date 2020/6/14 22:14
 */
@RestController
@RequestMapping("/sys-user")
public class SysUserController extends BaseController<SysUser, SysUserDto, String> {

    @Autowired
    private ISysUserService sysUserService;

    /**
     * 获取当前用户信息
     *
     * @return
     */
    @GetMapping("/get-current-user")
    public ResDto<SysUserDto> getCurrentUser() {
        return ResDto.success(sysUserService.toDto(sysUserService.getCurrentUser()));
    }

    @PostMapping("/pageList")
    public ResDto pageList(@RequestBody MyPageRequest pageRequest) {
        return ResDto.success(sysUserService.findPage(Page.of(pageRequest.getPage(), pageRequest.getSize())));
    }

    /**
     * 更新用户详情信息
     *
     * @param userDto 用户DTO
     * @return
     */
    @PostMapping("/update-info")
    public ResDto updateInfo(@RequestBody SysUserDto userDto) {

        return ResDto.success(sysUserService.updateInfo(userDto));
    }

    @Override
    public IBaseService<SysUser, SysUserDto, String> getBaseService() {
        return sysUserService;
    }
}
