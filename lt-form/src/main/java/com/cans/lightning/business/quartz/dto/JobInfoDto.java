package com.cans.lightning.business.quartz.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * JOB信息DTO
 *
 * @author cans
 * @date 2021-03-14 16:48
 **/
@Getter
@Setter
public class JobInfoDto implements Serializable {

    /**
     * 任务名称
     */
    private String jobName;

    /**
     * 任务组
     */
    private String jobGroupName;

    /**
     * 描述
     */
    private String description;

    /**
     * 状态
     */
    private String jobStatus;

    /**
     * CORN表达式
     */
    private String corn;

    /**
     * 下次运行时间
     */
    Date nextFireTime;

}
