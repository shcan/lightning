package com.cans.lightning.business.lowcode.form.design;

import com.cans.lightning.business.lowcode.dto.AppTableFieldDto;
import com.cans.lightning.business.lowcode.exceptions.FormBusinessException;
import com.cans.lightning.business.lowcode.form.bean.FormBindBean;
import com.cans.lightning.business.lowcode.form.constant.FormCacheConstants;
import com.cans.lightning.business.lowcode.form.bean.AppFormBean;
import com.cans.lightning.business.lowcode.form.bean.FormFieldBean;
import com.cans.lightning.business.lowcode.form.cache.AppFormBeanCacheManager;
import com.cans.lightning.business.lowcode.form.design.save.FormDesignSaveHandlerChainBuilder;
import com.cans.lightning.business.lowcode.form.design.save.context.AppFormDesignSaveContext;
import com.cans.lightning.business.lowcode.vo.AppFormBeanVo;
import com.cans.lightning.business.pms.service.api.ISysUserService;
import com.cans.lightning.cache.redis.RedisCache;
import com.cans.lightning.lock.bean.LockInfo;
import com.cans.lightning.lock.manager.api.LockManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.compress.utils.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * @author: qpy
 * @date: 2024-02-04 10:41
 * @description:
 */
@Component
public class FormDesignManagerImpl implements FormDesignManager {

    private static final Logger logger = LoggerFactory.getLogger(FormDesignManager.class);

    @Autowired
    private FormDesignSaveHandlerChainBuilder formDesignSaveHandlerChainBuilder;
    @Autowired
    private AppFormBeanCacheManager appFormBeanCacheManager;
    @Autowired
    private LockManager lockManager;
    @Autowired
    private ISysUserService userService;


    @Override
    public void saveToDb(AppFormBean appFormBean) throws ExecutionException {
        AppFormBean editFormBean = appFormBeanCacheManager.getEditFormBean(appFormBean.getId());
        formDesignSaveHandlerChainBuilder.getSaveHandlerChain().runSaveHandle(new AppFormDesignSaveContext<>(editFormBean));
    }

    @Override
    public List<FormBindBean> loadFormBind(String formId) {
        AppFormBean editFormBean = appFormBeanCacheManager.getEditFormBean(formId);
        if (editFormBean == null) {
            throw new FormBusinessException("缓存已失效,请重新打开表单");
        }
        List<FormBindBean> formBindList = editFormBean.getFormBindList();
        if (formBindList == null) {
            return Lists.newArrayList();
        }
        return formBindList;
    }

    @Override
    public Object loadFormField(String formId) {
        AppFormBean editFormBean = appFormBeanCacheManager.getEditFormBean(formId);
        Map<String, FormFieldBean> fieldMap = editFormBean.getFieldMap();
        List<Map<String, Object>> res = Lists.newArrayList();
        for (FormFieldBean value : fieldMap.values()) {
            res.add(Map.of("id", value.getDbFieldName(),
                    "name", value.getFieldName(),
                    "sysField",value.isSysField()));
        }
        return res;
    }

    @Override
    public AppFormBean getFormBean(String id) {
        return appFormBeanCacheManager.get(id);
    }

    @Override
    public AppFormBeanVo editForm(String formId) {
        AppFormBeanVo appFormBeanVo = new AppFormBeanVo();
        AppFormBean editFormBean = appFormBeanCacheManager.createEditBean(formId);
        BeanUtils.copyProperties(editFormBean, appFormBeanVo);
        return appFormBeanVo;

    }

    @Override
    public void mergeBaseInfo(MergeData mergeData) throws RuntimeException {
        AppFormBean editFormBean = appFormBeanCacheManager.getEditFormBean(mergeData.getFormId());
        if (editFormBean == null) {
            throw new RuntimeException("表单缓存已失效");
        }

        switch (mergeData.getType()) {
            case "base":
                this.updateBaseInfo(mergeData.getData(), editFormBean);
                break;
            case "formBind":
                this.updateFormBind(mergeData.getData(), editFormBean);
                break;
            default:
                throw new RuntimeException("未知的mergeType");
        }
    }

    private void updateBaseInfo(String data, AppFormBean editFormBean) {
        Gson gson = new Gson();
        AppFormBean appFormBean = gson.fromJson(data, AppFormBean.class);
        appFormBean.setFieldIndex(editFormBean.getFieldIndex());
        editFormBean.setMainTableBean(appFormBean.getMainTableBean());
        editFormBean.setSonTableList(appFormBean.getSonTableList());
        editFormBean.setFieldMap(appFormBean.getFieldMap());
        editFormBean.setViewBean(appFormBean.getViewBean());
        appFormBeanCacheManager.updateEditFormBean(editFormBean);
    }

    private void updateFormBind(String data, AppFormBean editFormBean) {
        List<FormBindBean> formBindBeanList = new Gson().fromJson(data, new TypeToken<List<FormBindBean>>() {
        }.getType());
        editFormBean.setFormBindList(formBindBeanList);
        appFormBeanCacheManager.updateEditFormBean(editFormBean);
    }

    @Override
    public void removeEdit(String formId) {
        appFormBeanCacheManager.removeEditFormBean(formId);
    }

    @Override
    public AppFormBean getEditBean(String formId) throws ExecutionException {
        return appFormBeanCacheManager.getEditFormBean(formId);
    }

    @Override
    public AppTableFieldDto addField(AppTableFieldDto tableFieldDto) throws ExecutionException {
        AppFormBean editBean = getEditBean(tableFieldDto.getFormId());
        FormFieldBean formFieldBean = editBean.addField(tableFieldDto);
        tableFieldDto.setId(formFieldBean.getId());
        tableFieldDto.setDbFieldName(formFieldBean.getDbFieldName());
        appFormBeanCacheManager.updateEditFormBean(editBean);
        return tableFieldDto;
    }

}
