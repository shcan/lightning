package com.cans.lightning.business.lowcode.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.cans.lightning.base.mapper.IBaseMapper;
import com.cans.lightning.base.service.impl.BaseServiceImpl;
import com.cans.lightning.business.lowcode.dao.AppFormViewDao;
import com.cans.lightning.business.lowcode.dao.AppFormViewItemDao;
import com.cans.lightning.business.lowcode.dto.AppFormViewDto;
import com.cans.lightning.business.lowcode.entity.AppFormView;
import com.cans.lightning.business.lowcode.entity.AppFormViewItem;
import com.cans.lightning.business.lowcode.form.bean.ViewBean;
import com.cans.lightning.business.lowcode.form.bean.ViewItemBean;
import com.cans.lightning.business.lowcode.mapper.AppFormViewMapper;
import com.cans.lightning.business.lowcode.service.api.IAppFormViewService;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 表单视图
 *
 * @author shencan
 * @date 2020/6/14 20:53
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class AppFormViewServiceImpl extends BaseServiceImpl<AppFormView,  AppFormViewDto, String> implements IAppFormViewService {

    private static final Logger logger = LoggerFactory.getLogger(AppFormViewServiceImpl.class);

    @Autowired
    private AppFormViewDao appFormViewDao;
    @Autowired
    private AppFormViewItemDao appFormViewItemDao;
    @Autowired
    private AppFormViewMapper appFormViewMapper;

    @Override
    public BaseMapper<AppFormView> getDaoImpl() {
        return appFormViewDao;
    }

    @Override
    public IBaseMapper<AppFormView, AppFormViewDto, String> getMapperImpl() {
        return appFormViewMapper;
    }

    @Override
    public List<AppFormViewDto> findByFormId(String formId) {
        LambdaQueryWrapper<AppFormView> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AppFormView::getFormId, formId).orderByAsc(AppFormView::getSortId);
        List<AppFormView> select = appFormViewDao.selectList(queryWrapper);
        return appFormViewMapper.toDtos(select);
    }

    public List<AppFormViewItem> findViewItemByViewId(String viewId) {
        LambdaQueryWrapper<AppFormViewItem> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AppFormViewItem::getViewId, viewId).orderByAsc(AppFormViewItem::getSortId);
        return  appFormViewItemDao.selectList(queryWrapper);
    }

    @Override
    public void saveOrUpdate(ViewBean viewBean) {
        AppFormView appFormView = new AppFormView();
        appFormView.setId(viewBean.getId());
        appFormView.setFormId(viewBean.getFormId());
        appFormView.setViewName(viewBean.getViewName());
        appFormView.setFieldBind(JSON.toJSONString(viewBean.getFieldBind()));
        this.saveOrUpdate(appFormView);
        long sortId = 0;
        List<ViewItemBean> viewItemList = viewBean.getViewItemList();
        Set<String> saveIdSet = viewItemList.stream().map(ViewItemBean::getId).filter(Objects::nonNull).collect(Collectors.toSet());
        List<AppFormViewItem> dbViewItemList = this.findViewItemByViewId(viewBean.getId());
        for (AppFormViewItem appFormViewItem : dbViewItemList) {
            if(!saveIdSet.contains(appFormViewItem.getId())){
                appFormViewItemDao.deleteById(appFormViewItem.getId());
            }
        }
        Map<String, AppFormViewItem> dbViewItemMap = dbViewItemList.stream().collect(Collectors.toMap(AppFormViewItem::getId, Function.identity()));
        for (ViewItemBean viewItemBean : viewItemList) {
            AppFormViewItem appFormViewItem ;
            if(viewItemBean.getId() != null){
                appFormViewItem = dbViewItemMap.get(viewItemBean.getId());
                if(appFormViewItem != null){
                    appFormViewItem.setViewId(appFormView.getId());
                    appFormViewItem.setShowType(viewItemBean.getShowType());
                    appFormViewItem.setFormId(viewBean.getFormId());
                    appFormViewItem.setRefObj(JSON.toJSONString(viewItemBean.getRefObj()));
                    appFormViewItem.setSortId(sortId ++);
                    appFormViewItemDao.updateById(appFormViewItem);
                }else{
                    appFormViewItem = new AppFormViewItem();
                    appFormViewItem.setId(viewItemBean.getId());
                    appFormViewItem.setViewId(appFormView.getId());
                    appFormViewItem.setShowType(viewItemBean.getShowType());
                    appFormViewItem.setFormId(viewBean.getFormId());
                    appFormViewItem.setRefObj(JSON.toJSONString(viewItemBean.getRefObj()));
                    appFormViewItem.setSortId(sortId ++);
                    appFormViewItemDao.insert(appFormViewItem);
                }
            }

        }
    }

    @Override
    public void saveOrUpdate(AppFormViewItem appFormViewItem){
        if(appFormViewItem.getId() == null){
            appFormViewItemDao.insert(appFormViewItem);
        }else{
            appFormViewItemDao.updateById(appFormViewItem);
        }
    }

    @Override
    public List<AppFormViewItem> findItemByFormId(String formId){
        LambdaQueryWrapper<AppFormViewItem> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AppFormViewItem::getFormId, formId).orderByAsc(AppFormViewItem::getSortId);
        return appFormViewItemDao.selectList(queryWrapper);
    }
}
