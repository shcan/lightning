package com.cans.lightning.business.system.web;

import com.cans.lightning.business.system.service.api.IDictionaryItemService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * 数据字典
 *
 * @author shencan
 * @date 2020/6/21 20:16
 */
@RestController
@RequestMapping("/dict-item")
public class DictionaryItemController {

    @Autowired
    private IDictionaryItemService dictionaryItemService;

}
