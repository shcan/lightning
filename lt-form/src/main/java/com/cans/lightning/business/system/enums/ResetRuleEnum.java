package com.cans.lightning.business.system.enums;

import com.google.common.collect.Lists;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 编号重置规则枚举
 *
 * @author cans
 * @date 2021-04-22 11:11
 **/
public enum ResetRuleEnum {

    /**
     * 总是
     */
    ALWAYS(0, "总是"),
    /**
     * 从不
     */
    NEVER(1, "从不"),

    /**
     * 每天
     */
    DAY(2, "每天"),
    /**
     * 周
     */
    WEEK(3, "每周"),
    /**
     * 每月
     */
    MONTH(4, "每月"),
    /**
     * 每年
     */
    YEAR(5, "每年");

    private final Integer key;
    private final String value;

    ResetRuleEnum(Integer key, String value) {
        this.key = key;
        this.value = value;
    }

    public Integer getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    /**
     * 通过值得到value
     *
     * @param key
     * @return
     */
    public static String getValueByKey(Integer key) {
        if (!StringUtils.isEmpty(key)) {
            for (ResetRuleEnum e : ResetRuleEnum.values()) {
                if (e.getKey().equals(key)) {
                    return e.getValue();
                }
            }
        }
        return null;
    }

    /**
     * 通过值得到value
     *
     * @param key
     * @return
     */
    public static ResetRuleEnum getByKey(Integer key) {
        if (!StringUtils.isEmpty(key)) {
            for (ResetRuleEnum e : ResetRuleEnum.values()) {
                if (e.getKey().equals(key)) {
                    return e;
                }
            }
        }
        return null;
    }

    public static Integer getKeyByValue(String value) {
        if (!StringUtils.isEmpty(value)) {
            for (ResetRuleEnum e : ResetRuleEnum.values()) {
                if (e.getValue().equals(value)) {
                    return e.getKey();
                }
            }
        }
        return null;
    }

    public static List<Map<String, Object>> list() {

        List<Map<String, Object>> list = Lists.newArrayList();

        for (ResetRuleEnum e : ResetRuleEnum.values()) {
            HashMap<String, Object> map = new HashMap<>(1);
            map.put("key", e.getKey());
            map.put("value", e.getValue());
            list.add(map);
        }

        return list;

    }
}
