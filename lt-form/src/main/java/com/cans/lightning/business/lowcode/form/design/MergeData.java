package com.cans.lightning.business.lowcode.form.design;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author cans
 * @date 2024/9/15 17:44
 * @description TODO
 */
@Getter
@Setter
public class MergeData implements Serializable {
    private String formId;
    private String type;
    private String data;
}
