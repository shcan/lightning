package com.cans.lightning.business.lowcode.form.bean;

import com.cans.lightning.business.core.bean.BaseBean;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 表单权限
 *
 * @author cans
 * @date 2023/11/28
 **/
@Getter
@Setter
public class FormAuthBean extends BaseBean {
    /**
     * 权限ID
     */
    private String authName;

    /**
     * 可用视图的ID
     */
    private List<String> scopeViewIdList;

    /**
     * 权限类型 系统默认/自建
     */
    private String authSystemType;

    /**
     * 权限属性 修改/新增
     */
    private String authOperator;

    /**
     * 字段权限列表
     */
    private List<FieldAuthBean> fieldAuthList;

    public FormAuthBean clone(AppFormBean formBean) throws CloneNotSupportedException{
        FormAuthBean fab = (FormAuthBean) super.clone();
        fab.setAuthName(this.authName);
        fab.setAuthSystemType(this.authSystemType);
        fab.setAuthOperator(this.authOperator);
        if (this.scopeViewIdList != null) {
            List<String> scopeViewIdList = Lists.newArrayList();
            scopeViewIdList.addAll(this.scopeViewIdList);
            fab.setScopeViewIdList(scopeViewIdList);
        }
        
        if (this.fieldAuthList != null) {
            List<FieldAuthBean> fieldAuthList = Lists.newArrayList();
            for (FieldAuthBean fieldAuthBean : this.fieldAuthList) {
                fieldAuthList.add(fieldAuthBean.clone(formBean));
            }
            fab.setFieldAuthList(fieldAuthList);
        }
        
        return fab;
    }
}
