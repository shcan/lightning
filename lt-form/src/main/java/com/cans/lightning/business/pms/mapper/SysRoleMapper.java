package com.cans.lightning.business.pms.mapper;

import com.cans.lightning.base.mapper.IBaseMapper;
import com.cans.lightning.business.pms.dto.SysRoleDto;
import com.cans.lightning.business.pms.entity.SysRole;
import org.mapstruct.Mapper;
import org.springframework.util.StringUtils;

/**
 * @author shencan
 * @date 2020/12/2 19:06
 */
@Mapper(componentModel = "spring", uses = SysMenuMapper.class)
public interface SysRoleMapper extends IBaseMapper<SysRole, SysRoleDto,String> {


    /**
     * 转换为Entity
     *
     * @param dto
     * @return
     */
    @Override
    SysRole toEntity(SysRoleDto dto);


    /**
     * 通过id获取实体
     *
     * @param id
     * @return
     */
    default SysRole fromId(String id) {
        if (StringUtils.isEmpty(id)) {
            return null;
        }
        SysRole role = new SysRole();
        role.setId(id);
        return role;
    }

}
