package com.cans.lightning.business.system.web;

import com.cans.lightning.business.system.service.api.ISystemConfigService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * 系统配置
 *
 * @author shencan
 * @date 2020/6/21 20:16
 */
@RestController
@RequestMapping("/sys-config")
public class SystemConfigController{

    @Autowired
    private ISystemConfigService systemConfigService;

}
