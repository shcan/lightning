package com.cans.lightning.business.lowcode.form.design.save.handler;

import com.cans.lightning.business.lowcode.dto.AppFormBindDto;
import com.cans.lightning.business.lowcode.entity.AppFormBind;
import com.cans.lightning.business.lowcode.form.bean.AppFormBean;
import com.cans.lightning.business.lowcode.form.bean.FormBindBean;
import com.cans.lightning.business.lowcode.form.design.save.AbstractBeanSaveHandler;
import com.cans.lightning.business.lowcode.form.design.save.context.AppFormDesignSaveContext;
import com.cans.lightning.business.lowcode.service.api.IAppFormBindService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * formBean -- 模板绑定信息保存
 *
 * @author cans
 * @date 2023/11/29
 **/
@Component
public class FormDesignBindSaveHandler extends AbstractBeanSaveHandler<AppFormBean> {

    @Resource
    private IAppFormBindService formBindService;

    @Override
    public AppFormBean saveOne(AppFormDesignSaveContext<AppFormBean> saveContext) {
        AppFormBean appFormBean = saveContext.getData();
        List<FormBindBean> formBindList = appFormBean.getFormBindList();
        List<AppFormBindDto> dbBindList = formBindService.findByFormId(appFormBean.getId());
        for (FormBindBean formBindBean : formBindList) {
            AppFormBind appFormBind = new AppFormBind().transferFrom(formBindBean);
            appFormBind.setFormId(appFormBean.getId());
            appFormBind.setAppId(appFormBean.getAppId());
            formBindService.saveOrUpdate(appFormBind);
        }

        Set<String> saveBindIdSet = formBindList.stream().map(FormBindBean::getId).collect(Collectors.toSet());
        Set<String> dbBindIdSet = dbBindList.stream().map(AppFormBindDto::getId).collect(Collectors.toSet());
        for (String dbId : dbBindIdSet) {
            if(!saveBindIdSet.contains(dbId)){
                formBindService.deleteById(dbId);
            }
        }

        return appFormBean;
    }


}
