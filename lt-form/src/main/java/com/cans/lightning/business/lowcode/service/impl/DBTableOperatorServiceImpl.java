package com.cans.lightning.business.lowcode.service.impl;

import com.cans.lightning.business.lowcode.dto.AppTableDto;
import com.cans.lightning.business.lowcode.dto.AppTableFieldDto;
import com.cans.lightning.business.lowcode.service.api.IDBTableDoOperatorService;
import com.cans.lightning.business.lowcode.service.api.IDBTableOperatorService;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.SmartInitializingSingleton;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author cans
 * @date 2022/11/12
 **/
@Service("dBTableOperatorService")
public class DBTableOperatorServiceImpl implements IDBTableOperatorService, SmartInitializingSingleton {

    @Autowired
    private ApplicationContext applicationContext;
    private Map<Integer, IDBTableOperatorService> dbTableOperatorServiceMap;

    @Override
    public void createTable(AppTableDto tableDto) {
        this.getImpl().createTable(tableDto);
    }

    @Override
    public void createField(AppTableDto tableDto,AppTableFieldDto fieldDto) {
        this.getImpl().createField(tableDto,fieldDto);
    }

    @Override
    public void ifNotExitCreate(AppTableDto appTableDto, AppTableFieldDto fieldDto) {
        this.getImpl().ifNotExitCreate(appTableDto,fieldDto);
    }

    @Override
    public void afterSingletonsInstantiated() {
        Map<String, IDBTableDoOperatorService> beansOfType = applicationContext.getBeansOfType(IDBTableDoOperatorService.class);
        dbTableOperatorServiceMap = Maps.newHashMap();
        for (IDBTableDoOperatorService value : beansOfType.values()) {
            dbTableOperatorServiceMap.put(value.getDbType(),value);
        }
    }

    private IDBTableOperatorService getImpl(){
        IDBTableOperatorService dbTableOperatorService = dbTableOperatorServiceMap.values().stream().toList().get(0);
        if(dbTableOperatorService == null){
            throw new RuntimeException("不支持的数据库类型: ");
        }
        return dbTableOperatorService;
    }

}
