package com.cans.lightning.business.lowcode.service.api;

import com.cans.lightning.base.service.api.IBaseService;
import com.cans.lightning.business.lowcode.dto.AppMenuDto;
import com.cans.lightning.business.lowcode.entity.AppMenu;

import java.util.List;


/**
 * 应用信息
 *
 * @author shencan
 * @date 2020/6/14 20:39
 */
public interface IAppMenuService extends IBaseService<AppMenu, AppMenuDto, String> {

    List<AppMenuDto> getAppMenuList(String appId);

    void saveOrUpdateDtoList(String appId,List<AppMenuDto> appMenuDtoList);

    List<AppMenuDto> getUserAppMenu(String appId);
}
