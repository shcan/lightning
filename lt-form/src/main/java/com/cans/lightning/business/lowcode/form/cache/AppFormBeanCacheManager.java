package com.cans.lightning.business.lowcode.form.cache;

import com.cans.lightning.business.lowcode.form.bean.AppFormBean;
import com.cans.lightning.business.lowcode.form.constant.FormCacheConstants;
import com.cans.lightning.business.lowcode.form.design.editingcache.FormEditingCache;
import com.cans.lightning.business.lowcode.form.design.editingcache.FormEditingCacheFactory;
import com.cans.lightning.business.lowcode.form.loader.formBean.FormBeanLoaderBuilder;
import com.cans.lightning.business.lowcode.form.ICacheManager;
import com.cans.lightning.cache.redis.RedisCache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import org.springframework.beans.factory.annotation.Autowired;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * @author cans
 * @date 2023/11/28
 **/
@Component
public class AppFormBeanCacheManager implements ICacheManager<AppFormBean>, ApplicationContextAware, InitializingBean {

    private static final Logger logger = LoggerFactory.getLogger(AppFormBeanCacheManager.class);

    @Autowired
    private FormBeanLoaderBuilder formBeanLoaderBuilder;

    private final LoadingCache<String, AppFormBean> formBeanCache = CacheBuilder.newBuilder()
            .maximumSize(5000) // 设置缓存容量
            .expireAfterWrite(50, TimeUnit.MINUTES) // 设置缓存项的过期时间
            .build(new AppFormBeanCacheLoader());


    @Autowired
    private RedisCache redisCache;

    /**
     * 使用抽象实现  默认: com.cans.lightning.business.lowcode.form.design.editingcache.RedisFormEditingCache
     * 在 setApplicationContext() 进行初始化
     */
    private FormEditingCache<String, AppFormBean> formEditingCache;
    private FormEditingCache<String, String> formEditingInfoCache;
    

    /**
     * 加载表单缓存
     * @date 2024/3/6 16:42
     * @since v1.0
     * @author qpy
     */
    @Override
    public void afterPropertiesSet() {
        
        // 1. 表单升级
        
        // 2. 表单缓存的创建(表单缓存、表单编辑缓存、其他缓存...)
        // initDeclare();
        // 3. 表单缓存的预加载
        
        
    }
    
    
    private AppFormBean loadAppFormBeanToDb(String id) {
        return formBeanLoaderBuilder.getLoader().runLoad(new AppFormBean(id));
    }

    @Override
    public AppFormBean get(String id) {
        try {
            if(true){
                return loadAppFormBeanToDb(id);
            }
            return formBeanCache.get(id);
        } catch (ExecutionException e) {
            logger.error("缓存加载异常,表单ID" + id);
        }
        return null;
    }

    @Override
    public void invalidate(String key) {
        formBeanCache.invalidate(key);
    }

    public AppFormBean createEditBean(String formId){
        AppFormBean editFormBean = getEditFormBean(formId);
        if(editFormBean != null){
            return editFormBean;
        }
        AppFormBean appFormBean = this.get(formId);
        formEditingCache.put(formId,appFormBean);
        return appFormBean;
    }

    public AppFormBean getEditFormBean(String formId) {
        AppFormBean appFormBean = redisCache.getCacheObject(FormCacheConstants.EDITING + formId);
        if (logger.isInfoEnabled()) {
            logger.info("获取编辑表单缓存: " + formId + " appFormBean is " + (appFormBean == null ? "空的" : "获取成功"));
        }
        return appFormBean;
    }

    public void updateEditFormBean(AppFormBean formBean) {
        String formId = formBean.getId();
        formEditingCache.put(formId, formBean);
        formEditingInfoCache.put(formId, "表单加锁:" + formId);
        if (logger.isInfoEnabled()) {
            logger.info("更新表单缓存, " + formId);
        }
    }

    public void removeEditFormBean(String formId) {
        boolean removeForm = formEditingCache.remove(formId);
        boolean removeEditInfo = formEditingInfoCache.remove(formId);
        if (logger.isInfoEnabled()) {
            logger.info("移除表单缓存, " + formId + ", removeForm:" + removeForm + ", removeEditInfo:" + removeEditInfo);
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext ctx) throws BeansException {
        // 初始化缓存信息
        String cacheType = ctx.getEnvironment().getProperty("lightning.form.editCache", "redis");
        formEditingCache = FormEditingCacheFactory.createEditCache(FormCacheConstants.EDITING, cacheType, ctx);
        logger.info("初始化表单编辑缓存: " + formEditingCache);
        formEditingInfoCache = FormEditingCacheFactory.createEditCache(FormCacheConstants.EDITING_INFO, cacheType, ctx);
        logger.info("初始化表单编辑缓存信息: " + formEditingInfoCache);
    }

    public class AppFormBeanCacheLoader extends CacheLoader<String, AppFormBean> {
        @Override
        public AppFormBean load(String key) throws Exception {
            // 往DB中查询数据
            logger.info("开始从DB加载-->AppFormBean======formId:" + key);
            return loadAppFormBeanToDb(key);
        }
    }
}
