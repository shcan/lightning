package com.cans.lightning.business.lowcode.dto;

import com.cans.lightning.base.dto.BaseDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 应用定义
 *
 * @author cans
 * @date 2021/12/5
 **/
@Getter
@Setter
public class AppDefinitionDto extends BaseDto<String> {
    /**
     * 包名称
     */
    private String name;
}
