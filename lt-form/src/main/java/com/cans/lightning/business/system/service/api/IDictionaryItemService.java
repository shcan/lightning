package com.cans.lightning.business.system.service.api;

import com.cans.lightning.base.service.api.IBaseService;
import com.cans.lightning.business.system.dto.DictionaryItemDto;
import com.cans.lightning.business.system.entity.DictionaryItem;

import java.util.List;


/**
 * 数据字典单项服务
 *
 * @author shencan
 * @date 2020/6/14 20:39
 */
public interface IDictionaryItemService extends IBaseService<DictionaryItem, DictionaryItemDto, String> {

    List<DictionaryItemDto> findByDictId(String id);
}
