package com.cans.lightning.business.lowcode.form;

import com.cans.lightning.business.lowcode.service.impl.AppMenuServiceImpl;
import org.springframework.stereotype.Component;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author cans
 * @date 2022/11/6
 **/
@Component
public class AppMenuManager {

    @Autowired
    private AppMenuServiceImpl AppMenuService;

}
