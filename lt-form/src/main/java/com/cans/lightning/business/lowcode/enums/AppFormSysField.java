package com.cans.lightning.business.lowcode.enums;

/**
 * @author cans
 * @date 2023/11/25
 **/
public class AppFormSysField {
    public static final String ID = "id";
    public static final String CREATE_TIME = "create_time";
    public static final String LAST_CHANGE_TIME = "last_change_time";
}
