package com.cans.lightning.business.lowcode.mapper;

import com.cans.lightning.base.mapper.IBaseMapper;
import com.cans.lightning.business.lowcode.dto.AppTableDto;
import com.cans.lightning.business.lowcode.entity.AppTable;
import org.mapstruct.Mapper;
import org.springframework.util.StringUtils;

/**
 * @author shencan
 * @date 2020/12/2 19:06
 */
@Mapper(componentModel = "spring")
public interface AppTableMapper extends IBaseMapper<AppTable, AppTableDto,String> {

    /**
     * 转换为Entity
     *
     * @param dto
     * @return
     */
    @Override
    AppTable toEntity(AppTableDto dto);


    /**
     * 通过id获取实体
     *
     * @param id
     * @return
     */
    default AppTable fromId(String id) {
        if (StringUtils.isEmpty(id)) {
            return null;
        }
        AppTable entity = new AppTable();
        entity.setId(id);
        return entity;
    }
}
