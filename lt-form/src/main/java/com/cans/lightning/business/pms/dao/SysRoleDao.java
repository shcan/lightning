package com.cans.lightning.business.pms.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cans.lightning.business.pms.entity.SysRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author shencan
 * @date 2020/6/14 20:33
 */
@Mapper
public interface SysRoleDao extends BaseMapper<SysRole> {
}
