package com.cans.lightning.business.lowcode.dao;

import com.cans.lightning.base.dao.api.IBaseDao;
import com.cans.lightning.business.lowcode.entity.AppMenu;
import org.apache.ibatis.annotations.Mapper;

/**
 * 菜单
 *
 * @author shencan
 * @date 2020/6/14 20:33
 */
@Mapper
public interface AppMenuDao extends IBaseDao<AppMenu> {
}
