package com.cans.lightning.business.quartz.web;

import com.cans.lightning.base.dto.ResDto;
import com.cans.lightning.business.quartz.service.QuartzService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
@RestController
@RequestMapping("/quartz")
public class QuartzController {

    @Autowired
    private QuartzService quartzService;


    @PostMapping("/delete")
    public ResDto delete(String name,String group) {

        quartzService.deleteJob(name, group);

        return ResDto.success();
    }

    @GetMapping("/all")
    public ResDto getAll() {

        return ResDto.success(quartzService.queryAllJob());
    }

    @GetMapping("/update-job")
    public ResDto updateJob() {

        quartzService.updateJob("job", "test","0/1 * * * * ? ");

        return ResDto.success();
    }
}
