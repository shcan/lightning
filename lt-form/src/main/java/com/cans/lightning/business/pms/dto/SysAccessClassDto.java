package com.cans.lightning.business.pms.dto;

import com.cans.lightning.base.dto.BaseDto;
import com.cans.lightning.base.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * API权限控制-类
 *
 * @author cans
 * @date 2021-05-07 14:08
 **/
@Getter
@Setter
public class SysAccessClassDto extends BaseDto<String> {

    /**
     * 类标识
     */
    private String classMark;

    /**
     * 全限定类名
     */
    private String className;

    /**
     * 非限定类名
     */
    private String shortClassName;
}
