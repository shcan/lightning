package com.cans.lightning.business.lowcode.form.design.save.context;

import lombok.Getter;
import lombok.Setter;

/**
 * @author cans
 * @date 2023/12/1
 **/
@Getter
@Setter
public class AppFormDesignSaveContext<T> {

    public AppFormDesignSaveContext(T data){
        this.data = data;
    }

    private T data;
}
