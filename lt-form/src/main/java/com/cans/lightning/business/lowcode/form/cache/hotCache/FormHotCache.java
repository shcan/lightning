package com.cans.lightning.business.lowcode.form.cache.hotCache;

import com.cans.lightning.cache.CacheChangeAction;
import com.cans.lightning.cache.enums.ActionType;
import com.cans.lightning.cache.hotCache.HotCache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.LongAdder;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

@Slf4j
public class FormHotCache<K extends Serializable, V extends Serializable> implements HotCache<K, V> {

    /**
     * 缓存key
     */
    private String cacheName;
    /**
     * google guava内存缓存
     */
    private LoadingCache<K, Optional<V>> data;
    /**
     * 数据不存在时, 数据加载器
     */
    private MapDataLoader<K, V> mapDataLoader;
    /**
     * 初始化锁
     */
    private CountDownLatch latch;

    /**
     * 更新锁
     */
    private ReentrantLock lock;

    /**
     * 数据重新加载的版本号
     */
    private LongAdder loadVersion;

    /**
     * 初始化数据加载器
     */
    private HotCacheInitializer<K, V> hotCacheInitializer;

    private HotCacheContext<K> ctx;

    protected FormHotCache() {
        // 留给代理 com.cans.lightning.cache.hotCache.proxy.HotCacheInterceptor 去继承用的
    }

    public FormHotCache(String cacheName,
                        MapDataLoader<K, V> mapDataLoader,
                        HotCacheInitializer<K, V> hotCacheInitializer,
                        HotCacheContext<K> ctx) {
        this.cacheName = cacheName;
        this.mapDataLoader = mapDataLoader;
        this.latch = new CountDownLatch(1);
        this.lock = new ReentrantLock();
        this.loadVersion = new LongAdder();
        this.hotCacheInitializer = hotCacheInitializer;
        this.ctx = ctx;
    }

    @Override
    public V get(K key) {
        if (key == null) {
            return null;
        }
        try {
            return data.get(key)
                    .orElse(null);
        }
        catch (Exception e) {
            log.error("get hot cache 【 {} 】 key: {} error!", cacheName, key, e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void updateChange(List<CacheChangeAction<K, V>> cacheChangeActions) {
        for (CacheChangeAction<K, V> changeAction : cacheChangeActions) {
            if (changeAction.getActionType() == ActionType.Add) {
                // do nothing
            }
            else if (changeAction.getActionType() == ActionType.Remove) {
                data.invalidate(changeAction.getKey());
            }
            else if (changeAction.getActionType() == ActionType.Update) {
                data.invalidate(changeAction.getKey());
                // data.put(changeAction.getKey(), Optional.ofNullable(changeAction.getNewV()));
            }
            log.info("hot cache 【 {} 】 update key: {} action: {}", cacheName, changeAction.getKey(), changeAction.getActionType());
        }
    }

    @Override
    public Map<K, V> getBatch(Collection<K> keys) {
        try {
            return data.getAll(keys)
                    .entrySet()
                    .stream().filter(entry -> entry.getValue().isPresent())
                    .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().get()));
        }
        catch (Exception e) {
            log.error("get hot cache 【 {} 】 keys: {} error!", cacheName, keys, e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void remove(K key) {
        // 不需要做操作 全部移到 updateChange() 实现 通过事件触发移除操作
    }

    @Override
    public void removeBatch(Collection<K> keys) {
        // 不需要做操作 全部移到 updateChange() 实现 通过事件触发移除操作
    }

    @Override
    public void reload() {
        // 这里其实可以加锁, 不然同一时间调用加载
        long start = System.currentTimeMillis();
        try {
            long localVersion = loadVersion.longValue();
            lock.lock();
            if (localVersion != loadVersion.longValue()) {
                log.info("reload hot cache 【 {} 】 version: {} is not equal to current version: {}", cacheName, localVersion, loadVersion.longValue());
                return;
            }
            if (data == null) {
                try {
                    this.selfInit();
                }
                finally {
                    // 加载了之后不再需要这些信息
                    latch.countDown();
                    latch = null;
                    ctx = null;
                }
            }
            else {
                // 程序运行过程中 重新加载
                this.runningReload();
            }
            loadVersion.increment();
        }
        catch (Exception e) {
            log.error("reload hot cache 【 {} 】 error!", cacheName, e);
            throw new RuntimeException(e);
        }
        finally {
            lock.unlock();
            log.info("reload hot cache 【 {} 】 cost time: {} ms", cacheName, System.currentTimeMillis() - start);
        }
    }

    @Override
    public Object mergeTransactionData(Object result, List<CacheChangeAction<Serializable,
                                        Serializable>> transactionObj, String methodName, Object[] args) {
        // 策略:
        // 如果当前线程删除了缓存 则不管删除
        // 如果更新了缓存, 则将事务更新的值 返回到 result
        switch (methodName) {
            case "get": {
                for (CacheChangeAction<Serializable, Serializable> change : transactionObj) {
                    Serializable key = change.getKey();
                    if (!Objects.equals(key, args[0])) {
                        continue;
                    }
                    // 保持更新的值一致
                    if (change.getActionType() == ActionType.Update && change.getNewV() != null) {
                        result = change.getNewV();
                    }
                }
            }
            case "getBatch": {
                // do noting
                // 表单缓存变更意义不大
                break;
            }
        }

        return result;
    }

    @Override
    public void init() {
        reload();
    }

    private void runningReload() {
        // 对已经加载的数据 程序加载
        List<K> list = new ArrayList<>(data.asMap().keySet());
        if (list.size() > 50) {
            log.warn("hot cache 【 {} 】 load data size: {} is too large, may cause OOM", cacheName, list.size());
        }
        data.invalidateAll();

        Map<K, V> kvMap = mapDataLoader.loadBatch(list);
        add2CacheMap(kvMap);
        log.info("hot cache 【 {} 】 reload data size: {}", cacheName, kvMap.size());
    }

    private void selfInit() {
        this.data = CacheBuilder
                .newBuilder()
                .softValues()
                .initialCapacity(200)
                .maximumSize(200)
                .expireAfterAccess(7, TimeUnit.DAYS)
                .removalListener(notify -> {
                    log.info("hot cache 【 {} 】 remove key: {}", cacheName, notify.getKey());
                })
                .build(new CacheLoader<K, Optional<V>>() {
                    @Override
                    public Optional<V> load(K key) throws Exception {
                        return Optional.ofNullable(mapDataLoader.load(key));
                    }

                    @Override
                    public Map<K, Optional<V>> loadAll(Iterable<? extends K> keys) throws Exception {
                        List<K> list = new ArrayList<>();
                        keys.forEach(list::add);
                        Map<K, V> kvMap = mapDataLoader.loadBatch(list);
                        return kvMap.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, entry -> Optional.ofNullable(entry.getValue())));
                    }
                });
        ctx.runTask(ids -> {
            Map<K, V> kvMap = hotCacheInitializer.load4Init(ids);
            add2CacheMap(kvMap);
            log.info("hot cache 【 {} 】 ids【{}】 init data size: {}", ids, cacheName, kvMap.size());
        });
    }

    private void add2CacheMap(Map<K, V> map) {
        if (map == null || map.isEmpty()) {
            return;
        }
        map.forEach((key, value) -> data.put(key, Optional.ofNullable(value)));
    }

    @Override
    public String getKey() {
        return cacheName;
    }

    @Override
    public String getCacheDefine() {
        return "hot cache " + cacheName;
    }
}
