package com.java.util.concurrent;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CompletableFuture;

/**
 * @author qpy
 * @date 2024-03-06 17:06
 * @description 并发框架测试
 */
public class CompletableFutureTest {
    
    private final static Logger logger = LoggerFactory.getLogger(CompletableFutureTest.class);
    
    @Test
    public void testSimple() throws InterruptedException {
        // https://mp.weixin.qq.com/s?__biz=MzU4OTMwODQ4Nw==&mid=2247484679&idx=3&sn=d97e41ebd9260d9f7f8cff2c67c7be39&chksm=fdce322fcab9bb39dbdab0f0b68bc85846ba4c64646a1dd3f2f2cdb7e107034a8b03222978ff&scene=27
        CompletableFuture.runAsync(() -> {
            
            logger.info("打印日志");
        });
        
        Thread.sleep(1000);
    }
}
