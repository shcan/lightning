package com.cans.lightning.business.lowcode.form.cache.util;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class FormCacheLoggerTest {

    private final static Logger logger = LoggerFactory.getLogger(FormCacheLoggerTest.class);
    
    @Test
    void markStartLog() throws InterruptedException {
        String key = "开始初始化缓存信息";
        FormCacheLogger.markStartLog(key);
        try {
            Thread.sleep(1000);
        }
        finally {
            FormCacheLogger.markEndLog(key);
        }

        key = "开始初始化缓存信息2";
        FormCacheLogger.markStartLog(key);
        key = "开始初始化缓存信息3";
        FormCacheLogger.markStartLog(key);
    }

    @Test
    void markEndLog() {
    }

    @Test
    void monitor() {
        String key = "开始初始化缓存信息";
        FormCacheLogger.monitor(key, () -> {
            Thread.sleep(1000);
        });
    }

    @Test
    void testMonitor() {
        String key = "开始初始化缓存信息";
        String monitorResult = FormCacheLogger.monitor(key, () -> {
            Thread.sleep(1000);
            return "执行结果.....";
        });
        logger.info(monitorResult);
    }
}
