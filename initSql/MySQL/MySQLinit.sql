/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.101.34
 Source Server Type    : MySQL
 Source Server Version : 50740
 Source Host           : 192.168.101.34:3306
 Source Schema         : lightning

 Target Server Type    : MySQL
 Target Server Version : 50740
 File Encoding         : 65001

 Date: 04/11/2023 15:20:41
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for app_definition
-- ----------------------------
DROP TABLE IF EXISTS `app_definition`;
CREATE TABLE `app_definition`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `last_change_time` datetime(0) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for app_field
-- ----------------------------
DROP TABLE IF EXISTS `app_field`;
CREATE TABLE `app_field`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `last_change_time` datetime(0) NOT NULL,
  `field_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `field_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `field_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `length` int(11) NULL DEFAULT NULL,
  `form_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `sort_id` int(11) NULL DEFAULT NULL,
  `width` int(11) NULL DEFAULT NULL,
  `db_field_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `table_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `db_field_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for app_form
-- ----------------------------
DROP TABLE IF EXISTS `app_form`;
CREATE TABLE `app_form`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `last_change_time` datetime(0) NOT NULL,
  `app_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `form_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `db_table_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of app_form
-- ----------------------------
INSERT INTO `app_form` VALUES ('7d03b72d-d368-4620-8d0b-6090b6b5bc10', '2022-05-03 05:36:24', '2022-05-03 05:36:24', '', '1', NULL);
INSERT INTO `app_form` VALUES ('a4260104-d52b-48af-a23b-8bd1d2ea54f5', '2022-05-03 04:46:13', '2022-05-03 04:46:13', '', '第一个表单', NULL);
INSERT INTO `app_form` VALUES ('dfa396eb-70af-4a1e-9949-d0308edbb939', '2022-11-07 15:40:39', '2022-11-07 15:40:39', 'bfb7321f-d4eb-4aa3-be91-5a5a24e38931', '测试的表单拉', NULL);
INSERT INTO `app_form` VALUES ('f3307ac7-4e01-4844-a89c-fae0a37a0a0a', '2022-11-06 05:49:14', '2022-11-06 05:50:24', NULL, '', '');

-- ----------------------------
-- Table structure for app_index
-- ----------------------------
DROP TABLE IF EXISTS `app_index`;
CREATE TABLE `app_index`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `value` bigint(20) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `last_change_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for app_table
-- ----------------------------
DROP TABLE IF EXISTS `app_table`;
CREATE TABLE `app_table`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `last_change_time` datetime(0) NOT NULL,
  `app_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `db_table_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `form_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `table_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `table_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;
-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `last_change_time` datetime(0) NOT NULL,
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tweets_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKgftr17kf5cy5590wj4r4taats`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for file_info
-- ----------------------------
DROP TABLE IF EXISTS `file_info`;
CREATE TABLE `file_info`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `last_change_time` datetime(0) NOT NULL,
  `file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `file_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `file_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_access_class
-- ----------------------------
DROP TABLE IF EXISTS `sys_access_class`;
CREATE TABLE `sys_access_class`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `last_change_time` datetime(0) NOT NULL,
  `class_mark` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `class_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `short_class_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_access_method
-- ----------------------------
DROP TABLE IF EXISTS `sys_access_method`;
CREATE TABLE `sys_access_method`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `last_change_time` datetime(0) NOT NULL,
  `method_mark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `method_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `request_method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `sys_access_class_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKt7pydj1e7boh2gn8okb9js292`(`sys_access_class_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;
-- ----------------------------
-- Table structure for sys_code_generate_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_code_generate_config`;
CREATE TABLE `sys_code_generate_config`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `last_change_time` datetime(0) NOT NULL,
  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `current_number` decimal(19, 2) NOT NULL,
  `day` int(11) NULL DEFAULT NULL,
  `increment` int(11) NOT NULL,
  `init_number` decimal(19, 2) NOT NULL,
  `last_generate_date_time` datetime(0) NULL DEFAULT NULL,
  `month` int(11) NULL DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `number_length` int(11) NULL DEFAULT NULL,
  `prefix` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `reset_rule` int(11) NULL DEFAULT NULL,
  `year` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_999wvmoq82w6htry7vd8h8mvk`(`code`) USING BTREE,
  UNIQUE INDEX `idx_code`(`code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `last_change_time` datetime(0) NOT NULL,
  `idx` int(11) NULL DEFAULT NULL,
  `key_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `memo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `pre_set` int(11) NULL DEFAULT NULL,
  `value` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_jf6m0vbqf8wet1sjh8aofmuwe`(`key_code`) USING BTREE,
  INDEX `idx_key_code`(`key_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;
-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `last_change_time` datetime(0) NOT NULL,
  `dept_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dept_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;
-- ----------------------------
-- Table structure for sys_dictionary
-- ----------------------------
DROP TABLE IF EXISTS `sys_dictionary`;
CREATE TABLE `sys_dictionary`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `last_change_time` datetime(0) NOT NULL,
  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `idx` int(11) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_ah1isfy5alow4rtkty9vri85r`(`code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;
-- ----------------------------
-- Table structure for sys_dictionary_item
-- ----------------------------
DROP TABLE IF EXISTS `sys_dictionary_item`;
CREATE TABLE `sys_dictionary_item`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `last_change_time` datetime(0) NOT NULL,
  `idx` int(11) NULL DEFAULT NULL,
  `label` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `dictionary_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKadg3vwji0g3oxi9t0pb85auvs`(`dictionary_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;
-- ----------------------------
-- Table structure for sys_file_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_file_info`;
CREATE TABLE `sys_file_info`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `last_change_time` datetime(0) NOT NULL,
  `file_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `file_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `file_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `storage_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;
-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `last_change_time` datetime(0) NOT NULL,
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `idx` int(11) NULL DEFAULT NULL,
  `menu_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `menu_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `parent_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `i18n` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_b679yhxaq3tpo2ri78i4tndgl`(`menu_code`) USING BTREE,
  INDEX `FK2jrf4gb0gjqi8882gxytpxnhe`(`parent_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;
-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('40289fc875e9875e0175e98f78ce0001', '2020-11-21 06:48:15', '2020-12-04 06:53:13', 'meh', 2, 'pms_role', '角色管理', '/pms/role', '94288427757870d7017578af1d860004', NULL);
INSERT INTO `sys_menu` VALUES ('40289fc875e9875e0175e995f1310002', '2020-11-21 06:55:19', '2020-12-04 06:53:21', 'ordered-list', 3, 'pms_menu', '菜单管理', '/pms/menu', '94288427757870d7017578af1d860004', NULL);
INSERT INTO `sys_menu` VALUES ('40289fc87637f4cf0176385e439a0004', '2020-12-06 14:04:30', '2020-12-09 12:18:30', 'setting', 10, 'setting', '系统设置(未完成)', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('40289fc87637f4cf0176385f5b6b0005', '2020-12-06 14:05:42', '2020-12-06 14:05:42', 'star', 1, 'system_config', '系统配置(未完成)', '/setting/system', '40289fc87637f4cf0176385e439a0004', NULL);
INSERT INTO `sys_menu` VALUES ('40289fc87637f4cf01763860942b0006', '2020-12-06 14:07:02', '2020-12-06 14:07:34', 'read', 2, 'setting_dictionary', '数据字典(未完成)', '/setting/dictionary', '40289fc87637f4cf0176385e439a0004', NULL);
INSERT INTO `sys_menu` VALUES ('80a2cc24-c837-4672-8f4a-429be6c0e5af', '2021-12-05 12:54:27', '2021-12-05 12:54:27', 'gold', 1, 'app_def', '应用管理', '/low_code/app_def', 'c80531d2-f7e0-4a84-b463-5111556dc062', NULL);
INSERT INTO `sys_menu` VALUES ('94288427757870d7017578af1d860004', '2020-10-30 08:45:44', '2020-12-09 12:18:15', 'cluster', 6, 'pms', '权限管理', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES ('94288427757870d7017578af506c0005', '2020-10-30 08:45:57', '2020-12-04 06:53:04', 'user', 1, 'pms_user', '用户管理', '/pms/user', '94288427757870d7017578af1d860004', NULL);
INSERT INTO `sys_menu` VALUES ('c80531d2-f7e0-4a84-b463-5111556dc062', '2021-12-05 12:51:45', '2021-12-05 12:51:45', 'car', 2, 'low_code', '应用定制', '/low_code', NULL, NULL);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `last_change_time` datetime(0) NOT NULL,
  `memo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `role_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `role_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_jqdita2l45v2gglry7bp8kl1f`(`role_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('40289fc875e90b400175e90ea8e70000', '2020-11-21 04:27:34', '2020-12-20 08:01:52', '管理员', '0001', '管理员', 1);
INSERT INTO `sys_role` VALUES ('94288427757870d7017578be49a90009', '2020-10-30 09:02:18', '2020-12-03 14:04:20', '普通用户', '002-001', '普通用户', 1);

-- ----------------------------
-- Table structure for sys_role_menus
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menus`;
CREATE TABLE `sys_role_menus`  (
  `role_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `menu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE,
  UNIQUE INDEX `UK_17qat0kqrt0r9udrmm5m6ai77`(`menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menus
-- ----------------------------
INSERT INTO `sys_role_menus` VALUES ('40289fc875e90b400175e90ea8e70000', '40288188762c63b601762c7078960000');
INSERT INTO `sys_role_menus` VALUES ('40289fc875e90b400175e90ea8e70000', '94288427757870d70175789b30d30002');
INSERT INTO `sys_role_menus` VALUES ('40289fc875e90b400175e90ea8e70000', '94288427757870d7017578af1d860004');

-- ----------------------------
-- Table structure for sys_role_methods
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_methods`;
CREATE TABLE `sys_role_methods`  (
  `role_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `method_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`role_id`, `method_id`) USING BTREE,
  INDEX `FK3ea68qalv5jmohmkswf7plqb8`(`method_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;
-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `last_change_time` datetime(0) NOT NULL,
  `age` int(11) NULL DEFAULT NULL,
  `avatar_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `expire_time` datetime(0) NULL DEFAULT NULL,
  `login_time` datetime(0) NULL DEFAULT NULL,
  `memo` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nickname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone_num` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sex` int(11) NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dept_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `super_man` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_51bvuyvihefoh4kp5syh2jpi4`(`username`) USING BTREE,
  INDEX `FKb3pkx0wbo6o8i8lj0gxr37v1n`(`dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('40289fc8764725800176474f9fc30000', '2020-12-09 11:42:49', '2020-12-09 11:47:33', 18, NULL, '123@163.com', NULL, NULL, NULL, '管理员12', '$2a$10$6gPTUYlr5alkt98iGFyaIOGcwQH8Y79FRanGpCnqbEBF.M9KCyrhm', '12345', 1, 1, 'admin', NULL, 1);

-- ----------------------------
-- Table structure for sys_user_roles
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_roles`;
CREATE TABLE `sys_user_roles`  (
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `role_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE,
  INDEX `FKqwiuml6b7mjmk48u5b9hmk853`(`role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_roles
-- ----------------------------
INSERT INTO `sys_user_roles` VALUES ('40289fc8764725800176474f9fc30000', '40289fc875e90b400175e90ea8e70000');

-- ----------------------------
-- Table structure for thumbs_up
-- ----------------------------
DROP TABLE IF EXISTS `thumbs_up`;
CREATE TABLE `thumbs_up`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `last_change_time` datetime(0) NOT NULL,
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKio5q021upl9drecvsb5o863sj`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;
-- ----------------------------
-- Table structure for tweets
-- ----------------------------
DROP TABLE IF EXISTS `tweets`;
CREATE TABLE `tweets`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `last_change_time` datetime(0) NOT NULL,
  `content` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `up` int(11) NULL DEFAULT NULL,
  `up_ids` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

DROP TABLE IF EXISTS `app_menu`;
CREATE TABLE `app_menu`
(
    `id`               varchar(36) COLLATE utf8mb4_bin NOT NULL,
    `create_time`      datetime                        NOT NULL,
    `last_change_time` datetime                        NOT NULL,
    `app_id`           varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
    `type`             int(3) DEFAULT NULL,
    `name`             varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- menu add field
-- ----------------------------
ALTER TABLE sys_menu ADD menu_type int(3) DEFAULT NULL;
ALTER TABLE sys_menu ADD binding_id varchar(36) DEFAULT NULL;
ALTER TABLE sys_menu ADD open_type varchar (10) DEFAULT NULL;

-- ----------------------------
-- app menu add field
-- ----------------------------
ALTER TABLE app_menu ADD app_menu_type int(3) DEFAULT NULL;
ALTER TABLE app_menu ADD binding_id varchar(36) DEFAULT NULL;
ALTER TABLE app_menu ADD level int(3) DEFAULT NULL;
ALTER TABLE app_menu ADD url varchar(255)  DEFAULT NULL;
ALTER TABLE app_menu ADD sort_id int(10) DEFAULT NULL;
ALTER TABLE app_menu ADD open_type varchar (10) DEFAULT NULL;

ALTER TABLE app_field ADD field_index int(10) DEFAULT NULL;

CREATE TABLE `app_form_view`
(
    `id`               VARCHAR(36) COLLATE utf8mb4_bin NOT NULL,
    `create_time`      datetime                        NOT NULL,
    `last_change_time` datetime                        NOT NULL,
    `app_id`           VARCHAR(36) COLLATE utf8mb4_bin   DEFAULT NULL,
    `form_id`          VARCHAR(36) COLLATE utf8mb4_bin   DEFAULT NULL,
    `sort_id`          VARCHAR(36) COLLATE utf8mb4_bin   DEFAULT NULL,
    `view_name`         VARCHAR(255) COLLATE utf8mb4_bin  DEFAULT NULL,
    `field_bind`        VARCHAR(4000) COLLATE utf8mb4_bin DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
);

CREATE TABLE `app_form_view_item`
(
    `id`               varchar(36) COLLATE utf8mb4_bin NOT NULL,
    `create_time`      datetime                        NOT NULL,
    `last_change_time` datetime                        NOT NULL,
    `form_id`          VARCHAR(36) COLLATE utf8mb4_bin   DEFAULT NULL,
    `view_id`           varchar(36) COLLATE utf8mb4_bin   DEFAULT NULL,
    `show_type`         varchar(255) COLLATE utf8mb4_bin  DEFAULT NULL,
    `ref_obj`           varchar(4000) COLLATE utf8mb4_bin DEFAULT NULL,
    `sort_id`          VARCHAR(36) COLLATE utf8mb4_bin   DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
);

ALTER TABLE app_form ADD field_index int(10) DEFAULT NULL;

CREATE TABLE `app_form_bind`
(
    `id`               varchar(36) COLLATE utf8mb4_bin NOT NULL,
    `create_time`      datetime                        NOT NULL,
    `last_change_time` datetime                        NOT NULL,
    `form_id`          VARCHAR(36) COLLATE utf8mb4_bin   DEFAULT NULL,
    `app_id`           varchar(36) COLLATE utf8mb4_bin   DEFAULT NULL,
    `bind_name`        varchar(255) COLLATE utf8mb4_bin  DEFAULT NULL,
    `field_list_show`  varchar(400) COLLATE utf8mb4_bin DEFAULT NULL,
    `sort_setting`     VARCHAR(400) COLLATE utf8mb4_bin  DEFAULT NULL,
    `filter_field`     varchar(255) COLLATE utf8mb4_bin  DEFAULT NULL,
    `auth_info`        varchar(255) COLLATE utf8mb4_bin  DEFAULT NULL,
    `sort_id`          VARCHAR(36) COLLATE utf8mb4_bin   DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
);

SET FOREIGN_KEY_CHECKS = 1;
