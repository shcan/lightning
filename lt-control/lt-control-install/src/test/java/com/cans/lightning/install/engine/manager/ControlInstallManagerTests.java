package com.cans.lightning.install.engine.manager;

import com.cans.lightning.install.InstallControlBoot;
import com.cans.lightning.install.engine.dto.InstallContext;
import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.io.File;

@SpringBootTest
@ContextConfiguration(classes = InstallControlBoot.class)
public class ControlInstallManagerTests {

    @Resource
    private ControlInstallManager controlInstallManager;

    @Test
    void testInstall() {
        InstallContext context = new InstallContext().setTempFile(new File("C:\\Users\\Administrator\\Desktop\\控件安装-测试控件\\控件安装-测试控件.zip"));
        controlInstallManager.install(context);
    }
}


