package com.cans.lightning.install;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InstallControlBoot {
    public static void main(String[] args) {
        SpringApplication.run(InstallControlBoot.class, args);
    }
}
