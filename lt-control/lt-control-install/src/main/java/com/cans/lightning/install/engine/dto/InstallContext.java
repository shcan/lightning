package com.cans.lightning.install.engine.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.File;

/**
 * 控件安装上下文
 */
@Setter
@Getter
@Accessors(chain = true)
public class InstallContext {

    /**
     * 上传的文件
     */
    private File tempFile;


    /**
     * 解压后的文件目录
     */
    private File realUnzipFileDir;

    /**
     * 如果是安装 true, 如果是update false
     */
    private boolean installOrUpdate = true;

}
