package com.cans.lightning.install.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cans.lightning.install.business.mapper.AppControlMapper;
import com.cans.lightning.install.business.po.AppControl;
import com.cans.lightning.install.business.service.AppControlService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author qpy
 * @since 2024-08-09
 */
@Service
public class AppControlServiceImpl extends ServiceImpl<AppControlMapper, AppControl> implements AppControlService {

}
