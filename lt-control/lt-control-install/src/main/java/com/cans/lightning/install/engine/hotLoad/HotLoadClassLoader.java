package com.cans.lightning.install.engine.hotLoad;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

/**
 * 热部署类加载器
 * note: 为了实现不加载错误的资源, 需要对ClassLoader的 loadClass、getResource、getResources 方法进行重写,
 * 包含控件key的内容就本ClassLoader加载, 否则还是交给 遵循双亲委派加载
 *
 * 需要再注意, 由于 loadClass 也是通过getResource()获取的资源文件, 所以不需要重写 loadClass
 */
@Slf4j
public class HotLoadClassLoader extends URLClassLoader {

    private HotLoadConfig hotLoadConfig;

    public HotLoadClassLoader(URL[] urls, ClassLoader parent, HotLoadConfig hotLoadConfig) {
        super(urls, parent);
        this.hotLoadConfig = hotLoadConfig;
    }

    @Override
    protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
        // 实现不实现此方法都一样, 但是为了通用实现, 还是先用当前类加载, 再走父类加载逻辑吧
        synchronized (getClassLoadingLock(name)) {
            Class<?> c = findLoadedClass(name);
            if (c == null) {
                try {
                    c = findClass(name);
                } catch (Exception e) {
                }
                if (c == null) {
                    // If still not found, then invoke the parent's findClass
                    // method to find the class.
                    c = super.loadClass(name, resolve);
                }
                else {
                    log.info("key {}, loadClass: name:{}", hotLoadConfig.getHotLoadKey(), name);
                }
            }
            if (resolve) {
                resolveClass(c);
            }
            return c;
        }
    }

    @Override
    public URL getResource(String name) {
        // findResource是直接查找资源, getResource是先走双亲委派的逻辑查找资源, 如果没有找到, 再调用findResource
        boolean hotLoadResource = isHotLoadResource(name);
        log.info("key {}, getResource: name:{} hotLoadResource:{}", hotLoadConfig.getHotLoadKey(), name, hotLoadResource);
        return hotLoadResource ? super.findResource(name) : super.getResource(name);
    }

    @Override
    public Enumeration<URL> getResources(String name) throws IOException {
        boolean hotLoadResource = isHotLoadResource(name);
        log.info("key {}, getResources: name:{} hotLoadResource:{}", hotLoadConfig.getHotLoadKey(), name, hotLoadResource);
        return hotLoadResource ? super.findResources(name) : super.getResources(name);
    }

    private boolean isHotLoadResource(String name) {
        List<String> list = Arrays.asList(name.split("(\\.)|(/)|(\\\\)"));
        return list.stream().anyMatch(s -> s.equals(hotLoadConfig.getHotLoadKey()));
    }
}
