package com.cans.lightning.install.engine.hotLoad;

import com.cans.lightning.base.exceptions.BusinessException;
import com.cans.lightning.install.engine.hotLoad.framework.mybatis.MyBatisConfigurationTemplate;
import com.cans.lightning.install.engine.hotLoad.framework.spring.HotLoadConfigApplicationContext;
import com.cans.lightning.install.engine.hotLoad.framework.springmvc.SpringMvcHotLoadSupport;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.util.*;
import java.util.regex.Pattern;

/**
 * 热部署安装器实现
 */
@Slf4j
@Component
public class HotLoadInstaller implements ApplicationContextAware {

    /**
     * 热部署上下文存储
     */
    private static Map<String, HotLoadContext> hotLoadContextMap = Maps.newConcurrentMap();
    private static Map<String, Object> hotLoadLockMap = Maps.newConcurrentMap();


    private static ApplicationContext applicationContext;

    /**
     * lib目录样式
     */
    private static final Pattern PATTERN_LIB = Pattern.compile(".*(\\*|/*)lib(\\*|/*)$");
    /**
     * class目录样式
     */
    private static final Pattern PATTERN_CLASSES = Pattern.compile(".*(\\*|/*)classes(\\*|/*)$");

    /**
     * 热部署平台功能实现
     * @param hotLoadConfig 热部署配置
     * @return 部署结果 success/false
     */
    public static InstallResult hotLoad(HotLoadConfig hotLoadConfig) {
        try {
            doHotLoad(hotLoadConfig);
            return new InstallResult();
        }
        catch (Exception e) {
            log.error("hotLoad error", e);
            return InstallResult.builder().success(false).message(e.getMessage()).build();
        }
    }

    public static InstallResult hotUnload(String hotLoadKey) {
        try {
            doHotUnload(hotLoadKey);
            return new InstallResult();
        }
        catch (Exception e) {
            log.error("hotUnload occur exception key {}, error: {}", hotLoadKey, e.getMessage());
            throw new BusinessException(e.getMessage(), e);
        }
    }

    public static HotLoadContext getHotLoadContext(String hotLoadKey) {
        return hotLoadContextMap.get(hotLoadKey);
    }

    private static void doHotLoad(HotLoadConfig hotLoadConfig) {
        // 1. 校验配置
        validateConfig(hotLoadConfig);
        String hotLoadKey = hotLoadConfig.getHotLoadKey();

        HotLoadClassLoader hotLoadClassLoader = null;
        try {
            synchronized (getHotLoadKey(hotLoadKey)) {

                // 2. 如果安装 不允许再重复安装
                if (isInstalled(hotLoadKey)) {
                    log.warn("hotLoadKey: {} is installed, can not install again", hotLoadConfig.getHotLoadKey());
                    throw new RuntimeException("hotLoadKey is installed, can not install again");
                }

                // 1. 创建类加载器
                hotLoadClassLoader = createHotLoadClassLoader(hotLoadConfig);

                // 2. 创建spring容器
                HotLoadConfigApplicationContext hotLoadSpringContext = new HotLoadConfigApplicationContext();
                hotLoadSpringContext.setParent(applicationContext);
                hotLoadSpringContext.setClassLoader(hotLoadClassLoader);

                HotLoadContext hotLoadContext = new HotLoadContext(hotLoadConfig, hotLoadClassLoader, hotLoadSpringContext);
                // 注册配置类到ioc里面
                registerConfigClass(hotLoadSpringContext, hotLoadConfig, hotLoadClassLoader);
                // 注册框架支持 eg:mybatis
                registerFrameSupport(hotLoadContext);
                hotLoadSpringContext.refresh();

                // 注册spring对于其他框架的支持 需要在 refresh() 之后进行注册
                registerSpringMvcFrameSupport(hotLoadContext);

                hotLoadContextMap.put(hotLoadKey, hotLoadContext);
                log.info("hotLoad success key: {}, url: {}", hotLoadKey, Arrays.asList(hotLoadClassLoader.getURLs()));
            }
        }
        catch (Exception e) {
            if (hotLoadClassLoader != null) {
                try {
                    hotLoadClassLoader.close();
                } catch (IOException ex) {
                    log.warn("hotLoadClassLoader close occur exception, ignore this. {}", ex.getMessage());
                }
            }

            hotLoadContextMap.remove(hotLoadKey);
            // 也加不了多少key进去, 可以保留
            if (hotLoadLockMap.size() > 10000) {
                hotLoadLockMap.remove(hotLoadKey);
            }
            log.error("hotLoad occur exception key {}, error: {}", hotLoadConfig.getHotLoadKey(), e.getMessage());
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private static void doHotUnload(String hotLoadKey) {
        if (!ObjectUtils.isEmpty(hotLoadKey) || !isInstalled(hotLoadKey)) {
            throw new BusinessException("hotLoadKey is empty or not installed. " + hotLoadKey);
        }
        log.info("hotUnload starting, key: {}", hotLoadKey);

        HotLoadContext hotLoadContext = hotLoadContextMap.get(hotLoadKey);
        synchronized (getHotLoadKey(hotLoadKey)) {
            // 移除spring mvc
            SpringMvcHotLoadSupport.uninstall(hotLoadContext);

            hotLoadContext.close();
            hotLoadContextMap.remove(hotLoadKey);
            hotLoadLockMap.remove(hotLoadKey);
        }
    }

    private static void registerConfigClass(HotLoadConfigApplicationContext hotLoadSpringContext, HotLoadConfig hotLoadConfig, HotLoadClassLoader hotLoadClassLoader) throws ClassNotFoundException {

        List<Class> configClass = new ArrayList<>();
        for (String springConfigurationClass : hotLoadConfig.getSpringConfigurationClass()) {
            configClass.add(hotLoadClassLoader.loadClass(springConfigurationClass));
        }
        hotLoadSpringContext.register(configClass.toArray(new Class[0]));
    }

    private static void registerSpringMvcFrameSupport(HotLoadContext hotLoadContext) {
        SpringMvcHotLoadSupport.hotLoad2ParentApplicationContext(hotLoadContext);
    }

    private static void registerFrameSupport(HotLoadContext hotLoadContext) throws Exception {
        // 1. mybatis框架支持
        MyBatisConfigurationTemplate.buildMyBatisSpringConfig(hotLoadContext);
    }

    private static HotLoadClassLoader createHotLoadClassLoader(HotLoadConfig hotLoadConfig) {
        // 1. 父类加载器
        ClassLoader parentClassLoader = HotLoadInstaller.class.getClassLoader();

        // 2. 加载的文件路径
        List<Path> hotLoadClassPath = hotLoadConfig.getHotLoadClassPath();
        Set<URL> classPaths = new LinkedHashSet<>();
        for (Path path : hotLoadClassPath) {
            try {
                String absolutePath = path.toFile().getAbsolutePath();
                // 只添加 "/lib" 和 "/classes" 下的目录
                if (PATTERN_LIB.matcher(absolutePath).matches()) {
                    for (File file : Objects.requireNonNull(path.toFile().listFiles())) {
                        if (file.getName().endsWith(".jar")) {
                            classPaths.add(file.toURI().toURL());
                        }
                    }
                }
                else if (PATTERN_CLASSES.matcher(absolutePath).matches()) {
                    classPaths.add(path.toUri().toURL());
                }
                else {
                    log.warn("hotLoadClassPath: {} is not support, ignore this", path.toFile().getAbsolutePath());
                }
            } catch (MalformedURLException e) {
                throw new RuntimeException(e);
            }
        }
        log.info("create HotLoadClassLoader key {} hotLoadClassPath: {}", hotLoadConfig.getHotLoadKey(), classPaths);
        return new HotLoadClassLoader(classPaths.toArray(new URL[0]), parentClassLoader, hotLoadConfig);
    }

    private static boolean isInstalled(String hotLoadKey) {
        return hotLoadContextMap.get(hotLoadKey) != null;
    }

    private static void validateConfig(HotLoadConfig hotLoadConfig) {
        // 1. 逻辑校验
        String hotLoadKey = hotLoadConfig.getHotLoadKey();
        Objects.requireNonNull(hotLoadKey, "hotLoadKey is not allow null");
        if (ObjectUtils.isEmpty(hotLoadConfig.getHotLoadClassPath())) {
            log.warn("hotLoadClassPath is empty, hotLoadKey: {}", hotLoadKey);
            throw new RuntimeException("hotLoadClassPath is empty");
        }
        List<String> configClass = hotLoadConfig.getSpringConfigurationClass();
        if (CollectionUtils.isEmpty(configClass)) {
            log.warn("springConfigurationClass is empty, hotLoadKey: {}", hotLoadKey);
            throw new RuntimeException("springConfigurationClass is empty");
        }
        // 校验配置类文件是否存在
        // 可用通过先构建ClassLoader的方式来判断
        // 也可以判断文件的类型, 手动创建URL对象来实现
        // 由于代码过多 暂不做判断
        // for (String cClass : configClass) {
        // }
    }

    private static Object getHotLoadKey(String hotLoadKey) {
        Object newLock = new Object();
        Object lock = hotLoadLockMap.putIfAbsent(hotLoadKey, newLock);
        if (lock == null) {
            lock = newLock;
        }
        return lock;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
