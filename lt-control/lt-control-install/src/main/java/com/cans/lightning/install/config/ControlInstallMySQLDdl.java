package com.cans.lightning.install.config;

import com.baomidou.mybatisplus.extension.ddl.IDdl;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

/**
 * 自定义控件按钮脚本执行
 */
public class ControlInstallMySQLDdl implements IDdl {

    private DataSource dataSource;

    public ControlInstallMySQLDdl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void runScript(Consumer<DataSource> consumer) {
        consumer.accept(dataSource);
    }

    @Override
    public List<String> getSqlFiles() {
        return Collections.singletonList("com/cans/lightning/install/business/po/ddl.sql");
    }
}
