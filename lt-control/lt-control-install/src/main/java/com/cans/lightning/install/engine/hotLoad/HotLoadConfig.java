package com.cans.lightning.install.engine.hotLoad;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.nio.file.Path;
import java.util.List;
import java.util.Set;

/**
 * 热部署配置
 */
@Setter
@Getter
@Builder
public class HotLoadConfig {

    /**
     * 热部署的key
     * /@see com.cans.lightning.install.engine.hotLoad.HotLoadInstaller#hotLoadContextMap
     */
    private String hotLoadKey;

    /**
     * 热部署的配置类列表
     */
    private List<String> springConfigurationClass;

    /**
     * 热部署 类加载器的资源路径
     */
    private List<Path> hotLoadClassPath;

    /**
     * spring 框架扫描时, 会拿此包判断, 扫描此包下的类、资源文件
     */
    private Set<String> frameScanPackage;
}
