package com.cans.lightning.install.engine.exceptions;

import com.cans.lightning.base.exceptions.BusinessException;

/**
 * 控件安装的异常
 */
public class ControlInstallException extends BusinessException {
    public ControlInstallException(String msg) {
        super(msg);
    }
}
