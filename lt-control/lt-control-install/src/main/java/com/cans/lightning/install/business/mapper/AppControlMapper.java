package com.cans.lightning.install.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cans.lightning.install.business.po.AppControl;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author qpy
 * @since 2024-08-09
 */
@Mapper
public interface AppControlMapper extends BaseMapper<AppControl> {

}
