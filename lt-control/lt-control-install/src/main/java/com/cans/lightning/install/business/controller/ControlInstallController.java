package com.cans.lightning.install.business.controller;

import com.cans.lightning.base.dto.ResDto;
import com.cans.lightning.form.bean.fieldCtrl.CustomControlCache;
import com.cans.lightning.install.business.po.AppControl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author qpy
 * @since 2024-08-09
 */
@RestController
@RequestMapping("/control/install")
public class ControlInstallController {

    @RequestMapping("/list")
    public ResDto<List<Map<String, Object>>> list() {
        return ResDto.success(CustomControlCache.getCustomControlMap().values().stream().map(c -> {
            Map<String, Object> map = new HashMap<>();
            map.put("key", c.getKey());
            map.put("class", c.getClass().getName());
            map.put("valid", c.isValid());
            return map;
        }).collect(Collectors.toList()));
    }
}
