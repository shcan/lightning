package com.cans.lightning.install.engine.context;

public class ControlSettingContext {

    public static final String DIR_SEP = "/";

    /**
     * 控件代码包的文件路径
     */
    public static final String CONTROL_INSTALL_PATH = System.getProperty("user.dir") + "/controlInstall";

    /**
     * 临时解压目录
     */
    public static final String CONTROL_INSTALL_TEMP_PATH = CONTROL_INSTALL_PATH + "/temp";

    /**
     * 安装包配置文件名
     */
    public static final String CONTROL_CONFIG = "control-config.json";

    /**
     * 控件安装目录的时间戳, 由于可能存在多次安装, 每次安装使用时间戳来决定使用哪个
     */
    public static final String CONTROL_INSTALL_DATE_FORMAT = "yyyymmddHHmmss";

    /**
     * 后端代码需要的目录
     */
    public static final String CONTROL_INSTALL_CODE_DIR = "lightning/WEB-INF";

    /**
     * 控件的前缀
     */
    public static final String CONTROL_CUSTOM_PREFIX = "custom_";
}
