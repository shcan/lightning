package com.cans.lightning.install.business.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cans.lightning.base.dto.BaseDto;
import com.cans.lightning.install.engine.enums.ControlInstallState;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@TableName("app_control")
@Setter
@Getter
public class AppControl extends BaseDto<Long> {

    @TableField("control_name")
    private String controlName;

    /**
     * 自定义控件的控件id
     */
    @TableField("control_key")
    private String controlKey;

    /**
     * 自定义控件版本 eg: 1.0.0 1.1.0
     */
    @TableField("control_version")
    private String controlVersion;

    /**
     * 0. 自带的自定义控件
     * 1. 自带的自定义按钮
     * 2. 需要安装的自定义控件
     * 3. 需要安装的自定义按钮
     */
    @TableField("control_type")
    private String controlType;

    /**
     * 控件安装的目录名称  以此名称作为控件热部署的目录 必填
     */
    @TableField("control_dir_name")
    private String controlDirName;

    /**
     * 支持的lt版本 默认不做功能
     */
    @TableField("support_version")
    private String supportVersion;

    @TableField("create_user")
    private String createUser;

    /**
     * 控件生效时间
     */
    @TableField("effect_start_date")
    private Date effectStartDate;

    /**
     * 控件失效时间
     */
    @TableField("effect_end_date")
    private Date effectEndDate;

    /**
     * 自定义控件的状态
     * @see ControlInstallState
     */
    @TableField("state")
    private int state;

    @TableField("json")
    private String json;
}

