package com.cans.lightning.install.engine.utils;

import com.cans.lightning.form.bean.fieldCtrl.CustomControlCache;
import com.cans.lightning.form.bean.fieldCtrl.FormFieldCustomControl;
import com.cans.lightning.install.engine.hotLoad.HotLoadConfig;
import com.cans.lightning.install.engine.hotLoad.HotLoadContext;
import com.cans.lightning.install.engine.hotLoad.HotLoadInstaller;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Map;
import java.util.Objects;

@Slf4j
public class ControlHotLoadUtils {

    public static void addControl2ControlCache(HotLoadConfig hotLoadConfig, String controlKey) {
        HotLoadContext hotLoadContext = HotLoadInstaller.getHotLoadContext(hotLoadConfig.getHotLoadKey());
        Objects.requireNonNull(hotLoadContext, "hot load context require not null, Unknown exception occurred. key:" + hotLoadConfig.getHotLoadKey());

        AnnotationConfigApplicationContext applicationContext = hotLoadContext.getApplicationContext();
        Map<String, FormFieldCustomControl> customControlMap = applicationContext.getBeansOfType(FormFieldCustomControl.class);
        for (Map.Entry<String, FormFieldCustomControl> control : customControlMap.entrySet()) {
            if (control.getValue().getKey().equals(controlKey)) {
                control.getValue().setValid(true);
                log.info("find hotLoad control, key: 【{}】, class:{}", controlKey, control.getValue().getClass().getName());
                CustomControlCache.addCustomControl(control.getValue());
                return;
            }
        }
    }

}
