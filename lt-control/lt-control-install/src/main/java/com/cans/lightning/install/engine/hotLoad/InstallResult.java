package com.cans.lightning.install.engine.hotLoad;

import lombok.*;

import java.util.List;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InstallResult {

    private boolean success = true;
    private String message;
    private List<Object> data;
}
