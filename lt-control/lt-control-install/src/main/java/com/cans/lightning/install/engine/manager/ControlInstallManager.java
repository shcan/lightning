package com.cans.lightning.install.engine.manager;

import com.cans.lightning.base.dto.ResDto;
import com.cans.lightning.install.engine.dto.InstallContext;

import java.io.File;

/**
 * 控件安装管理器
 */
public interface ControlInstallManager {


    /**
     * 安装控件
     * @param context 控件文件
     * @return resDto
     */
    ResDto<String> install(InstallContext context);
}
