package com.cans.lightning.install.engine.dto;


import lombok.Getter;
import lombok.Setter;

/**
 *
 */
@Getter
@Setter
public class ControlConfigDto {

    private String controlName;

    /**
     * 自定义控件的控件id
     */
    private String controlKey;

    /**
     * 自定义控件版本 eg: 1.0.0 1.1.0
     */
    private String controlVersion;

    /**
     * 0. 自带的自定义控件
     * 1. 自带的自定义按钮
     * 2. 需要安装的自定义控件
     * 3. 需要安装的自定义按钮
     */
    private String controlType = "2";

    /**
     * 支持的lt版本 默认不做功能
     */
    private String supportVersion;

    /**
     * 配置类名称  必填
     */
    private String configurationClass;

    /**
     * 控件安装的目录名称  以此名称作为控件热部署的目录 必填
     */
    private String controlDirName;

}
