package com.cans.lightning.install.engine.hotLoad;

import com.cans.lightning.install.engine.hotLoad.framework.springmvc.SpringMvcHotLoadContext;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;

/**
 * 热部署上下文, 存储到 HotLoadInstaller里面
 */
@Setter
@Getter
@Slf4j
public class HotLoadContext {

    private HotLoadConfig hotLoadConfig;

    private HotLoadClassLoader hotLoadClassLoader;

    private AnnotationConfigApplicationContext applicationContext;

    private SpringMvcHotLoadContext springMvcHotLoadContext;

    public HotLoadContext(HotLoadConfig hotLoadConfig, HotLoadClassLoader hotLoadClassLoader, AnnotationConfigApplicationContext applicationContext) {
        this.hotLoadConfig = hotLoadConfig;
        this.hotLoadClassLoader = hotLoadClassLoader;
        this.applicationContext = applicationContext;
    }

    public void close() {
        try {
            applicationContext.stop();
        }
        catch (Exception e) {
            log.error("close applicationContext error", e);
        }
        try {
            hotLoadClassLoader.close();
        } catch (IOException e) {
            log.error("close hotLoadClassLoader error", e);
        }
        applicationContext = null;
        hotLoadClassLoader = null;
    }
}
