package com.cans.lightning.install.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class ControlInstallConfiguration {

    /**
     * 配置数据库脚本
     */
    @Bean
    public ControlInstallMySQLDdl controlInstallMySQLDdl(DataSource dataSource) {
        return new ControlInstallMySQLDdl(dataSource);
    }
}
