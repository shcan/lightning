package com.cans.lightning.install.engine.hotLoad.framework.spring;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.ResolvableType;

@Slf4j
public class HotLoadConfigApplicationContext extends AnnotationConfigApplicationContext {

    @Override
    protected void publishEvent(Object event, ResolvableType typeHint) {
        log.warn("not support publishEvent.");
    }
}
