package com.cans.lightning.install.engine.manager.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cans.lightning.base.dto.ResDto;
import com.cans.lightning.install.business.po.AppControl;
import com.cans.lightning.install.business.service.AppControlService;
import com.cans.lightning.install.engine.dto.ControlConfigDto;
import com.cans.lightning.install.engine.dto.InstallContext;
import com.cans.lightning.install.engine.enums.ControlInstallState;
import com.cans.lightning.install.engine.exceptions.ControlInstallException;
import com.cans.lightning.install.engine.hotLoad.HotLoadConfig;
import com.cans.lightning.install.engine.hotLoad.HotLoadInstaller;
import com.cans.lightning.install.engine.hotLoad.InstallResult;
import com.cans.lightning.install.engine.manager.ControlInstallManager;
import com.cans.lightning.utils.file.FileUtils;
import com.cans.lightning.utils.system.IdUtils;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static com.cans.lightning.install.engine.context.ControlSettingContext.*;
import static com.cans.lightning.install.engine.utils.ControlHotLoadUtils.addControl2ControlCache;

@Component
@Slf4j
public class ControlInstallManagerImpl implements ControlInstallManager {

    @Resource
    private AppControlService appControlService;

    @Transactional
    @Override
    public ResDto<String> install(InstallContext context) {

        try {
            // 1. 解压文件到具体目录、并且校验配置是否正确
            ControlConfigDto configDto = unzipFile2ControlDir(context);
            log.info("finished step 1, unzip file to {}", context.getRealUnzipFileDir());

            // 2. 插入数据到数据库
            saveRecord2Db(context, configDto);
            log.info("finished step 2, success save record to database.");

            // 3. 执行安装逻辑
            installControl(context, configDto);
            log.info("finished step 3, success install this control.");
        }
        catch (ControlInstallException e) {
            if (context.getRealUnzipFileDir() != null) {
                log.info("occur error on install control, delete realUnzipFileDir 【{}】", context.getRealUnzipFileDir().getAbsolutePath());
                FileUtils.deleteFile(context.getRealUnzipFileDir().getAbsolutePath());
            }
            log.error("do error on install control, error message: 【{}】", e.getMessage());
            throw e;
        }
        return ResDto.success();
    }

    /**
     * 保存记录到数据库, 需要确保的是, 如果数据库中已经存在记录, 则做更新操作
     * @param context
     * @param configDto
     */
    private void saveRecord2Db(InstallContext context, ControlConfigDto configDto) {
        // 时间需要 代码写粗糙点
        List<AppControl> list = appControlService.lambdaQuery().eq(AppControl::getControlKey, configDto.getControlKey()).list();
        AppControl appControl;
        if (!list.isEmpty()) {
            // 更新
            appControl = list.get(0);
            // 更新状态
            context.setInstallOrUpdate(false);
        }
        else {
            appControl = new AppControl();
            appControl.setId(IdUtils.getUuidLong());
        }

        if (!context.isInstallOrUpdate()) {
            // 如果是更新 需要更高版本的控件
            compareVersion(configDto, appControl);
        }

        // 属性复制
        BeanUtil.copyProperties(configDto, appControl);
        appControl.setCreateTime(new Date());
        appControl.setLastChangeTime(new Date());
        appControl.setState(ControlInstallState.INSTALL_SUCCESS.getState());
        appControl.setJson(JSON.toJSONString(configDto));
        // 保存控件
        appControlService.saveOrUpdate(appControl);
    }

    private void compareVersion(ControlConfigDto configDto, AppControl appControl) {
        // 1. 控件自己的版本号比较
        if (configDto.getControlVersion().compareTo(appControl.getControlVersion()) <= 0) {
            throw new ControlInstallException("control version is lower than the current version");
        }
    }

    /**
     * 执行控件安装逻辑
     */
    private void installControl(InstallContext context, ControlConfigDto configDto) {
        // 1. 热部署上下文  Spring配置类 + 类加载器(将类加载器目录加上)
        // spring配置(configDto.getConfigurationClass())
        // 类加载器(context.getRealUnzipFileDir()下面的 lightning/WEB-INF )
        HotLoadConfig hotLoadConfig = buildHotLoadConfig(context, configDto);
        InstallResult installResult = HotLoadInstaller.hotLoad(hotLoadConfig);
        if (!installResult.isSuccess()) {
            throw new ControlInstallException("hot load install error, error message: " + installResult.getMessage());
        }

        // 2. 将控件的缓存刷新
        addControl2ControlCache(hotLoadConfig, configDto.getControlKey());

        // 3. 通知其他节点进行安装
        // 插入控件安装日志,
    }

    /**
     * 构建热部署的配置信息
     */
    private HotLoadConfig buildHotLoadConfig(InstallContext context, ControlConfigDto configDto) {

        File file = new File(context.getRealUnzipFileDir(), CONTROL_INSTALL_CODE_DIR);
        if (!file.exists()) {
            throw new ControlInstallException("no found directory in " + context.getRealUnzipFileDir() + DIR_SEP + CONTROL_INSTALL_CODE_DIR);
        }
        List<Path> hotLoadClassPath = new ArrayList<>();
        for (File file1 : Objects.requireNonNull(file.listFiles())) {
            hotLoadClassPath.add(Paths.get(file1.getAbsolutePath()));
        }
        String frameScanPackage = configDto.getConfigurationClass().substring(0, configDto.getConfigurationClass().lastIndexOf("."));

        // 构建热部署配置
        return HotLoadConfig.builder()
                .hotLoadKey(CONTROL_CUSTOM_PREFIX + configDto.getControlKey())
                .springConfigurationClass(Collections.singletonList(configDto.getConfigurationClass()))
                .frameScanPackage(Collections.singleton(frameScanPackage))
                .hotLoadClassPath(hotLoadClassPath)
                .build();
    }

    /**
     * 解压控件包到目录, 并且校验配置是否正确
     * @return 配置
     */
    private ControlConfigDto unzipFile2ControlDir(InstallContext context) {

        String tempUnzipPath = CONTROL_INSTALL_TEMP_PATH + "/" + IdUtils.getUuidLong();
        try {
            // 1. 先将文件放到temp目录, 等待获取配置后再放入到具体控件key的目录
            FileUtils.mkdirIfNotExist(tempUnzipPath);
            File unzipTempFileDir = FileUtils.unzip(context.getTempFile(), tempUnzipPath);
            log.info("unzip file from【{}】 to temp dir【{}】: ", unzipTempFileDir.getAbsolutePath(), tempUnzipPath);
            ControlConfigDto configDto = JSONObject.parseObject(FileUtils.readString(tempUnzipPath + "/" + CONTROL_CONFIG), ControlConfigDto.class);

            // 2. 需要校验下文件是否存在
            validateConfig(configDto, unzipTempFileDir);

            // 3. 将文件解压到对应key的目录里面去
            String dateFormatStr = DateUtil.format(new Date(), CONTROL_INSTALL_DATE_FORMAT);
            configDto.setControlDirName(dateFormatStr);
            log.info("read 【control-config.json】: {}", JSON.toJSONString(configDto));
            String realUnzipPath = CONTROL_INSTALL_PATH + "/" + configDto.getControlKey() + "/" + dateFormatStr;
            FileUtils.mkdirIfNotExist(realUnzipPath);

            File realUnzipFileDir = FileUtils.unzip(context.getTempFile(), realUnzipPath);
            log.info("unzip file from【{}】 to real control dir【{}】: ", unzipTempFileDir.getAbsolutePath(), realUnzipPath);

            context.setRealUnzipFileDir(realUnzipFileDir);
            return configDto;
        }
        finally {
            // 需要清空temp文件
            FileUtils.deleteFile(tempUnzipPath);
        }
    }

    /**
     * 校验控件包是否正确
     */
    private void validateConfig(ControlConfigDto configDto, File unzipTempFileDir) {
        if (configDto == null) {
            throw new ControlInstallException("control-config.json not exists");
        }

        // 校验 configDto.getConfigurationClass() 配置类文件是否存在
        // ... 校验逻辑太多了  后续校验
    }
}
