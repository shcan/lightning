package com.cans.lightning.install.engine.hotLoad.framework.mybatis;

import com.cans.lightning.install.engine.hotLoad.HotLoadContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

/**
 * MyBatis 模板配置
 */
@Slf4j
public class MyBatisConfigurationTemplate {

    private static final String MAPPER_LOCATION_SUFFIX = "/**/mapper/xml/*.xml";
    private static final String SQL_SESSION_FACTORY_BEAN_SUFFIX = "_sqlSessionFactoryBean";

    public static void buildMyBatisSpringConfig(HotLoadContext hotLoadContext) throws Exception {
        AnnotationConfigApplicationContext applicationContext = hotLoadContext.getApplicationContext();
        applicationContext.registerBeanDefinition(hotLoadContext.getHotLoadConfig().getHotLoadKey() + SQL_SESSION_FACTORY_BEAN_SUFFIX, sqlSessionFactoryBean(hotLoadContext).getBeanDefinition());
        applicationContext.registerBeanDefinition(hotLoadContext.getHotLoadConfig().getHotLoadKey() + "_sqlSessionTemplate", sqlSessionTemplate(hotLoadContext).getBeanDefinition());
        applicationContext.registerBeanDefinition(hotLoadContext.getHotLoadConfig().getHotLoadKey() + "_mapperScannerConfigurer", mapperScannerConfigurer(hotLoadContext).getBeanDefinition());
        log.info("register {}, _sqlSessionTemplate, _mapperScannerConfigurer to {}", SQL_SESSION_FACTORY_BEAN_SUFFIX, hotLoadContext.getApplicationContext().getClass().getName());
    }

    /**
     * 创建 MapperScannerConfigurer 的定义
     * @param hotLoadContext hotLoadContext
     * @return BeanDefinitionBuilder
     */
    public static BeanDefinitionBuilder mapperScannerConfigurer(HotLoadContext hotLoadContext) {
        BeanDefinitionBuilder bdBuilder = BeanDefinitionBuilder.genericBeanDefinition(MapperScannerConfigurer.class);
        bdBuilder.addPropertyValue("basePackage", hotLoadContext.getHotLoadConfig().getFrameScanPackage().toArray()[0]);
        bdBuilder.addPropertyValue("annotationClass", Mapper.class);
        bdBuilder.addPropertyValue("sqlSessionFactoryBeanName", hotLoadContext.getHotLoadConfig().getHotLoadKey() + SQL_SESSION_FACTORY_BEAN_SUFFIX);
        return bdBuilder;
    }

    public static BeanDefinitionBuilder sqlSessionFactoryBean(HotLoadContext hotLoadContext) throws Exception {
        BeanDefinitionBuilder bdBuilder = BeanDefinitionBuilder.genericBeanDefinition(SqlSessionFactoryBean.class);
        bdBuilder.addPropertyReference("dataSource", "dataSource");
        ResourcePatternResolver resourceResolver = new PathMatchingResourcePatternResolver(hotLoadContext.getHotLoadClassLoader());
        String path = hotLoadContext.getHotLoadConfig().getFrameScanPackage().toArray()[0].toString();
        path= String.join("/", path.split("\\."));
        bdBuilder.addPropertyValue("mapperLocations", resourceResolver.getResources("classpath:" + path  + MAPPER_LOCATION_SUFFIX));
        return bdBuilder;
    }

    public static BeanDefinitionBuilder sqlSessionTemplate(HotLoadContext hotLoadContext) throws Exception {
        BeanDefinitionBuilder bdBuilder = BeanDefinitionBuilder.genericBeanDefinition(SqlSessionTemplate.class);
        bdBuilder.addConstructorArgReference(hotLoadContext.getHotLoadConfig().getHotLoadKey() + SQL_SESSION_FACTORY_BEAN_SUFFIX);
        return bdBuilder;
    }
}
