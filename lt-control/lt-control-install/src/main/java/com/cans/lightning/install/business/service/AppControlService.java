package com.cans.lightning.install.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cans.lightning.install.business.po.AppControl;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author qpy
 * @since 2024-08-09
 */
public interface AppControlService extends IService<AppControl> {

}
