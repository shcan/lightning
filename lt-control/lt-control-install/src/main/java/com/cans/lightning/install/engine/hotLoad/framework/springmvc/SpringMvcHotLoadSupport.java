package com.cans.lightning.install.engine.hotLoad.framework.springmvc;

import com.cans.lightning.install.engine.hotLoad.HotLoadContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.support.AopUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.core.MethodIntrospector;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Controller;
import org.springframework.util.ClassUtils;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.handler.AbstractHandlerMethodMapping;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * spring mvc热部署支持
 */
@Slf4j
public class SpringMvcHotLoadSupport {

    public static void hotLoad2ParentApplicationContext(HotLoadContext context) {
        ApplicationContext parentApp = context.getApplicationContext().getParent();
        RequestMappingHandlerMapping mapping = parentApp.getBean(RequestMappingHandlerMapping.class);

        ApplicationContext currentApp = context.getApplicationContext();
        String[] beanNamesForType = currentApp.getBeanNamesForType(Object.class);

        SpringMvcHotLoadContext mvcContext = new SpringMvcHotLoadContext();
        context.setSpringMvcHotLoadContext(mvcContext);

        log.info("start spring mvc hot load, beanNamesForType: {}", context.getHotLoadConfig().getHotLoadKey());
        for (String beanName : beanNamesForType) {
            if (!beanName.startsWith("scopedTarget.")) {
                Class<?> type = null;
                try {
                    type = currentApp.getType(beanName);
                }
                catch (Exception e) {
                    log.warn("get bean type error, continue, beanName: {}", beanName);
                }
                if (type != null && isHandler(type, context)) {
                    Class<?> userType = ClassUtils.getUserClass(type);
                    List<RequestMappingInfo> alreadyAdd = new ArrayList<>();
                    Map<Method, RequestMappingInfo> mappingInfoMap = createMappingInfoMap(userType, context, mapping);
                    mappingInfoMap.forEach((method, mappingInfo) -> {
                        try {
                            Method invocableMethod = AopUtils.selectInvocableMethod(method, userType);
                            Method registerHandlerMethod = RequestMappingHandlerMapping.class.getDeclaredMethod("registerHandlerMethod", Object.class, Method.class, RequestMappingInfo.class);
                            registerHandlerMethod.setAccessible(true);
                            ReflectionUtils.invokeMethod(registerHandlerMethod, mapping, currentApp.getBean(beanName), invocableMethod, mappingInfo);
                            log.info("register mappingInfo beanName:{} method:{} mappingInfo:{} to {}", beanName, method, mappingInfo, mapping);
                            alreadyAdd.add(mappingInfo);
                        }
                        catch (Exception e) {
                            log.warn("register mapping info error: {}", e.getMessage());
                            for (RequestMappingInfo requestMappingInfo : alreadyAdd) {
                                mapping.unregisterMapping(requestMappingInfo);
                                log.info("unregister mappingInfo beanName:{} method:{} mappingInfo:{}", beanName, method, requestMappingInfo);
                            }
                            alreadyAdd.clear();
                        }
                    });
                    mvcContext.getMappingInfos().addAll(alreadyAdd);
                }
            }
        }
    }

    /**
     * 卸载
     */
    public static void uninstall(HotLoadContext context) {
        ApplicationContext parentApp = context.getApplicationContext().getParent();
        RequestMappingHandlerMapping mapping = parentApp.getBean(RequestMappingHandlerMapping.class);
        for (RequestMappingInfo mappingInfo : context.getSpringMvcHotLoadContext().getMappingInfos()) {
            mapping.unregisterMapping(mappingInfo);
            log.info("unregister mappingInfo mappingInfo:{}, mapping:{}", mappingInfo, mapping);
        }
    }

    private static Map<Method, RequestMappingInfo> createMappingInfoMap(Class<?> userType, HotLoadContext context, RequestMappingHandlerMapping mapping) {

        return MethodIntrospector.selectMethods(userType,
                (MethodIntrospector.MetadataLookup<RequestMappingInfo>) method -> {
                    try {
                        Method getMappingForMethod = mapping.getClass().getDeclaredMethod("getMappingForMethod", Method.class, Class.class);
                        getMappingForMethod.setAccessible(true);
                        return (RequestMappingInfo) ReflectionUtils.invokeMethod(getMappingForMethod, mapping, method, userType);
                    }
                    catch (Exception ex) {
                        throw new IllegalStateException("Invalid mapping on handler class [" +
                                userType.getName() + "]: " + method, ex);
                    }
                });
    }

    private static boolean isHandler(Class<?> type, HotLoadContext context) {
        boolean controller = AnnotatedElementUtils.hasAnnotation(type, Controller.class);
        RequestMapping annotation = AnnotationUtils.findAnnotation(type, RequestMapping.class);
        if (controller && annotation != null) {
            String[] value = annotation.value();
            if (value.length == 0) {
                return false;
            }
            for (String s : value) {
                if (!s.contains(context.getHotLoadConfig().getHotLoadKey())) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}
