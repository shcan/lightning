package com.cans.lightning.install.engine.initializer;

import com.alibaba.fastjson.JSONObject;
import com.cans.lightning.install.business.po.AppControl;
import com.cans.lightning.install.business.service.AppControlService;
import com.cans.lightning.install.engine.dto.ControlConfigDto;
import com.cans.lightning.install.engine.exceptions.ControlInstallException;
import com.cans.lightning.install.engine.hotLoad.HotLoadConfig;
import com.cans.lightning.install.engine.hotLoad.HotLoadInstaller;
import com.cans.lightning.install.engine.hotLoad.InstallResult;
import com.cans.lightning.install.engine.utils.ControlHotLoadUtils;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static com.cans.lightning.install.engine.context.ControlSettingContext.*;

/**
 * 控件热部署 启动将控件 热部署
 */
@Component
@Slf4j
public class ControlHotLoadInitializer implements ApplicationListener<ContextRefreshedEvent> {

    @Resource
    private AppControlService appControlService;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        List<AppControl> list = appControlService.query().list();
        HotLoadConfig hotLoadConfig = null;
        log.info("starting hotLoadInstaller");
        for (AppControl appControl : list) {
            try {
                log.info("starting hotLoadInstaller control:{}", appControl.getControlKey());
                hotLoadConfig = buildHotLoadConfig(appControl);
                InstallResult installResult = HotLoadInstaller.hotLoad(hotLoadConfig);
                if (installResult.isSuccess()) {
                    ControlHotLoadUtils.addControl2ControlCache(hotLoadConfig, appControl.getControlKey());
                }
                else {
                    log.warn("hotLoadInstaller not success control:{} error: {}", appControl.getControlKey(), installResult.getMessage());
                }
            }
            catch (Exception e) {
                log.warn("hotLoadInstaller error, and will remove this control:{} error: {}", appControl.getControlKey(), e.getMessage());
                if (hotLoadConfig != null) {
                    HotLoadInstaller.hotUnload(hotLoadConfig.getHotLoadKey());
                }
            }
        }
    }


    private HotLoadConfig buildHotLoadConfig(AppControl appControl) {

        // 基本路径 + controlKey + 控件安装时间戳目录
        File file = new File(CONTROL_INSTALL_PATH + DIR_SEP + appControl.getControlKey() + DIR_SEP + appControl.getControlDirName(), CONTROL_INSTALL_CODE_DIR);
        if (!file.exists()) {
            throw new ControlInstallException("no found directory in " + file);
        }
        List<Path> hotLoadClassPath = new ArrayList<>();
        for (File file1 : Objects.requireNonNull(file.listFiles())) {
            hotLoadClassPath.add(Paths.get(file1.getAbsolutePath()));
        }
        ControlConfigDto configDto = JSONObject.parseObject(appControl.getJson(), ControlConfigDto.class);
        String frameScanPackage = configDto.getConfigurationClass().substring(0, configDto.getConfigurationClass().lastIndexOf("."));

        // 构建热部署配置
        return HotLoadConfig.builder()
                .hotLoadKey(CONTROL_CUSTOM_PREFIX + configDto.getControlKey())
                .springConfigurationClass(Collections.singletonList(configDto.getConfigurationClass()))
                .frameScanPackage(Collections.singleton(frameScanPackage))
                .hotLoadClassPath(hotLoadClassPath)
                .build();
    }
}
