package com.cans.lightning.install.engine.enums;

/**
 * 自定义控件安装状态
 * 1. 成功安装且能用
 * 2. 数据库中有安装记录, 但是用户自己停用了
 * 3. 数据库中有安装记录, 但是代码未加载到
 * 4. 数据库有记录, 且有代码, 但是控件过期了
 */
public enum ControlInstallState {

    // 成功安装且能用
    INSTALL_SUCCESS(1,"success install and can use"),
    // 数据库中有安装记录, 但是用户自己停用了
    MANUAL_DEACTIVATE(2,"success install but manual_deactivate"),
    // 数据库中有安装记录, 但是代码未加载到
    INSTALL_NO_CODE(3,"record in database, but no code in environment"),
    // 数据库中有安装记录, 但是代码未加载到
    INSTALL_EXPIRE(4,"record in database, but expire");

    private final int state;
    private final String desc;

    ControlInstallState(int state, String desc) {
        this.state = state;
        this.desc = desc;
    }

    public int getState() {
        return state;
    }

    public String getDesc() {
        return desc;
    }

}
