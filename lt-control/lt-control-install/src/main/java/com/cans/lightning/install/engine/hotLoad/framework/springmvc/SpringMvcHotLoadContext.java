package com.cans.lightning.install.engine.hotLoad.framework.springmvc;

import com.google.common.collect.Lists;
import lombok.*;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SpringMvcHotLoadContext {

    /**
     * 存储 RequestMappingInfo
     */
    List<RequestMappingInfo> mappingInfos = Lists.newArrayList();
}
