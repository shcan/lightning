
-- 根据当前项目中的 com.cans.lightning.install.business.po.AppControl.java 文件创建表定义
create table IF NOT EXISTS app_control (
  id                bigint not null,
  control_name      varchar(255) comment '控件名称',
  control_key       varchar(255) comment '控件key',
  control_version   varchar(255) comment '控件版本',
  control_type      smallint comment '1.插件 2.按钮',
  control_dir_name  varchar(255) comment '控件的存储目录',
  support_version   varchar(255) comment '支持版本',
  create_user       varchar(255) comment '创建人',
  effect_start_date datetime comment '控件生效时间',
  effect_end_date   datetime comment '控件失效时间',
  state             smallint comment '控件状态',
  json              text comment '控件自定义json',
  create_time       datetime comment '创建时间',
  last_change_time  datetime,
  primary key (id)
);