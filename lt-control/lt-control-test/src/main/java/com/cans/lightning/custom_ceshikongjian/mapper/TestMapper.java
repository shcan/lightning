package com.cans.lightning.custom_ceshikongjian.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface TestMapper {

    @Select("select count(*) from app_definition")
    int selectCount();

    int selectCount2();
}
