package com.cans.lightning.custom_ceshikongjian;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan
@Configuration
public class TestConfiguration {
}
