package com.cans.lightning.custom_ceshikongjian.ctrl;

import com.cans.lightning.form.bean.fieldCtrl.FormFieldCustomControl;
import org.springframework.stereotype.Component;

@Component
public class TestCtrl extends FormFieldCustomControl {
    @Override
    public String getKey() {
        return "ceshikongjian";
    }

    @Override
    public String getText() {
        return "测试控件";
    }

    @Override
    public boolean canUse() {
        return true;
    }
}
