package com.cans.lightning.custom_ceshikongjian.controller;

import com.cans.lightning.custom_ceshikongjian.mapper.TestMapper;
import com.cans.lightning.custom_ceshikongjian.service.TestService;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/custom_ceshikongjian")
public class TestController {

    @Resource
    private TestService testService;

    @Resource
    private TestMapper testMapper;

    @RequestMapping("/test")
    public String test() {
        return testService.test();
    }


    @RequestMapping("/selectCount")
    public String selectCount() {
        return testMapper.selectCount() + "";
    }


    @RequestMapping("/selectCount2")
    public String selectCount2() {
        return testMapper.selectCount2() + "";
    }



}
