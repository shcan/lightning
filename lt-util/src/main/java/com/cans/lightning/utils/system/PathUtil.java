package com.cans.lightning.utils.system;

import java.time.LocalDate;

/**
 * 文件路劲生成工具
 *
 * @author shencan
 * @date 2020/6/26 21:11
 */
public class PathUtil {

    public static String getBasePath() {

        LocalDate now = LocalDate.now();

        return "/" + now.getYear() + "/" + now.getMonthValue() + "/" + now.getDayOfMonth();
    }
}
