package com.cans.lightning.utils.file;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ZipUtil;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * 文件实用工具
 */
public class FileUtils {

    /**
     * 解压文件到指定目录
     * @param srcZipFile 源压缩文件
     * @param destPath 压缩到目标目录
     * @return 压缩后的目标目录
     */
    public static File unzip(File srcZipFile, String destPath) {
        File file = FileUtil.file(destPath);
        // 使用 Hutool 工具操作
        return ZipUtil.unzip(srcZipFile, file);
    }

    /**
     * 目录不存在 则创建目录
     * @param path
     */
    public static void mkdirIfNotExist(String path) {
        File file = FileUtil.file(path);
        if (!file.exists()) {
            file.mkdirs();
        }
    }

    public static boolean fileExist(String path) {
        File file = FileUtil.file(path);
        return file.exists();
    }

    public static void deleteFile(String path) {
        File file = FileUtil.file(path);
        FileUtil.del(file);
    }

    /**
     * 读取文件到字符串
     */
    public static String readString(String path) {
        File file = FileUtil.file(path);
        return FileUtil.readString(file, StandardCharsets.UTF_8);
    }
}
