package com.cans.lightning.utils.system;

import java.util.UUID;

/**
 * ID生成器工具类
 * 
 * @author ruoyi
 */
public class IdUtils
{
    /**
     * 获取随机UUID
     * 
     * @return 随机UUID
     */
    public static String randomUUID()
    {
        return UUID.randomUUID().toString();
    }

    /**
     * 简化的UUID，去掉了横线
     * 
     * @return 简化的UUID，去掉了横线
     */
    public static String simpleUUID()
    {
        return UUID.randomUUID().toString().replaceAll("-","");
    }

    /**
     * 获取随机UUID，使用性能更好的ThreadLocalRandom生成UUID
     */
    public static long getUuidLong() {
        return UUID.randomUUID().getLeastSignificantBits();
    }
}
