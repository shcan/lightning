# lightning - *闪电*
让开发快如闪电

#### 介绍
企业级后台软件开发脚手架,一站式全栈开发方案,让开发快如闪电
已低代码为核心的企业级办公系统

#### 软件架构
* Springboot
* Beetl3 持久层框架   [Beetl3文档链接直达](https://www.kancloud.cn/xiandafu/beetl3_guide/1992542 "Beetl3文档")
* Spring Data Redis redis访问
* Spring Security spring无缝集成安全框架,权限管理
* Mapstruct DTO转换工具,告别手写转换
* Quartz 定时调度框架

##### 基础模块
1.  BaseEntity BaseService BaseDao BaserController
2.  Redis缓存
3.  RabbitMQ 完善中
4.  Spring Security 完善中

#### 工程模块

* 后端: lightning	
  * lt-core 平台工程
  * lt-organization
  * lt-form-api 表单抽象工程
  * lt-form 表单核心工程
  * lt-bpm 工作流(未做)
  * lt-report(未做)
  * lt-util util实用类

#### 各模块功能

##### lt-core

* 缓存
* 框架功能

##### lt-organization

* 用户
* 组织(未做)

##### lt-form

* 表单设计
* 表单运行态

##### lt-bpm

* 协同模块(未做)

* 工作流设计(未做)
* 工作流运行态(未做)

##### lt-report

* 报表设计(未做)
* 报表运行(未做)

##### lt-util

* 实用类



#### 基础功能

1. 用户 角色 菜单 权限管理
2. 数据字典
3. 系统配置
4. 定时调度 动态定时任务
5. 低代码平台
   1. 表单设计器
   2. 报表中心
   3. 菜单权限
   4. 枚举和基础数据
#### 使用说明

1. 拉取对应前端项目 [对应前端VUE项目链接直达](https://gitee.com/shcan/lightning-web-vue)
2. 前后端启动后浏览器可访问
3. 初始化SQL见根目录下/initSql/MySQLinit.sql 初始化数据库
4. 配置数据库和redis链接信息,启动项目
5. 默认用户名/密码  admin/123456    也可以自行注册


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
